/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "qsortselection.h"
#include "../MzShared/multidimselection.h"
#include "../MzShared/dataforconsole.h"

const double machineepsilon = 1e-12;

QSortSelectionReduction::QSortSelectionReduction()
{
    normal_plus = NULL;
    normal_multi = NULL;
}

QSortSelectionReduction::~QSortSelectionReduction()
{
    if(normal_plus != NULL) free(normal_plus);
    normal_plus = NULL;
    if(normal_multi != NULL) free(normal_multi);
    normal_multi = NULL;
}

////------------------------------------------------------------------------------
//void QSortSelectionReduction::NormalizeMinMax(void)
//{
//    double min, max, d;
//    int f, v;

//    if(normal_plus) free(normal_plus);
//    if(normal_multi) free(normal_multi);
//    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
//    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

//    for(f = 0; f < data->featurenumber; f++)
//    {
//        v = 0;
//        min = max = data->values[data->featurenumber * v + f];
//        for(v = 1; v < data->vectornumber; v++)
//        {
//            d = data->values[data->featurenumber * v + f];

//            if(d > max) max = d;
//            if(d < min) min = d;
//        }
//        normal_plus[f] = -(max+min)/2.0;
//        max -= min;
//        if(max >= machineepsilon) max = 2.0/max;
//        else max = 1.0;
//        normal_multi[f] = max;
//        min = normal_plus[f];

//        for(v = 0; v < data->vectornumber; v++)
//        {
//            d = data->values[data->featurenumber * v + f];
//            d += min; d *= max;
//            data->values[data->featurenumber * v + f] = d;
//        }
//    }
//}

//------------------------------------------------------------------------------
void QSortSelectionReduction::Standardize(void)
{
    double mean, stdd, d;
    int f, v;

    if(normal_plus) free(normal_plus);
    if(normal_multi) free(normal_multi);
    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

    for(f = 0; f < data->featurenumber; f++)
    {
        mean = 0;
        stdd = 0;
        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            mean += d;
            stdd += (d * d);
        }
        mean /= data->vectornumber;
        stdd /= data->vectornumber;
        stdd -= (mean * mean);
        stdd = sqrt(stdd);

        normal_plus[f] = -mean;
        if(stdd >= machineepsilon) stdd = 1.0/stdd;
        else stdd = 1.0;
        normal_multi[f] = stdd;

        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            d -= mean; d *= stdd;
            data->values[data->featurenumber * v + f] = d;
        }
    }
}


bool QSortSelectionReduction::BubleSortMaxMin(int size, double* Qtable, unsigned int* Qsorted)
{
    for(int kmax = size-1; kmax > 0; kmax--)
    {
        for(int k = 0; k < kmax; k++)
        {
            if(Qtable[Qsorted[k]] < Qtable[Qsorted[k+1]])
            {
                unsigned int qs;
                qs = Qsorted[k]; Qsorted[k] = Qsorted[k+1]; Qsorted[k+1] = qs;
            }
        }
    }
    return true;
}

bool QSortSelectionReduction::BubleSortMinMax(int size, double* Qtable, unsigned int* Qsorted)
{
    for(int kmax = size-1; kmax > 0; kmax--)
    {
        for(int k = 0; k < kmax; k++)
        {
            if(Qtable[Qsorted[k]] > Qtable[Qsorted[k+1]])
            {
                unsigned int qs;
                qs = Qsorted[k]; Qsorted[k] = Qsorted[k+1]; Qsorted[k+1] = qs;
            }
        }
    }
    return true;
}


bool QSortSelectionReduction::SelectMutualInformation(double* Qtable, unsigned int* Qsorted)
{
    unsigned int v;
    int b, a, picked;
    double f;
    int nooffeatures = data->featurenumber;
    int noofclasses = data->classnumber;
    unsigned int noofvectors = data->vectornumber;
    int noofbins = log2(noofvectors) + 2;//0x1000;
    //int noofbins = 0x1000;

    unsigned int* probab;
    unsigned int* proba;
    unsigned int* probb;
    probab = new unsigned int[noofbins*noofclasses];
    proba = new unsigned int[noofbins];
    probb = new unsigned int[noofclasses];

    NotifyProgress(BEGINS, 1);
    //NormalizeMinMax();
    Standardize();

    for(picked = 0; picked < nooffeatures; picked++)
    {
        Qsorted[picked] = picked;
        Qtable[picked] = -1.0;
    }

    for(picked = 0; picked < nooffeatures; picked++)
    {
        memset(probab, 0, noofbins*noofclasses*sizeof(unsigned int));
        memset(proba, 0, noofbins*sizeof(unsigned int));
        memset(probb, 0, noofclasses*sizeof(unsigned int));

        unsigned int vs = 0;
        for(b = 0; b < noofclasses; b++)
        {
            unsigned int vm = data->classendvectorindex[b];
            for(v = vs; v < vm; v++)
            {
                f = data->values[nooffeatures * v + picked];//mdf(v);

                //a = (noofbins-1) * (f+1.0)/2.0;
                //a = (noofbins-1) * (f+2.0)/4.0;
                a = (noofbins-1) * (f+3.0)/6.0;
                if(a >= noofbins) a = noofbins-1;
                if(a < 0) a = 0;

                proba[a]++;
                probb[b]++;
                probab[a + b * noofbins]++;
            }
            vs = vm;
        }

        f = 0.0;
        for(b = 0; b < noofclasses; b++)
        {
            for(a = 0; a < noofbins; a++)
            {
                // I(A,B) = E[ p(a,b) log(p(a,b)/p(a)p(b)) ]
                if(proba[a]!=0 && probb[b]!=0 && probab[a + b * noofbins]!=0)
                {
                    f += (probab[a + b * noofbins]*(log2((double)noofvectors*probab[a + b * noofbins]/(proba[a]*probb[b]))));
                }
            }
        }
        f /= noofvectors;
        Qtable[picked] = f;
        NotifyProgress(STEP, 1);
    }
    delete[] probb;
    delete[] proba;
    delete[] probab;

    BubleSortMaxMin(nooffeatures, Qtable, Qsorted);
    NotifyProgress(COMPLETED, 1);
    return true;
}



bool QSortSelectionReduction::SelectFisher(double* Qtable, unsigned int* Qsorted)
{
    NotifyProgress(BEGINS, 1);

    unsigned int v;
    int picked, c;
    int nooffeatures = data->featurenumber;
    for(picked = 0; picked < nooffeatures; picked++)
    {
        Qsorted[picked] = picked;
        Qtable[picked] = -1.0;
    }

    for(picked = 0; picked < nooffeatures; picked++)
    {
        double miuall = 0.0;
        double varall = 0.0;
        double varcla = 0.0;
        unsigned int vm = data->vectornumber;
        for(v = 0; v < vm; v++)
        {
            double f = data->values[nooffeatures * v + picked];//mdf(v);
            miuall += f;
            varall += (f*f);
        }
        miuall /= vm;
        varall /= vm;
        varall -= (miuall * miuall);

        unsigned int vs = 0;
        for(c = 0; c < data->classnumber; c++)
        {
            double miuloc = 0.0;
            double varloc = 0.0;
            unsigned int vm = data->classendvectorindex[c];
            for(v = vs; v < vm; v++)
            {
                double f = data->values[nooffeatures * v + picked];//mdf(v);
                miuloc += f;
                varloc += (f*f);
            }
            miuloc /= (vm-vs);
            varloc /= (vm-vs);
            varloc -= (miuloc * miuloc);
            varcla += varloc;
            vs = vm;
        }
        Qtable[picked] = (data->classnumber * varall + machineepsilon) / (varcla + machineepsilon);
        NotifyProgress(STEP, 1);
    }

    BubleSortMaxMin(nooffeatures, Qtable, Qsorted);
    NotifyProgress(COMPLETED, 1);
    return true;
}
