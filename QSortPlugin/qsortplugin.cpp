/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//#include <QtGlobal>
//#include <QtPlugin>
//#include <QFileDialog>
#include "../MzShared/mzdefines.h"

#include "qsortplugin.h"
#include "../MzShared/multidimselection.h"
//#include "../MzShared/selectionlist.h"
//#include "../MzShared/testdialog.h"

#include <string>
#include <sstream>

const int NumberOfFeaturesToSelect = 3;

void* MzNewPluginObject(void)
{
    return new QSortPlugin();
}

void MzDeletePluginObject(void* object)
{
    delete static_cast<QSortPlugin *> (object);
}

QSortPlugin::QSortPlugin()
    :QSortSelectionReduction()
{
    data = NULL;
    pull_data = NULL;
    Qtable = NULL;
    Qsorted = NULL;
}

QSortPlugin::~QSortPlugin()
{
    if(data != NULL) delete[] data;
}

void QSortPlugin::NotifyProgress(int /*notification*/, int /*dimensionality*/)
{
}

const char *QSortPlugin::getName(void)
{
    return "Feature filtration";
}



void* QSortPlugin::connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function)
{
    void* ret = gui_tools->addMenuAction(name, tip, onActionTable.size());
    if(name != NULL && function != NULL)
        onActionTable.push_back(function);
    return ret;
}
void QSortPlugin::callBack(const unsigned int index)
{
    if(index < onActionTable.size())
        if(onActionTable[index] != NULL)
            (this->*(onActionTable[index]))();
}


bool QSortPlugin::initiateTablePlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    connectMenuAction("Fisher discriminant...", "Selection of features based on Fisher's discriminant", &QSortPlugin::on_menuSelectFisher_triggered);
    connectMenuAction("Mutual information...", "Feature selection of maximum mutual information", &QSortPlugin::on_menuSelectMutual_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &QSortPlugin::on_menuAbout_triggered);
    return true;
}

bool QSortPlugin::initiateMapsPlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    return false;
}


void QSortPlugin::on_menuAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>qmazda QuickSortPlugin</h2> " << std::endl;
    ss << QMAZDA_VERSION << "<br>" << std::endl;
    ss << QMAZDA_COPYRIGHT << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." <<  std::endl;
    gui_tools->showAbout("About QSortPlugin", ss.str().c_str());
}

bool QSortPlugin::before_selection_this(void)
{
    if(data != NULL) delete data;
    data = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;

    data = new DataForSelection();
    pull_data->getData(data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data", 3);
        return false;
    }
    Qtable = new double[data->featurenumber];
    Qsorted = new unsigned int[data->featurenumber];
    return true;
}
void QSortPlugin::thread_selection_this()
{
    switch(method)
    {
    case 0: success = SelectFisher(Qtable, Qsorted); break;
    case 1: success = SelectMutualInformation(Qtable, Qsorted); break;
    }
}
void QSortPlugin::after_selection_this(void)
{
    if(success)
    {
        std::vector<std::string> classifierNames;
        std::vector<bool> selectionResult;
        unsigned int features = data->featurenumber;
        if(Qtable != NULL && Qsorted != NULL)
        {
            int count = 0;
            for(unsigned int c = 0; c < features; c++)
            {
                if(Qsorted[c] < features)
                {
                    std::stringstream ss;
                    ss << data->featurenames[Qsorted[c]] << " (" << Qtable[Qsorted[c]] << ")";
                    classifierNames.push_back(ss.str());
                    selectionResult.push_back(count < NumberOfFeaturesToSelect);
                    count++;
                }
                else break;
            }
            if(gui_tools->selectClassifiers(&classifierNames, &selectionResult, "Select features"))
            {
                count = 0;
                for(unsigned int c = 0; c < selectionResult.size(); c++)
                {
                    if(selectionResult[c])  count ++;
                }
                if(count > 0)
                {
                    SelectedFeatures ret;
                    ret.featurenumber = count;
                    ret.featureoriginalindex = new int[count];
                    count = 0;
                    int cc = 0;
                    for(int c = 0; c < data->featurenumber; c++)
                    {
                        if(Qsorted[c] < features)
                        {
                            if(selectionResult[cc])
                            {
                                ret.featureoriginalindex[count] = data->featureoriginalindex[Qsorted[c]];
                                count++;
                            }
                            cc++;
                        }
                    }
                    pull_data->setSelection(&ret);
                }
            }
        }
    }
    else
    {
        gui_tools->showMessage("Error", "Selection failed", 3);
    }
    if(data != NULL) delete data;
    data = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;
}

bool QSortPlugin::before_selection(void* object)
{
    return ((QSortPlugin*)object)->before_selection_this();
}
void QSortPlugin::thread_selection(void* object, void* /*notifier_object*/,
                                 MzStepNotificationFunction /*step_notifier*/,
                                 MzTextNotificationFunction /*text_notifier*/)
{
//    ((QSortPlugin*)object)->notifier_object = notifier_object;
//    ((QSortPlugin*)object)->step_notifier = step_notifier;
//    ((QSortPlugin*)object)->text_notifier = text_notifier;
    ((QSortPlugin*)object)->thread_selection_this();
}
void QSortPlugin::after_selection(void* object)
{
    ((QSortPlugin*)object)->after_selection_this();
}

void QSortPlugin::on_menuSelectFisher_triggered()
{
    method = 0;
    gui_tools->InitiateAnalysisActions(this, &before_selection, &thread_selection, &after_selection);
}

void QSortPlugin::on_menuSelectMutual_triggered()
{
    method = 1;
    gui_tools->InitiateAnalysisActions(this, &before_selection, &thread_selection, &after_selection);
}

bool QSortPlugin::openFile(std::string* /*filename*/)
{
    return false;
}
void QSortPlugin::cancelAnalysis(void)
{
}
