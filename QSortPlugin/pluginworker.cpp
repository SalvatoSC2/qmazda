/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pluginworker.h"
#include "QMessageBox"

PluginWorker::PluginWorker(int method_in, double* Qtable, unsigned int* Qsorted, DataForSelection* data, Progress* progressdialog_in, QObject *parent) :
    QObject(parent)
    ,QSortSelectionReduction(data)
{
    method = method_in;
    progressdialog = progressdialog_in;
    this->Qtable = Qtable;
    this->Qsorted = Qsorted;
}

PluginWorker::~PluginWorker()
{
}

void PluginWorker::NotifyProgress(int notification, int dimensionality)
{
    emit notifyProgressSignal(notification, dimensionality);
}

void PluginWorker::breakAnalysis(void)
{
//    breakanalysis = true;
}

void PluginWorker::process_sel()
{
    bool ok = false;
    switch(method)
    {
    case 1: ok = SelectFisher(Qtable, Qsorted); break;
    case 2: ok = SelectMutualInformation(Qtable, Qsorted); break;
    }
    if(ok) emit finished(true);
    else  emit finished(false);
    emit finished();
}
