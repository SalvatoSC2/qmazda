
/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2015 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../MzShared/getoption.h"
#include "../MzShared/imageio.h"
#include "../MzShared/mzmapio.h"
#include "../MzShared/mzroi.h"
#include "../MzShared/dataforconsole.h"
#include <sstream>
#include <vector>
#include <fstream>

char* input_image;
char* input_regions;
char* output_data;
bool append;
bool hexadecimal;
bool isprinthelp;

//=============================================================================
int scan_parameters(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-i", "--input", input_image)
        else GET_STRING_OPTION("-r", "--regions", input_regions)
        else GET_STRING_OPTION("-o", "--output", output_data)
        else GET_NOARG_OPTION("-x", "--save-hex", hexadecimal, true)
        else GET_NOARG_OPTION("-a", "--append", append, true)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

//=============================================================================
// Print help
void printhelp(const char* argv0)
{
//------------------------------------------------------------------------------------------v
    printf("%s exports data from feature maps to csv file.\n", argv0);
    printf("2015.03.29 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -i, --input <file>      Load feature map image from <file>\n");
    printf("  -r, --regions <file>    Load regions of interest <file>\n");
    printf("  -o, --output <file>     Save results to csv <file>\n");
    printf("  -x, --save-hex          Save double precision results in hexadecimal format\n");
    printf("  -a, --append            Append data to output file if available\n");
    printf("  /?, --help              Display this help and exit\n\n");
}

//=============================================================================
//=============================================================================
// Main function
int main(int argc, char* argv[])
{
    char Cooname[] = "XYZABCDEFGHI";
    int ret = scan_parameters(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp(argv[0]);
        return ret;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try %s --help for more information.\n", argv[0]);
        return ret;
    }

    if(input_image == NULL) {fprintf(stderr, "Unspecified input map (-i)\n"); return 2;}
    if(input_regions == NULL) {fprintf(stderr, "Unspecified regions file (-r)\n"); return 3;}
    if(output_data == NULL) {fprintf(stderr, "Unspecified output file (-o)\n"); return 4;}


    std::vector<MzRoi*> rois;
    std::vector<MzMap*> maps;
    std::ofstream file;

//Loading maps
    int result;
    if(MzMapFunctions::LoadSubsequentMapFromPagedTiff(input_image, NULL) <= 0) return 5;
    do{
        MzMap* newmap = new MzMap();
        result = MzMapFunctions::LoadSubsequentMapFromPagedTiff(NULL, newmap);
        if(result >= 0)
        {
            maps.push_back(newmap);
        }
        else
        {
            delete newmap;
        }
    }while(result > 0);
    MzMapFunctions::LoadSubsequentMapFromPagedTiff(NULL, NULL);
    if(maps.size() <= 0) return 5;

//Loading ROIs
    MzRoiFunctions::LoadMzRoi(input_regions, &rois);
    if(rois.size() <= 0) return 6;

//Open csv file
    if(append) file.open(output_data, std::ofstream::out | std::ofstream::app);
    else file.open(output_data);
    if (!file.is_open()) return 7;
    if (!file.good()) return 7;



    int imsize[MZDIMENSIONS];
    for(unsigned int d = 0; d < MZDIMENSIONS; d++)
    {
        imsize[d] = -1;
        std::vector<MzMap*>::iterator mapit;
        for(mapit = maps.begin(); mapit != maps.end(); ++mapit)
        {
            if((unsigned int) imsize[d] > (*mapit)->size[d]) imsize[d] = (*mapit)->size[d];
        }
    }

    if(!append)
    {
        for(unsigned int d = 0; d < MZDIMENSIONS; d++)
        {
            if(imsize[d] > 1)
            {
                file << "Gt" << Cooname[d] << ",";
            }
        }

        std::vector<MzMap*>::iterator mapit;
        for(mapit = maps.begin(); mapit != maps.end(); ++mapit)
        {
            file << (*mapit)->name << ",";
        }
        file << "Category" << std::endl;
    }



    int r = 0;
    int j;
    std::vector<MzRoi*>::iterator roiit;
    for(roiit = rois.begin(); roiit != rois.end(); ++roiit, r++)
    {
        std::stringstream ss;
        if((*roiit)->name.empty())
        {
            ss << "C" << r;
        }
        else
        {
            ss << (*roiit)->name;
        }

        unsigned int* size = (*roiit)->size;
        int* shift = (*roiit)->shift;
        unsigned int xr[MZDIMENSIONS];
        int xrmin[MZDIMENSIONS];
        int xrmax[MZDIMENSIONS];
        for(unsigned int d = 0; d < MZDIMENSIONS; d++)
        {
            xr[d] = xrmin[d] = shift[d] < 0 ? -shift[d] : 0;
            xrmax[d] = (int)(size[d])+shift[d] > imsize[d] ? imsize[d]-shift[d] : size[d];
        }

        for_begin(xr, xrmin, 1, MZDIMENSIONS)
        {
            MZROIALIGNTYPE* liner = (*roiit)->line(xr);
            xr[0] = xrmin[0];
            unsigned int xi[MZDIMENSIONS];
            for(unsigned int d = 0; d < MZDIMENSIONS; d++)
            {
                xi[d] = xr[d] + shift[d];
            }
            for(; (int)xr[0] < xrmax[0]; xr[0]++, xi[0]++)
            {


                if(MzRoi::is(xr[0], liner))
                {
                    for(unsigned int d = 0; d < MZDIMENSIONS; d++)
                    {
                        if(imsize[d] > 1)
                        {
                            if(hexadecimal)
                            {
                                file << DataTable::doubleToHex(xi[d]) << ",";
                            }
                            else file << xi[d] << ",";
                        }
                    }

                    std::vector<MzMap*>::iterator mapit;
                    for(mapit = maps.begin(); mapit != maps.end(); ++mapit)
                    {
                        double value;
                        switch((*mapit)->pixel)
                        {
                            case MZPIXELF32:
                            {
                                value = (*mapit)->value(xi);
                            }
                            break;
                            case MZPIXELF64:
                            {
                                value = (*mapit)->value(xi);
                            }
                            break;
                        }

                        if(hexadecimal)
                        {
                            file << DataTable::doubleToHex(value) << ",";
                        }
                        else file << value << ",";
                    }
                    file << ss.str() << std::endl;
                }
            }
        }
        for_end(xr, xrmin, xrmax, 1, MZDIMENSIONS);
    }

    file.close();
    return 0;
}
