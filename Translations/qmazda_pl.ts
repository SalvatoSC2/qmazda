<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl_PL">
<context>
    <name>AddNoise</name>
    <message>
        <location filename="../MzReport/addnoise.ui" line="14"/>
        <source>Add noise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/addnoise.ui" line="22"/>
        <source>Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/addnoise.ui" line="30"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/addnoise.ui" line="35"/>
        <source>Uniform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/addnoise.ui" line="47"/>
        <source>Signal to noise ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/addnoise.ui" line="84"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/addnoise.ui" line="91"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FoldsDialog</name>
    <message>
        <location filename="../MzReport/foldsdialog.ui" line="14"/>
        <source>Folds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/foldsdialog.ui" line="22"/>
        <source>Number of folds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/foldsdialog.ui" line="36"/>
        <source>Random allocation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/foldsdialog.ui" line="53"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/foldsdialog.ui" line="63"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/foldsdialog.ui" line="70"/>
        <source>Close</source>
        <translation type="unfinished">Zamknij</translation>
    </message>
</context>
<context>
    <name>LdaPlugin</name>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="331"/>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="385"/>
        <source>Load classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="332"/>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="386"/>
        <source>Load rules to classify data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="336"/>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="390"/>
        <source>Save classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="337"/>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="391"/>
        <source>Save rules to classify data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="341"/>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="395"/>
        <source>Analysis Options...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="342"/>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="396"/>
        <source>Set options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="348"/>
        <source>Selection and training...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="349"/>
        <source>Selection of features and training of classifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="353"/>
        <source>Projection and training...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="354"/>
        <source>Projection of feature space and training of classifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="358"/>
        <source>Compute MDF&apos;s...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="359"/>
        <source>Compute most discriminating features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="363"/>
        <source>Test classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="364"/>
        <source>Test classifier performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="370"/>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="414"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="371"/>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="415"/>
        <source>Info about this plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="402"/>
        <source>Compute MDF maps...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="403"/>
        <source>Compute maps of most discriminating features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="407"/>
        <source>Segment image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="408"/>
        <source>Image segmentation by the linear classifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="908"/>
        <source>Save linear classifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="910"/>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="965"/>
        <source>Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaplugin.cpp" line="963"/>
        <source>Load linear classifier</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="17"/>
        <location filename="../MzImage/mainwindow.cpp" line="278"/>
        <source>MzImage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="37"/>
        <location filename="../MzMaps/mainwindow.ui" line="37"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="52"/>
        <location filename="../MzMaps/mainwindow.ui" line="44"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="64"/>
        <location filename="../MzMaps/mainwindow.ui" line="52"/>
        <source>Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="72"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="76"/>
        <source>Morphology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="86"/>
        <source>Morfology for all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="115"/>
        <location filename="../MzReport/mainwindow.ui" line="137"/>
        <location filename="../MzMaps/mainwindow.ui" line="57"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="122"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="136"/>
        <source>Load and save tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="155"/>
        <source>Analysis tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="172"/>
        <source>Regions of interest list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="208"/>
        <source>Add region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="222"/>
        <source>Remove region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="225"/>
        <location filename="../MzImage/mainwindow.ui" line="239"/>
        <location filename="../MzImage/mainwindow.ui" line="557"/>
        <location filename="../MzImage/mainwindow.ui" line="585"/>
        <location filename="../MzImage/mainwindow.ui" line="605"/>
        <location filename="../MzImage/mainwindow.ui" line="622"/>
        <location filename="../MzMaps/mainwindow.ui" line="240"/>
        <location filename="../MzMaps/mainwindow.ui" line="268"/>
        <location filename="../MzMaps/mainwindow.ui" line="418"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="236"/>
        <source>Visible: all, selected or none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="279"/>
        <source>Draw tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="303"/>
        <source>Viewing parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="689"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="734"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="776"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="809"/>
        <source>Image info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="846"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="868"/>
        <source>Feature name set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="918"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="923"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="936"/>
        <source>Add branch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="950"/>
        <source>Remove branch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="964"/>
        <location filename="../MzImage/mainwindow.ui" line="1067"/>
        <source>Collapse or expand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="978"/>
        <source>Set default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="992"/>
        <source>Options statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1023"/>
        <source>Drawing parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1049"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1054"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1081"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1112"/>
        <source>Rendering view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1130"/>
        <location filename="../MzMaps/mainwindow.ui" line="437"/>
        <source>Zoom-in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1139"/>
        <location filename="../MzMaps/mainwindow.ui" line="446"/>
        <source>Zoom-out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1148"/>
        <location filename="../MzMaps/mainwindow.ui" line="455"/>
        <source>1:1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1157"/>
        <source>Save Roi...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1160"/>
        <source>Save Roi..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1169"/>
        <location filename="../MzImage/mainwindow.ui" line="1172"/>
        <location filename="../MzMaps/mainwindow.ui" line="476"/>
        <location filename="../MzMaps/mainwindow.ui" line="479"/>
        <source>Load Image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1181"/>
        <location filename="../MzImage/mainwindow.ui" line="1184"/>
        <source>Load Roi...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1193"/>
        <location filename="../MzImage/mainwindow.ui" line="1196"/>
        <location filename="../MzMaps/mainwindow.ui" line="464"/>
        <location filename="../MzMaps/mainwindow.ui" line="467"/>
        <source>Save Image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1205"/>
        <source>Load Options...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1214"/>
        <location filename="../MzImage/mainwindow.ui" line="1217"/>
        <source>Save Options...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1229"/>
        <location filename="../MzImage/mainwindow.ui" line="1232"/>
        <source>Pencil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1244"/>
        <location filename="../MzImage/mainwindow.ui" line="1247"/>
        <source>Flood fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1259"/>
        <location filename="../MzImage/mainwindow.ui" line="1262"/>
        <source>Erase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1274"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1286"/>
        <location filename="../MzImage/mainwindow.ui" line="1289"/>
        <source>Overlaping mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1301"/>
        <location filename="../MzImage/mainwindow.ui" line="1304"/>
        <source>Rectangle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1316"/>
        <location filename="../MzImage/mainwindow.ui" line="1319"/>
        <source>Elipse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1331"/>
        <location filename="../MzImage/mainwindow.ui" line="1334"/>
        <source>Active contour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1343"/>
        <source>Image view switch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1352"/>
        <location filename="../MzImage/mainwindow.ui" line="1355"/>
        <source>Roi view switch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1364"/>
        <location filename="../MzImage/mainwindow.ui" line="1367"/>
        <location filename="../MzReport/mainwindow.ui" line="179"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1376"/>
        <location filename="../MzImage/mainwindow.ui" line="1379"/>
        <source>Erase region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1391"/>
        <location filename="../MzImage/mainwindow.ui" line="1394"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1399"/>
        <source>Options...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1404"/>
        <location filename="../MzImage/mainwindow.ui" line="1457"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1409"/>
        <location filename="../MzImage/mainwindow.ui" line="1462"/>
        <location filename="../MzReport/mainwindow.ui" line="363"/>
        <location filename="../MzReport/mainwindow.ui" line="366"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1414"/>
        <location filename="../MzImage/mainwindow.ui" line="1447"/>
        <source>Dilate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1419"/>
        <location filename="../MzImage/mainwindow.ui" line="1452"/>
        <source>Erode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1424"/>
        <location filename="../MzImage/mainwindow.ui" line="1442"/>
        <source>Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1429"/>
        <source>Roi from sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1432"/>
        <source>Roi from grayscales between sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1437"/>
        <source>Split roi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1471"/>
        <source>Roi based computation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1480"/>
        <source>Map computation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1489"/>
        <source>Point driven computation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1494"/>
        <source>Remove small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1506"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1518"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1551"/>
        <source>Render 2D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1566"/>
        <source>Render 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1575"/>
        <source>Background color...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="17"/>
        <location filename="../MzReport/mainwindow.cpp" line="363"/>
        <location filename="../MzReport/mainwindow.cpp" line="883"/>
        <source>MzReport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="72"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="87"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="91"/>
        <source>Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="107"/>
        <source>Vectors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="127"/>
        <source>&amp;Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="165"/>
        <source>Open report...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="174"/>
        <source>Save selected...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="184"/>
        <source>Copy highlighted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="187"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="196"/>
        <location filename="../MzReport/mainwindow.ui" line="225"/>
        <source>Check all</source>
        <translation>Zaznacz wszystkie</translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="199"/>
        <source>Check all the features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="208"/>
        <location filename="../MzReport/mainwindow.ui" line="237"/>
        <source>Uncheck all</source>
        <translation>Odznacz wszystkie</translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="211"/>
        <source>Uncheck all the features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="216"/>
        <location filename="../MzReport/mainwindow.ui" line="245"/>
        <source>Invert selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="228"/>
        <source>Check all the columns (vectors)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="240"/>
        <source>Uncheck all the columns (vectors)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="250"/>
        <source>Reneame classes...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="255"/>
        <source>Random selection...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="264"/>
        <source>Scatter plot...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="269"/>
        <source>Highlight all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="272"/>
        <source>Ctrl+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="281"/>
        <source>Save...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="286"/>
        <source>New window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="291"/>
        <source>Folds...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="294"/>
        <source>Select vectors from folds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="303"/>
        <source>Names from file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="314"/>
        <source>Use checked features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="317"/>
        <source>Use checked features for analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="325"/>
        <source>Use unchecked features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="330"/>
        <source>Append columns...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="335"/>
        <source>Append rows...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="340"/>
        <source>Add noise...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1527"/>
        <location filename="../MzReport/mainwindow.ui" line="349"/>
        <location filename="../MzMaps/mainwindow.ui" line="488"/>
        <source>Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.ui" line="1536"/>
        <location filename="../MzReport/mainwindow.ui" line="358"/>
        <location filename="../MzMaps/mainwindow.ui" line="497"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="371"/>
        <location filename="../MzReport/mainwindow.ui" line="381"/>
        <source>Check highlighted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="376"/>
        <location filename="../MzReport/mainwindow.ui" line="386"/>
        <source>Uncheck highlighted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="395"/>
        <source>Names from clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.ui" line="404"/>
        <source>Copy checked names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="334"/>
        <source>MZScatterPlot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="437"/>
        <source>Load features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="439"/>
        <location filename="../MzReport/mainwindow.cpp" line="735"/>
        <location filename="../MzReport/mainwindow.cpp" line="812"/>
        <source>Comma separated vectors(*.csv) (*.csv);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="646"/>
        <source>Save all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="648"/>
        <location filename="../MzReport/mainwindow.cpp" line="669"/>
        <source>Comma separated values (*.csv) (*.csv);;Double precision hexadecimals (*.cshv) (*.cshv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="667"/>
        <source>Save selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="711"/>
        <source>Load feature names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="713"/>
        <source>Text file (*.txt) (*.txt);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="733"/>
        <source>Load columns to append</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/mainwindow.cpp" line="810"/>
        <source>Load rows to append</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.cpp" line="328"/>
        <source>Save image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.cpp" line="330"/>
        <source>Image files (*.tif *.tiff *.bmp *.png);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.cpp" line="344"/>
        <source>Load image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.cpp" line="346"/>
        <source>Image files (*.tif *.tiff *.bmp *.png *.dcm *.dicom *.nii *.nifti *.hdr *.nii.gz);;All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.cpp" line="414"/>
        <source>Load roi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.cpp" line="416"/>
        <location filename="../MzImage/mainwindow.cpp" line="429"/>
        <source>Region of interest (*.roi);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.cpp" line="427"/>
        <source>Save roi</source>
        <translation>Zapisz obszar</translation>
    </message>
    <message>
        <location filename="../MzImage/mainwindow.cpp" line="587"/>
        <source>New</source>
        <translation>Nowy</translation>
    </message>
    <message>
        <location filename="../MzImage/optionsdock.cpp" line="1613"/>
        <source>Save generator options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/optionsdock.cpp" line="1615"/>
        <location filename="../MzImage/optionsdock.cpp" line="1643"/>
        <source>Generator options (*.ftr);;All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzImage/optionsdock.cpp" line="1641"/>
        <source>Load generator options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.ui" line="17"/>
        <location filename="../MzMaps/mainwindow.cpp" line="377"/>
        <source>MzMaps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.ui" line="91"/>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.ui" line="289"/>
        <source>Grayscale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.ui" line="298"/>
        <source>Grayscale inverted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.ui" line="307"/>
        <source>Pseudo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.ui" line="316"/>
        <source>Pseudo inverted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.ui" line="325"/>
        <source>Eight level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.cpp" line="385"/>
        <source>Load feature maps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.cpp" line="387"/>
        <location filename="../MzMaps/mainwindow.cpp" line="418"/>
        <source>Floating point feature map (*.map.tiff);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzMaps/mainwindow.cpp" line="416"/>
        <source>Save feature maps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/mainwindow.ui" line="14"/>
        <source>MaZda Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/mainwindow.ui" line="39"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="14"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="48"/>
        <source>Training</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="73"/>
        <location filename="../VschPlugin/vschoptions.ui" line="22"/>
        <source>Feature values normalization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="81"/>
        <location filename="../VschPlugin/vschoptions.ui" line="30"/>
        <source>Standardize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="86"/>
        <location filename="../VschPlugin/vschoptions.ui" line="35"/>
        <source>Normalize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="91"/>
        <location filename="../VschPlugin/vschoptions.ui" line="40"/>
        <source>Use original </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="103"/>
        <location filename="../VschPlugin/vschoptions.ui" line="80"/>
        <location filename="../HebPlugin/rdaoptions.ui" line="105"/>
        <source>Class to distinguish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="111"/>
        <source>All the classes (@@@)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="116"/>
        <source>All the pairs (@@)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="121"/>
        <source>Individual classes vs. others (@)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="133"/>
        <location filename="../VschPlugin/vschoptions.ui" line="52"/>
        <location filename="../HebPlugin/rdaoptions.ui" line="77"/>
        <source>Maximum subspace dimensionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="147"/>
        <location filename="../VschPlugin/vschoptions.ui" line="66"/>
        <location filename="../HebPlugin/rdaoptions.ui" line="91"/>
        <source>Analysis time limit in seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="168"/>
        <source>Testing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="196"/>
        <source>Wildcard:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="203"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="210"/>
        <source>Check items matching the wildcard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="224"/>
        <source>Uncheck items matching the wildcard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="240"/>
        <source>Classifiers&apos; ensamble</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="268"/>
        <location filename="../VschPlugin/vschoptions.ui" line="159"/>
        <location filename="../HebPlugin/rdaoptions.ui" line="138"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LdaPlugin/ldaoptions.ui" line="275"/>
        <location filename="../VschPlugin/vschoptions.ui" line="166"/>
        <location filename="../HebPlugin/rdaoptions.ui" line="145"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschoptions.ui" line="14"/>
        <location filename="../HebPlugin/rdaoptions.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschoptions.ui" line="88"/>
        <location filename="../HebPlugin/rdaoptions.ui" line="113"/>
        <source>One-by-one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschoptions.ui" line="100"/>
        <source>Margin estimation by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschoptions.ui" line="108"/>
        <source>Upscaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschoptions.ui" line="113"/>
        <source>Expanding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschoptions.ui" line="125"/>
        <source>Minimum margin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaoptions.ui" line="22"/>
        <source>Discriminant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaoptions.ui" line="30"/>
        <source>Fisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaoptions.ui" line="35"/>
        <source>Fisher in logarithmized space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaoptions.ui" line="40"/>
        <source>Accuracy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaoptions.ui" line="45"/>
        <source>Balanced accuracy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaoptions.ui" line="50"/>
        <source>Fisher to closest cluster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaoptions.ui" line="55"/>
        <source>Fisher in logarithmized space to cloasest cluster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaoptions.ui" line="60"/>
        <source>Accuracy to closest cluster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaoptions.ui" line="65"/>
        <source>Balanced accuracy to closest cluster</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Parameters</name>
    <message>
        <location filename="../MzGengui/parameters.ui" line="20"/>
        <source>qmazda Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="33"/>
        <source>-m (mode)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="47"/>
        <source>roi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="52"/>
        <source>local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="64"/>
        <source>-i (input image)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="74"/>
        <location filename="../MzGengui/parameters.ui" line="99"/>
        <location filename="../MzGengui/parameters.ui" line="124"/>
        <location filename="../MzGengui/parameters.ui" line="149"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="89"/>
        <source>-f (feature list)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="114"/>
        <source>-r (regions of interest)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="139"/>
        <source>-o (output file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="164"/>
        <source>-s (pixel step)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="191"/>
        <source>-x (use hexadecimal coding)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="198"/>
        <source>-a (append to existing file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="225"/>
        <source>Run analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.ui" line="232"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.cpp" line="91"/>
        <source>Open input file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.cpp" line="93"/>
        <location filename="../MzGengui/parameters.cpp" line="117"/>
        <source>Tagged image file format (*.tif; *tiff);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.cpp" line="103"/>
        <source>Open feature list file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.cpp" line="105"/>
        <source>Text file format (*.txt);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.cpp" line="115"/>
        <source>Open regions file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.cpp" line="127"/>
        <source>Open output file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzGengui/parameters.cpp" line="129"/>
        <source>Comma separated values (*.csv);;Tagged image file format (*.tif; *tiff);;All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Plot3D</name>
    <message>
        <location filename="../MzReport/plot3d.ui" line="14"/>
        <source>MZScatterPlot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/plot3d.ui" line="144"/>
        <source>Select font...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/plot3d.ui" line="158"/>
        <source>Animate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/plot3d.ui" line="175"/>
        <source>Save plot...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/plot3d.ui" line="201"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/plot3d.ui" line="215"/>
        <source>Close</source>
        <translation type="unfinished">Zamknij</translation>
    </message>
    <message>
        <location filename="../MzReport/plot3d.cpp" line="249"/>
        <source>Save plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/plot3d.cpp" line="251"/>
        <source>Scalable vector graphics (*.svg) (*.svg);;Portable document format (*.pdf) (*.pdf);;Tagged image file format (*tiff) (*tiff);;JPEG image (*.jpeg) (*.jpeg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/plot3d.cpp" line="285"/>
        <source>MaZda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/plot3d.cpp" line="286"/>
        <source>Scatter plot</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Progress</name>
    <message>
        <location filename="../MzShared/progress.ui" line="14"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzShared/progress.ui" line="32"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyValue</name>
    <message>
        <location filename="../MzReport/propertyvalue.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/propertyvalue.ui" line="42"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/propertyvalue.ui" line="47"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/propertyvalue.ui" line="60"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzReport/propertyvalue.ui" line="67"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSortPlugin</name>
    <message>
        <location filename="../QSortPlugin/qsortplugin.cpp" line="63"/>
        <source>Fisher discriminant...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../QSortPlugin/qsortplugin.cpp" line="64"/>
        <source>Selection of features based on Fisher&apos;s discriminant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../QSortPlugin/qsortplugin.cpp" line="68"/>
        <source>Mutual information...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../QSortPlugin/qsortplugin.cpp" line="69"/>
        <source>Feature selection of maximum mutual information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RadPlugin</name>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="171"/>
        <source>Load classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="172"/>
        <source>Load rules to classify data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="176"/>
        <source>Save classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="177"/>
        <source>Save rules to classify data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="181"/>
        <source>Analysis Options...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="182"/>
        <source>Set options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="188"/>
        <source>Selection and training...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="189"/>
        <source>Selection of features and training of classifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="193"/>
        <source>Projection and training...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="194"/>
        <source>Projection of feature space and training of classifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="198"/>
        <source>Compute MDF&apos;s...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="199"/>
        <source>Compute most discriminating features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="203"/>
        <source>Test classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="204"/>
        <source>Test classifier performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="489"/>
        <source>Save linear classifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="491"/>
        <location filename="../HebPlugin/rdaplugin.cpp" line="554"/>
        <source>Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../HebPlugin/rdaplugin.cpp" line="552"/>
        <source>Load linear classifier</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionList</name>
    <message>
        <location filename="../MzShared/selectionlist.ui" line="14"/>
        <source>Feature ranking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzShared/selectionlist.ui" line="48"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzShared/selectionlist.ui" line="55"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TestDialog</name>
    <message>
        <location filename="../MzShared/testdialog.cpp" line="54"/>
        <location filename="../MzShared/testdialog.cpp" line="80"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VschPlugin</name>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="214"/>
        <location filename="../VschPlugin/vschplugin.cpp" line="260"/>
        <source>Load classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="215"/>
        <location filename="../VschPlugin/vschplugin.cpp" line="261"/>
        <source>Load rules to classify data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="219"/>
        <location filename="../VschPlugin/vschplugin.cpp" line="265"/>
        <source>Save classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="220"/>
        <location filename="../VschPlugin/vschplugin.cpp" line="266"/>
        <source>Save rules to classify data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="224"/>
        <source>Analysis options...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="225"/>
        <source>Set options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="231"/>
        <source>Feature selection...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="232"/>
        <source>Selection of features based on Fisher&apos;s discriminant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="236"/>
        <source>Train classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="237"/>
        <location filename="../VschPlugin/vschplugin.cpp" line="242"/>
        <source>Compute rules to classify data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="241"/>
        <source>Test classifier...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="272"/>
        <source>Segment image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="273"/>
        <source>Image segmentation by the VSCH classifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="279"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="280"/>
        <source>Info about this plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="642"/>
        <source>Save linear classifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="644"/>
        <location filename="../VschPlugin/vschplugin.cpp" line="678"/>
        <source>Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../VschPlugin/vschplugin.cpp" line="676"/>
        <source>Load linear classifier</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>testdialog</name>
    <message>
        <location filename="../MzShared/testdialog.ui" line="14"/>
        <source>Confusion matrix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzShared/testdialog.ui" line="51"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MzShared/testdialog.ui" line="61"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
</context>
</TS>
