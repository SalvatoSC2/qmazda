/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LDASELECTION_H
#define LDASELECTION_H
#include "../MzShared/multidimselection.h"
#include "../MzShared/dataforconsole.h"

#include <ap.h>
//#ifdef alglib
#include <linalg.h>
#include <dataanalysis.h>
//#else
//#include <lda.h>
//#endif

//#ifdef alglib
using namespace alglib;
//using namespace alglib_impl;

//#else
//using namespace ap;
//#endif

#include "../MzShared/classifierio.h"

#define NORMNONE -1
#define STANDARDIZE 0
#define NORMALIZE 1

#define THRESHOLD_FROM_STANDARD_DEV 0
#define THRESHOLD_FROM_ACCURACY 1
#define THRESHOLD_FROM_BALANCED_ACCURACY 2

#define DIRECTION_FROM_LDA 0
#define DIRECTION_FROM_MEANS 1

const char LdaSelectionReductionClassifierName[] = "MzLinearClassifiers2013";

class LdaSelectionReduction : public MultiDimensionalSelection, public ClassifierAccessInterface
{
public:
    virtual void NotifyProgressText(std::string text);
    virtual void NotifyProgressStep(void);
    virtual void NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected);

    double GoalFunction(unsigned int dimensions, unsigned int *picked_features);
    double ClassifierTraining(unsigned int dimensions, unsigned int* selected);

//    bool saveClassifierToFile(const char* filename);

    LdaSelectionReduction();
    ~LdaSelectionReduction();

    void setParameters(int dimensions, int maxtime);
    void setClassifier(Classifiers *classifier);//, int classtodistinguish = -1);
    void setInputData(DataForSelection* data);
    void setOutputBuffers(double* Qtable, unsigned int* Qsorted);
    //void setMapBuffer(MapsForSegmentation* input, MapsForSegmentation* output);
    void setReportStream(ofstream* report);

    bool computeMdfMaps(void);
    bool computeSegmentation(void);
    bool computeTest(void);
    bool computeSelect(void);
    bool computeProject(void);
    bool computeMdf(void);
//    bool computeMapmdfs(bool segment = false);

    const char* getClassifierMagic(void){return LdaSelectionReductionClassifierName;}
    bool loadClassifierFromFile(const char* filename);
    bool configureForClassification(vector<string>* featurenames);
    unsigned int classifyFeatureVector(double* featureVector);
    void segmentImage(unsigned int vectornumber, MazdaMapPixelType **values, MazdaMapPixelType *result);

    std::vector<std::string> getFeatureNames(void);
    std::vector<std::string> getClassNames(void);

    void mdfFromFeatureVector(double* featureVector, double* mdfs);

protected:
    DataForSegmentation dataMap;
    DataForSelection* data;
    ofstream *report;
    string* confusionTableText;
    double* Qtable;
    unsigned int* Qsorted;
    //    int classtodistinguish;

    int normalizeoption;
    unsigned int dimensions;
    int maxtime;
    int threshold_algorithm;
    int direction_algorithm;
    Classifiers* classifier;

private:
    int enumerateClasses(vector<string> *outClassNames);
    unsigned int* featureReindexLut;
    vector<string> outClassNames;
    unsigned int* outClassIndices;
    //vector<Classifier> *classifiers;
    unsigned int* classFireRequired;
// Funkcje do zmodyfikowania
    bool ComputeMdfs(double* table, unsigned int mdfsnumber);
// Spr czy sa potrzebne
//    bool featureReindex(int* featurereindex, vector<string> featurenames);
//    bool mapReindex(int* featurereindex, vector<string> featurenames);
//    bool ensembles;
//    bool Training(double* Qtable);
//    void StoreClassifier(unsigned int dim, double* miu, double* std, unsigned int *picked, double discriminant, real_1d_array* projection);
    void StoreClassifier(unsigned int dim, double thresh, bool rev, unsigned int *picked, double discriminant, real_1d_array* projection);
    void sortValCla(double* val, bool* cla, real_1d_array* mdf, unsigned int br, unsigned int end);
    void threshValCla(double* val, bool* cla, double* th, unsigned int br, unsigned int length, bool balanced);
    bool meantomean(const real_2d_array &xy, const ae_int_t npoints, const ae_int_t dims, real_1d_array &w);

    void computeThreshold(double* threshold, bool* rev, double miu[2], double std[2], real_1d_array *mdf, unsigned int br);
    void StoreProjectionMatrix(unsigned int dim, unsigned int* picked, real_2d_array* xy, real_2d_array* projection);
//    MapsForSegmentation *inmaps;
//    MapsForSegmentation *outmaps;
    int classtodistinguish1;
    int classtodistinguish2;
    vector<string> outclassnames;
    bool computeclassifier;
    double* normal_plus;
    double* normal_multi;
//    inline double GoalFunction1(unsigned int dim, unsigned int* picked);
//    inline double GoalFunctionNN(unsigned int dim, unsigned int* picked);
//    inline double GoalFunctionM(unsigned int dim, unsigned int* picked);
    void NormalizeMinMax(void);
    void Standardize(void);
};

#endif // LDASELECTION_H
