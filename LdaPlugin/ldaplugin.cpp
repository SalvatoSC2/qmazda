/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../MzShared/mzdefines.h"

//#include <QtGlobal>
#include "ldaplugin.h"
#include "../MzShared/multidimselection.h"

#include <string>
#include <sstream>
#include <stdlib.h>
#include <limits>
#include <time.h>

void* MzNewPluginObject(void)
{
    return new LdaPlugin();
}

void MzDeletePluginObject(void* object)
{
    delete static_cast<LdaPlugin *> (object);
}

void LdaPlugin::NotifyProgressStep(void)
{
    (*step_notifier)(notifier_object);
}
void LdaPlugin::NotifyProgressText(std::string text)
{
    (*text_notifier)(notifier_object, text);
}
void LdaPlugin::NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected)
{
    time_t currenttime = time(NULL);
    char* ct = ctime(&currenttime);
    strtok(ct, "\n\r");
    char text[256];

    switch(notification)
    {
    case BEGINS: sprintf(text, "Begins %iD at %s\n", dimensionality, ct); (*text_notifier)(notifier_object, text); break;
    case STOPPED: sprintf(text, "Stopped %.2f%%\n", (float)(value*100.0)); (*text_notifier)(notifier_object, text); break;
    case CANCELED: sprintf(text, "Canceled\n"); (*text_notifier)(notifier_object, text); break;
    case COMPLETED: sprintf(text, "Completed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case FAILED: sprintf(text,"Failed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case SUCCESS:
        sprintf(text,"D = %i Q = %f\n", dimensionality, (float)value);
        (*text_notifier)(notifier_object, text);
        if(data->featurenames != NULL && selected != NULL)
        {
            for(int d = 0; d < dimensionality; d++)
            {
                sprintf(text," %s %i\n", data->featurenames[selected[d]].c_str(), selected[d]);
                //emit notifyProgressText(text);
                (*text_notifier)(notifier_object, text);
            }
        }
        break;
    }
}


bool LdaPlugin::before_selection_this(void)
{
    if(! SetMachineLearningOptions()) return false; //User canceled
    if(! startThreadIn()) return false;
    newTempClassifier();
    unsigned int sizeQsorted = data->featurenumber;
    mz_uint64 maxSteps = 1;
    mz_uint64 maxstepsold = maxSteps;
    if(sizeQsorted < dimensions) dimensions = sizeQsorted;
    for(unsigned int d = 0; d < dimensions; d++)
    {
        for(unsigned int k = 0; k <= d; k++)
        {
            maxSteps *= (data->featurenumber-k);
            if(maxSteps / (data->featurenumber-k) != maxstepsold)
            {
                maxSteps = 0;
                k = d = dimensions;
            }
            maxstepsold = maxSteps;
        }
    }
//    classtodistinguish = -1;
    if(classifier == NULL)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }
    if(sizeQsorted > 0)
    {
        if(Qtable != NULL) delete[] Qtable;
        if(Qsorted != NULL) delete[] Qsorted;
        Qtable = new double[sizeQsorted];
        Qsorted = new unsigned int[sizeQsorted];
    }
//    if(mode_index < 0) classtodistinguish = mode_index;
//    else
//    {
//        for(int c = 0; c < data->classnumber; c++)
//        {
//            if(data->classnames[c] == classname.toStdString()) classtodistinguish = c;
//        }
//    }
    setInputData(data);
    setClassifier(classifier);//, classtodistinguish);
    setParameters(dimensions, maxtime);
    setOutputBuffers(Qtable, Qsorted);
    gui_tools->openProgressDialog(maxtime+data->classnumber, maxSteps, this, "Linear classifier learning");
    return true;
}
void LdaPlugin::thread_selection_this()
{
    breakanalysis = false;
    success = computeSelect();
}
void LdaPlugin::after_selection_this(void)
{
    stopThreadIn();
    if(success)
    {
        setClassifierFromTemp();
        std::vector<std::string> classifierNames;
        std::vector<bool> selectionResult;
        unsigned int features = data->featurenumber;
        if(Qtable != NULL && Qsorted!= NULL)
        {
            for(unsigned int c = 0; c < features; c++)
            {
                if(Qsorted[c] < features)
                {
                    std::stringstream ss;
                    ss << data->featurenames[Qsorted[c]] << " (" << Qtable[Qsorted[c]] << ")";
                    classifierNames.push_back(ss.str());
                    selectionResult.push_back(true);
                }
                else break;
            }
        }
        else
        {
            for(unsigned int c = 0; c < features; c++)
            {
                std::stringstream ss;
                ss << data->featurenames[c] << " (" << Qtable[Qsorted[c]] << ")";
                classifierNames.push_back(ss.str());
                selectionResult.push_back(true);
            }
        }

        if(gui_tools->selectClassifiers(&classifierNames, &selectionResult, "Select features"))
        {
            int count = 0;
            for(unsigned int c = 0; c < selectionResult.size(); c++)
            {
                if(selectionResult[c])  count ++;
            }
            if(count > 0)
            {
                SelectedFeatures ret;
                ret.featurenumber = count;
                ret.featureoriginalindex = new int[count];
                count = 0;
                int cc = 0;
                for(int c = 0; c < data->featurenumber; c++)
                {
                    if(Qsorted[c] < features)
                    {
                        if(selectionResult[cc])
                        {
                            ret.featureoriginalindex[count] = data->featureoriginalindex[Qsorted[c]];
                            count++;
                        }
                        cc++;
                    }
                }
                pull_data->setSelection(&ret);
            }
        }
    }
    else
    {
        releaseTempClassifier();
        gui_tools->showMessage("Error", "Selection failed", 3);
    }
    stopThreadOut();
}
bool LdaPlugin::before_selection(void* object)
{
    return ((LdaPlugin*)object)->before_selection_this();
}
void LdaPlugin::thread_selection(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((LdaPlugin*)object)->notifier_object = notifier_object;
    ((LdaPlugin*)object)->step_notifier = step_notifier;
    ((LdaPlugin*)object)->text_notifier = text_notifier;
    ((LdaPlugin*)object)->thread_selection_this();
}
void LdaPlugin::after_selection(void* object)
{
    ((LdaPlugin*)object)->after_selection_this();
}
void LdaPlugin::on_menuSelect_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_selection, &thread_selection, &after_selection);
}

bool LdaPlugin::before_test_this(void)
{
    if(! SelectClassifiersOptions()) return false; //User canceled
    if(!setTempClassifier()) return false;

    std::vector<std::string> featureNames;
    unsigned int classifiers_size = classifier->classifiers.size();
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int features_number = (classifier->classifiers)[c].featurenames.size();
        for(unsigned int k = 0; k < features_number; k++)
        {
            featureNames.push_back((classifier->classifiers)[c].featurenames[k]);
        }
    }
    if(!startThreadIn(&featureNames)) return false;
    featureNames.clear();

    mz_uint64 maxSteps = data->vectornumber;
    unsigned int sizeQsorted;
    outclassnames.clear();

    int cc = 0;
    for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c)
    {
        unsigned int mc = c->classnames.size();
        unsigned int i;
        if(mc > 2) mc = 2;
        for(i = 0; i < mc; i++)
        {
            bool newname = true;
            for(unsigned int ii = 0; ii < outclassnames.size(); ii++)
            {
                if(outclassnames[ii] == c->classnames[i])
                {
                    newname = false;
                    break;
                }
            }
            if(newname) outclassnames.push_back(c->classnames[i]);
            cc++;
        }
    }
    outclassnames.push_back("#");
    sizeQsorted = data->classnumber*outclassnames.size();

//    classtodistinguish = -1;
    if(classifier == NULL)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }
    if(sizeQsorted > 0)
    {
        if(Qsorted != NULL) delete[] Qsorted;
        Qsorted = new unsigned int[sizeQsorted];
    }
//    if(mode_index < 0) classtodistinguish = mode_index;
//    else
//    {
//        for(int c = 0; c < data->classnumber; c++)
//        {
//            if(data->classnames[c] == classname.toStdString()) classtodistinguish = c;
//        }
//    }
    confusionTableText = &confusionMatrixText;

    int oldn = normalizeoption;
    normalizeoption = NORMNONE;
    setInputData(data);
    normalizeoption = oldn;
    setClassifier(classifier);//, classtodistinguish);
    setParameters(dimensions, maxtime);
    setOutputBuffers(Qtable, Qsorted);
    gui_tools->openProgressDialog(0, maxSteps, this, "Linear classifier test");
    return true;
}
void LdaPlugin::thread_test_this()
{
    breakanalysis = false;
    success = computeTest();
}
void LdaPlugin::after_test_this(void)
{
    stopThreadIn();
    if(success)
    {
        gui_tools->showTestResults(&confusionMatrixText, "Test results");
    }
    else
    {
        gui_tools->showMessage("Error", "Test failed. Features required by the classifier may be missing.", 3);
    }
    stopThreadOut();
}
bool LdaPlugin::before_test(void* object)
{
    return ((LdaPlugin*)object)->before_test_this();
}
void LdaPlugin::thread_test(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((LdaPlugin*)object)->notifier_object = notifier_object;
    ((LdaPlugin*)object)->step_notifier = step_notifier;
    ((LdaPlugin*)object)->text_notifier = text_notifier;
    ((LdaPlugin*)object)->thread_test_this();
}
void LdaPlugin::after_test(void* object)
{
    ((LdaPlugin*)object)->after_test_this();
}
void LdaPlugin::on_menuTest_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_test, &thread_test, &after_test);
}

void LdaPlugin::thread_mdfs_this()
{
    breakanalysis = false;
    success = computeMdf();
}
void LdaPlugin::after_mdfs_this(void)
{
    stopThreadIn();
    releaseTempClassifier();
    if(success)
    {
        unsigned int* sorted = new unsigned int[data->featurenumber];
        for(int c = 0; c < data->featurenumber; c++) sorted[c] = c;
        {
            int count = data->featurenumber;
            if(count > 0)
            {
                SelectedFeatures ret;
                ret.featurenumber = count;
                ret.featureoriginalindex = new int[count];
                for(int c = 0; c < data->featurenumber; c++)
                {
                    ret.featureoriginalindex[c] = sorted[c];
                }
                pull_data->setData(data, &ret);
            }
        }
        delete[] sorted;
        Qtable = NULL;
    }
    else
    {
        gui_tools->showMessage("Error", "Computation failed. Features required by the classifier may be missing.", 3);
    }
    stopThreadOut();
}
void LdaPlugin::thread_mdfs(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((LdaPlugin*)object)->notifier_object = notifier_object;
    ((LdaPlugin*)object)->step_notifier = step_notifier;
    ((LdaPlugin*)object)->text_notifier = text_notifier;
    ((LdaPlugin*)object)->thread_mdfs_this();
}
void LdaPlugin::after_mdfs(void* object)
{
    ((LdaPlugin*)object)->after_mdfs_this();
}
void LdaPlugin::on_menuMdfs_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_test, &thread_mdfs, &after_mdfs);
}

bool LdaPlugin::before_projection_this(void)
{
    if(! startThreadIn()) return false;
    newTempClassifier();
    mz_uint64 maxSteps = 1;
//    int classtodistinguish = -3;

    setInputData(data);
    setClassifier(classifier);//, classtodistinguish);
    setParameters(dimensions, maxtime);
    setOutputBuffers(Qtable, Qsorted);
    gui_tools->openProgressDialog(maxtime, maxSteps, this, "Linear discriminant analysis");
    return true;
}

void LdaPlugin::thread_projection_this()
{
    breakanalysis = false;
    success = computeProject();
}
void LdaPlugin::after_projection_this(void)
{
    stopThreadIn();
    if(success)
    {
        setClassifierFromTemp();
        gui_tools->showMessage("Information", "Projection complete", 1);
    }
    else
    {
        releaseTempClassifier();
        gui_tools->showMessage("Error", "Projection failed", 3);
    }
    stopThreadOut();
}
bool LdaPlugin::before_projection(void* object)
{
    return ((LdaPlugin*)object)->before_projection_this();
}
void LdaPlugin::thread_projection(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((LdaPlugin*)object)->notifier_object = notifier_object;
    ((LdaPlugin*)object)->step_notifier = step_notifier;
    ((LdaPlugin*)object)->text_notifier = text_notifier;
    ((LdaPlugin*)object)->thread_projection_this();
}
void LdaPlugin::after_projection(void* object)
{
    ((LdaPlugin*)object)->after_projection_this();
}

void LdaPlugin::on_menuProject_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_projection, &thread_projection, &after_projection);
}

bool LdaPlugin::before_segmentation_this(void)
{
    if(! SelectClassifiersOptions()) return false; //User canceled
    if(! setTempClassifier()) return false;
    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();
    unsigned int classifiers_size = classifier->classifiers.size();
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int features_number = (classifier->classifiers)[c].featurenames.size();
        for(unsigned int k = 0; k < features_number; k++)
        {
            dataMap.featurenames.push_back((classifier->classifiers)[c].featurenames[k]);
        }
    }
    dataMap.resultnames.push_back("LinearEnsemble");
    if(! pull_data->getData(&dataMap))
    {
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    if(classifier == NULL)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }
    gui_tools->openProgressDialog(0, dataMap.vectornumber, this, "Linear classifier segmentation");
    return true;
}
void LdaPlugin::thread_segmentation_this()
{
    breakanalysis = false;
    success = computeSegmentation();
}
void LdaPlugin::after_segmentation_this(void)
{
    stopThreadIn();
    stopThreadOut();
}
bool LdaPlugin::before_segmentation(void* object)
{
    return ((LdaPlugin*)object)->before_segmentation_this();
}
void LdaPlugin::thread_segmentation(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((LdaPlugin*)object)->notifier_object = notifier_object;
    ((LdaPlugin*)object)->step_notifier = step_notifier;
    ((LdaPlugin*)object)->text_notifier = text_notifier;
    ((LdaPlugin*)object)->thread_segmentation_this();
}
void LdaPlugin::after_segmentation(void* object)
{
    ((LdaPlugin*)object)->after_segmentation_this();
}
void LdaPlugin::on_menuSegment_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_segmentation, &thread_segmentation, &after_segmentation);
}

bool LdaPlugin::before_mdfmaps_this(void)
{
    if(! SelectClassifiersOptions()) return false; //User canceled
    if(! setTempClassifier()) return false;
    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();
    unsigned int classifiers_size = classifier->classifiers.size();
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int features_number = (classifier->classifiers)[c].featurenames.size();
        for(unsigned int k = 0; k < features_number; k++)
        {
            dataMap.featurenames.push_back((classifier->classifiers)[c].featurenames[k]);
        }
        dataMap.resultnames.push_back(classifier->classifiers[c].getName());
    }
    if(! pull_data->getData(&dataMap))
    {
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    if(classifier == NULL)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }
    gui_tools->openProgressDialog(0, dataMap.vectornumber, this, "MDF maps");
    return true;
}
void LdaPlugin::thread_mdfmaps_this()
{
    breakanalysis = false;
    success = computeMdfMaps();
}
void LdaPlugin::after_mdfmaps_this(void)
{
    stopThreadIn();
    stopThreadOut();
}
bool LdaPlugin::before_mdfmaps(void* object)
{
    return ((LdaPlugin*)object)->before_mdfmaps_this();
}
void LdaPlugin::thread_mdfmaps(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((LdaPlugin*)object)->notifier_object = notifier_object;
    ((LdaPlugin*)object)->step_notifier = step_notifier;
    ((LdaPlugin*)object)->text_notifier = text_notifier;
    ((LdaPlugin*)object)->thread_mdfmaps_this();
}
void LdaPlugin::after_mdfmaps(void* object)
{
    ((LdaPlugin*)object)->after_mdfmaps_this();
}
void LdaPlugin::on_menuMdfMaps_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_mdfmaps, &thread_mdfmaps, &after_mdfmaps);
}


void LdaPlugin::cancelAnalysis(void)
{
    breakanalysis = true;
}

LdaPlugin::LdaPlugin()
{
    data = NULL;
    classifier = NULL;
    mainclassifier = NULL;
    Qtable = NULL;
    Qsorted = NULL;
    dimensions = 3;
    maxtime = 60;
    normalizeoption = STANDARDIZE;
    threshold_algorithm = THRESHOLD_FROM_STANDARD_DEV;
    direction_algorithm = DIRECTION_FROM_LDA;
//    mode_index = -2;
    pull_data = NULL;
    gui_tools = NULL;

    plugin_save = NULL;
    plugin_mdfs = NULL;
    plugin_test = NULL;
    classifieruse = NULL;
}

LdaPlugin::~LdaPlugin()
{
    if(data != NULL) delete data;
    if(mainclassifier != NULL) delete mainclassifier;
}

const char* LdaPlugin::getName(void)
{
    return "Linear discriminant analysis";
}


bool LdaPlugin::newTempClassifier(void)
{
    releaseTempClassifier();
    classifier = new Classifiers(LdaSelectionReductionClassifierName);
    return true;
}

bool LdaPlugin::setTempClassifier(void)
{
    releaseTempClassifier();
    if(mainclassifier == NULL) return false;
    if(classifieruse == NULL) *classifier = *mainclassifier;
    else
    {
        classifier = new Classifiers(LdaSelectionReductionClassifierName);
        bool none = true;
        int size = mainclassifier->classifiers.size();

        for(int i = 0; i < size; i++)
        {
            if(classifieruse[i])
            {
                classifier->classifiers.push_back(mainclassifier->classifiers[i]);
                none = false;
            }
        }
        if(none)
        {
            releaseTempClassifier();
            return false;
        }
    }
    return true;
}

bool LdaPlugin::releaseTempClassifier(void)
{
    if(classifier != NULL) delete classifier;
    classifier = NULL;
    return true;
}

bool LdaPlugin::setClassifierFromTemp(void)
{
    if(mainclassifier != NULL) delete mainclassifier;
    mainclassifier = classifier;
    classifier = NULL;

    if(classifier != NULL)
    {
        int size = mainclassifier->classifiers.size();
        if(classifieruse != NULL) delete[] classifieruse;
        classifieruse = new bool[size];
        for(int i = 0; i < size; i++) classifieruse[i] = true;
    }
    return true;
}

bool LdaPlugin::startThreadIn(void)
{
    if(data != NULL) delete data;
    data = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;
    data = new DataForSelection();
    pull_data->getData(data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    return true;
}

bool LdaPlugin::startThreadIn(std::vector<std::string>* featureNames)
{
    if(data != NULL) delete data;
    data = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;

    data = new DataForSelection();
    pull_data->getData(featureNames, data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    return true;
}

void LdaPlugin::stopThreadIn(void)
{
    gui_tools->closeProgressDialog();
}

void LdaPlugin::stopThreadOut(void)
{
    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();
    if(data != NULL) delete data;
    data = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;

    if(mainclassifier == NULL)
    {
        gui_tools->menuEnable(plugin_save, false);
        gui_tools->menuEnable(plugin_mdfs, false);
        gui_tools->menuEnable(plugin_test, false);
    }
    else
    {
        gui_tools->menuEnable(plugin_save, true);
        gui_tools->menuEnable(plugin_mdfs, true);
        gui_tools->menuEnable(plugin_test, true);
    }
}

void* LdaPlugin::connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function)
{
    void* ret = gui_tools->addMenuAction(name, tip, onActionTable.size());
    if(name != NULL && function != NULL)
        onActionTable.push_back(function);
    return ret;
}

bool LdaPlugin::initiateTablePlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;

    connectMenuAction("Load classifier...", "Load rules to classify data", &LdaPlugin::on_menuLoad_triggered);
    plugin_save = connectMenuAction("Save classifier...", "Save rules to classify data", &LdaPlugin::on_menuSave_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("Selection and training...", "Selection of features and training of the classifier", &LdaPlugin::on_menuSelect_triggered);
    connectMenuAction("Projection and training...", "Projection of feature space and training of the classifier", &LdaPlugin::on_menuProject_triggered);
    plugin_mdfs = connectMenuAction("Compute MDF's...", "Compute most discriminating features", &LdaPlugin::on_menuMdfs_triggered);
    plugin_test = connectMenuAction("Test classifier...", "Test classifier performance", &LdaPlugin::on_menuTest_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &LdaPlugin::on_menuAbout_triggered);
    stopThreadOut();
    return true;
}

bool LdaPlugin::initiateMapsPlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    connectMenuAction("Load classifier...", "Load rules to classify data", &LdaPlugin::on_menuLoad_triggered);
    plugin_save = connectMenuAction("Save classifier...", "Save rules to classify data", &LdaPlugin::on_menuSave_triggered);
    connectMenuAction(NULL, NULL, NULL);
    plugin_mdfs = connectMenuAction("Compute MDF maps...", "Compute maps of most discriminating features", &LdaPlugin::on_menuMdfMaps_triggered);
    plugin_test = connectMenuAction("Segment image...", "Image segmentation by the linear classifier", &LdaPlugin::on_menuSegment_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &LdaPlugin::on_menuAbout_triggered);
    stopThreadOut();
    return true;
}

void LdaPlugin::callBack(const unsigned int index)
{
//    typedef void (LdaPlugin::*ActionFunction)(void);
//        union u_ptm_cast {
//            ActionFunction f;
//            void* v;
//        };
//        u_ptm_cast ftov;
//        u_ptm_cast vtof;
//        ActionFunction aa = & LdaPlugin::on_menuAbout_triggered;
//        ftov.f = aa;
//        void* a1 = ftov.v;
//        vtof.v = a1;
//        ActionFunction bb = vtof.f;
//        (this->*bb)();  //I tak nie dziala

    if(index < onActionTable.size())
        if(onActionTable[index] != NULL)
            (this->*(onActionTable[index]))();
}

void LdaPlugin::on_menuAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>qmazda LdaPlugin</h2> " << std::endl;
    ss << QMAZDA_VERSION << "<br>" << std::endl;
    ss << QMAZDA_COPYRIGHT << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." << std::endl;
    ss << "Home: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html </a> <br>" << std::endl;
    ss << "Sources: <a href=\"https://gitlab.com/qmazda/qmazda\"> https://gitlab.com/qmazda/qmazda </a> <br>" << std::endl;
    ss << "<br>Built with:<br>" << std::endl;
    ss << "- alglib" << " <a href=\"http://www.alglib.net/\">http://www.alglib.net</a> <br>" << std::endl;
    gui_tools->showAbout("About LdaPlugin", ss.str().c_str());
}

bool LdaPlugin::SetMachineLearningOptions(void)
{
    std::stringstream ss;
    ss << "t;";
    switch(normalizeoption)
    {
    case NORMNONE: ss << "none"; break;
    case NORMALIZE: ss << "minmax"; break;
    default: ss << "standardize"; break;
    }
    ss << ";none;standardize;minmax";
    gui_tools->addPropertyOptionsDialog("Normalization", ss.str(), "Feature values normalization");
    ss.str(std::string());

    ss << "t;";
    switch(threshold_algorithm)
    {
    case THRESHOLD_FROM_ACCURACY: ss << "accuracy"; break;
    case THRESHOLD_FROM_BALANCED_ACCURACY: ss << "balanced"; break;
    default: ss << "bayes"; break;
    }
    ss << ";bayes;accuracy;balanced";
    gui_tools->addPropertyOptionsDialog("Threshold", ss.str(), "Select criterion to estimate threshold");
    ss.str(std::string());

    ss << "t;";
    switch(direction_algorithm)
    {
    case DIRECTION_FROM_MEANS: ss << "means"; break;
    default: ss << "lda"; break;
    }
    ss << ";lda;means";
    gui_tools->addPropertyOptionsDialog("Direction", ss.str(), "Select how to estimate direction");
    ss.str(std::string());

    ss << "i;" << maxtime << ";1;2000000000";
    gui_tools->addPropertyOptionsDialog("Time", ss.str(), "Maximum time for selection in seconds");
    ss.str(std::string());

    ss << "i;" << dimensions << ";1;32";
    gui_tools->addPropertyOptionsDialog("Dimensionality", ss.str(), "Maximum number of features selected");
    ss.str(std::string());

    if(gui_tools->openOptionsDialog("Linear classifier training"))
    {
        std::string ns;
        char* end;
        gui_tools->getValueOptionsDialog("Time", &ns);
        maxtime = strtol(ns.c_str(), &end, 10);
        gui_tools->getValueOptionsDialog("Dimensionality", &ns);
        dimensions = strtol(ns.c_str(), &end, 10);
        gui_tools->getValueOptionsDialog("Normalization", &ns);
        if(ns == "none") normalizeoption = NORMNONE;
        else if(ns == "minmax") normalizeoption = NORMALIZE;
        else normalizeoption = STANDARDIZE;

        gui_tools->getValueOptionsDialog("Threshold", &ns);
        if(ns == "accuracy") threshold_algorithm = THRESHOLD_FROM_ACCURACY;
        else if(ns == "balanced") threshold_algorithm = THRESHOLD_FROM_BALANCED_ACCURACY;
        else threshold_algorithm = THRESHOLD_FROM_STANDARD_DEV;

        gui_tools->getValueOptionsDialog("Direction", &ns);
        if(ns == "means") direction_algorithm = DIRECTION_FROM_MEANS;
        else direction_algorithm = DIRECTION_FROM_LDA;

        gui_tools->closeOptionsDialog();
        return true;
    }
    gui_tools->closeOptionsDialog();
    return false;
}


bool LdaPlugin::SelectClassifiersOptions(void)
{
    std::vector<std::string> classifierNames;
    std::vector<bool> selectionResult;
    if(mainclassifier != NULL)
    {
        unsigned int imax = mainclassifier->classifiers.size();
        if(classifieruse == NULL)
        {
            classifieruse = new bool[imax];
            for(unsigned int i = 0; i < imax; i++) classifieruse[i] = true;
        }
        for(unsigned int i = 0; i < imax; i++)
        {
            classifierNames.push_back(mainclassifier->classifiers[i].getName());
            selectionResult.push_back(classifieruse[i]);
        }
        if(gui_tools->selectClassifiers(&classifierNames, &selectionResult, "Select classifiers"))
        {
            if(imax > selectionResult.size()) imax = selectionResult.size();
            for(unsigned int i = 0; i < imax; i++)
            {
                classifieruse[i] = selectionResult[i];
            }
            return true;
        }
    }
    return false;
}

void LdaPlugin::on_menuSave_triggered()
{
    if(mainclassifier != NULL)
    {
        std::string fileName;
        unsigned int filter = 0;
        if(gui_tools->getSaveFile(&fileName, &filter))
        {
            if(!SelectClassifiersOptions())
            {
                gui_tools->showMessage("Error", "Failed to save classifier (no selection)", 3);
                return;
            }
            if(!setTempClassifier())
            {
                gui_tools->showMessage("Error", "Failed to save classifier (set empty)", 3);
                return;
            }
            if(!classifier->saveClassifier(fileName.c_str(), (filter==1)))
            {
                gui_tools->showMessage("Error", "Failed to save classifier", 3);
            }
            releaseTempClassifier();
        }
    }
    else
    {
        gui_tools->showMessage("Warning", "No classifier to save. Train first.", 2);
    }
}

bool LdaPlugin::openFile(string *filename)
{
    newTempClassifier();
    if(!classifier->loadClassifier(filename->c_str()))
    {
        releaseTempClassifier();
        return false;
    }
    else
    {
        if(classifier != NULL)
        {
            setClassifierFromTemp();
        }
        stopThreadOut();
        return true;
    }
}

void LdaPlugin::on_menuLoad_triggered()
{
    std::string fileName;
    if(gui_tools->getOpenFile(&fileName))
    {
        newTempClassifier();
        if(!classifier->loadClassifier(fileName.c_str()))
        {
            releaseTempClassifier();
            gui_tools->showMessage("Error", "Failed to load classifier", 3);
        }
        else
        {
            setClassifierFromTemp();
            if(classifieruse != NULL) delete[] classifieruse;
            classifieruse = NULL;
        }
    }
    stopThreadOut();
}
