#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T08:44:48
#
#-------------------------------------------------

#QT       += core
#QT       += gui
#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG -= app_bundle
CONFIG -= qt

TEMPLATE        = lib
CONFIG         += shared
CONFIG         += plugin

TARGET          = $$qtLibraryTarget(LdaPlugin)
DESTDIR         = ../../Executables

SOURCES += \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp \
    ../MzShared/multidimselection.cpp \
    ldaplugin.cpp \
    ../MzShared/classifierio.cpp \
    ldaselection.cpp

HEADERS +=\
    ../MzShared/dataforconsole.h \
    ../MzShared/mzselectioninterface.h \
    ../MzShared/multidimselection.h \
    ldaplugin.h \
    ../MzShared/classifierio.h \
    ldaselection.h

include(../Pri/config.pri)
include(../Pri/alglib.pri)
