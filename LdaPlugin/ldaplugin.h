/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TUTORIALPLUGIN_H
#define TUTORIALPLUGIN_H

//#include <QtGlobal>
//#include <QObject>
//#include <QThread>
//#include <QTextStream>
#include "../MzShared/mzselectioninterface.h"
// #include "pluginworker.h"
#include "ldaselection.h"
//enum LdaNormalization {LN_NONE = 0, LN_STD = 1, LN_MINMAX = 2};

class LdaPlugin : public MzSelectionPluginInterface, LdaSelectionReduction
{
    typedef void(LdaPlugin::*OnActionFunctionPointer)(void);
//    Q_OBJECT
public:
//    Q_INTERFACES(MzSelectionPluginInterface)
    void NotifyProgressStep(void);
    void NotifyProgressText(std::string text);
    void NotifyProgressStage(const int notification, const int dimensionality,
                             const double value, const unsigned int* selected);
    LdaPlugin();
    ~LdaPlugin();
    const char *getName(void);
    bool initiateTablePlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools);
    bool initiateMapsPlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools);
    void callBack(const unsigned int index);

    bool openFile(std::string* filename);
    void cancelAnalysis(void);


    bool before_selection_this(void);
    static bool before_selection(void* object);
    void thread_selection_this(void);
    static void thread_selection(void* object,
                                 void *notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier);
    void after_selection_this(void);
    static void after_selection(void* object);


    bool before_test_this(void);
    static bool before_test(void* object);
    void thread_test_this(void);
    static void thread_test(void* object,
                                 void *notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier);
    void after_test_this(void);
    static void after_test(void* object);


    void thread_mdfs_this(void);
    static void thread_mdfs(void* object,
                                 void *notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier);
    void after_mdfs_this(void);
    static void after_mdfs(void* object);


    bool before_projection_this(void);
    static bool before_projection(void* object);
    void thread_projection_this(void);
    static void thread_projection(void* object,
                                 void *notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier);
    void after_projection_this(void);
    static void after_projection(void* object);


    bool before_segmentation_this(void);
    void thread_segmentation_this();
    void after_segmentation_this(void);
    static bool before_segmentation(void* object);
    static void thread_segmentation(void* object, void* notifier_object,
                                     MzStepNotificationFunction step_notifier,
                                     MzTextNotificationFunction text_notifier);
    static void after_segmentation(void* object);


    bool before_mdfmaps_this(void);
    void thread_mdfmaps_this();
    void after_mdfmaps_this(void);
    static bool before_mdfmaps(void* object);
    static void thread_mdfmaps(void* object, void* notifier_object,
                                     MzStepNotificationFunction step_notifier,
                                     MzTextNotificationFunction text_notifier);
    static void after_mdfmaps(void* object);


//public slots:
    void on_menuAbout_triggered();
    void on_menuTest_triggered();
    void on_menuProject_triggered();
    void on_menuMdfs_triggered();
    void on_menuSelect_triggered();
    void on_menuSave_triggered();
    void on_menuLoad_triggered();
    void on_menuSegment_triggered();
    void on_menuMdfMaps_triggered();

private:
    void* plugin_save;
    void* plugin_mdfs;
    void* plugin_test;

    bool SetMachineLearningOptions(void);
    bool SelectClassifiersOptions(void);

    bool newTempClassifier(void);
    bool setTempClassifier(void);
    bool releaseTempClassifier(void);
    bool setClassifierFromTemp(void);

    bool startThreadIn(void);
    bool startThreadIn(std::vector<std::string>* featureNames);
    void stopThreadIn(void);
    void stopThreadOut(void);

    Classifiers* mainclassifier;
    bool* classifieruse;
    MzGuiRelatedInterface* gui_tools;
    MzPullDataInterface* pull_data;

//    QString classname;
//    int mode_index;
    std::vector<std::string> outclassnames;
    bool success;
    void* notifier_object;
    MzStepNotificationFunction step_notifier;
    MzTextNotificationFunction text_notifier;
    std::string confusionMatrixText;

    std::vector <OnActionFunctionPointer> onActionTable;
    void* connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function);
};

extern "C"
{
    MZ_DECL_EXPORT void* MzNewPluginObject(void);
    MZ_DECL_EXPORT void MzDeletePluginObject(void* object);
}

#endif // TUTORIALPLUGIN_H
