#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScrollArea>
#include <QWidget>
#include <QString>
#include <QDropEvent>
#include <QMimeData>
#include <QPainter>
#include <QByteArray>


namespace Ui {
class MainWindow;
}


class RawRenderer : public QWidget
{
    Q_OBJECT
public:
    RawRenderer(QWidget* parent = 0):QWidget(parent)
    {
        image = NULL;
    }
    ~RawRenderer(){}
    void setImage(QImage* image)
    {
        this->image = image;
        if(height() != image->height() || width() != image->width())
            resize(image->width(), image->height());
        update();
    }
private:
    void paintEvent(QPaintEvent* /*e*/)
    {
        if(image != NULL)
        {
            QPainter painter(this);
            painter.drawImage(image->rect(), *image);
        }
    }
    QImage* image;
};


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void dragEnterEvent(QDragEnterEvent* event)
    {
        event->acceptProposedAction();
    }
    void dragMoveEvent(QDragMoveEvent* event)
    {
        event->acceptProposedAction();
    }
    void dragLeaveEvent(QDragLeaveEvent* event)
    {
        event->accept();
    }
    void dropEvent(QDropEvent* event);

private slots:
    void on_actionLoad_triggered();

    void on_spinBox_Width_valueChanged(int arg1);

    void on_spinBox_Height_valueChanged(int arg1);

    void on_comboBox_Pixel_currentIndexChanged(int index);

    void on_spinBox_Skip_valueChanged(int arg1);

    void on_toolButton_Left_clicked();

    void on_toolButton_Right_clicked();

    void on_toolButton_Up_clicked();

    void on_toolButton_Dn_clicked();

private:
    Ui::MainWindow *ui;

    RawRenderer* renderer;
    QScrollArea* scrollArea;
    QString filename;
    QImage image;

    void loadImage(QString* fileName);
    void reload(void);
    void buffer2QImage(QByteArray *buffer, qint64 isize, int i);
    unsigned int sizeOfPixel(int* index);
};

#endif // MAINWINDOW_H
