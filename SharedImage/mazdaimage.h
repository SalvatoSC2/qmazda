/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAZDA_IMAGE_H
#define MAZDA_IMAGE_H

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <string>
#include "mazdadummy.h"

#pragma pack(push)
#pragma pack(1)
template< typename TChannel, unsigned int VChannels >
/**
 * @brief The RGBPixel struct to be used for allocation of multichannel images with MazdaImage class
 *
 * Example of use:
 * @code
 * typedef MazdaImage< MultiChannelPixel<unsigned char, 2> > RGB24BitImage;
 * unsigned int size[2];
 * double spacing[2];
 * size[0] = 320; spaing[0] = 1.0;
 * size[1] = 240; spaing[1] = 1.0;
 * RGB24BitImage* image = new RGB24BitImage(size, spacing);
 * @endcode
 */
struct MultiChannelPixel
{
    typedef TChannel ChannelType;
    const static int Channels = VChannels;
    TChannel channel[VChannels];
};
#pragma pack(pop)


template< typename TPixel, unsigned int VDimensions >
/**
 * @brief The MazdaImage class image container template
 *
 * Example of use:
 * @code
 * typedef MazdaImage< unsigned char, 2 > EightBitImage;
 * unsigned int size[2];
 * double spacing[2];
 * size[0] = 320; spaing[0] = 1.0;
 * size[1] = 240; spaing[1] = 1.0;
 * EightBitImage* image = new EightBitImage(size, spacing);
 * unsigned int x[2];
 * x[0] = 15; x[1] = 44;
 * SetPixel(x, 200);
 * unsigned char value = GetPixel(x);
 * @endcode
 */
class MazdaImage : public MazdaDummy
{
public:
    typedef TPixel PixelType;
    const static int Dimensions = VDimensions;
    /**
     * @brief MazdaImage costructor
     * @param size image size
     * @param spacing image spacing of pixels/voxels
     * @param erase true to erase allocated memory
     */
    MazdaImage(const unsigned int size[VDimensions], const double spacing[VDimensions], const bool erase = false)
    {
        size_t count = 1;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            this->size[d] = size[d];
            this->spacing[d] = spacing[d];
            count *= size[d];
        }
        data = new TPixel[count];
        if(erase) memset(data, 0, sizeof(TPixel)*count);
        deallocate = true;
    }
    /**
     * @brief MazdaImage costructor
     * @param size image size
     * @param spacing image spacing of pixels/voxels
     * @param data pointer to already allocated data buffer
     */
    MazdaImage(const unsigned int size[VDimensions], const double spacing[VDimensions], TPixel* data)
    {
        size_t count = 1;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            this->size[d] = size[d];
            this->spacing[d] = spacing[d];
            count *= size[d];
        }
        this->data = data;
        deallocate = true;
    }
    ~MazdaImage(void)
    {
        if(deallocate) delete[] data;
    }
    /**
     * @brief Erase zeroes all the image memory
     */
    void Erase(void)
    {
        size_t count = 1;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            count *= size[d];
        }
        memset(data, 0, sizeof(TPixel)*count);
    }

    /**
     * @brief DeallocateAtDestruction controls data buffer deallocation at the object destruction
     * @param deallocate true to deallocate, false to leave the data buffer allocated
     */
    void DeallocateAtDestruction(bool deallocate)
    {
        this->deallocate = deallocate;
    }
    /**
     * @brief GetDataPointer returns pointer to data buffer
     * @return pointer to data buffer
     */
    TPixel* GetDataPointer(void)
    {
        return data;
    }
    /**
     * @brief GetPixelPointer returns pointer to the pixel at given coordinates
     * @param coords coordinates
     * @return pointer to the pixel
     */
    TPixel* GetPixelPointer(const unsigned int coords[VDimensions])
    {
        size_t p = coords[VDimensions-1];
        for(unsigned int d = VDimensions-2; d < VDimensions; d--)
        {
            p *= size[d];
            p += coords[d];
        }
        return data + p;
    }
    /**
     * @brief GetPixel returns pixel at given coordinates
     * @param coords coordinates
     * @return pixel
     */
    TPixel GetPixel(const unsigned int coords[VDimensions])
    {
        return *GetPixelPointer(coords);
    }
    /**
     * @brief SetPixel sets pixel at given coordinates
     * @param coords coordinates
     * @param value pixel
     */
    void SetPixel(const unsigned int coords[VDimensions], TPixel value)
    {
        *GetPixelPointer(coords) = value;
    }
    /**
     * @brief GetPixelSize returns pixel size in bytes
     * @return size
     */
    unsigned int GetPixelSize(void)
    {
        return sizeof(TPixel);
    }
    /**
     * @brief GetDimensions returns number of image dimensions
     * @return dimensions
     */
    unsigned int GetDimensions(void)
    {
        return VDimensions;
    }
    /**
     * @brief GetSize retrieves the image size
     * @param size vector to store the size
     */
    void GetSize(unsigned int size[VDimensions])
    {
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            size[d] = this->size[d];
        }
    }

//    unsigned int GetSizeD(unsigned int d)
//    {
//        return this->size[d];
//    }
    /**
     * @brief GetSpacing retrieves the image spacing
     * @param spacing vector
     */
    void GetSpacing(double spacing[VDimensions])
    {
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            spacing[d] = this->spacing[d];
        }
    }
    void SetSpacing(double spacing[VDimensions])
    {
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            this->spacing[d] = spacing[d];
        }
    }


    /**
     * @brief GetName retrieves a class name associated with the region
     * @return string with class name
     */
    std::string GetName(void)
    {
        return name;
    }
    /**
     * @brief SetName sets a class name associated with the region
     * @name string with class name
     */
    void SetName(std::string name)
    {
        this->name = name;
    }
private:
    TPixel* data;
    double spacing[VDimensions];
    unsigned int size[VDimensions];
    bool deallocate;
    std::string name;
};

template< typename TMazdaImage >
/**
 * @brief The MazdaImageIterator class is a MazdaImage iterator over all the image
 *
 * It is the fastest implemented itarator for MazdaImage objects.
 * The iterator does not enable current coordinates access.
 *
 * Example of usage:
 * @code
 * typedef MazdaImage<unsigned char, 3> MIType;
 * MIType* obraz = new MIType(size, spacing, true);
 * MazdaImageIterator<MIType> iterator(obraz);
 * do{
 *     iterator.SetPixel(255);
 *     ++iterator;
 * }while(! iterator.IsBehind(void));
 * iterator.GoToEnd();
 * do{
 *     iterator.SetPixel(127);
 *     --iterator;
 * }while(! iterator.IsBelow(void));
 * @endcode
 * @author Piotr M. Szczypinski
 */
class MazdaImageIterator
{
public:
    typedef typename TMazdaImage::PixelType PixelType;
    /**
     * @brief MazdaImageIterator constructor
     * @param image image to be iterated
     */
    MazdaImageIterator(TMazdaImage* image)
    {
        unsigned int size[TMazdaImage::Dimensions];
        image->GetSize(size);
        pointer = begin = image->GetDataPointer();
        size_t count = 1;
        for(unsigned int d = 0; d < TMazdaImage::Dimensions; d++)
        {
            count *= size[d];
        }
        end = begin+count;
    }
    /**
     * @brief GoToBegin resets iterator at the image begin
     */
    void GoToBegin(void)
    {
        pointer = begin;
    }
    /**
     * @brief GoToEnd resets iterator at the image end
     */
    void GoToEnd(void)
    {
        pointer = end - 1;
    }
    /**
     * @brief IsBehind indicates the end of iteration process
     * @return true when iterator reaches the end of the image data
     */
    bool IsBehind(void)
    {
        return (pointer >= end);
    }
    /**
     * @brief IsBelow indicates the end of iteration process
     * @return true when iterator goes below the begining of the image data
     */
    bool IsBelow(void)
    {
        return (pointer < begin);
    }
    /**
     * @brief SetPixel sets the current pixel
     * @param value to be set
     */
    void SetPixel(PixelType value)
    {
        *pointer = value;
    }
    /**
     * @brief GetPixel gets the current pixel
     * @return pixel
     */
    PixelType GetPixel(void)
    {
        return *pointer;
    }
    /**
     * @brief GetPixelPointer gets the pointer to the current pixel
     * @return pixel's pointer
     */
    PixelType* GetPixelPointer(void)
    {
        return pointer;
    }
    /**
     * @brief operator ++ increments iterator to get to the next pixel
     */
    void operator++()
    {
        pointer++;
    }
    /**
     * @brief operator -- decrements iterator to get to the previous pixel
     */
    void operator--()
    {
        pointer--;
    }
    /**
     * @brief operator ++ increments iterator by i
     * @param i
     */
    void operator+=(int i)
    {
        pointer+=i;
    }
    /**
     * @brief operator -- decrements iterator by i
     * @param i
     */
    void operator-=(int i)
    {
        pointer-=i;
    }
private:
    PixelType* begin;
    PixelType* pointer;
    PixelType* end;
};

template< typename TMazdaImage >
/**
 * @brief The MazdaImageRegionIterator class: MazdaImage iterator over rectangular/cubic region/volume
 *
 * It enables iteration over rectangular or cubic region of image.
 * The iterator enables access to the current internal coordinates.
 * The corresponding begin[.] coordinate must be smaller or equal to the end[.] coordinate
 *
 * Example of usage:
 * @code
 * typedef MazdaRoi<unsigned int, 3> MRType;
 * MRType* roi = new MRType(begin, end);
 * int sub_region_begin[3];
 * int sub_region_end[3];
 * sub_region_begin[0] = 2; sub_region_end[0] = 17;
 * sub_region_begin[1] = 2; sub_region_end[1] = 20;
 * sub_region_begin[2] = 1; sub_region_end[2] = 9;
 * MazdaRoiRegionIterator<MRType> iterator(roi, sub_region_begin, sub_region_end);
 * do{
 *     iterator.ClrPixel();
 *     bool isRoi = iterator.GetPixel();
 *     ++iterator;
 * }while(! iterator.IsBehind());
 * @endcode
 * @author Piotr M. Szczypinski
 */
class MazdaImageRegionIterator
{
public:
    typedef typename TMazdaImage::PixelType PixelType;
    /**
     * @brief MazdaImageRegionIterator constructor of this iterator
     * @param image pointer to the TMazdaImage object to be iterated
     * @param begin region's top-left... coordinates
     * @param end region's bottom-right... coordinates
     */
    MazdaImageRegionIterator(TMazdaImage* image, unsigned int begin[TMazdaImage::Dimensions], unsigned int end[TMazdaImage::Dimensions])
    {
        size_t ll = 1;
        unsigned int size[TMazdaImage::Dimensions];
        image->GetSize(size);
        for(unsigned int d = 0; d < TMazdaImage::Dimensions; d++)
        {
            this->index[d] = begin[d];
            this->begin[d] = begin[d];
            this->end[d] = end[d];
            this->step[d] = ll;
            this->line[d] = ll * (end[d] - begin[d] + 1);
            ll *= size[d];
        }
        pointer = pointerbegin = image->GetPixelPointer(begin);
        finished = false;
    }
    /**
     * @brief GoToBegin resets the iterator
     */
    void GoToBegin(void)
    {
        for(unsigned int d = 0; d < TMazdaImage::Dimensions; d++)
        {
            index[d] = begin[d];
        }
        pointer = pointerbegin;
        finished = false;
    }
    /**
     * @brief IsBehind indicates end of iteration process
     * @return true when iterator reaches the end of the regions data
     */
    bool IsBehind(void)
    {
        return finished;
    }
    /**
     * @brief SetPixel sets the current pixel
     * @param pixel value to set
     */
    void SetPixel(PixelType value)
    {
        *pointer = value;
    }
    /**
     * @brief GetPixel returns the current pixel
     * @return pixel
     */
    PixelType GetPixel(void)
    {
        return *pointer;
    }
    /**
     * @brief GetPixelPointer gets the pointer to the current pixel
     * @return pixel's pointer
     */
    PixelType* GetPixelPointer(void)
    {
        return pointer;
    }
    /**
     * @brief GetIndex returnes the coordinate of current pixel
     * @param dir coordinate x-0, y-1, z-2 ...
     * @return
     */
    unsigned int GetIndex(const unsigned int dir)
    {
        return index[dir];
    }
    unsigned int* GetIndex(void)
    {
        return index;
    }
    /**
     * @brief operator ++ increments iterator to get to the next pixel
     */
    void operator++()
    {
        unsigned int d;
        pointer++;
        index[0]++;
        if(index[0] <= end[0])
        {
            return;
        }
        else
        {
            index[0] = begin[0];
            pointer -= line[0];
        }
        for(d = 1; d < TMazdaImage::Dimensions; d++)
        {
            index[d]++;
            pointer += step[d];
            if(index[d] <= end[d])
            {
                return;
            }
            else
            {
                index[d] = begin[d];
                pointer -= line[d];
            }
        }
        finished = true;
    }
private:
    size_t step[TMazdaImage::Dimensions];
    size_t line[TMazdaImage::Dimensions];
    unsigned int index[TMazdaImage::Dimensions];
    unsigned int begin[TMazdaImage::Dimensions];
    unsigned int end[TMazdaImage::Dimensions];
    PixelType* pointer;
    PixelType* pointerbegin;
    bool finished;
};


template< typename TMazdaImage >
/**
 * @brief The MazdaImageRegionReversiveIterator class: MazdaImage iterator over rectangular/cubic region/volume, forward or reverse in any direction
 *
 * It enables iteration over rectangular or cubic region.
 * The corresponding begin[.] coordinate must be smaller or equal to the end[.] coordinate
 *
 * Example of usage:
 * @code
 * typedef MazdaImage<unsigned char, 3> MIType;
 * MIType* obraz = new MIType(size, spacing, true);
 * int sub_region_begin[3];
 * int sub_region_end[3];
 * sub_region_begin[0] = 2; sub_region_end[0] = 17;
 * sub_region_begin[1] = 20; sub_region_end[1] = 2;
 * sub_region_begin[2] = 9; sub_region_end[2] = 1;
 * MazdaImageRegionReversiveIterator<MIType> iterator(obraz, sub_region_begin, sub_region_end);
 * do{
 *     iterator.SetPixel(127);
 *     unsigned char value = iterator.GetPixel();
 *     ++iterator;
 * }while(! iterator.IsBehind());
 * @endcode
 * @author Piotr M. Szczypinski */
class MazdaImageRegionReversiveIterator
{
public:
    typedef typename TMazdaImage::PixelType PixelType;
    /**
     * @brief MazdaImageRegionReversiveIterator constructor of this iterator
     * @param image pointer to the TMazdaImage object to be iterated
     * @param begin region's top-left... coordinates
     * @param end region's bottom-right... coordinates
     */
    MazdaImageRegionReversiveIterator(TMazdaImage* image, unsigned int begin[TMazdaImage::Dimensions], unsigned int end[TMazdaImage::Dimensions])
    {
        size_t ll = 1;
        unsigned int size[TMazdaImage::Dimensions];
        image->GetSize(size);
        for(unsigned int d = 0; d < TMazdaImage::Dimensions; d++)
        {
            this->index[d] = 0;
            this->end[d] = abs((int)end[d]-(int)begin[d]);
            if(begin[d] < end[d])
            {
                this->step[d] = ll;
                this->line[d] = ll * (end[d] - begin[d] + 1);
            }
            else
            {
                this->step[d] = -ll;
                this->line[d] = -ll * (begin[d] - end[d] + 1);
            }
            ll *= size[d];
        }
        pointer = pointerbegin = image->GetPixelPointer(begin);
        finished = false;
    }
    /**
     * @brief GoToBegin resets the iterator
     */
    void GoToBegin(void)
    {
        for(unsigned int d = 0; d < TMazdaImage::Dimensions; d++)
        {
            index[d] = 0;
        }
        pointer = pointerbegin;
        finished = false;
    }
    /**
     * @brief IsBehind indicates end of iteration process
     * @return true when iterator reaches the end of the regions data
     */
    bool IsBehind(void)
    {
        return finished;
    }
    /**
     * @brief SetPixel sets the current pixel
     * @param value of pixel to set
     */
    void SetPixel(PixelType value)
    {
        *pointer = value;
    }
    /**
     * @brief GetPixel returns the current pixel
     * @return pixel
     */
    PixelType GetPixel(void)
    {
        return *pointer;
    }
    /**
     * @brief GetPixel returns a pointer to the current pixel
     * @return pixel pinter
     */
    PixelType* GetPixelPointer(void)
    {
        return pointer;
    }
    /**
     * @brief operator ++ increments iterator to get to the next pixel
     */
    void operator++()
    {
        unsigned int d;
        for(d = 0; d < TMazdaImage::Dimensions; d++)
        {
            index[d]++;
            pointer += step[d];
            if(index[d] <= end[d])
            {
                return;
            }
            else
            {
                index[d] = 0;
                pointer -= line[d];
            }
        }
        finished = true;
    }
private:
    long int step[TMazdaImage::Dimensions];
    long int line[TMazdaImage::Dimensions];
    unsigned int index[TMazdaImage::Dimensions];
    unsigned int end[TMazdaImage::Dimensions];
    PixelType* pointer;
    PixelType* pointerbegin;
    bool finished;
};


template< typename TMazdaImage >
/**
 * @brief The MazdaImageRegionFlippedIterator class: MazdaImage iterator over rectangular/cubic region/volume to be rotated and flipped
 *
 * It enables iteration over rectangular or cubic region in arbitrary chosen direction.
 * The corresponding begin[.] coordinate can be smaller, equal or higher than the end[.] coordinate
 * It enables looping in arbitrary order of directions.
 * The iterator is used if the image is rotated and must be iterated with respect to its current orientation.
 *
 * Example of usage:
 * @code
 * typedef MazdaImage<unsigned char, 3> MIType;
 * MIType* obraz = new MIType(size, spacing, true);
 * int sub_region_begin[3];
 * int sub_region_end[3];
 * sub_region_begin[0] = 7; sub_region_end[0] = 7;
 * sub_region_begin[1] = 20; sub_region_end[1] = 2;
 * sub_region_begin[2] = 1; sub_region_end[2] = 9;
 * unsigned int looping_order[3]
 * looping_order[0] = 2; looping_order[1] = 0; looping_order[2] = 1;
 * MazdaImageRegionFlippedIterator<MIType> iterator(obraz, sub_region_begin, sub_region_end, looping_order);
 * do{
 *     iterator.SetPixel(127);
 *     unsigned char value = iterator.GetPixel();
 *     ++iterator;
 * }while(! iterator.IsBehind());
 * @endcode
 * @author Piotr M. Szczypinski */
class MazdaImageRegionFlippedIterator
{
public:
    typedef typename TMazdaImage::PixelType PixelType;
    /**
     * @brief MazdaImageRegionFlippedIterator constructor of this iterator
     * @param image pointer to the TMazdaImage object to be iterated
     * @param begin region's top-left... coordinates
     * @param end region's bottom-right... coordinates
     * @param direction order of directions, looping order
     */
    MazdaImageRegionFlippedIterator(TMazdaImage* image, unsigned int begin[TMazdaImage::Dimensions], unsigned int end[TMazdaImage::Dimensions], unsigned int direction[TMazdaImage::Dimensions])
    {
        size_t ll = 1;
        unsigned int size[TMazdaImage::Dimensions];
        image->GetSize(size);
        for(unsigned int d = 0; d < TMazdaImage::Dimensions; d++)
        {
            this->direction[d] = direction[d];
            this->index[d] = 0;
            this->end[d] = abs((int)end[d]-(int)begin[d]);
            if(begin[d] < end[d])
            {
                this->step[d] = ll;
                this->line[d] = ll * (end[d] - begin[d] + 1);
            }
            else
            {
                this->step[d] = -ll;
                this->line[d] = -ll * (begin[d] - end[d] + 1);
            }
            ll *= size[d];
        }
        pointer = pointerbegin = image->GetPixelPointer(begin);
        finished = false;
    }
    /**
     * @brief GoToBegin resets the iterator
     */
    void GoToBegin(void)
    {
        for(unsigned int d = 0; d < TMazdaImage::Dimensions; d++)
        {
            index[d] = 0;
        }
        pointer = pointerbegin;
        finished = false;
    }
    /**
     * @brief IsBehind indicates end of iteration process
     * @return true when iterator reaches the end of the regions data
     */
    bool IsBehind(void)
    {
        return finished;
    }
    /**
     * @brief SetPixel sets the current pixel
     * @param value of pixel to set
     */
    void SetPixel(PixelType value)
    {
        *pointer = value;
    }
    /**
     * @brief GetPixel returns the current pixel
     * @return pixel
     */
    PixelType GetPixel(void)
    {
        return *pointer;
    }
    /**
     * @brief GetPixel returns a pointer to the current pixel
     * @return pixel pinter
     */
    PixelType* GetPixelPointer(void)
    {
        return pointer;
    }
    /**
     * @brief operator ++ increments iterator to get to the next pixel
     */
    void operator++()
    {
        unsigned int dd, d;
        for(dd = 0; dd < TMazdaImage::Dimensions; dd++)
        {
            d = direction[dd];
            index[d]++;
            pointer += step[d];
            if(index[d] <= end[d])
            {
                return;
            }
            else
            {
                index[d] = 0;
                pointer -= line[d];
            }
        }
        finished = true;
    }
private:
    long int step[TMazdaImage::Dimensions];
    long int line[TMazdaImage::Dimensions];
    unsigned int direction[TMazdaImage::Dimensions];
    unsigned int index[TMazdaImage::Dimensions];
    unsigned int end[TMazdaImage::Dimensions];
    PixelType* pointer;
    PixelType* pointerbegin;
    bool finished;
};

#endif // MAZDA_IMAGE_H
