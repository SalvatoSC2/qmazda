#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T08:44:48
#
#-------------------------------------------------

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    test.cpp

HEADERS += \
    mazdaimage.h \
    mazdaroi.h \
    mazdaimageio.h \
    mazdaroiio.h \
    mazdadummy.h

LIBS += -ltiff

#INCLUDEPATH +=  "/usr/include/ITK-4.5/Common"\
#                "/usr/include/ITK-4.5"\
#                "/usr/include/ITK-4.5/Utilities/vxl/vcl"\
#                "/usr/include/ITK-4.5/Utilities/vxl/core"\
#                "/usr/include/ITK-4.5/IO"\
#                "/usr/include/ITK-4.5/Utilities"\
#                "/usr/include/ITK-4.5/BasicFilters"\
#                "/usr/include/ITK-4.5/Review"

#    LIBS += \
#                        -litksys-4.5\
#                        -lITKBiasCorrection-4.5\
#                        -lITKBioCell-4.5\
#                        -lITKCommon-4.5\
#                        -lITKDICOMParser-4.5\
#                        -litkdouble-conversion-4.5\
#                        -lITKEXPAT-4.5\
#                        -lITKFEM-4.5\
#                        -lITKIOBioRad-4.5\
#                        -lITKIOBMP-4.5\
#                        -lITKIOCSV-4.5\
#                        -lITKIOGDCM-4.5\
#                        -lITKIOGE-4.5\
#                        -lITKIOGIPL-4.5\
#                        -lITKIOHDF5-4.5\
#                        -lITKIOImageBase-4.5\
#                        -lITKIOIPL-4.5\
#                        -lITKIOJPEG-4.5\
#                        -lITKIOLSM-4.5\
#                        -lITKIOMesh-4.5\
#                        -lITKIOMeta-4.5\
#                        -lITKIOMRC-4.5\
#                        -lITKIONIFTI-4.5\
#                        -lITKIONRRD-4.5\
#                        -lITKIOPNG-4.5\
#                        -lITKIOSiemens-4.5\
#                        -lITKIOSpatialObjects-4.5\
#                        -lITKIOStimulate-4.5\
#                        -lITKIOTIFF-4.5\
#                        -lITKIOTransformBase-4.5\
#                        -lITKIOTransformHDF5-4.5\
#                        -lITKIOTransformInsightLegacy-4.5\
#                        -lITKIOTransformMatlab-4.5\
#                        -lITKIOVTK-4.5\
#                        -lITKIOXML-4.5\
#                        -lITKKLMRegionGrowing-4.5\
#                        -lITKLabelMap-4.5\
#                        -lITKMesh-4.5\
#                        -lITKMetaIO-4.5\
#                        -litkNetlibSlatec-4.5\
#                        -lITKniftiio-4.5\
#                        -lITKNrrdIO-4.5\
#                        -litkopenjpeg-4.5\
#                        -lITKOptimizers-4.5\
#                        -lITKPath-4.5\
#                        -lITKPolynomials-4.5\
#                        -lITKQuadEdgeMesh-4.5\
#                        -lITKSpatialObjects-4.5\
#                        -lITKStatistics-4.5\
#                        -litkv3p_lsqr-4.5\
#                        -litkv3p_netlib-4.5\
#                        -litkvcl-4.5\
#                        -lITKVideoCore-4.5\
#                        -lITKVideoIO-4.5\
#                        -litkvnl_algo-4.5\
#                        -litkvnl-4.5\
#                        -lITKVNLInstantiation-4.5\
#                        -lITKVTK-4.5\
#                        -lITKWatersheds-4.5\
#                        -lITKznz-4.5


INCLUDEPATH +=  "/usr/include/ITK-4.7/Common"\
                "/usr/include/ITK-4.7"\
                "/usr/include/ITK-4.7/Utilities/vxl/vcl"\
                "/usr/include/ITK-4.7/Utilities/vxl/core"\
                "/usr/include/ITK-4.7/IO"\
                "/usr/include/ITK-4.7/Utilities"\
                "/usr/include/ITK-4.7/BasicFilters"\
                "/usr/include/ITK-4.7/Review"

LIBS += \
                        -litksys-4.7\
                        -lITKBiasCorrection-4.7\
                        -lITKBioCell-4.7\
                        -lITKCommon-4.7\
                        -lITKDICOMParser-4.7\
                        -litkdouble-conversion-4.7\
                        -lITKEXPAT-4.7\
                        -lITKFEM-4.7\
                        -lITKIOBioRad-4.7\
                        -lITKIOBMP-4.7\
                        -lITKIOCSV-4.7\
                        -lITKIOGDCM-4.7\
                        -lITKIOGE-4.7\
                        -lITKIOGIPL-4.7\
                        -lITKIOHDF5-4.7\
                        -lITKIOImageBase-4.7\
                        -lITKIOIPL-4.7\
                        -lITKIOJPEG-4.7\
                        -lITKIOLSM-4.7\
                        -lITKIOMesh-4.7\
                        -lITKIOMeta-4.7\
                        -lITKIOMRC-4.7\
                        -lITKIONIFTI-4.7\
                        -lITKIONRRD-4.7\
                        -lITKIOPNG-4.7\
                        -lITKIOSiemens-4.7\
                        -lITKIOSpatialObjects-4.7\
                        -lITKIOStimulate-4.7\
                        -lITKIOTIFF-4.7\
                        -lITKIOTransformBase-4.7\
                        -lITKIOTransformHDF5-4.7\
                        -lITKIOTransformInsightLegacy-4.7\
                        -lITKIOTransformMatlab-4.7\
                        -lITKIOVTK-4.7\
                        -lITKIOXML-4.7\
                        -lITKKLMRegionGrowing-4.7\
                        -lITKLabelMap-4.7\
                        -lITKMesh-4.7\
                        -lITKMetaIO-4.7\
                        -litkNetlibSlatec-4.7\
                        -lITKniftiio-4.7\
                        -lITKNrrdIO-4.7\
                        -litkopenjpeg-4.7\
                        -lITKOptimizers-4.7\
                        -lITKPath-4.7\
                        -lITKPolynomials-4.7\
                        -lITKQuadEdgeMesh-4.7\
                        -lITKSpatialObjects-4.7\
                        -lITKStatistics-4.7\
                        -litkv3p_lsqr-4.7\
                        -litkv3p_netlib-4.7\
                        -litkvcl-4.7\
                        -lITKVideoCore-4.7\
                        -lITKVideoIO-4.7\
                        -litkvnl_algo-4.7\
                        -litkvnl-4.7\
                        -lITKVNLInstantiation-4.7\
                        -lITKVTK-4.7\
                        -lITKWatersheds-4.7\
                        -lITKznz-4.7
