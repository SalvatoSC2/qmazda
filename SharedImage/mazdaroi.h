/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAZDA_ROI_H
#define MAZDA_ROI_H

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <string>
#include <stdio.h>
#include "mazdadummy.h"

const unsigned int RoiDefaultColors[16] =
{
    0x00FF0000, 0x0000FF00, 0x000000FF,
    0x0000FFFF, 0x00FF00FF, 0x00FFFF00,
    0x00FF8000, 0x00FF0080, 0x0080FF00,
    0x0000FF80, 0x008000FF, 0x000080FF,
    0x00FFC400, 0x00C4FF00, 0x00C400FF,
    0x0000FFC4
};

template< typename TBlock, unsigned int VDimensions >
/**
 * @brief The MazdaRoi class is a region of interest container.
 *
 * One bit per pixel is used. The single line of the region is constituted by
 * integer number of TBlock type variables. The VDimension determines number
 * of image dimensions.
 *
 * The region covers the image starting from the coordinates defined by the
 * begin[] vector. The region or volume size is defined by the size[] vector.
 *
 * Example of usage:
 * @code
 * int begin[3];
 * int end[3];
 * begin[0] = 2; end[0] = 17;
 * begin[1] = 2; end[1] = 20;
 * begin[2] = 1; end[2] = 9;
 * typedef MazdaRoi<unsigned int, 3> MRType;
 * MRType* roi = new MRType(begin, end);
 * int x[3];
 * x[0] = 5; x[1] = 18; x[2] = 7;
 * roi->SetRefiPixel(x);
 * bool isRoi = roi->GetRefiPixel(x);
 * @endcode
 * @author Piotr M. Szczypinski
 */
class MazdaRoi : public MazdaDummy
{
public:
    typedef TBlock BlockType;
    const static int Dimensions = VDimensions;
    MazdaRoi()
    {
        data = NULL;
    }
    /**
     * @brief MazdaRoi constructs empty region and allocates memory for it.
     * @param begin vector to define top-left(-far) coordinates
     * @param end vector to define bottom-right(-close) coordinates
     */
    MazdaRoi(const int begin[VDimensions], const int end[VDimensions])
    {
        data = NULL;
        Reallocate(begin, end);
    }
    ~MazdaRoi(void)
    {
        if(data != NULL)
            delete[] data;
    }
    /**
     * @brief Shift shifts the region of interest in image coordinates
     * @param shift vector
     */
    void Shift(const int shift[VDimensions])
    {
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            this->begin[d] += shift[d];
            this->end[d] += shift[d];
        }
    }

    /**
     * @brief SetBegin
     * @param begin
     */
    void SetBegin(const int begin[VDimensions])
    {
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            this->begin[d] = begin[d];
            this->end[d] = begin[d]+size[d]-1;
        }
    }


    /**
     * @brief Reallocate erases data and allocates memory for a new region
     * @param begin vector to define top-left(-far) coordinates
     * @param end vector to define bottom-right(-close) coordinates
     */
    void Reallocate(const int begin[VDimensions], const int end[VDimensions])
    {
        unsigned int d;
        if(data != NULL) delete[] data;
        data = NULL;
        for(d = 0; d < VDimensions; d++)
        {
            this->begin[d] = begin[d];
            this->end[d] = end[d];
            int s = end[d] - begin[d];
            if(s < 0) return;
            size[d] = s + 1;
        }
        linesize = (size[0] + ((sizeof(TBlock)<<3) - 1)) / (sizeof(TBlock)<<3);
        unsigned long int allsize = 1;
        for(unsigned int i = 1; i < VDimensions; i++) allsize *= size[i];
        if(allsize > 0)
        {
            data = new TBlock[allsize*linesize];
            memset(data, 0,  allsize*linesize*sizeof(TBlock));
        }
    }
    /**
     * @brief getSize returns size of data buffer in bytes
     * @return size
     */
    unsigned long int GetDataSize(void)
    {
        if(data == NULL) return 0;
        linesize = (size[0] + ((sizeof(TBlock)<<3) - 1)) / (sizeof(TBlock)<<3);
        unsigned long int allsize = 1;
        for(int i = 1; i < VDimensions; i++) allsize *= size[i];
        return allsize*linesize*sizeof(TBlock);
    }
    /**
     * @brief Erase deletes data
     */
    void Erase(void)
    {
        if(data != NULL) delete[] data;
        data = NULL;
    }
    /**
     * @brief Clears all the region pixels
     */
    void ClrAll(void)
    {
        unsigned int allsize = 1;
        for(int i = 1; i < VDimensions; i++) allsize *= size[i];
        memset(data, 0,  allsize*linesize*sizeof(TBlock));
    }
    /**
     * @brief GetRefiPixel returns pixel at given coordinates
     * @param coords vector of coordinates in reference to image coordinates
     * @return true if pixel is set
     */
    bool GetRefiPixel(const int coords[VDimensions])
    {
        int c[VDimensions];
        for(unsigned int d = 0; d < VDimensions; d++) c[d] = coords[d] - begin[d];
        return GetPixel(c);
    }
    /**
     * @brief SetRefiPixel sets pixel at given coordinates
     * @param coords vector of coordinates in reference to image coordinates
     */
    void SetRefiPixel(const int coords[VDimensions])
    {
        int c[VDimensions];
        for(unsigned int d = 0; d < VDimensions; d++) c[d] = coords[d] - begin[d];
        SetPixel(c);
    }
    /**
     * @brief ClrRefiPixel clears pixel at given coordinates
     * @param coords vector of coordinates in reference to image coordinates
     */
    void ClrRefiPixel(const int coords[VDimensions])
    {
        int c[VDimensions];
        for(unsigned int d = 0; d < VDimensions; d++) c[d] = coords[d] - begin[d];
        ClrPixel(c);
    }
    /**
     * @brief GetPixel returns pixel at given coordinates
     * @param coords vector of the region internal coordinates
     * @return true if pixel is set
     */
    bool GetPixel(const int coords[VDimensions])
    {
        TBlock* block;
        TBlock mask;
        GetBlock(coords, &block, &mask);
        return *block & mask;
    }
    /**
     * @brief SetPixel sets pixel at given coordinates
     * @param coords vector of the region internal coordinates
     */
    void SetPixel(const int coords[VDimensions])
    {
        TBlock* block;
        TBlock mask;
        GetBlock(coords, &block, &mask);
        *block |= mask;
    }
    /**
     * @brief ClrPixel clears pixel at given coordinates
     * @param coords vector of the region internal coordinates
     */
    void ClrPixel(const int coords[VDimensions])
    {
        TBlock* block;
        TBlock mask;
        GetBlock(coords, &block, &mask);
        *block &= (~mask);
    }
    /**
     * @brief GetDataPointer returns pointer to the data
     * @return data pointer
     */
    TBlock* GetDataPointer(void)
    {
        return data;
    }
    /**
     * @brief IsEmpty
     * @return
     */
    bool IsEmpty()
    {
        return (data == NULL);
    }
    /**
     * @brief GetBlockSize returns size of a TBlock type of data
     * @return size of TBlock
     */
    unsigned int GetBlockSize(void)
    {
        return sizeof(TBlock);
    }
    /**
     * @brief GetDimensions returns number of dimensions
     * @return number of dimensions
     */
    unsigned int GetDimensions(void)
    {
        return VDimensions;
    }
    /**
     * @brief GetSize is to retrieve size of the region
     * @param size is a vector for size storage
     */
    void GetSize(unsigned int size[VDimensions])
    {
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            size[d] = this->size[d];
        }
    }
    /**
     * @brief GetBegin is to retrieve top-left(-far) corner/vertex of the region rectangle/cube with reference to the image coordinates
     * @param begin is a vector for coordinates storage
     */
    void GetBegin(int begin[VDimensions])
    {
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            begin[d] = this->begin[d];
        }
    }
    /**
     * @brief GetEnd is to retrieve bottom-right(-close) corner/vertex of the region rectangle/cube with reference to the image coordinates
     * @param end is a vector for coordinates storage
     */
    void GetEnd(int end[VDimensions])
    {
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            end[d] = this->end[d];
        }
    }
    /**
     * @brief GetColor retrieves a value coding RGB color of a region's overlay
     * @return color
     */
    unsigned int GetColor(void)
    {
        return color;
    }
    /**
     * @brief SetColor sets a value coding RGB color of a region's overlay
     * @param color
     */
    void SetColor(unsigned int color)
    {
        this->color = color;
    }
    /**
     * @brief GetVisibility
     * @return
     */
    int GetVisibility(void)
    {
        return visibility;
    }
    /**
     * @brief SetVisibility
     * @param visibility
     */
    void SetVisibility(int visibility)
    {
        this->visibility = visibility;
    }
    /**
     * @brief GetName retrieves a class name associated with the region
     * @return string with class name
     */
    std::string GetName(void)
    {
        return name;
    }
    /**
     * @brief SetName sets a class name associated with the region
     * @name string with class name
     */
    void SetName(std::string name)
    {
        this->name = name;
    }
private:
    void GetBlock(const int x[VDimensions], TBlock** block, TBlock* mask)
    {
        int p = x[VDimensions - 1];
        for(unsigned int d = VDimensions-2; d > 0; d--)
        {
            p *= size[d];
            p += x[d];
        }
        *block = data + (p*linesize) + (x[0] / (sizeof(TBlock)<<3));
        *mask = ((TBlock)1 << (x[0] & ((sizeof(TBlock)<<3) - 1)));
    }
    TBlock* data;
    unsigned int linesize;
    unsigned int size[VDimensions];
    int begin[VDimensions];
    int end[VDimensions];
    std::string name;
    unsigned int color;
    int visibility;
};

template< typename TMazdaRoi >
/**
 * @brief The MazdaRoiIterator class is a MazdaRoi iterator over all the region's rectangle or cube
 *
 * It is the fastest implemented itarator for MazdaRoi objects.
 * The iterator does not enable current coordinates access.
 *
 * Example of usage:
 * @code
 * typedef MazdaRoi<unsigned int, 3> MRType;
 * MRType* roi = new MRType(begin, end);
 * MazdaRoiIterator<MRType> iterator(roi);
 * do{
 *     iterator.SetPixel();
 *     bool isRoi = iterator.GetPixel();
 *     ++iterator;
 * }while(! iterator.IsBehind());
 * @endcode
 * @author Piotr M. Szczypinski
 */
class MazdaRoiIterator
{
public:
    typedef typename TMazdaRoi::BlockType TBlockType;
    /**
     * @brief MazdaRoiIterator constructor of this iterator
     * @param roi pointer to the TMazdaRoi object to be iterated
     */
    MazdaRoiIterator(TMazdaRoi* roi)
    {
        unsigned int size[TMazdaRoi::Dimensions];
        roi->GetSize(size);
        block = blockbegin = roi->GetDataPointer();
        mask = 1;
        x = 0;
        line = 0;
        linesnumber = 1;
        for(unsigned int d = 1; d < TMazdaRoi::Dimensions; d++)
        {
            linesnumber *= size[d];
        }
        linelength = size[0];
        finished = false;
    }
    /**
     * @brief GoToBegin resets the iterator
     */
    void GoToBegin(void)
    {
        x = 0;
        line = 0;
        block = blockbegin;
        mask = 1;
        finished = false;
    }
    /**
     * @brief GetPixel returns the current pixel
     */
    bool GetPixel(void)
    {
        return *block & mask;
    }
    /**
     * @brief SetPixel sets the current pixel
     */
    void SetPixel(void)
    {
        *block |= mask;
    }
    /**
     * @brief ClrPixel clears the current pixel
     */
    void ClrPixel(void)
    {
        *block &= (~mask);
    }
    /**
     * @brief GetBlockPointer
     * @return
     */
    TBlockType* GetBlockPointer(void)
    {
        return block;
    }
    /**
     * @brief IsBehind indicates end of iteration
     * @return true when iterator reaches the end of the regions data
     */
    bool IsBehind(void)
    {
        return finished;
    }
    /**
     * @brief operator ++ increments iterator to get to the next pixel
     */
    void operator++()
    {
        x++;
        if(x < linelength)
        {
            mask <<= 1;
            if(mask == 0)
            {
                block++;
                mask = 1;
            }
        }
        else
        {
            line++;
            x = 0;
            if(line >= linesnumber)
            {
                finished = true;
            }
            else
            {
                block++;
                mask = 1;
            }
        }
    }
private:
    TBlockType* blockbegin;
    unsigned int linesnumber;
    unsigned int line;
    unsigned int linelength;
    unsigned int x;
    TBlockType* block;
    TBlockType mask;
    bool finished;
};


template< typename TMazdaRoi >
/**
 * @brief The MazdaRoiRegionIterator class: MazdaRoi iterator over rectangular/cubic region/volume
 *
 * It enables iteration over rectangular or cubic sub-region.
 * The iterator enables access to the current internal coordinates.
 * The corresponding begin[.] coordinate must be smaller or equal to the end[.] coordinate
 *
 * Example of usage:
 * @code
 * typedef MazdaRoi<unsigned int, 3> MRType;
 * MRType* roi = new MRType(begin, end);
 * int sub_region_begin[3];
 * int sub_region_end[3];
 * sub_region_begin[0] = 2; sub_region_end[0] = 17;
 * sub_region_begin[1] = 2; sub_region_end[1] = 20;
 * sub_region_begin[2] = 1; sub_region_end[2] = 9;
 * MazdaRoiRegionIterator<MRType> iterator(roi, sub_region_begin, sub_region_end);
 * do{
 *     iterator.ClrPixel();
 *     bool isRoi = iterator.GetPixel();
 *     ++iterator;
 * }while(! iterator.IsBehind());
 * @endcode
 * @author Piotr M. Szczypinski
 */
class MazdaRoiRegionIterator
{
public:
    typedef typename TMazdaRoi::BlockType TBlockType;
    /**
     * @brief MazdaRoiRegionIterator constructor of this iterator
     * @param roi pointer to the TMazdaRoi object to be iterated
     * @param begin subregion's top-left... coordinates
     * @param end subregion's bottom-right... coordinates
     * @param refi if true coordinates are refering to the image, if false they are region's internal coordinates
     */
    MazdaRoiRegionIterator(TMazdaRoi* roi, const int begin[TMazdaRoi::Dimensions], const int end[TMazdaRoi::Dimensions], const bool refi = true)
    {
        unsigned int size[TMazdaRoi::Dimensions];
        //int this->begin[TMazdaRoi::Dimensions];
        roi->GetSize(size);
        unsigned int d = 0;
        if(refi)
        {
            int roibegin[TMazdaRoi::Dimensions];
            int roiend[TMazdaRoi::Dimensions];
            roi->GetBegin(roibegin);
            roi->GetEnd(roiend);
            for(d = 0; d < TMazdaRoi::Dimensions; d++)
            {
                this->begin[d] = begin[d] - roibegin[d];
                this->end[d] = end[d] - roibegin[d];
            }
        }
        else
        {
            for(d = 0; d < TMazdaRoi::Dimensions; d++)
            {
                this->begin[d] = begin[d];
                this->end[d] = end[d];
            }
        }
        startblock = roi->GetDataPointer() +  (this->begin[0] / (sizeof(TBlockType)<<3));
        startmask = ((TBlockType)1) << (this->begin[0] % (sizeof(TBlockType)<<3));
        size_t linesize = (size[0] + ((sizeof(TBlockType)<<3) - 1)) / (sizeof(TBlockType)<<3);
        line[0] = (this->end[0] / (sizeof(TBlockType)<<3)) - (this->begin[0] / (sizeof(TBlockType)<<3));
        step[0] = (end[0] > begin[0] ? 1 : 0);
        this->end[0] -= this->begin[0];
        for(d = 1; d < TMazdaRoi::Dimensions; d++)
        {
            this->x[d] = 0;
            this->step[d] = linesize;
            this->line[d] = linesize * (this->end[d] - this->begin[d]);
            startblock += (this->begin[d]*linesize);
            linesize *= size[d];
            this->end[d] -= this->begin[d];
        }
        GoToBegin();
    }
    /**
     * @brief GoToBegin resets the iterator
     */
    void GoToBegin(void)
    {
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            x[d] = 0;
        }
        block = startblock;
        mask = startmask;
        finished = false;
    }
    /**
     * @brief GetPixel returns the current pixel
     * @return pixel
     */
    bool GetPixel(void)
    {
        return *block & mask;
    }
    /**
     * @brief SetPixel sets the current pixel
     */
    void SetPixel(void)
    {
        *block |= mask;
    }
    /**
     * @brief ClrPixel clears the current pixel
     */
    void ClrPixel(void)
    {
        *block &= (~mask);
    }
    /**
     * @brief IsBehind indicates end of iteration process
     * @return true when iterator reaches the end of the regions data
     */
    bool IsBehind(void)
    {
        return finished;
    }
    /**
     * @brief GetIndex returnes the coordinate of current pixel
     * @param dir coordinate index x-0, y-1, z-2 ...
     * @return
     */
    unsigned int GetIndex(const unsigned int dir)
    {
        return x[dir];
    }

    unsigned int GetIndexFromRoiBegin(const unsigned int dir)
    {
        return x[dir]+begin[dir];
    }
    /**
     * @brief GetBlockPointer
     * @return
     */
    TBlockType* GetBlockPointer(void)
    {
        return block;
    }
    /**
     * @brief operator ++ increments iterator to get to the next pixel
     */
    void operator++()
    {
        x[0]++;
        if(x[0] <= end[0])
        {
            mask <<= 1;
            if(mask == 0)
            {
                block++;
                mask = 1;
            }
            return;
        }
        else
        {
            x[0] = 0;
            block -= line[0];
            mask = startmask;
        }
        for(unsigned int d = 1; d < TMazdaRoi::Dimensions; d++)
        {
            x[d]++;
            if(x[d] <= end[d])
            {
                block += step[d];
                return;
            }
            else
            {
                x[d] = 0;
                block -= line[d];
            }
        }
        finished = true;
    }
private:
    size_t step[TMazdaRoi::Dimensions];
    size_t line[TMazdaRoi::Dimensions];
    unsigned int x[TMazdaRoi::Dimensions];
    unsigned int begin[TMazdaRoi::Dimensions];
    unsigned int end[TMazdaRoi::Dimensions];
    TBlockType* startblock;
    TBlockType startmask;
    TBlockType* block;
    TBlockType mask;
    bool finished;
};


template< typename TMazdaRoi >
/**
 * @brief The MazdaRoiRegionReversiveIterator class: MazdaRoi iterator over rectangular/cubic region/volume
 *
 * It enables iteration over rectangular or cubic sub-region.
 * The iterator does not enable access to the current internal coordinates.
 * The corresponding begin[.] coordinate can be smaller, equal or higher than the end[.] coordinate
 *
 * Example of usage:
 * @code
 * typedef MazdaRoi<unsigned int, 3> MRType;
 * MRType* roi = new MRType(begin, end);
 * int sub_region_begin[3];
 * int sub_region_end[3];
 * sub_region_begin[0] = 17; sub_region_end[0] = 1;
 * sub_region_begin[1] = 20; sub_region_end[1] = 3;
 * sub_region_begin[2] = 1; sub_region_end[2] = 9;
 * MazdaRoiRegionReversiveIterator<MRType> iterator(roi, sub_region_begin, sub_region_end);
 * do{
 *     iterator.ClrPixel();
 *     bool isRoi = iterator.GetPixel();
 *     ++iterator;
 * }while(! iterator.IsBehind());
 * @endcode
 * @author Piotr M. Szczypinski
 */
class MazdaRoiRegionReversiveIterator
{
public:
    typedef typename TMazdaRoi::BlockType TBlockType;
    /**
     * @brief MazdaRoiRegionReversiveIterator constructor of this iterator
     * @param roi pointer to the TMazdaRoi object to be iterated
     * @param begin subregion's top-left... coordinates
     * @param end subregion's bottom-right... coordinates
     * @param refi if true coordinates are refering to the image, if false they are region's internal coordinates
     */
    MazdaRoiRegionReversiveIterator(TMazdaRoi* roi, const int begin[TMazdaRoi::Dimensions], const int end[TMazdaRoi::Dimensions], const bool refi = true)
    {
        unsigned int size[TMazdaRoi::Dimensions];
        int this_begin[TMazdaRoi::Dimensions];
        roi->GetSize(size);
        unsigned int d = 0;
        if(refi)
        {
            int roibegin[TMazdaRoi::Dimensions];
            int roiend[TMazdaRoi::Dimensions];
            roi->GetBegin(roibegin);
            roi->GetEnd(roiend);
            for(d = 0; d < TMazdaRoi::Dimensions; d++)
            {
                this_begin[d] = begin[d] - roibegin[d];
                this->end[d] = end[d] - roibegin[d];
            }
        }
        else
        {
            for(d = 0; d < TMazdaRoi::Dimensions; d++)
            {
                this_begin[d] = begin[d];
                this->end[d] = end[d];
            }
        }
        startblock = roi->GetDataPointer() +  (this_begin[0] / (sizeof(TBlockType)<<3));
        startmask = ((TBlockType)1) << (this_begin[0] % (sizeof(TBlockType)<<3));
        long int linesize = (size[0] + ((sizeof(TBlockType)<<3) - 1)) / (sizeof(TBlockType)<<3);
        line[0] = (this->end[0] / (sizeof(TBlockType)<<3)) - (this_begin[0] / (sizeof(TBlockType)<<3));
        step[0] = (end[0] >= begin[0] ? 1 : 0);
        this->end[0] -= this_begin[0];
        this->end[0] = abs(this->end[0]);
        for(d = 1; d < TMazdaRoi::Dimensions; d++)
        {
            this->x[d] = 0;
            if(this->end[d] < this_begin[d]) this->step[d] = -linesize;
            else this->step[d] = linesize;
            this->line[d] = linesize * ((int)this->end[d] - (int)this_begin[d]);
            startblock += (this_begin[d]*linesize);
            linesize *= size[d];
            this->end[d] -= this_begin[d];
            this->end[d] = abs(this->end[d]);
        }
        GoToBegin();
    }
    /**
     * @brief GoToBegin resets the iterator
     */
    void GoToBegin(void)
    {
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            x[d] = 0;
        }
        block = startblock;
        mask = startmask;
        finished = false;
    }
    /**
     * @brief GetPixel returns the current pixel
     */
    bool GetPixel(void)
    {
        return *block & mask;
    }
    /**
     * @brief SetPixel sets the current pixel
     */
    void SetPixel(void)
    {
        *block |= mask;
    }
    /**
     * @brief ClrPixel clears the current pixel
     */
    void ClrPixel(void)
    {
        *block &= (~mask);
    }
    /**
     * @brief GetBlockPointer
     * @return
     */
    TBlockType* GetBlockPointer(void)
    {
        return block;
    }
    /**
     * @brief IsBehind indicates end of iteration process
     * @return true when iterator reaches the end of the regions data
     */
    bool IsBehind(void)
    {
        return finished;
    }
    /**
     * @brief operator ++ increments iterator to get to the next pixel
     */
    void operator++()
    {
        x[0]++;
        if(x[0] <= end[0])
        {
            if(step[0])
            {
                mask <<= 1;
                if(mask == 0)
                {
                    block++;
                    mask = 1;
                }
            }
            else
            {
                mask >>= 1;
                if(mask == 0)
                {
                    block--;
                    mask = ((TBlockType)1) << (sizeof(TBlockType)*8-1);
                }
            }
            return;
        }
        else
        {
            x[0] = 0;
            block -= line[0];
            mask = startmask;
        }
        for(unsigned int d = 1; d < TMazdaRoi::Dimensions; d++)
        {
            x[d]++;
            if(x[d] <= end[d])
            {
                block += step[d];
                return;
            }
            else
            {
                x[d] = 0;
                block -= line[d];
            }
        }
        finished = true;
    }
private:
    long int step[TMazdaRoi::Dimensions];
    long int line[TMazdaRoi::Dimensions];
    int x[TMazdaRoi::Dimensions];
    int end[TMazdaRoi::Dimensions];
    TBlockType* startblock;
    TBlockType startmask;
    TBlockType* block;
    TBlockType mask;
    bool finished;
};



template< typename TMazdaRoi >
/**
 * @brief The MazdaRoiRegionFlippedIterator class: MazdaRoi iterator over rectangular/cubic region/volume
 *
 * It enables iteration over rectangular or cubic sub-region in arbitrary chosen direction.
 * The corresponding begin[.] coordinate can be smaller, equal or higher than the end[.] coordinate
 * It enables looping in arbitrary order of directions.
 * The iterator is used if the image is rotated and must be iterated with respect to its current orientation.
 *
 * Example of usage:
 * @code
 * typedef MazdaRoi<unsigned int, 3> MRType;
 * MRType* roi = new MRType(begin, end);
 * int sub_region_begin[3];
 * int sub_region_end[3];
 * sub_region_begin[0] = 2; sub_region_end[0] = 17;
 * sub_region_begin[1] = 20; sub_region_end[1] = 2;
 * sub_region_begin[2] = 1; sub_region_end[2] = 9;
 * unsigned int looping_order[3]
 * looping_order[0] = 2; looping_order[1] = 0; looping_order[2] = 1;
 * MazdaRoiRegionIterator<MRType> iterator(roi, sub_region_begin, sub_region_end, looping_order);
 * do{
 *     iterator.ClrPixel();
 *     bool isRoi = iterator.GetPixel();
 *     ++iterator;
 * }while(! iterator.IsBehind());
 * @endcode
 * @author Piotr M. Szczypinski
 */
class MazdaRoiRegionFlippedIterator
{
public:
    typedef typename TMazdaRoi::BlockType TBlockType;
    /**
     * @brief MazdaRoiRegionFlippedIterator constructor of this iterator
     * @param roi pointer to the TMazdaRoi object to be iterated
     * @param begin subregion's top-left... coordinates
     * @param end subregion's bottom-right... coordinates
     * @param direction
     * @param refi if true coordinates are refering to the image, if false they are region's internal coordinates
     */
    MazdaRoiRegionFlippedIterator(TMazdaRoi* roi, const int begin[TMazdaRoi::Dimensions], const int end[TMazdaRoi::Dimensions], const unsigned int direction[TMazdaRoi::Dimensions], const bool refi = true)
    {
        unsigned int size[TMazdaRoi::Dimensions];
        int this_begin[TMazdaRoi::Dimensions];
        roi->GetSize(size);
        unsigned int d = 0;
        if(refi)
        {
            int roibegin[TMazdaRoi::Dimensions];
            int roiend[TMazdaRoi::Dimensions];
            roi->GetBegin(roibegin);
            roi->GetEnd(roiend);
            for(d = 0; d < TMazdaRoi::Dimensions; d++)
            {
                this_begin[d] = begin[d] - roibegin[d];
                this->end[d] = end[d] - roibegin[d];
            }
        }
        else
        {
            for(d = 0; d < TMazdaRoi::Dimensions; d++)
            {
                this_begin[d] = begin[d];
                this->end[d] = end[d];
            }
        }
        startblock = roi->GetDataPointer() +  (this_begin[0] / (sizeof(TBlockType)<<3));
        startmask = ((TBlockType)1) << (this_begin[0] % (sizeof(TBlockType)<<3));
        long int linesize = (size[0] + ((sizeof(TBlockType)<<3) - 1)) / (sizeof(TBlockType)<<3);
        line[0] = (this->end[0] / (sizeof(TBlockType)<<3)) - (this_begin[0] / (sizeof(TBlockType)<<3));
        step[0] = (end[0] >= begin[0] ? 1 : 0);
        this->end[0] -= this_begin[0];
        this->end[0] = abs(this->end[0]);
        this->direction[0] = direction[0];
        for(d = 1; d < TMazdaRoi::Dimensions; d++)
        {
            this->x[d] = 0;
            this->direction[d] = direction[d];
            if(this->end[d] < this_begin[d]) this->step[d] = -linesize;
            else this->step[d] = linesize;
            this->line[d] = linesize * ((int)this->end[d] - (int)this_begin[d]);
            startblock += (this_begin[d]*linesize);
            linesize *= size[d];
            this->end[d] -= this_begin[d];
            this->end[d] = abs(this->end[d]);
        }
        GoToBegin();
    }
    /**
     * @brief GoToBegin resets the iterator
     */
    void GoToBegin(void)
    {
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            x[d] = 0;
        }
        block = startblock;
        mask = startmask;
        finished = false;
    }
    /**
     * @brief GetPixel returns the current pixel
     */
    bool GetPixel(void)
    {
        return *block & mask;
    }
    /**
     * @brief SetPixel sets the current pixel
     */
    void SetPixel(void)
    {
        *block |= mask;
    }
    /**
     * @brief ClrPixel clears the current pixel
     */
    void ClrPixel(void)
    {
        *block &= (~mask);
    }
    /**
     * @brief GetBlockPointer
     * @return
     */
    TBlockType* GetBlockPointer(void)
    {
        return block;
    }
    /**
     * @brief IsBehind indicates end of iteration process
     * @return true when iterator reaches the end of the regions data
     */
    bool IsBehind(void)
    {
        return finished;
    }
    /**
     * @brief operator ++ increments iterator to get to the next pixel
     */
    void operator++()
    {
        unsigned int dd, d;
        for(dd = 0; dd < TMazdaRoi::Dimensions; dd++)
        {
            d = direction[dd];

            if(d == 0)
            {
                x[0]++;
                if(x[0] <= end[0])
                {
                    if(step[0])
                    {
                        mask <<= 1;
                        if(mask == 0)
                        {
                            block++;
                            mask = 1;
                        }
                    }
                    else
                    {
                        mask >>= 1;
                        if(mask == 0)
                        {
                            block--;
                            mask = ((TBlockType)1) << (sizeof(TBlockType)*8-1);
                        }
                    }
                    return;
                }
                else
                {
                    x[0] = 0;
                    block -= line[0];
                    mask = startmask;
                }
            }
            else
            {
                x[d]++;
                if(x[d] <= end[d])
                {
                    block += step[d];
                    return;
                }
                else
                {
                    x[d] = 0;
                    block -= line[d];
                }
            }
        }
        finished = true;
    }
private:
    unsigned int direction[TMazdaRoi::Dimensions];
    long int step[TMazdaRoi::Dimensions];
    long int line[TMazdaRoi::Dimensions];
    int x[TMazdaRoi::Dimensions];
    int end[TMazdaRoi::Dimensions];
    TBlockType* startblock;
    TBlockType startmask;
    TBlockType* block;
    TBlockType mask;
    bool finished;
};


template< typename TBlock >
/**
 * @brief The RoiAddOperation class defines function to draw in the roi data and is used in MazdaRoiRegionCopier class template
 */
class RoiAddOperation
{
public:
    inline static void copy(TBlock* d, const TBlock s)
    {
        *d |= s;
    }
};

template< typename TBlock >
/**
 * @brief The RoiAddOperation class defines function to erase the roi data and is used in MazdaRoiRegionCopier class template
 */
class RoiRemoveOperation
{
public:
    inline static void copy(TBlock* d, const TBlock s)
    {
        d &= (~s);
    }
};


template< typename TMazdaRoi, typename TCopy >
/**
 * @brief The MazdaRoiRegionCopier class defines helper functions to copy regions or to copy region fragments between TMazdaRoi objects.
 *
 * Example of usage:
 * @code
 * typedef MazdaRoi<unsigned int, 3> MRType;
 * MRType* src = new MRType(src_begin, src_end);
 * MRType* dst = new MRType(dst_begin, dst_end);
 * MazdaRoiRegionCopier< MRType, RoiAddOperation<MRType::BlockType> > copier;
 * copier.CopyRefi(roi, dest);
 * @endcode
 * @author Piotr M. Szczypinski
 */
class MazdaRoiRegionCopier
{
public:
    typedef typename TMazdaRoi::BlockType TBlockType;

    TMazdaRoi* Copy(TMazdaRoi* source)
    {
        int begin[TMazdaRoi::Dimensions];
        int end[TMazdaRoi::Dimensions];
        TMazdaRoi* destination = NULL;
        if(source == NULL) return destination;
        unsigned long int alls = source->GetDataSize();
        if(alls > 0)
        {
            source->GetBegin(begin);
            source->GetEnd(end);
            destination = new TMazdaRoi(begin, end);
            memcpy(destination->GetDataPointer(), source->GetDataPointer(), alls);
        }
        else destination = new TMazdaRoi();
        destination->SetName(source->GetName());
        destination->SetColor(source->GetColor());
        destination->SetVisibility(source->GetVisibility());

        return destination;
    }
    /**
     * @brief Copy copies the subregion defined by begin coordinates (region internal) and blocksize, from source to destination region.
     * @param source region
     * @param sbegin source region top-left coordinate to copy
     * @param destination region
     * @param dbegin destination region top-left coordinate to copy
     * @param blocksize size of a block (sub-region) to copy
     */
    void Copy(TMazdaRoi* source, const int sbegin[TMazdaRoi::Dimensions], TMazdaRoi* destination, const int dbegin[TMazdaRoi::Dimensions], const unsigned int blocksize[TMazdaRoi::Dimensions])
    {
        unsigned int d;
        unsigned int x[TMazdaRoi::Dimensions];
        unsigned int dsize[TMazdaRoi::Dimensions];
        unsigned int ssize[TMazdaRoi::Dimensions];
        destination->GetSize(dsize);
        source->GetSize(ssize);
        size_t dlinesize = (dsize[0] + ((sizeof(TBlockType)<<3) - 1)) / (sizeof(TBlockType)<<3);
        size_t slinesize = (ssize[0] + ((sizeof(TBlockType)<<3) - 1)) / (sizeof(TBlockType)<<3);
        size_t dstep[TMazdaRoi::Dimensions];
        size_t dline[TMazdaRoi::Dimensions];
        size_t sstep[TMazdaRoi::Dimensions];
        size_t sline[TMazdaRoi::Dimensions];
        dline[0] = ((blocksize[0]+dbegin[0]-1) / (sizeof(TBlockType)<<3)) - (dbegin[0] / (sizeof(TBlockType)<<3));
        dstep[0] = 0;
        sline[0] = ((blocksize[0]+sbegin[0]-1) / (sizeof(TBlockType)<<3)) - (sbegin[0] / (sizeof(TBlockType)<<3));
        sstep[0] = 0;
        x[0] = 0;
        TBlockType* dblock = destination->GetDataPointer() +  (dbegin[0] / (sizeof(TBlockType)<<3));
        TBlockType* sblock = source->GetDataPointer() +  (sbegin[0] / (sizeof(TBlockType)<<3));
        unsigned int srcs = sbegin[0] % (sizeof(TBlockType)<<3);
        unsigned int dsts = dbegin[0] % (sizeof(TBlockType)<<3);
        unsigned int count = blocksize[0];
        for(d = 1; d < TMazdaRoi::Dimensions; d++)
        {
            x[d] = 0;
            sstep[d] = slinesize;
            sline[d] = slinesize * (blocksize[d]-1);
            dstep[d] = dlinesize;
            dline[d] = dlinesize * (blocksize[d]-1);
            dblock += (dbegin[d]*dlinesize);
            sblock += (sbegin[d]*slinesize);
            dlinesize *= dsize[d];
            slinesize *= ssize[d];
        }
        do
        {
            CopyShiftBitsBlockLittleEndian(sblock, srcs, dblock, dsts, count);
            for(d = 1; d < TMazdaRoi::Dimensions; d++)
            {
                x[d]++;
                if(x[d] < blocksize[d])
                {
                    dblock += dstep[d];
                    sblock += sstep[d];
                    break;
                }
                else
                {
                    x[d] = 0;
                    dblock -= dline[d];
                    sblock -= sline[d];
                }
            }
        }while(d < TMazdaRoi::Dimensions);
        destination->SetName(source->GetName());
        destination->SetColor(source->GetColor());
        destination->SetVisibility(source->GetVisibility());
    }
    /**
     * @brief CopyRefi copies the subregion defined by begin coordinates (image referenced) and blocksize, from source to destination region.
     * @param source region
     * @param destination region
     */
    void CopyRefi(TMazdaRoi* source, TMazdaRoi* destination, int begin[TMazdaRoi::Dimensions], unsigned int blocksize[TMazdaRoi::Dimensions])
    {
        int sbegin[TMazdaRoi::Dimensions];
        int dbegin[TMazdaRoi::Dimensions];
        source->GetBegin(sbegin);
        destination->GetBegin(dbegin);
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            sbegin[d] = begin[d]-sbegin[d];
            dbegin[d] = begin[d]-dbegin[d];
        }
        Copy(source, sbegin, destination, dbegin, blocksize);
    }
    /**
     * @brief CopyRefi copies the common subregion from source to destination region
     * @param source region
     * @param destination region
     */
    void CopyRefi(TMazdaRoi* source, TMazdaRoi* destination)
    {
        int sbegin[TMazdaRoi::Dimensions];
        int dbegin[TMazdaRoi::Dimensions];
        source->GetBegin(sbegin);
        destination->GetBegin(dbegin);
        int send[TMazdaRoi::Dimensions];
        int dend[TMazdaRoi::Dimensions];
        source->GetEnd(send);
        destination->GetEnd(dend);
        int min;
        unsigned int blocksize[TMazdaRoi::Dimensions];
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            sbegin[d] = sbegin[d] < dbegin[d] ? dbegin[d] : sbegin[d];
            min = send[d] < dend[d] ? send[d] : dend[d];
            if(min < sbegin[d]) return;
            else blocksize[d] = min - sbegin[d] + 1;
        }
        CopyRefi(source, destination, sbegin, blocksize);
    }
private:
    void CopyShiftBitsBlockLittleEndian(TBlockType* src, unsigned int srcs, TBlockType* dst, unsigned int dsts, unsigned int count)
    {
        int inputblocks = (count + srcs + (sizeof(TBlockType)<<3) - 1)/(sizeof(TBlockType)<<3);
        int shiftleft = (int)srcs - (int)dsts;
        int tail =  (- count - srcs) % (sizeof(TBlockType)<<3);
        int shiftright;
        TBlockType* srctemp = src;
        TBlockType* dsttemp = dst;
        TBlockType firstlast;

        if(inputblocks == 1)
        {
            firstlast = (*srctemp << tail);
            firstlast = (firstlast >> tail);
            firstlast = ((firstlast >> srcs) << srcs);

            if(shiftleft > 0)
            {
                if(firstlast) TCopy::copy(dsttemp, (firstlast >> shiftleft));
            }
            else if(shiftleft < 0)
            {
                shiftright = -shiftleft;
                shiftleft = sizeof(TBlockType)*8 - shiftright;
                TCopy::copy(dsttemp, (firstlast << shiftright));
                dsttemp++;
                firstlast >>= shiftleft;
                if(firstlast) TCopy::copy(dsttemp, firstlast);
            }
            else
            {
                if(firstlast) TCopy::copy(dsttemp, firstlast);
            }
        }
        else
        {
            if(shiftleft > 0)
            {
                shiftright = (sizeof(TBlockType)<<3) - shiftleft;
                inputblocks--;
                TCopy::copy(dsttemp, (((*srctemp >> srcs) << srcs) >> shiftleft));
                srctemp++;
                for(int xblocki = 1; xblocki < inputblocks; xblocki++)
                {
                    TCopy::copy(dsttemp, (*srctemp << shiftright));
                    dsttemp++;
                    TCopy::copy(dsttemp, (*srctemp >> shiftleft));
                    srctemp++;
                }
                firstlast = (*srctemp << tail);
                firstlast = (firstlast >> tail);
                TCopy::copy(dsttemp, (firstlast << shiftright));
                dsttemp++;
                firstlast = (firstlast >> shiftleft);
                if(firstlast) TCopy::copy(dsttemp, firstlast);
            }
            else if(shiftleft < 0)
            {
                shiftright = -shiftleft;
                shiftleft = sizeof(TBlockType)*8 - shiftright;
                inputblocks--;
                firstlast = ((*srctemp >> srcs) << srcs);
                TCopy::copy(dsttemp, (firstlast << shiftright));
                dsttemp++;
                TCopy::copy(dsttemp, (firstlast >> shiftleft));
                srctemp++;
                for(int xblocki = 1; xblocki < inputblocks; xblocki++)
                {
                    TCopy::copy(dsttemp, (*srctemp << shiftright));
                    dsttemp++;
                    TCopy::copy(dsttemp, (*srctemp >> shiftleft));
                    srctemp++;
                }
                firstlast = (*srctemp << tail);
                firstlast = (firstlast >> tail);
                TCopy::copy(dsttemp, (firstlast << shiftright));
                dsttemp++;
                firstlast >>= shiftleft;
                if(firstlast) TCopy::copy(dsttemp, firstlast);
            }
            else
            {
                inputblocks--;
                TCopy::copy(dsttemp, ((*srctemp >> srcs) << srcs));
                srctemp++;
                for(int xblocki = 1; xblocki < inputblocks; xblocki++)
                {
                    dsttemp++;
                    TCopy::copy(dsttemp, (*srctemp));
                    srctemp++;
                }
                dsttemp++;
                firstlast = (*srctemp << tail);
                firstlast = (firstlast >> tail);
                if(firstlast) TCopy::copy(dsttemp, firstlast);
            }
        }
    }
};

template< typename TMazdaRoi>
/**
 * @brief The MazdaRoiResizer class defines helper functions to resize region data.
 *
 * Example of upsizing region to be stored in a file:
 * @code
 * MazdaRoiResizer< MRType > resizer;
 * MRType* forStorage = resizer.ImageReference(roi);
 * @endcode
 *
 * Example of cropping region to allocate less memory:
 * @code
 * MazdaRoiResizer< MRType > resizer;
 * MRType* cropped = resizer.Crop(roi);
 * @endcode
 * @author Piotr M. Szczypinski
 */
class MazdaRoiResizer
{
public:
    typedef typename TMazdaRoi::BlockType TBlockType;
    /**
     * @brief GetBounds get real bounds of region
     * @param roi region
     * @param begin vector for top-left coordinates storage
     * @param end vector for bottom-right coordinates storage
     */
    static void GetBounds(TMazdaRoi* roi, unsigned int begin[TMazdaRoi::Dimensions], unsigned int end[TMazdaRoi::Dimensions])
    {
        unsigned int d;
        unsigned int x[TMazdaRoi::Dimensions];
        unsigned int size[TMazdaRoi::Dimensions];
        roi->GetSize(size);
        for(d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            x[d] = 0;
            begin[d] = size[d];
            end[d] = 0;
        }
        MazdaRoiIterator<TMazdaRoi> iterator(roi);
        do
        {
            if(iterator.GetPixel())
            {
                for(d = 0; d < TMazdaRoi::Dimensions; d++)
                {
                    if(begin[d] > x[d]) begin[d] = x[d];
                    if(end[d] < x[d]) end[d] = x[d];
                }
            }
            ++iterator;
            for(d = 0; d < TMazdaRoi::Dimensions; d++)
            {
                x[d]++;
                if(x[d] < size[d])
                {

                    break;
                }
                else
                {
                    x[d] = 0;
                }
            }
        }while(d < TMazdaRoi::Dimensions);
    }
    /**
     * @brief GetBoundsRefi get real bounds of region, image referenced coordinates.
     * @param roi region
     * @param begin vector for top-left coordinates storage
     * @param end vector for bottom-right coordinates storage
     */
    static void GetBoundsRefi(TMazdaRoi* roi, int begin[TMazdaRoi::Dimensions], int end[TMazdaRoi::Dimensions])
    {
        unsigned int b[TMazdaRoi::Dimensions];
        unsigned int e[TMazdaRoi::Dimensions];
        int rb[TMazdaRoi::Dimensions];
        GetBounds(roi, b, e);
        roi->GetBegin(rb);
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            begin[d] = (int) b[d] + rb[d];
            end[d] = (int) e[d] + rb[d];
        }
    }
    /**
     * @brief Crop crops region around real bounds
     * @param roi input region to crop
     * @return resulting cropped region object pointer
     */
    static TMazdaRoi* Crop(TMazdaRoi* roi)
    {
        int rb[TMazdaRoi::Dimensions];
        int re[TMazdaRoi::Dimensions];
        TMazdaRoi* dest;
        GetBoundsRefi(roi, rb, re);
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            if(rb[d] > re[d])
            {
                dest = new TMazdaRoi();
                dest->SetName(roi->GetName());
                dest->SetColor(roi->GetColor());
                dest->SetVisibility(roi->GetVisibility());
            }
        }
        dest = new TMazdaRoi(rb, re);
        dest->SetName(roi->GetName());
        dest->SetColor(roi->GetColor());
        dest->SetVisibility(roi->GetVisibility());
        MazdaRoiRegionCopier< TMazdaRoi, RoiAddOperation<TBlockType> > copier;
        copier.CopyRefi(roi, dest);
        return dest;
    }
    /**
     * @brief Upsize upsize roi data to correspond to image size
     * @param roi input region
     * @param imagesize size of image
     * @return resulting region object pointer
     */
    static TMazdaRoi* Upsize(TMazdaRoi* roi, const unsigned int imagesize[TMazdaRoi::Dimensions])
    {
        int rb[TMazdaRoi::Dimensions];
        int re[TMazdaRoi::Dimensions];
        //GetBoundsRefi(roi, rb, re);
//        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
//        {
//            if(rb[d] < 0) rb[d] = 0;
//            if(re[d] >= (int)imagesize[d]) re[d] = imagesize[d];
//        }
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            rb[d] = 0;
            re[d] = imagesize[d] - 1;
        }
        TMazdaRoi* dest = new TMazdaRoi(rb, re);
        MazdaRoiRegionCopier< TMazdaRoi, RoiAddOperation<TBlockType> > copier;
        copier.CopyRefi(roi, dest);
        return dest;
    }
    /**
     * @brief Upsize upsize region to start at zero coordinates
     * @param roi input region
     * @return resulting region object pointer
     */
    static TMazdaRoi* Upsize(TMazdaRoi* roi)
    {
        int sbegin[TMazdaRoi::Dimensions];
        int roiend[TMazdaRoi::Dimensions];
        roi->GetEnd(roiend);
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            sbegin[d] = 0;
        }
        TMazdaRoi* dest = new TMazdaRoi(sbegin, roiend);
        MazdaRoiRegionCopier< TMazdaRoi, RoiAddOperation<TBlockType> > copier;
        copier.CopyRefi(roi, dest);
        return dest;
    }
    /**
     * @brief ImageReference resize image to be stored to a file
     * @param roi input region
     * @return resulting region object pointer or NULL if out of range
     */
    static TMazdaRoi* ImageReference(TMazdaRoi* roi)
    {
        int rb[TMazdaRoi::Dimensions];
        int re[TMazdaRoi::Dimensions];
        GetBoundsRefi(roi, rb, re);
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            rb[d] = 0;
            if(rb[d] > re[d]) return NULL;
        }
        TMazdaRoi* dest = new TMazdaRoi(rb, re);
        MazdaRoiRegionCopier< TMazdaRoi, RoiAddOperation<TBlockType> > copier;
        copier.CopyRefi(roi, dest);
        return dest;
    }
    /**
     * @brief Resize resizes region with respect to begin and end bounds
     * @param roi input region
     * @param begin vector
     * @param end vector
     * @return resulting region object pointer
     */
    static TMazdaRoi* Resize(TMazdaRoi* roi, const int begin[TMazdaRoi::Dimensions], const int end[TMazdaRoi::Dimensions])
    {
        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
        {
            if(begin[d] > end[d]) return NULL;
        }
        TMazdaRoi* dest = new TMazdaRoi(begin, end);
        MazdaRoiRegionCopier< TMazdaRoi, RoiAddOperation<TBlockType> > copier;
        copier.CopyRefi(roi, dest);
        return dest;
    }
};

#endif // MAZDA_ROI_H
