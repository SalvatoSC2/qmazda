//#include "mazdaimage.h"
//#include "mazdaroi.h"
//#include "mazdaimageio.h"
//#include "mazdaroiio.h"
//#include <stdio.h>
//#include <stdlib.h>
//#include <ctime>

typedef unsigned short int MazdaImagePixelType;
typedef float MazdaMapPixelType;
typedef unsigned int MazdaRoiBlockType;



//void test_image_container_and_iterators(void)
//{
//    unsigned int size[3];
//    double spacing[3];
//    size[0] = 13;
//    size[1] = 11;
//    size[2] = 4;

//    int begin[3];
//    int end[3];
//    int x[3];

//    begin[0] = 2; end[0] = 17;
//    begin[1] = 2; end[1] = 20;
//    begin[2] = 1; end[2] = 9;



//    unsigned int dir[3];


//    typedef MazdaImage<int, 3> MIType;

//    MIType* obraz = new MIType(size, spacing, true);
//    printf("Setting data.\n"); fflush(stdout);
//    MazdaImageIterator<MIType> iterator_linear(obraz);
//    for(x[2] = 0; x[2] < size[2]; x[2]++)
//        for(x[1] = 0; x[1] < size[1]; x[1]++)
//            for(x[0] = 0; x[0] < size[0]; x[0]++)
//            {
//                iterator_linear.SetPixel(x[2]*10000+x[1]*100+x[0]);
//                ++iterator_linear;
//            }


//    printf("Printing data forward:\n"); fflush(stdout);
//    iterator_linear.GoToBegin();
//    for(x[2] = 0; x[2] < size[2]; x[2]++)
//    {
//        for(x[1] = 0; x[1] < size[1]; x[1]++)
//        {
//            for(x[0] = 0; x[0] < size[0]; x[0]++)
//            {
//                printf("%.6i ", iterator_linear.GetPixel());
//                ++iterator_linear;
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }


//    printf("Printing data reverse:\n"); fflush(stdout);
//    iterator_linear.GoToEnd();
//    for(x[2] = 0; x[2] < size[2]; x[2]++)
//    {
//        for(x[1] = 0; x[1] < size[1]; x[1]++)
//        {
//            for(x[0] = 0; x[0] < size[0]; x[0]++)
//            {
//                printf("%.6i ", iterator_linear.GetPixel());
//                --iterator_linear;
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }




//    unsigned int beginu[3];
//    unsigned int endu[3];

//    printf("Printing data region:\n"); fflush(stdout);
//    beginu[0] = 2; endu[0] = 9;
//    beginu[1] = 2; endu[1] = 4;
//    beginu[2] = 1; endu[2] = 2;
//    MazdaImageRegionIterator<MIType> iterator_region(obraz, beginu, endu);
//    iterator_linear.GoToEnd();
//    for(x[2] = beginu[2]; x[2] <= endu[2]; x[2]++)
//    {
//        for(x[1] = beginu[1]; x[1] <= endu[1]; x[1]++)
//        {
//            for(x[0] = beginu[0]; x[0] <= endu[0]; x[0]++)
//            {
//                printf("%.6i ", iterator_region.GetPixel());
//                ++iterator_region;
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }

//    printf("Printing data region two ways:\n"); fflush(stdout);
//    beginu[0] = 9; endu[0] = 2;
//    beginu[1] = 2; endu[1] = 4;
//    beginu[2] = 3; endu[2] = 0;

//    MazdaImageRegionReversiveIterator<MIType> iterator_reversive(obraz, beginu, endu);
//    for(x[2] = 0; x[2] <= abs(endu[2]-beginu[2]); x[2]++)
//    {
//        for(x[1] = 0; x[1] <= abs(endu[1]-beginu[1]); x[1]++)
//        {
//            for(x[0] = 0; x[0] <= abs(endu[0]-beginu[0]); x[0]++)
//            {
//                if(iterator_reversive.IsBehind()) printf("Error: Behind \n");;
//                printf("%.6i ", iterator_reversive.GetPixel());
//                ++iterator_reversive;
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }
//    if(! iterator_reversive.IsBehind()) printf("Error: Not behind \n");;


//    printf("Printing data region rotated:\n"); fflush(stdout);
//    beginu[0] = 9; endu[0] = 2;
//    beginu[1] = 2; endu[1] = 4;
//    beginu[2] = 3; endu[2] = 0;
//    dir[0] = 2; dir[1] = 0; dir[2] = 1;


//    MazdaImageRegionFlippedIterator<MIType> iterator_flipped(obraz, beginu, endu, dir);
//    for(x[dir[2]] = 0; x[dir[2]] <= abs(endu[dir[2]]-beginu[dir[2]]); x[dir[2]]++)
//    {
//        for(x[dir[1]] = 0; x[dir[1]] <= abs(endu[dir[1]]-beginu[dir[1]]); x[dir[1]]++)
//        {
//            for(x[dir[0]] = 0; x[dir[0]] <= abs(endu[dir[0]]-beginu[dir[0]]); x[dir[0]]++)
//            {
//                if(iterator_flipped.IsBehind()) printf("Error: Behind \n");;
//                printf("%.6i ", iterator_flipped.GetPixel());
//                ++iterator_flipped;
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }
//    if(! iterator_flipped.IsBehind()) printf("Error: Not behind \n");;

//    printf("Printing data region rotated slice:\n"); fflush(stdout);
//    beginu[0] = 12; endu[0] = 0;
//    beginu[1] = 10; endu[1] = 10;
//    beginu[2] = 0; endu[2] = 3;
//    dir[0] = 1; dir[1] = 2; dir[2] = 0;

//    MazdaImageRegionFlippedIterator<MIType> iterator_flipped2(obraz, beginu, endu, dir);
//    for(x[dir[2]] = 0; x[dir[2]] <= abs(endu[dir[2]]-beginu[dir[2]]); x[dir[2]]++)
//    {
//        for(x[dir[1]] = 0; x[dir[1]] <= abs(endu[dir[1]]-beginu[dir[1]]); x[dir[1]]++)
//        {
//            for(x[dir[0]] = 0; x[dir[0]] <= abs(endu[dir[0]]-beginu[dir[0]]); x[dir[0]]++)
//            {
//                if(iterator_flipped2.IsBehind()) printf("Error: Behind \n");;
//                printf("%.6i ", iterator_flipped2.GetPixel());
//                ++iterator_flipped2;
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }
//    if(! iterator_flipped2.IsBehind()) printf("Error: Not behind \n");;
//}









//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


//void test_roi_container_and_iterators(void)
//{
////    unsigned int size[3];
////    size[0] = 13;
////    size[1] = 11;
////    size[2] = 4;

//    int begin[3];
//    int end[3];
//    int x[3];

//    begin[0] = 2; end[0] = 17;
//    begin[1] = 2; end[1] = 20;
//    begin[2] = 1; end[2] = 9;

//    typedef MazdaRoi<unsigned char, 3> MRType;
//    MRType* roi = new MRType(begin, end);

//    printf("Fill roi:\n"); fflush(stdout);
//    for(x[2] = begin[2]; x[2] <= end[2]; x[2]++)
//    {
//        for(x[1] = begin[1]; x[1] <= end[1]; x[1]++)
//        {
//            for(x[0] = begin[0]; x[0] <= end[0]; x[0]++)
//            {
//                if(x[0]*x[0]+x[1]*x[1]+x[2]*x[2] < 70) roi->SetRefiPixel(x);
//                else roi->ClrRefiPixel(x);
//            }
//        }
//    }


////    roi->Print();

//    printf("Printing data direct:\n"); fflush(stdout);
//    for(x[2] = begin[2]; x[2] <= end[2]; x[2]++)
//    {
//        for(x[1] = begin[1]; x[1] <= end[1]; x[1]++)
//        {
//            for(x[0] = begin[0]; x[0] <= end[0]; x[0]++)
//            {
//                if(roi->GetRefiPixel(x)) printf("*");
//                else printf(".");
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }

//    printf("Iterator MazdaRoiFromBeginIterator:\n"); fflush(stdout);
//    MazdaRoiIterator<MRType> iterator_roi(roi);
//    for(x[2] = begin[2]; x[2] <= end[2]; x[2]++)
//    {
//        for(x[1] = begin[1]; x[1] <= end[1]; x[1]++)
//        {
//            for(x[0] = begin[0]; x[0] <= end[0]; x[0]++)
//            {
//                if(iterator_roi.IsBehind()) printf("Error: Behind \n");;
//                if(iterator_roi.GetPixel()) printf("*");
//                else printf(".");
//                ++iterator_roi;
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }
//    if(! iterator_roi.IsBehind()) printf("Error: Not behind \n");;


//    begin[0] = 3; end[0] = 16;
//    begin[1] = 3; end[1] = 11;
//    begin[2] = 2; end[2] = 6;
//    MazdaRoiRegionIterator<MRType> iterator_roiregion(roi, begin, end, true);

//    printf("Iterator MazdaRoiRegionIterator:\n"); fflush(stdout);
//    for(x[2] = begin[2]; x[2] <= end[2]; x[2]++)
//    {
//        for(x[1] = begin[1]; x[1] <= end[1]; x[1]++)
//        {
//            for(x[0] = begin[0]; x[0] <= end[0]; x[0]++)
//            {
//                if(iterator_roiregion.IsBehind()) printf("Error: Behind \n");;
//                if(iterator_roiregion.GetPixel()) printf("*");
//                else printf(".");
//                ++iterator_roiregion;
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }
//    if(! iterator_roiregion.IsBehind()) printf("Error: Not behind \n");;



////    begin[0] = 16; end[0] = 3;
////    begin[1] = 11; end[1] = 3;
////    begin[2] = 2; end[2] = 6;
////    MazdaRoiRegionReversiveIterator<MRType> iterator_roirev(roi, begin, end, true);

////    printf("Iterator MazdaRoiRegionIterator:\n"); fflush(stdout);
////    for(x[2] = 0; x[2] <= abs(end[2]-begin[2]); x[2]++)
////    {
////        for(x[1] = 0; x[1] <= abs(end[1]-begin[1]); x[1]++)
////        {
////            for(x[0] = 0; x[0] <= abs(end[0]-begin[0]); x[0]++)
////            {
////                if(iterator_roirev.IsBehind()) printf("Error: Behind \n");;
////                if(iterator_roirev.GetPixel()) printf("*");
////                else printf(".");
////                ++iterator_roirev;
////            }
////            printf("\n");
////        }
////        printf("\n");
////    }
////    if(! iterator_roirev.IsBehind()) printf("Error: Not behind \n");;



////    unsigned int dir[3];
////    begin[0] = 16; end[0] = 3;
////    begin[1] = 11; end[1] = 3;
////    begin[2] = 2; end[2] = 6;
////    dir[0] = 1; dir[1] = 2; dir[2] = 0;
////    MazdaRoiRegionFlippedIterator<MRType> iterator_roiflip(roi, begin, end, dir, true);
////    printf("Iterator MazdaRoiRegionFlippedIterator:\n"); fflush(stdout);
////    for(x[dir[2]] = 0; x[dir[2]] <= abs(end[dir[2]]-begin[dir[2]]); x[dir[2]]++)
////    {
////        for(x[dir[1]] = 0; x[dir[1]] <= abs(end[dir[1]]-begin[dir[1]]); x[dir[1]]++)
////        {
////            for(x[dir[0]] = 0; x[dir[0]] <= abs(end[dir[0]]-begin[dir[0]]); x[dir[0]]++)
////            {

////                if(iterator_roiflip.IsBehind()) printf("Error: Behind \n");;
////                if(iterator_roiflip.GetPixel()) printf("*");
////                else printf(".");
////                ++iterator_roiflip;
////            }
////            printf("\n");
////        }
////        printf("\n");
////    }
////    if(! iterator_roirev.IsBehind()) printf("Error: Not behind \n");;



//printf("Copy:\n"); fflush(stdout);

//begin[0] = 3; end[0] = 16;
//begin[1] = 3; end[1] = 11;
//begin[2] = 2; end[2] = 6;

//MRType* dest = new MRType(begin, end);
////MazdaRoiRegionCopierOr< MRType > copier;
//MazdaRoiRegionCopier< MRType, RoiAddOperation<MRType::BlockType> > copier2;

//clock_t begint, endt;
//begint = clock();
//for(unsigned int d = 0; d < 10000; d++)
//    copier2.CopyRefi(roi, dest);
//endt = clock();
//printf("Time = %i\n", endt-begint);

////begint = clock();
////for(unsigned int d = 0; d < 1000000; d++)
////    copier.CopyOrRefi(roi, dest);
////endt = clock();
////printf("Time = %i\n", endt-begint);


//MazdaRoiIterator<MRType> iterator_dest(dest);
//for(x[2] = begin[2]; x[2] <= end[2]; x[2]++)
//{
//    for(x[1] = begin[1]; x[1] <= end[1]; x[1]++)
//    {
//        for(x[0] = begin[0]; x[0] <= end[0]; x[0]++)
//        {
//            if(iterator_dest.IsBehind()) printf("Error: Behind \n");;
//            if(iterator_dest.GetPixel()) printf("*");
//            else printf(".");
//            ++iterator_dest;
//        }
//        printf("\n");
//    }
//    printf("\n");
//}
//if(! iterator_dest.IsBehind()) printf("Error: Not behind \n");;




//printf("Upsize:\n"); fflush(stdout);

//MazdaRoiResizer< MRType > resizer;
//MRType* upsized = resizer.Upsize(dest);


//upsized->GetBegin(begin);
//upsized->GetEnd(end);
//MazdaRoiIterator<MRType> iterator_upsized(upsized);
//for(x[2] = begin[2]; x[2] <= end[2]; x[2]++)
//{
//    for(x[1] = begin[1]; x[1] <= end[1]; x[1]++)
//    {
//        for(x[0] = begin[0]; x[0] <= end[0]; x[0]++)
//        {
//            if(iterator_upsized.IsBehind()) printf("Error: Behind \n");;
//            if(iterator_upsized.GetPixel()) printf("*");
//            else printf(".");
//            ++iterator_upsized;
//        }
//        printf("\n");
//    }
//    printf("\n");
//}
//if(! iterator_dest.IsBehind()) printf("Error: Not behind \n");;


//printf("Crop:\n"); fflush(stdout);
//MRType* cropped = resizer.ImageReference(upsized);

//cropped->GetBegin(begin);
//cropped->GetEnd(end);
//MazdaRoiIterator<MRType> iterator_cropped(cropped);
//for(x[2] = begin[2]; x[2] <= end[2]; x[2]++)
//{
//    for(x[1] = begin[1]; x[1] <= end[1]; x[1]++)
//    {
//        for(x[0] = begin[0]; x[0] <= end[0]; x[0]++)
//        {
//            if(iterator_cropped.IsBehind()) printf("Error: Behind \n");;
//            if(iterator_cropped.GetPixel()) printf("*");
//            else printf(".");
//            ++iterator_cropped;
//        }
//        printf("\n");
//    }
//    printf("\n");
//}
//if(! iterator_dest.IsBehind()) printf("Error: Not behind \n");;


////    unsigned char d, s;
////    d = 3;
////    s = 5;
////    Copier< unsigned char, CopyOrOperation <unsigned char> > copier;

////    copier.operate(d, s);

////    printf("d = %i s = %i \n", (int)d, (int)s);

////    d = 3;
////    s = 5;
////    Copier< unsigned char, CopyNandOperation <unsigned char> > copier2;

////    copier2.operate(d, s);

////    printf("d = %i s = %i \n", (int)d, (int)s);

//}

//int testLoadSaveImage(int argc, char* argv[])
//{
//    typedef MultiChannelPixel <MazdaImagePixelType, 3> PixelRGBType;
//    typedef MazdaImage<MazdaImagePixelType, 2> MIType2D;
//    typedef MazdaImage<PixelRGBType , 2> MITypeRGB;
//    typedef MazdaImage<MazdaImagePixelType, 3> MIType3D;

//    MazdaDummy* image = MazdaImageIO< MIType2D, MazdaImagePixelType >::Read("/home/piotr/Program/qmazda/SharedImage/arnold77.png");
//    //MazdaDummy* image = MazdaImageIO< MIType2D >::Read("/home/piotr/Obrazy/lena64.png", &info);
//    //MazdaDummy* image = MazdaImageIO< MIType2D >::Read("/home/piotr/Program/qmazda/SharedImage/test.nii", &info);
//    //MazdaDummy* image = MazdaImageIO< MIType2D >::Read("/home/piotr/Pulpit/180010018_180010018/expacs_192.168.2.6/950C8151/8A26A6EC/8A26A70A.dcm", &info);


//    if(dynamic_cast < MIType2D* >(image))
//    {
//        printf("2D\n");
//        MIType2D* im = dynamic_cast < MIType2D* >(image);
//        unsigned int size[2];
//        unsigned int x[2];
//        im->GetSize(size);
//        MazdaImageIterator<MIType2D> iterator(im);
//        for(x[1] = 0; x[1] < size[1]; x[1]++)
//        {
//            for(x[0] = 0; x[0] < size[0]; x[0]++)
//            {
//                MazdaImagePixelType pel = iterator.GetPixel();
//                if(pel < 0x3000) std::cout << "M";
//                else if(pel < 0x6000) std::cout << "X";
//                else if(pel < 0x9000) std::cout << "/";
//                else if(pel < 0xC000) std::cout << ".";
//                else std::cout << " ";
//                ++iterator;
//            }
//            std::cout << std::endl;
//        }
//        std::cout << std::endl;

//        bool saved = MazdaImageIO<MIType2D, MazdaImagePixelType >::Write("/home/piotr/Program/qmazda/SharedImage/wynik_image2D.tiff", im);
//        std::cout << "Saved : " << saved << std::endl;
//        std::cout << im->GetName();
//    }
//    if(dynamic_cast < MITypeRGB* >(image))
//    {
//        printf("RGB 2D\n");
//        MITypeRGB* im = dynamic_cast < MITypeRGB* >(image);
//        unsigned int size[2];
//        unsigned int x[2];
//        im->GetSize(size);
//        MazdaImageIterator<MITypeRGB> iterator(im);
//        for(x[1] = 0; x[1] < size[1]; x[1]++)
//        {
//            for(x[0] = 0; x[0] < size[0]; x[0]++)
//            {
//                PixelRGBType* pel = iterator.GetPixelPointer();
//                for(int d = 0; d < 3; d++)
//                {
//                    if(pel->channel[d] < 0x3000) std::cout << "M";
//                    else if(pel->channel[d] < 0x6000) std::cout << "X";
//                    else if(pel->channel[d] < 0x9000) std::cout << "/";
//                    else if(pel->channel[d] < 0xC000) std::cout << ".";
//                    else std::cout << " ";
//                }
//                ++iterator;
//            }
//            std::cout << std::endl;
//        }
//        std::cout << std::endl;
//        bool saved = MazdaImageIO<MITypeRGB, MazdaImagePixelType>::Write("/home/piotr/Program/qmazda/SharedImage/wynik_imageRGB.dicom", im);
//        std::cout << "Saved : " << saved << std::endl;
//        std::cout << im->GetName();
//    }
//    if(dynamic_cast < MIType3D* >(image))
//    {
//        printf("3D\n");
//        MIType3D* im = dynamic_cast < MIType3D* >(image);
//        unsigned int size[3];
//        unsigned int x[3];
//        im->GetSize(size);
//        MazdaImageIterator<MIType3D> iterator(im);

//        for(x[2] = 0; x[2] < size[2]; x[2]++)
//        {
//            for(x[1] = 0; x[1] < size[1]; x[1]++)
//            {
//                for(x[0] = 0; x[0] < size[0]; x[0]++)
//                {
//                    MazdaImagePixelType pel = iterator.GetPixel();
//                    if(pel < 0x3000) std::cout << "M";
//                    else if(pel < 0x6000) std::cout << "X";
//                    else if(pel < 0x9000) std::cout << "/";
//                    else if(pel < 0xC000) std::cout << ".";
//                    else std::cout << " ";
//                    ++iterator;
//                }
//                std::cout << std::endl;
//            }
//            std::cout << std::endl;
//        }
//        std::cout << std::endl;


//        bool saved = MazdaImageIO<MIType3D, MazdaImagePixelType>::Write("/home/piotr/Program/qmazda/SharedImage/wynik_image3D.nii", im);
//        std::cout << "Saved : " << saved << std::endl;
//        std::cout << im->GetName();

//    }

//    delete image;

//    std::cout << "<<<<<<<<<<<< Exit";
//}

//int testLoadSaveRoi(int argc, char* argv[])
//{
//    typedef MazdaRoi<unsigned int, 2> MRType;
//    //std::vector <MRType*> rois = MazdaRoiIO<MRType>::Read("/home/piotr/Program/qmazda/SharedImage/testroi.tiff");
//    std::vector <MRType*> rois = MazdaRoiIO<MRType>::Read("/home/piotr/Obrazy/Arnold.roi");
//    //std::vector <MRType*> rois = MazdaRoiIO<MRType>::Read("/home/piotr/Program/qmazda/SharedImage/2droi.tiff");

//    std::cout << "Liczba roi: " << rois.size() << std::endl;
//    for(int r = 0; r < rois.size(); r++)
//    {
//        int begin[MRType::Dimensions];
//        int end[MRType::Dimensions];
//        int x[MRType::Dimensions];

//        std::cout << rois[r]->GetName() << " : " <<rois[r]->GetColor() << std::endl;
//        if(rois[r]->IsEmpty()) continue;
//        rois[r]->GetBegin(begin);
//        rois[r]->GetEnd(end);

////        for(x[2] = begin[2]; x[2] <= end[2]; x[2]++)
//        {
//            for(x[1] = begin[1]; x[1] <= end[1]; x[1]++)
//            {
//                for(x[0] = begin[0]; x[0] <= end[0]; x[0]++)
//                {
//                    if(rois[r]->GetRefiPixel(x)) printf("*");
//                    else printf(".");
//                }
//                printf("\n");
//            }
//            printf("\n");
//        }
//    }
//    bool saved = MazdaRoiIO<MRType>::Write("/home/piotr/Program/qmazda/SharedImage/wynik_roi.tiff", &rois, NULL);
//    std::cout << "Saved : " << saved << std::endl;

//    for(int r = rois.size() - 1; r >= 0; r--)
//    {
//        delete rois[r];
//        rois.pop_back();
//    }

//    return 0;
//}


//int testLoadSaveMaps(int argc, char* argv[])
//{
//    typedef MazdaImage<MazdaMapPixelType, 2> MMType;
//    std::vector <MMType*> maps = MazdaMapIO<MMType>::Read("/home/piotr/Program/qmazda/SharedImage/testmap.tiff");

//    std::cout << "Liczba map: " << maps.size() << std::endl;
//    for(int r = 0; r < maps.size(); r++)
//    {
//        unsigned int size[MMType::Dimensions];
//        int x[MMType::Dimensions];
//        std::cout << maps[r]->GetName() << std::endl;
//        maps[r]->GetSize(size);
//        double min = 0;
//        double max = 0;

//        MazdaImageIterator<MMType> iterator(maps[r]);
//        for(x[1] = 0; x[1] < size[1]; x[1]++)
//        {
//            for(x[0] = 0; x[0] < size[0]; x[0]++)
//            {
//                MazdaMapPixelType pel = iterator.GetPixel();
//                if(min > pel)min = pel;
//                if(max < pel)max = pel;
//                ++iterator;
//            }
//        }
//        iterator.GoToBegin();
//        for(x[1] = 0; x[1] < size[1]; x[1]++)
//        {
//            for(x[0] = 0; x[0] < size[0]; x[0]++)
//            {
//                MazdaMapPixelType pel = iterator.GetPixel();
//                pel -= min;
//                pel /= (max-min);
//                if(pel < 0.2) std::cout << "M";
//                else if(pel < 0.4) std::cout << "X";
//                else if(pel < 0.6) std::cout << "/";
//                else if(pel < 0.8) std::cout << ".";
//                else std::cout << " ";
//                ++iterator;
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }
//    bool saved = MazdaMapIO<MMType>::Write("/home/piotr/Program/qmazda/SharedImage/wynik_map.tiff", &maps);
//    std::cout << "Saved : " << saved << std::endl;

//    for(int r = maps.size() - 1; r >= 0; r--)
//    {
//        delete maps[r];
//        maps.pop_back();
//    }
//    return 0;
//}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


////class RenderingContainerInterface
////{
////public:
////    virtual void undo() = 0;
////};

////template< unsigned int VDimensions >
////class RenderingContainer : public RenderingContainerInterface
////{
////public:
////    void undo();
////    void dimensions()
////    {
////        std::cout << "D : " << VDimensions << std::endl;
////    }
////};

////template< unsigned int VDimensions >
////void RenderingContainer<VDimensions>::undo ()
////{
////    std::cout << "undo" << std::endl;
////}

#include "mazdaroi.h"
#include "mazdaroiio.h"

// Deklaracja typu ROI dla obrazu dwuwymiarowego
typedef MazdaRoi<unsigned int, 2> MR2DType;

int main(int argc, char* argv[])
{
// Definiuje pusta liste wskaznikow do obszarow
    std::vector <MR2DType*> rois;

    unsigned int SizeX = 320;
    unsigned int SizeY = 240;

    int begin[MR2DType::Dimensions];
    int end[MR2DType::Dimensions];
// Okreslam poczatek obszaru we wspolrzednych obrazu
    begin[0] = 0; begin[1] = 0;
// Okreslam koniec obszaru we wspolrzednych obrazu (RozmiarX-1, RozmiarY-1)
    end[0] = SizeX-1; end[1] = SizeY-1;

// Tworze pusty, wyzerowany ROI
    MR2DType* roi1 = new MR2DType(begin, end);

// Tworze liniowy iterator dla ROI
    MazdaRoiIterator<MR2DType> iterator1(roi1);
// Indeksy x,y sa mi potrzebne do narysoania kolka
    while(! iterator1.IsBehind())
    {
        for(int y = 0; y < SizeY; y++)
        for(int x = 0; x < SizeX; x++)
        {
            if ((x-120)*(x-120) + (y-80)*(y-80) < 40*40) iterator1.SetPixel();
            ++iterator1;
        }
    }

// Doadeje nazwe klasy i kolor (czerwony)
    roi1->SetName("KlasaCzerwony");
    roi1->SetColor(0xff0000);

// Dodaje pierwszy roi do listy
    rois.push_back(roi1);

// Tworze pusty, wyzerowany ROI nr 2.
    MR2DType* roi2 = new MR2DType(begin, end);


// Opcjonalnie wykorzystuje iterator, ktory daje dostep do aktualnych wspolrzednych
// Nie sa potrzebne dodatkowe petle for po x i y
    MazdaRoiRegionIterator<MR2DType> iterator2(roi2, begin, end);
    while(! iterator2.IsBehind())
    {
        int x = iterator2.GetIndex(0);
        int y = iterator2.GetIndex(1);
        if ((x-180)*(x-180) + (y-140)*(y-140) < 80*80) iterator2.SetPixel();
        ++iterator2;
    }

// Doadeje nazwe klasy i kolor (czerwony)
    roi2->SetName("KlasaZielony");
    roi2->SetColor(0x00ff00);

// Dodaje drugi roi do listy
    rois.push_back(roi2);

// Zapis do pliku
    MazdaRoiIO<MR2DType>::Write("roi.tiff", &rois, NULL);

// Usuwanie roi z pamieci
    while(rois.size() > 0)
    {
        delete rois.back();
        rois.pop_back();
    }
    return 0;
}
