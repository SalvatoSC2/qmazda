/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//TO DO crop regions to size of image
//TO DO load from rgb and grey scale
//TO DO save and export

#ifndef MAZDAROIIO_H
#define MAZDAROIIO_H
#include "mazdaroi.h"
#include "mazdaimage.h"
#include <string>
#include <vector>
#include <fstream>
#include <tiffio.h>
#include "mazdaimageio.h"
//#include "mazdaimageutils.h"

struct PMS_ROI_2001
{
  char magic[12];
  uint16 xmax;
  uint16 ymax;
  uint16 roi_text_length[16];
  uint16 reserved[32];
};
struct PMS_VOI_2005
{
  char magic[12];
  uint16 xmax;
  uint16 ymax;
  uint16 zmax;
  uint16 roi_text_length[16];
};

template <typename MzRoi, typename MzRGBImage>
/**
 * @brief roisToRgb
 * @param roitable
 * @param overlay
 */
void MzRoisToRgb(std::vector<MzRoi *>* roitable, MzRGBImage* overlay, bool checkedOnly = true)
{
    unsigned int size[MzRGBImage::Dimensions];
    double spacing[MzRGBImage::Dimensions];
    overlay->GetSize(size);
    overlay->GetSpacing(spacing);

    typedef MazdaImage<unsigned int, MzRGBImage::Dimensions> CounterType;
    CounterType counter(size, spacing, true);

    for(typename std::vector< MzRoi* >::iterator roii = roitable->begin(); roii != roitable->end(); ++roii)
    {
        MzRoi* roi = (*roii);
        if(roi->IsEmpty()) continue;
        if((!roi->GetVisibility()) && checkedOnly) continue;

        int begin[MzRGBImage::Dimensions];
        int end[MzRGBImage::Dimensions];
        roi->GetBegin(begin);
        roi->GetEnd(end);

        bool quit = false;
        for(unsigned int d = 0; d < MzRGBImage::Dimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= size[d]) end[d] = size[d]-1;
        }
        if(quit) continue;

        MazdaImageRegionIterator<CounterType> ci = MazdaImageRegionIterator<CounterType>(&counter, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoi> ri = MazdaRoiRegionIterator<MzRoi>(roi, begin, end, true);
        //MazdaRoiIterator<MzRoi> ri = MazdaRoiIterator<MzRoi>(roi);
        while(!ri.IsBehind())
        {
            if(ri.GetPixel()) (*(ci.GetPixelPointer()))++;
            ++ri;
            ++ci;
        }
    }
    overlay->Erase();
    for(typename std::vector< MzRoi* >::iterator roii = roitable->begin(); roii != roitable->end(); ++roii)
    {
        MzRoi* roi = (*roii);
        if(roi->IsEmpty()) continue;
        if((!roi->GetVisibility()) && checkedOnly) continue;

        unsigned int rgbcolor = roi->GetColor();
        unsigned char color[4];
        for(unsigned int d = 0; d < 3; d++)
        {
            color[2-d] = rgbcolor&0xff;
            rgbcolor >>= 8;
        }
        int begin[MzRGBImage::Dimensions];
        int end[MzRGBImage::Dimensions];
        roi->GetBegin(begin);
        roi->GetEnd(end);

        bool quit = false;
        for(unsigned int d = 0; d < MzRGBImage::Dimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= size[d]) end[d] = size[d]-1;
        }
        if(quit) continue;

        MazdaImageRegionIterator<CounterType> ci = MazdaImageRegionIterator<CounterType>(&counter, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoi> ri = MazdaRoiRegionIterator<MzRoi>(roi, begin, end, true);
        //MazdaRoiIterator<MzRoi> ri = MazdaRoiIterator<MzRoi>(roi);
        MazdaImageRegionIterator<MzRGBImage> oi = MazdaImageRegionIterator<MzRGBImage>(overlay, (unsigned int*) begin, (unsigned int*) end);
        while(!ri.IsBehind())
        {
            if(ri.GetPixel())
            {
                unsigned int div = ci.GetPixel();
                if(div > 0)
                {
                    typename MzRGBImage::PixelType* op =  oi.GetPixelPointer();
                    for(unsigned int d = 0; d < 3; d++)
                    {
                        op->channel[d] += (color[d]/div);
                    }
                }
            }
            ++ri;
            ++ci;
            ++oi;
        }
    }
}


template< typename TMazdaRoi >
/**
 * @brief The MazdaRoiIO class implements writing and reading methods for MazdaRoi
 */
class MazdaRoiIO
{
public:

    /**
     * @brief isTiffRoi quickly checks if the file is one-bit-per-pixel tiff
     * @param filename
     * @return
     */
    static bool isTiffRoi(const std::string filename)
    {
        uint16 bps;
        TIFF* tiff = TIFFOpen(filename.c_str(), "r");
        if(!tiff) return false;
        TIFFGetFieldDefaulted(tiff, TIFFTAG_BITSPERSAMPLE, &bps);
        if(bps == 1)
        {
            TIFFClose(tiff);
            return true;
        }
        else
        {
            TIFFClose(tiff);
            return false;
        }
    }


    /**
     * @brief Read reads regions from multipage tiff
     * @param filename
     * @param info
     * @return list of regions
     */
    static std::vector < TMazdaRoi* > Read(std::string filename, unsigned int imageSize[TMazdaRoi::Dimensions] = nullptr)
    {
        std::vector <TMazdaRoi*> toReturn;
        try
        {
            if(PagedTiffReader(filename.c_str(), &toReturn, imageSize)) return toReturn;
        }
        catch(...){};
        toReturn.clear();
        try
        {
            if(TMazdaRoi::Dimensions == 2)
            {
                if(Old2DFormatReader(filename.c_str(), &toReturn, imageSize)) return toReturn;
            }
            else if(TMazdaRoi::Dimensions == 3)
            {
                if(Old3DFormatReader(filename.c_str(), &toReturn)) return toReturn;
            }
        }
        catch(...){};
        toReturn.clear();
        try
        {
            if(UseItkReader(filename.c_str(), &toReturn)) return toReturn;
        }
        catch(...){};
        toReturn.clear();

        return toReturn;
    }
    /**
     * @brief Write writes regions to a paged tiff
     * @param filename
     * @param rois
     * @return true on success
     */
    static bool Write(std::string filename, std::vector <TMazdaRoi*>* rois, unsigned int imageSize[TMazdaRoi::Dimensions] = nullptr)
    {
        return PagedTiffWriter(filename.c_str(), rois, imageSize);
    }

    /**
     * @brief Export export several regions to a file
     * @param filename
     * @param rois
     * @return zero on success
     */
    static bool ExportAsText(std::string filename, std::vector <TMazdaRoi*>* rois)
    {
        std::ofstream file;
        unsigned int rs = rois->size();
        if(rs <= 0) return false;
        file.open(filename.c_str());
        if (!file.is_open()) return false;
        if (!file.good()) return false;

        file << "Regions count = " << rs << std::endl;
        for(unsigned int r = 0; r < rs; r++)
        {
            TMazdaRoi* roi = (*rois)[r];
            file << "Name = " << roi->GetName() << std::endl;
            file << "Color R = " << ((roi->GetColor()>>16)&0xff) << " G = " << ((roi->GetColor()>>8)&0xff) << " B = " << (roi->GetColor()&0xff) << std::endl;
            file << "Coordinates:" << std::endl;
            int begin[TMazdaRoi::Dimensions];
            int end[TMazdaRoi::Dimensions];
            roi->GetBegin(begin);
            roi->GetEnd(end);

            MazdaRoiRegionIterator<TMazdaRoi> iterator(roi, begin, end);
            do{
                 if(iterator.GetPixel())
                 {
                     for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
                     {
                         file << "\t" << (iterator.GetIndex(d)+begin[d]);
                     }
                     file << std::endl;
                 }
                 ++iterator;
            }while(! iterator.IsBehind());
        }
        file.close();
        return true;
    }
    static bool ExportAsColor(std::string filename, std::vector <TMazdaRoi*>* rois, unsigned int* size)
    {
        typedef MultiChannelPixel <unsigned char, 3> PixelRoiRGBType;
        typedef MazdaImage<PixelRoiRGBType , TMazdaRoi::Dimensions> RoiRGBType;
        double spacing[TMazdaRoi::Dimensions];
        for(int d = 0; d < TMazdaRoi::Dimensions; d++) spacing[d] = 1.0;
        RoiRGBType* overlay = new RoiRGBType(size, spacing, true);
        MzRoisToRgb(rois, overlay, false);
        bool r = MazdaImageIO<RoiRGBType, unsigned char>::Write(filename, overlay);
        delete overlay;
        return r;
    }
    static bool ExportAsGreyLevels(std::string filename, std::vector <TMazdaRoi*>* rois, unsigned int* size)
    {
        unsigned int rs = rois->size();
        if(rs <= 0) return false;
        if(rs < 256)
        {
            typedef MazdaImage<unsigned char, TMazdaRoi::Dimensions> RoiGrayType;
            double spacing[TMazdaRoi::Dimensions];
            for(int d = 0; d < TMazdaRoi::Dimensions; d++) spacing[d] = 1.0;
            RoiGrayType* overlay = new RoiGrayType(size, spacing, true);
            unsigned char stepv = 255 / rs;
            unsigned char value = 255;
            for(typename std::vector< TMazdaRoi* >::iterator roii = rois->begin(); roii != rois->end(); ++roii)
            {
                TMazdaRoi* roi = (*roii);
                if(roi->IsEmpty()) continue;
                int begin[TMazdaRoi::Dimensions];
                int end[TMazdaRoi::Dimensions];
                roi->GetBegin(begin);
                roi->GetEnd(end);
                bool quit = false;
                for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
                {
                    if(begin[d] >= (int)size[d]) {quit = true; break;}
                    if(end[d] < 0) {quit = true; break;}
                    if(begin[d] < 0) begin[d] = 0;
                    if(end[d] >= size[d]) end[d] = size[d]-1;
                }
                if(quit) continue;
                MazdaImageRegionIterator<RoiGrayType> ci = MazdaImageRegionIterator<RoiGrayType>(overlay, (unsigned int*) begin, (unsigned int*) end);
                MazdaRoiRegionIterator<TMazdaRoi> ri = MazdaRoiRegionIterator<TMazdaRoi>(roi, begin, end, true);
                while(!ri.IsBehind())
                {
                    if(ri.GetPixel())
                        (*(ci.GetPixelPointer())) = value;
                    ++ri;
                    ++ci;
                }
                value -= stepv;
            }
            bool r = MazdaImageIO<RoiGrayType, unsigned char>::Write(filename, overlay);
            delete overlay;
            return r;
        }
        else
        {
            typedef MazdaImage<unsigned short int, TMazdaRoi::Dimensions> RoiGrayType;
            double spacing[TMazdaRoi::Dimensions];
            for(int d = 0; d < TMazdaRoi::Dimensions; d++) spacing[d] = 1.0;
            RoiGrayType* overlay = new RoiGrayType(size, spacing, true);
            unsigned short int stepv = 0xffff / rs;
            unsigned short int value = 0xffff;
            for(typename std::vector< TMazdaRoi* >::iterator roii = rois->begin(); roii != rois->end(); ++roii)
            {
                TMazdaRoi* roi = (*roii);
                if(roi->IsEmpty()) continue;
                int begin[TMazdaRoi::Dimensions];
                int end[TMazdaRoi::Dimensions];
                roi->GetBegin(begin);
                roi->GetEnd(end);
                bool quit = false;
                for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
                {
                    if(begin[d] >= (int)size[d]) {quit = true; break;}
                    if(end[d] < 0) {quit = true; break;}
                    if(begin[d] < 0) begin[d] = 0;
                    if(end[d] >= size[d]) end[d] = size[d]-1;
                }
                if(quit) continue;
                MazdaImageRegionIterator<RoiGrayType> ci = MazdaImageRegionIterator<RoiGrayType>(overlay, (unsigned int*) begin, (unsigned int*) end);
                MazdaRoiRegionIterator<TMazdaRoi> ri = MazdaRoiRegionIterator<TMazdaRoi>(roi, begin, end, true);
                while(!ri.IsBehind())
                {
                    if(ri.GetPixel())
                        (*(ci.GetPixelPointer())) = value;
                    ++ri;
                    ++ci;
                }
                value -= stepv;
            }
            bool r = MazdaImageIO<RoiGrayType, unsigned short int>::Write(filename, overlay);
            delete overlay;
            return r;
        }
    }
private:
    static bool UseItkReader(const char *filename, std::vector <TMazdaRoi*>* toReturn)
    {
        itk::ImageIOBase::IOPixelType pixelType;
        itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(filename, itk::ImageIOFactory::ReadMode);
        if( !imageIO )
            return false;
        imageIO->SetFileName(filename);
        imageIO->ReadImageInformation();
        pixelType = imageIO->GetPixelType();

        if(pixelType == itk::ImageIOBase::RGB || pixelType == itk::ImageIOBase::RGBA)
        {
            typedef itk::Image< itk::RGBPixel< unsigned char >, TMazdaRoi::Dimensions > ITKType;
            typedef itk::ImageFileReader<ITKType> ReaderType;
            typename ReaderType::Pointer reader = ReaderType::New();
            reader->SetFileName(filename);
            try
            {
                reader->Update();
            }
            catch (itk::ExceptionObject &ex)
            {
                std::cout << ex << std::endl;
                return false;
            }
            typename ITKType::Pointer itk_image = reader->GetOutput();
            const int MZMAXROISFROMRGBIMAGE = 4096;

            bool* regioncolors = new bool[MZMAXROISFROMRGBIMAGE];
            int* begin = new int[MZMAXROISFROMRGBIMAGE*TMazdaRoi::Dimensions];
            int* end = new int[MZMAXROISFROMRGBIMAGE*TMazdaRoi::Dimensions];

            unsigned int regioncolorsnumber = 0;
            unsigned int i;
            for(int d = 0; d < MZMAXROISFROMRGBIMAGE; d++) regioncolors[d] = false;
            itk::ImageRegionIterator<ITKType> itk_iterator(itk_image, itk_image->GetRequestedRegion());
            while(!itk_iterator.IsAtEnd())
            {
                typename ITKType::IndexType x = itk_iterator.GetIndex();
                typename ITKType::PixelType rgbpixel = itk_iterator.Get();
                unsigned char r = rgbpixel[0]&0xf0;
                unsigned char g = rgbpixel[1]&0xf0;
                unsigned char b = rgbpixel[2]&0xf0;

                if(b != r || r != g)
                {
                    unsigned short int pixel = (((unsigned short int)b)>>4) | ((unsigned short int)g) | (((unsigned short int)r)<<4);
                    if(!regioncolors[pixel])
                    {
                        regioncolorsnumber++;
                        regioncolors[pixel] = true;
                        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
                            begin[pixel*TMazdaRoi::Dimensions+d] = end[pixel*TMazdaRoi::Dimensions+d] = x[d];
                    }
                    else
                    {
                        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
                        {
                            if(begin[pixel*TMazdaRoi::Dimensions+d] > x[d]) begin[pixel*TMazdaRoi::Dimensions+d] = x[d];
                            if(end[pixel*TMazdaRoi::Dimensions+d] < x[d]) end[pixel*TMazdaRoi::Dimensions+d] = x[d];
                        }
                    }
                }
                ++itk_iterator;
            }
            if(regioncolorsnumber > 0 && regioncolorsnumber < MZMAXROISFROMRGBIMAGE)
            {
                for(i = 0; i < (*toReturn).size(); i++) delete (*toReturn)[i];
                toReturn->resize(regioncolorsnumber);
                unsigned int pixel = 0;
                for(i = 0; i < (*toReturn).size(); i++)
                {
                    for(; pixel < MZMAXROISFROMRGBIMAGE; pixel++)
                    {
                        if(regioncolors[pixel]) break;
                    }
                    TMazdaRoi* roi = new TMazdaRoi(begin + pixel*TMazdaRoi::Dimensions, end + pixel*TMazdaRoi::Dimensions);
                    (*toReturn)[i] = roi;
                    unsigned int color = ((pixel<<12) & 0xf00000) | (pixel & 0x0f) | ((pixel<<4) & 0x0ff0) | ((pixel<<8) & 0x0ff000);
                    roi->SetColor(color);
                    char str[64];
                    sprintf(str, "RGB%.6x", (color & 0xffffff));
                    roi->SetName(str);
                    roi->SetVisibility(true);
                    typename ITKType::IndexType rindex;
                    typename ITKType::SizeType rsize;
                    for(int d = 0; d < TMazdaRoi::Dimensions; d++)
                    {
                        rindex[d] = begin[pixel*TMazdaRoi::Dimensions+d];
                        rsize[d] = end[pixel*TMazdaRoi::Dimensions+d]-begin[pixel*TMazdaRoi::Dimensions+d]+1;
                    }
                    typename ITKType::RegionType region(rindex, rsize);
                    itk::ImageRegionIterator<ITKType> itk_rterator(itk_image, region);

                    MazdaRoiIterator<TMazdaRoi> roi_iterator(roi);
                    while(!itk_rterator.IsAtEnd())
                    {
                        typename ITKType::PixelType rgbpixel = itk_rterator.Get();
                        unsigned char r = rgbpixel[0]&0xf0;
                        unsigned char g = rgbpixel[1]&0xf0;
                        unsigned char b = rgbpixel[2]&0xf0;
                        unsigned short int tpixel = (((unsigned short int)b)>>4) | ((unsigned short int)g) | (((unsigned short int)r)<<4);
                        if(tpixel == pixel)
                        {
                            roi_iterator.SetPixel();
                        }
                        ++roi_iterator;
                        ++itk_rterator;
                    }
                    pixel++;
                }
            }

            delete[] regioncolors;
            delete[] begin;
            delete[] end;
        }
        else
        {
            typedef itk::Image< unsigned short int, TMazdaRoi::Dimensions > ITKType;
            typedef itk::ImageFileReader<ITKType> ReaderType;
            typename ReaderType::Pointer reader = ReaderType::New();
            reader->SetFileName(filename);
            try
            {
                reader->Update();
            }
            catch (itk::ExceptionObject &ex)
            {
                std::cout << ex << std::endl;
                return false;
            }
            typename ITKType::Pointer itk_image = reader->GetOutput();
            const int MZMAXROISFROMRGBIMAGE = 65536;

            bool* regioncolors = new bool[MZMAXROISFROMRGBIMAGE];
            int* begin = new int[MZMAXROISFROMRGBIMAGE*TMazdaRoi::Dimensions];
            int* end = new int[MZMAXROISFROMRGBIMAGE*TMazdaRoi::Dimensions];

            unsigned int regioncolorsnumber = 0;
            unsigned int i;
            for(int d = 0; d < MZMAXROISFROMRGBIMAGE; d++) regioncolors[d] = false;
            itk::ImageRegionIterator<ITKType> itk_iterator(itk_image, itk_image->GetRequestedRegion());
            while(!itk_iterator.IsAtEnd())
            {
                typename ITKType::IndexType x = itk_iterator.GetIndex();
                unsigned short int pixel = itk_iterator.Get() & 0xffff;
                if(pixel > 0)
                {
                    if(! regioncolors[pixel])
                    {
                        regioncolorsnumber++;
                        regioncolors[pixel] = true;
                        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
                            begin[pixel*TMazdaRoi::Dimensions+d] = end[pixel*TMazdaRoi::Dimensions+d] = x[d];
                    }
                    else
                    {
                        for(unsigned int d = 0; d < TMazdaRoi::Dimensions; d++)
                        {
                            if(begin[pixel*TMazdaRoi::Dimensions+d] > x[d]) begin[pixel*TMazdaRoi::Dimensions+d] = x[d];
                            if(end[pixel*TMazdaRoi::Dimensions+d] < x[d]) end[pixel*TMazdaRoi::Dimensions+d] = x[d];
                        }
                    }
                }
                ++itk_iterator;
            }
            if(regioncolorsnumber > 0 && regioncolorsnumber < MZMAXROISFROMRGBIMAGE)
            {
                for(i = 0; i < (*toReturn).size(); i++) delete (*toReturn)[i];
                toReturn->resize(regioncolorsnumber);
                unsigned int pixel = 0;
                for(i = 0; i < (*toReturn).size(); i++)
                {
                    for(; pixel < MZMAXROISFROMRGBIMAGE; pixel++)
                    {
                        if(regioncolors[pixel]) break;
                    }
                    TMazdaRoi* roi = new TMazdaRoi(begin + pixel*TMazdaRoi::Dimensions, end + pixel*TMazdaRoi::Dimensions);
                    (*toReturn)[i] = roi;
                    roi->SetColor(RoiDefaultColors[i%16]);
                    char str[64];
                    sprintf(str, "Gray%.5i", (pixel & 0xffff));
                    roi->SetName(str);
                    roi->SetVisibility(true);
                    typename ITKType::IndexType rindex;
                    typename ITKType::SizeType rsize;
                    for(int d = 0; d < TMazdaRoi::Dimensions; d++)
                    {
                        rindex[d] = begin[pixel*TMazdaRoi::Dimensions+d];
                        rsize[d] = end[pixel*TMazdaRoi::Dimensions+d]-begin[pixel*TMazdaRoi::Dimensions+d]+1;
                    }
                    typename ITKType::RegionType region(rindex, rsize);
                    itk::ImageRegionIterator<ITKType> itk_rterator(itk_image, region);

                    MazdaRoiIterator<TMazdaRoi> roi_iterator(roi);
                    while(!itk_rterator.IsAtEnd())
                    {
                        if((itk_rterator.Get() & 0xffff) == pixel)
                        {
                            roi_iterator.SetPixel();
                        }
                        ++roi_iterator;
                        ++itk_rterator;
                    }
                    pixel++;
                }
            }
            delete[] regioncolors;
            delete[] begin;
            delete[] end;
        }
        return true;
    }

    static void SwapBitsInBytesInBuffer(unsigned char* src, unsigned char* dst, int bytesnumber)
    {
        unsigned char* srctemp = src;
        unsigned char* dsttemp = dst;
        for(int i = 0; i < bytesnumber; i++)
        {
            unsigned char b = *srctemp;
            b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
            b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
            b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
            *dsttemp = b;
            srctemp++;
            dsttemp++;
        }
    }

    static bool PagedTiffReader(const char *filename, std::vector <TMazdaRoi*>* toReturn, unsigned int imageSize[TMazdaRoi::Dimensions])
    {
        TIFF* tiff = TIFFOpen(filename, "r");
        if(!tiff) return false;
        uint16 pages = TIFFNumberOfDirectories(tiff);
        unsigned int i, page;
        unsigned int size[TMazdaRoi::Dimensions>3 ? TMazdaRoi::Dimensions : 3];

        TMazdaRoi* newroi = nullptr;
        page = 0;
        do{
            char* name;
            uint32 temp32;
            uint16 temp16, bps, format;
            for(i = 0; i < TMazdaRoi::Dimensions; i++) size[i] = 1;
            TIFFGetFieldDefaulted(tiff, TIFFTAG_IMAGEWIDTH, &temp32);
            size[0] = temp32;
            TIFFGetFieldDefaulted(tiff, TIFFTAG_IMAGELENGTH, &temp32);
            size[1] = temp32;
            if(TMazdaRoi::Dimensions >= 3)
            {
                temp32 = 1;
                TIFFGetFieldDefaulted(tiff, TIFFTAG_IMAGEDEPTH, &temp32);
                if(temp32 > 1)
                {
                    size[2] = temp32;
                    size[1] /= size[2];
                }
                for(int i = 3; i < TMazdaRoi::Dimensions; i++) size[i] = 1;
            }
            TIFFGetFieldDefaulted(tiff, TIFFTAG_BITSPERSAMPLE, &bps); if(bps != 1) continue;
            TIFFGetFieldDefaulted(tiff, TIFFTAG_SAMPLESPERPIXEL, &temp16); if(temp16 != 1) continue;
            TIFFGetFieldDefaulted(tiff, TIFFTAG_SAMPLEFORMAT, &format); if(temp16 != SAMPLEFORMAT_UINT) continue;

            if (!TIFFGetField(tiff, TIFFTAG_IMAGEDESCRIPTION, &name))
            {
                if (!TIFFGetField(tiff, TIFFTAG_DOCUMENTNAME, &name)) name = nullptr;
            }
            int begin[TMazdaRoi::Dimensions];
            int end[TMazdaRoi::Dimensions];
            for(i = 0; i < TMazdaRoi::Dimensions; i++)
            {
                begin[i] = 0;
                end[i] = size[i] - 1;
            }
            newroi = new TMazdaRoi(begin, end);
            uint16* rlut;
            uint16* glut;
            uint16* blut;
            if(TIFFGetField(tiff, TIFFTAG_COLORMAP, &rlut, &glut, &blut))
            {
                newroi->SetColor((rlut[1]>>8) * 0x010000 + (glut[1]>>8) * 0x0100 + (blut[1]>>8));
            }
            else
            {
                newroi->SetColor(RoiDefaultColors[page%16]);
            }
            if(name != nullptr) newroi->SetName(name);
            newroi->SetVisibility(true);
            unsigned int y = 0;
            unsigned char* liner = new unsigned char[TIFFScanlineSize(tiff)];
            end[0] = 0;
            MazdaRoiRegionIterator<TMazdaRoi> iterator_roi(newroi, begin, end, false);
            unsigned int sizel8 = (size[0]+7)/8;
            do{
                TIFFReadScanline(tiff, (void*)liner, y);
                SwapBitsInBytesInBuffer(liner, (unsigned char*) iterator_roi.GetBlockPointer(), sizel8);
                ++iterator_roi; y++;
            }while(!iterator_roi.IsBehind());
            delete[] liner;

            TMazdaRoi* pnewroi = nullptr;
            if(imageSize != nullptr)
            {
                bool sizeok = true;
                unsigned int d;
                int begin[TMazdaRoi::Dimensions];
                int end[TMazdaRoi::Dimensions];
                int min[TMazdaRoi::Dimensions];
                int max[TMazdaRoi::Dimensions];
                newroi->GetBegin(begin);
                newroi->GetEnd(end);
                for(d = 0; d < TMazdaRoi::Dimensions; d++)
                {
                    min[d] = imageSize[d];
                    max[d] = -1;
                    if(end[d] < 0)
                    {
                        sizeok = false;
                        break;
                    }
                    if(begin[d] >= imageSize[d])
                    {
                        sizeok = false;
                        break;
                    }
                    if(begin[d] < 0) begin[d] = 0;
                    if(end[d] >= imageSize[d]) end[d] = imageSize[d];
                }
                if(sizeok)
                {
                    MazdaRoiRegionIterator<TMazdaRoi> iterator(newroi, begin, end, true);
                    do
                    {
                        if(iterator.GetPixel())
                        {
                            for(d = 0; d < TMazdaRoi::Dimensions; d++)
                            {
                                int x = iterator.GetIndex(d);
                                if(min[d] > x) min[d] = x;
                                if(max[d] < x) max[d] = x;
                            }
                        }
                        ++iterator;
                    }while(d < TMazdaRoi::Dimensions);
                    for(d = 0; d < TMazdaRoi::Dimensions; d++)
                    {
                        if(min[d] > max[d])
                        {
                            sizeok = false;
                            break;
                        }
                        min[d] += begin[d];
                        max[d] += begin[d];
                    }
                    if(sizeok) pnewroi = MazdaRoiResizer< TMazdaRoi >::Resize(newroi, min, max);
                }
            }
            else
            {
                pnewroi = MazdaRoiResizer< TMazdaRoi >::Crop(newroi);
            }

            if(pnewroi == nullptr)
            {
                pnewroi = new TMazdaRoi();
                pnewroi->SetColor(newroi->GetColor());
                pnewroi->SetName(newroi->GetName());
                pnewroi->SetVisibility(true);
            }
            delete newroi;
            if(pnewroi != nullptr) (*toReturn).push_back(pnewroi);
            page++;
        }while(TIFFReadDirectory(tiff) && page<pages);
        TIFFClose(tiff);
        return ((*toReturn).size() > 0);
    }

    static bool Old2DFormatReader(const char *filename, std::vector <TMazdaRoi*>* toReturn, unsigned int imageSize[TMazdaRoi::Dimensions])
    {
        if(TMazdaRoi::Dimensions != 2) return false;
        PMS_ROI_2001 header;
        std::ifstream file;
        file.open(filename, std::ios_base::in | std::ios_base::binary);
        if(!file.is_open()) return false;
        for(int k=0; k<32; k++) header.reserved[k] = 0;
        file.read((char*)(&header), sizeof(header));
        if(strncmp(header.magic, "PMS_ROI_2001", 12))
        {
            file.close();
            return false;
        }
        uint16* bufer;
        bufer = new uint16[header.xmax * header.ymax];
        file.read((char*)(bufer), header.xmax * header.ymax*sizeof(int16));
        unsigned int i, ii, x, y;
        uint16 mask;
        uint16 regionsmask = 0;
        int begin[16][TMazdaRoi::Dimensions];
        int end[16][TMazdaRoi::Dimensions];
        int vecs[TMazdaRoi::Dimensions];
        unsigned int regionscount = 0;

        for(y = 0; y < header.ymax; y++)
        {
            for(x = 0; x < header.xmax; x++)
            {
                mask = 1;
                int16 pix = bufer[x+y*header.xmax];

                for(i = 0; i < 16; i++)
                {
                    if(pix & mask)
                    {
                        if(mask & regionsmask)
                        {
                            if(begin[i][0] > x) begin[i][0] = x;
                            if(end[i][0] < x) end[i][0] = x;
                            if(begin[i][1] > y) begin[i][1] = y;
                            if(end[i][1] < y) end[i][1] = y;
                        }
                        else
                        {
                            begin[i][0] = x;
                            end[i][0] = x;
                            begin[i][1] = y;
                            end[i][1] = y;
                            regionsmask = regionsmask|mask;
                            regionscount++;
                        }
                    }
                    mask = mask << 1;
                }
            }
        }
        for(i = 0; i < 16; i++)
            for(int d = 2; d < TMazdaRoi::Dimensions; d++)
            {
                begin[i][d] = 0;
                end[i][d] = 0;
            }

        for(i = 2; i < TMazdaRoi::Dimensions; i++) vecs[i] = 0;
        toReturn->resize(regionscount);
        ii = 0;
        mask = 1;
        for(i = 0; i < 16; i++)
        {
            if(mask & regionsmask)
            {
                char bufo[1024];
                (*toReturn)[ii] = new TMazdaRoi(begin[i], end[i]);
                (*toReturn)[ii]->SetColor(RoiDefaultColors[i]);
                (*toReturn)[ii]->SetVisibility(1);

                if(header.roi_text_length[i]>0 && header.roi_text_length[i]<1023)
                {
                    file.read(bufo, header.roi_text_length[i]);
                    bufo[header.roi_text_length[i]] = 0;
                    (*toReturn)[ii]->SetName(bufo);
                }

                MazdaRoiIterator<TMazdaRoi> iterator_roi((*toReturn)[ii]);
                for(vecs[1] = begin[i][1]; vecs[1] <= end[i][1]; vecs[1]++)
                {
                    vecs[0] = begin[i][0];
                    uint16* ppix = bufer+vecs[0]+vecs[1]*header.xmax;
                    for(vecs[0] = begin[i][0]; vecs[0] <= end[i][0]; vecs[0]++)
                    {
                        if(*ppix & mask) iterator_roi.SetPixel();
                        ++iterator_roi;
                        ppix++;
                    }
                }
                ii++;
            }
            mask = mask << 1;
        }
        delete[] bufer;
        file.close();
        return true;
    }

    static bool Old3DFormatReader(const char *filename, std::vector <TMazdaRoi*>* toReturn)
    {
        if(TMazdaRoi::Dimensions != 3) return false;
        int i;
        PMS_VOI_2005 header;
        int file_version = 0;
        std::ifstream file;
        file.open(filename, std::ios_base::in | std::ios_base::binary);
        if(!file.is_open()) return false;

        file.read((char*)(&header), sizeof(header));
        if(strncmp(header.magic, "PMS_VOI_2005", 12) == 0) file_version = 1;
        if(strncmp(header.magic, "PMS_VOI_2006", 12) == 0) file_version = 2;
        if(file_version == 0) {file.close(); return false;}

        int xmax = header.xmax;
        int ymax = header.ymax;
        int zmax = header.zmax;
        size_t xyzmax = (size_t) xmax*ymax*zmax;
        char names[16][1024];
        for(i = 0; i < 16; i++)
        {
            if(header.roi_text_length[i] > 1023)
            {
                file.close();
                return false;
            }
            if(header.roi_text_length[i] > 0)
            {
                file.read(names[i], header.roi_text_length[i]);
                names[i][header.roi_text_length[i]] = 0;
            }
            else
            {
                sprintf(names[i], "Roi%i", i+1);
            }
        }
        uint16 *ptrz = new uint16[xyzmax];
        uint16 *ptrzt;
        memset(ptrz, 0, xyzmax);
        if(file_version == 1)
        {
            file.read((char*) ptrz, xyzmax);
        }
        else if(file_version == 2)
        {
            uint16 level = 0;
            uint16 length = 0;
            ptrzt = ptrz;
            for(size_t x = 0; x < xyzmax; x++)
            {
                if(length <= 0)
                {
                    file.read((char*) &level, sizeof(uint16));
                    file.read((char*) &length, sizeof(uint16));
                    if(length <= 0) break;
                }
                *ptrzt = level;
                length--;
                ptrzt++;
            }
        }
        file.close();

        int begin[16][TMazdaRoi::Dimensions];
        int end[16][TMazdaRoi::Dimensions];

        for(int rn = 0; rn < 16; rn++)
        {
            begin[rn][0] = xmax;
            end[rn][0] = 0;
            begin[rn][1] = ymax;
            end[rn][1] = 0;
            begin[rn][2] = zmax;
            end[rn][2] = 0;
        }

        ptrzt = ptrz;
        for(int z = 0; z < zmax; z++)
        for(int y = 0; y < ymax; y++)
        for(int x = 0; x < xmax; x++)
        {
            uint16 value = *ptrzt;
            for(i = 0; i < 16; i++)
            {
                if(value & 1)
                {
                    if(begin[i][0] > x) begin[i][0] = x;
                    if(end[i][0] < x) end[i][0] = x;
                    if(begin[i][1] > y) begin[i][1] = y;
                    if(end[i][1] < y) end[i][1] = y;
                    if(begin[i][2] > z) begin[i][2] = z;
                    if(end[i][2] < z) end[i][2] = z;
                }
                value >>= 1;
            }
            ptrzt++;
        }
        for(i = 0; i < 16; i++)
        {
            if(begin[i][0] < xmax)
            {
                uint16 mask = 1 << i;
                TMazdaRoi* roi = new TMazdaRoi(begin[i], end[i]);
                roi->SetColor(RoiDefaultColors[i]);
                roi->SetName(names[i]);
                roi->SetVisibility(1);

                MazdaRoiIterator<TMazdaRoi> iterator(roi);
                for(int z = begin[i][2]; z <= end[i][2]; z++)
                {
                    for(int y = begin[i][1]; y <= end[i][1]; y++)
                    {
                        ptrzt = ptrz + (z*ymax + y)*xmax + begin[i][0];
                        for(int x = begin[i][0]; x <= end[i][0]; x++)
                        {
                            uint16 value = *ptrzt;
                            if(value & mask)
                            {
                                iterator.SetPixel();
                            }
                            ++iterator;
                            ptrzt++;
                        }
                    }
                }
                (*toReturn).push_back(roi);
            }
        }
        delete[] ptrz;
        return true;
    }

    static bool PagedTiffWriter(const char *filename, std::vector <TMazdaRoi*>* rois, unsigned int imageSize[TMazdaRoi::Dimensions])
    {
        unsigned int page;
        TIFF* tiff = TIFFOpen(filename, "w");
        if (!tiff) return false;
        for(page = 0; page < rois->size(); page++)
        {
            TiffWriter(tiff, (*rois)[page], imageSize, page, rois->size());
            TIFFWriteDirectory(tiff);
        }
        TIFFClose(tiff);
        return true;
    }

    static void TiffWriter(TIFF* tiff, TMazdaRoi* source, unsigned int max[TMazdaRoi::Dimensions], unsigned int page, unsigned int pages)
    {
        unsigned int y = 0;
        unsigned int zero = 0;
        TMazdaRoi* dest;
        if(source->IsEmpty())
        {
            dest = new TMazdaRoi();
            dest->SetColor(source->GetColor());
            dest->SetName(source->GetName());
        }
        else if(max != nullptr) dest = MazdaRoiResizer< TMazdaRoi >::Upsize(source, max);
        else dest = MazdaRoiResizer< TMazdaRoi >::Upsize(source);

        unsigned int size[TMazdaRoi::Dimensions];
        for(zero = 0; zero < TMazdaRoi::Dimensions; zero++) size[zero] = 0;

        if(! dest->IsEmpty())
        {
            dest->GetSize(size);
            TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, size[0]);
            TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, size[1]);
            if(TMazdaRoi::Dimensions > 2)
                if(size[2] > 1)
                    TIFFSetField(tiff, TIFFTAG_IMAGEDEPTH, size[2]);
        }
        else
        {
            TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, 1);
            TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, 1);
        }
        TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 1);
        TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
        TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
        TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
        TIFFSetField(tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
        TIFFSetField(tiff, TIFFTAG_XRESOLUTION, (float)100);
        TIFFSetField(tiff, TIFFTAG_YRESOLUTION, (float)100);
        TIFFSetField(tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);

        uint16 rlut[2];
        uint16 glut[2];
        uint16 blut[2];
        blut[0] = 0;
        glut[0] = 0;
        rlut[0] = 0;

        unsigned int rgbacolor = dest->GetColor();
        blut[1] = (rgbacolor&0xff) * 0x0101;
        glut[1] = ((rgbacolor>>8)&0xff) * 0x0101;
        rlut[1] = ((rgbacolor>>16)&0xff) * 0x0101;

        TIFFSetField(tiff, TIFFTAG_COLORMAP, rlut, glut, blut);
        TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_PALETTE);
        //COMPRESSION_CCITTRLE = 2;
        //COMPRESSION_CCITTFAX3 = COMPRESSION_CCITT_T4 = 3;
        //COMPRESSION_CCITTFAX4 = COMPRESSION_CCITT_T6 = 4;
        TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_CCITTFAX4);

        std::string name = dest->GetName();
        if(! name.empty())
        {
            TIFFSetField(tiff, TIFFTAG_IMAGEDESCRIPTION, name.c_str());
            TIFFSetField(tiff, TIFFTAG_DOCUMENTNAME, name.c_str());
        }
        if(page <= pages)
        {
            TIFFSetField(tiff, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
            TIFFSetField(tiff, TIFFTAG_PAGENUMBER, page, pages);
        }
        if(! dest->IsEmpty())
        {
            unsigned int sizel8 = (size[0]+7)/8;
            unsigned char* liner = new unsigned char[sizel8];
            int begin[TMazdaRoi::Dimensions];
            int end[TMazdaRoi::Dimensions];
            dest->GetBegin(begin);
            dest->GetEnd(end);
            end[0] = 0;
            y = 0;
            MazdaRoiRegionIterator<TMazdaRoi> iterator(dest, begin, end, true);
            do{
                SwapBitsInBytesInBuffer((unsigned char*) iterator.GetBlockPointer(), liner, sizel8);
                TIFFWriteScanline(tiff, (void*)liner, y, 0);
                ++iterator; y++;
            }while(!iterator.IsBehind());
            delete[] liner;

        }
        else
        {
            TIFFWriteScanline(tiff, (void*)(&zero), 0, 0);
        }
        delete dest;
    }


//    void MzRoiFunctions::RoisToRgba(std::vector<MzRoi*> *source, unsigned char* rgbabuf, unsigned int imagesizes[MZDIMENSIONS], unsigned int stride, unsigned int step)
//    {
//        int d;
//        unsigned int imagesize = imagesizes[0];
//        for(d = 1; d < MZDIMENSIONS; d++) imagesize *= imagesizes[d];
//        unsigned int* counter = new unsigned int[imagesize];
//        memset(counter, 0, sizeof(unsigned int)*imagesize);


//        for(std::vector<MzRoi*>::iterator roii = source->begin(); roii != source->end(); ++roii)
//        {
//            MzRoi* roi = (*roii);
//            if(roi->data() == nullptr) continue;
//            if(! roi->visible) continue;

//            MzRoiImageIterator iterator;
//            if(iterator.For((*roii), (unsigned char*)counter, imagesizes, sizeof(unsigned int), imagesizes[0]*sizeof(unsigned int)))
//            {
//                do
//                {
//                    if(iterator.isRoi())
//                    {
//                        (* (unsigned int*) (iterator.getImage())) ++;
//                    }
//                }
//                while(iterator.Next());
//            }
//        }

//        for(std::vector<MzRoi*>::iterator roii = source->begin(); roii != source->end(); ++roii)
//        {
//            MzRoi* roi = (*roii);
//            if(roi->data() == nullptr) continue;
//            if(! roi->visible) continue;

//            unsigned int rgbacolor = (*roii)->rgbacolor;
//            MzRoiImageIterator iterator;
//            if(iterator.For((*roii), rgbabuf, imagesizes, step, stride))
//            {
//                do
//                {
//                    if(iterator.isRoi())
//                    {
//                        unsigned char* rgbbuftemp = iterator.getImage();
//                        unsigned int i = iterator.getLocusMore();
//                        unsigned int div = counter[i];

//                        if(div > 0)
//                        {
//                            *rgbbuftemp += (rgbacolor>>16)&0xff/div;
//                            rgbbuftemp++;
//                            *rgbbuftemp += (rgbacolor>>8)&0xff/div;
//                            rgbbuftemp++;
//                            *rgbbuftemp += rgbacolor&0xff/div;
//                        }
//                    }
//                }
//                while(iterator.NextMore());
//            }
//        }
//        delete[] counter;
//    }




//    /**
//     * @brief SaveROIToRGBImage
//     * @param filename
//     * @param rois
//     * @return
//     */
//    bool MzRoiFunctions::SaveROIToRGBImage(const char *filename, std::vector<MzRoi*> *rois, const unsigned int imagesize[MZDIMENSIONS])
//    {
//        unsigned int xmax, ymax;
//        xmax = imagesize[0];
//        ymax = 1;

//        for(unsigned int d = 1; d < MZDIMENSIONS; d++) ymax *= imagesize[d];

//        IplImage* roisi = cvCreateImage(cvSize(xmax, ymax), 8, 3);
//        memset(roisi->imageData, 0, ymax*roisi->widthStep);

//        unsigned int vecr[MZDIMENSIONS];
//        for(int d = 2; d < MZDIMENSIONS; d++) vecr[d] = 1;
//        vecr[1] = ymax;
//        vecr[0] = xmax;
//        RoisToRgba(rois, (unsigned char*)(roisi->imageData), vecr, roisi->widthStep, 3);
//        cvSaveImage(filename, roisi);
//        cvReleaseImage(&roisi);
//        return true;
//    }




//    /**
//     * @brief saveNifti
//     * @param filename
//     * @param roi
//     * @return
//     */
//    bool MzRoiFunctions::saveNifti(const char* filename, MzRoi* roi, float voxel[MZDIMENSIONS], unsigned int imagesizes[MZDIMENSIONS])
//    {
//    //https://searchcode.com/codesearch/view/8090498/
//    //http://nifti.nimh.nih.gov/pub/dist/src/utils/nifti1_test.c

//        int k;
//        int dims[8];
//        dims[0] = MZDIMENSIONS;
//        for(k = 0; k < MZDIMENSIONS; k++) dims[k+1] = imagesizes[k];
//        for(; k < 7; k++) dims[k+1] = 1;
//        nifti_image* pImage(nullptr);
//        pImage = nifti_make_new_nim( dims, NIFTI_TYPE_UINT8, true);

//        char* fn = (char*) malloc(strlen(filename)+1);
//        strcpy(fn, filename);
//        pImage->fname = fn;
//        if(! roi->name.empty())
//        {
//            char* rn = (char*) malloc(strlen(roi->name.c_str())+1);
//            strcpy(rn, roi->name.c_str());
//            pImage->iname = rn;
//        }
//        pImage->qform_code = 1;
//        pImage->datatype   = NIFTI_TYPE_UINT8;
//        pImage->dx = voxel[0];
//        pImage->dy = voxel[1];
//        pImage->dz = voxel[2];

//        unsigned char* data = (unsigned char*)pImage->data;

//        if(! RoiToByteImage(roi, data, 255, imagesizes)) return false;

//        nifti_image_write(pImage);
//        nifti_image_free(pImage);
//        return true;
//    }

//    bool MzRoiFunctions::saveText(const char* filename, MzRoi* roi, unsigned int imagesizes[MZDIMENSIONS])
//    {
//        std::ofstream file;
//        file.open(filename);
//        if (!file.is_open()) return false;
//        if (!file.good()) return false;

//        file << "Qmazda ROI" << std::endl;
//        file << "Color R = " << ((roi->rgbacolor>>16)&0xff) << " G = " << ((roi->rgbacolor>>8)&0xff) << " B = " << (roi->rgbacolor&0xff) << std::endl;
//        file << "Name = " << roi->name << std::endl;
//        file << "Pixel coordinates:" << std::endl;

//        MzRoiImageIterator iterator;
//        if(iterator.For(roi, 0, imagesizes, 1, imagesizes[0]))
//        {
//            do
//            {
//                if(iterator.isRoi())
//                {
//                    unsigned int l = iterator.getLocusMore();
//                    file << l %  imagesizes[0];
//                    l = l / imagesizes[0];
//                    for(int d = 1; d < MZDIMENSIONS; d++)
//                    {
//                        file  << "\t" << l %  imagesizes[d];
//                        l = l / imagesizes[d];
//                    }
//                    file << std::endl;
//                }
//            }while(iterator.NextMore());
//        }

//        file.close();
//        return true;
//    }


//    /**
//     * @brief LoadRoiFromRGBImage loads non-overlapped regions from color images. Colors define regions, grayscales are for background.
//     * @param filename
//     * @param toReturn
//     */
//    void MzRoiFunctions::LoadRoiFromRGBImage(const char *filename, std::vector<MzRoi*>* toReturn)
//    {
//        unsigned int i, x, y;
//        unsigned int regioncolors[MZMAXROISFROMRGBIMAGE];
//        unsigned int xmin[MZMAXROISFROMRGBIMAGE];
//        unsigned int xmax[MZMAXROISFROMRGBIMAGE];
//        unsigned int ymin[MZMAXROISFROMRGBIMAGE];
//        unsigned int ymax[MZMAXROISFROMRGBIMAGE];
//        unsigned int regioncolorsnumber = 0;
//        unsigned int vecs[MZDIMENSIONS];
//        for(i = 0; i < MZDIMENSIONS; i++) vecs[i] = 1;

//        IplImage* rois;
//        if((rois = cvLoadImage(filename, CV_LOAD_IMAGE_COLOR)) == 0 ) return;

//        for(y = 0; (int)y < rois->height && regioncolorsnumber < MZMAXROISFROMRGBIMAGE; y++)
//        {
//            for(x = 0; (int)x < rois->width && regioncolorsnumber < MZMAXROISFROMRGBIMAGE; x++)
//            {
//                CvScalar color = cvGet2D(rois, y, x);
//                unsigned char r, g, b;
//                b = color.val[0]; b &= 0xf0;
//                g = color.val[1]; g &= 0xf0;
//                r = color.val[2]; r &= 0xf0;

//                if(g == r && r == b) continue;

//                unsigned int intcolor = (((unsigned int)b)>>4) | ((unsigned int)g) | ((unsigned int)r)<<4;

//                for(i = 0; i < regioncolorsnumber; i++)
//                {
//                    if(intcolor == regioncolors[i])
//                    {
//                        if(xmin[i] > x) xmin[i] = x;
//                        if(xmax[i] < x) xmax[i] = x;
//                        if(ymin[i] > y) ymin[i] = y;
//                        if(ymax[i] < y) ymax[i] = y;
//                        break;
//                    }
//                }
//                if(i >= regioncolorsnumber && regioncolorsnumber < MZMAXROISFROMRGBIMAGE)
//                {
//                    regioncolors[regioncolorsnumber] = intcolor;
//                    xmin[regioncolorsnumber] = xmax[regioncolorsnumber] = x;
//                    ymin[regioncolorsnumber] = ymax[regioncolorsnumber] = y;
//                    regioncolorsnumber++;
//                }
//            }
//        }

//        if(regioncolorsnumber > 0 && regioncolorsnumber < MZMAXROISFROMRGBIMAGE)
//        {

//            for(i = 0; i < (*toReturn).size(); i++) delete (*toReturn)[i];
//            toReturn->resize(regioncolorsnumber);
//            for(i = 0; i < (*toReturn).size(); i++) (*toReturn)[i] = new MzRoi;

//            for(i = 0; i < regioncolorsnumber; i++)
//            {
//                vecs[0] = xmax[i]-xmin[i]+1;
//                vecs[1] = ymax[i]-ymin[i]+1;
//                (*toReturn)[i]->allocate(vecs);
//                (*toReturn)[i]->shift[0] = xmin[i];
//                (*toReturn)[i]->shift[1] = ymin[i];

//                ((*toReturn)[i]->rgbacolor) = ((regioncolors[i]<<12) & 0xf00000) | ((regioncolors[i]) & 0x0f) | ((regioncolors[i]<<4) & 0x0ff0) | ((regioncolors[i]<<8) & 0x0ff000);
//                char str[64];
//                sprintf(str, "RGB%.6x", ((*toReturn)[i]->rgbacolor & 0xffffff));
//                (*toReturn)[i]->name = str;
//            }

//            for(i = 0; i < MZDIMENSIONS; i++) vecs[i] = 0;
//            for(y = 0; (int)y < rois->height; y++)
//            {
//                for(x = 0; (int)x < rois->width; x++)
//                {
//                    CvScalar color = cvGet2D(rois, y, x);
//                    unsigned char r, g, b;
//                    b = color.val[0]; b &= 0xf0;
//                    g = color.val[1]; g &= 0xf0;
//                    r = color.val[2]; r &= 0xf0;

//                    if(g == r && r == b) continue;

//                    unsigned int intcolor = (((unsigned int)b)>>4) | ((unsigned int)g) | ((unsigned int)r)<<4;

//                    for(i = 0; i < regioncolorsnumber; i++)
//                    {
//                        if(intcolor == regioncolors[i])
//                        {
//                            vecs[0] = x - xmin[i];
//                            vecs[1] = y - ymin[i];
//                            (*toReturn)[i]->set(vecs);
//                        }
//                    }
//                }
//            }
//        }
//        cvReleaseImage(&rois);
//        return;
//    }

//    /**
//     * @brief LoadRoiFromRGBImage loads non-overlapped regions from color images. Colors define regions, grayscales are for background.
//     * @param filename
//     * @param toReturn
//     */
//    void MzRoiFunctions::LoadRoiFromGrayImage(const char *filename, std::vector<MzRoi*>* toReturn)
//    {
//        unsigned int i, x, y;
//        unsigned int xmin;
//        unsigned int xmax;
//        unsigned int ymin;
//        unsigned int ymax;
//        bool isregion = false;
//        unsigned int vecs[MZDIMENSIONS];
//        for(i = 0; i < MZDIMENSIONS; i++) vecs[i] = 1;

//        IplImage* rois;
//        if((rois = cvLoadImage(filename, CV_LOAD_IMAGE_GRAYSCALE)) == 0 ) return;

//        for(y = 0; (int)y < rois->height; y++)
//        {
//            for(x = 0; (int)x < rois->width; x++)
//            {
//                if(cvGet2D(rois, y, x).val[0] > 127)
//                {
//                    if(isregion)
//                    {
//                        if(xmin > x) xmin = x;
//                        if(xmax < x) xmax = x;
//                        if(ymin > y) ymin = y;
//                        if(ymax < y) ymax = y;
//                    }
//                    else
//                    {
//                        xmin = xmax = x;
//                        ymin = ymax = y;
//                    }
//                    isregion = true;
//                }
//            }
//        }

//        if(isregion)
//        {
//            for(i = 0; i < (*toReturn).size(); i++) delete (*toReturn)[i];
//            toReturn->resize(1);
//            for(i = 0; i < (*toReturn).size(); i++) (*toReturn)[i] = new MzRoi;

//            vecs[0] = xmax-xmin+1;
//            vecs[1] = ymax-ymin+1;
//            (*toReturn)[0]->allocate(vecs);
//            (*toReturn)[0]->shift[0] = xmin;
//            (*toReturn)[0]->shift[1] = ymin;

//            ((*toReturn)[0]->rgbacolor) = 0xff00;
//            (*toReturn)[0]->name = "Region";

//            for(i = 0; i < MZDIMENSIONS; i++) vecs[i] = 0;
//            for(y = 0; (int)y < rois->height; y++)
//            {
//                for(x = 0; (int)x < rois->width; x++)
//                {
//                    if(cvGet2D(rois, y, x).val[0] > 127)
//                    {
//                            vecs[0] = x - xmin;
//                            vecs[1] = y - ymin;
//                            (*toReturn)[0]->set(vecs);
//                    }
//                }
//            }
//        }
//        cvReleaseImage(&rois);
//        return;
//    }




};

#endif // MAZDAROIIO_H
