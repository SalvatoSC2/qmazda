#ifndef RADOPTIONS_H
#define RADOPTIONS_H

#include <QDialog>
#include "../MzShared/dataforconsole.h"

namespace Ui {
class OptionsDialog;
}

class OptionsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();
    void setOptions(DataForSelection* data, QString classname, int dimensions, int timemax, int time, int mode, int discriminant);
    void getOptions(int* dimensions, int* time, int* mode, int* discriminant);

private slots:
    void on_okButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::OptionsDialog *ui;
};

#endif // RADOPTIONS_H
