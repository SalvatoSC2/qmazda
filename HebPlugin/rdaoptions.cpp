#include "rdaoptions.h"
#include "ui_rdaoptions.h"


OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);
}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

void OptionsDialog::setOptions(DataForSelection* data, QString classname, int dimensions, int timemax, int time, int mode, int discriminant)
{
    ui->dimensionsSpinBox->setMinimum(0);
    ui->dimensionsSpinBox->setMaximum(data->featurenumber);
    ui->dimensionsSpinBox->setValue(dimensions);
    ui->timeSpinBox->setMinimum(0);
    ui->timeSpinBox->setMaximum(timemax);
    ui->timeSpinBox->setValue(time);

    for(int c = 0; c < data->classnumber; c++)
        ui->classComboBox->addItem(QString::fromStdString(data->classnames[c]));
    if(classname.isEmpty())
        ui->classComboBox->setCurrentIndex(mode + 1);
    else
        ui->classComboBox->setCurrentText(classname);

    ui->normalizationComboBox->setCurrentIndex(discriminant);
}


void OptionsDialog::getOptions(int* dimensions, int* time, int* mode, int* discriminant)
{
    *dimensions = ui->dimensionsSpinBox->value();
    *time = ui->timeSpinBox->value();
    *mode = ui->classComboBox->currentIndex() - 1;
    *discriminant = ui->normalizationComboBox->currentIndex();
}


void OptionsDialog::on_okButton_clicked()
{
    done(QDialog::Accepted);
}

void OptionsDialog::on_cancelButton_clicked()
{
    done(QDialog::Rejected);
}
