#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sstream>
#include "rdaselection.h"

using namespace alglib;

const double minimaleigen = 0.0000001;

RdaSelectionReduction::RdaSelectionReduction(DataForSelection* data_in, Classifiers *classifier_in,
                                             int goal_in, int classtodistinguish_in)
{
    data = data_in;
    classifier = classifier_in;
    classtodistinguish = classtodistinguish_in;
    parameter_goal = goal_in;
    //dimensions = dimensions_in;
    //maxtime = maxtime_in;

    acd = new ACC_CLASS_DATA[data->vectornumber];
    csorted = new int[data->vectornumber];
    fsorted = new double[data->vectornumber];
    features = new double[data->vectornumber];
}

RdaSelectionReduction::~RdaSelectionReduction()
{
    delete[] features;
    delete[] fsorted;
    delete[] csorted;
    delete[] acd;
}


bool RdaSelectionReduction::GoalFunctionStepTransform(unsigned int dim, unsigned int* picked, real_2d_array &evctrs, real_1d_array &mean)
{
    unsigned int vs, vm;
    real_2d_array covar;
    real_1d_array evalus;

    covar.setlength(dim, dim);
    evalus.setlength(dim);

// Covariance computation
    for(unsigned int f1 = 0; f1 < dim; f1++)
    {
        mean(f1) = 0.0;
        for(unsigned int f0 = f1; f0 < dim; f0++)
        {
            covar(f1, f0) = 0.0;
        }
    }
    vs = classtodistinguish > 0 ? data->classendvectorindex[classtodistinguish-1] : 0;
    vm = data->classendvectorindex[classtodistinguish];
    unsigned int vn = vm - vs;

    //int vnx = csvdat->categories[classtodistinguish].vects;
    //int vsx = csvdat->categories[classtodistinguish].start;
    //int vmax = vsx + vnx;
    //int vnn = csvdat->noofvectors;


    if(picked == NULL)
    {
        for(unsigned int v = vs; v < vm; v++)
        {
            for(unsigned int f1 = 0; f1 < dim; f1++)
            {
                double ff1 = data->values[data->featurenumber * v + f1];
                mean(f1) += ff1;

                for(unsigned int f0 = f1; f0 < dim; f0++)
                {
                    double ff0 = data->values[data->featurenumber * v + f0];
                    covar(f1, f0) += (ff0 * ff1);
                }
            }
        }
    }
    else
    {
        for(unsigned int v = vs; v < vm; v++)
        {
            for(unsigned int f1 = 0; f1 < dim; f1++)
            {
                double ff1 = data->values[data->featurenumber * v + picked[f1]];
                mean(f1) += ff1;

                for(unsigned int f0 = f1; f0 < dim; f0++)
                {
                    double ff0 = data->values[data->featurenumber * v + picked[f0]];
                    covar(f1, f0) += (ff0 * ff1);
                }
            }
        }
    }

    for(unsigned int f1 = 0; f1 < dim; f1++)
    {
        for(unsigned int f0 = f1; f0 < dim; f0++)
        {
            covar(f1, f0) /= vn;
        }
        mean(f1) /= vn;
    }
    for(unsigned int f1 = 0; f1 < dim; f1++)
    {
        for(unsigned int f0 = f1; f0 < dim; f0++)
        {
            covar(f1, f0) -= (mean(f1)*mean(f0));
        }
    }

// Eigenvalues computation
    if(!smatrixevd(covar, dim, 1, true, evalus, evctrs))
    {
        return false;
    }



/*
// Transformation computation
    for(unsigned int j = 0; j < dim; j++)
    {
        double norm = 0.0;
        for(unsigned int i = 0; i < dim; i++)
        {
            double d = evctrs(i,j);
            norm += d*d;
        }
        norm *= evalus(j);
        if(norm > machineepsilon)
        {
            norm = sqrt(norm);
            for(unsigned int i = 0; i < dim; i++)
            {
                evctrs(i,j) /= norm;
            }
        }
        else
        {
            //for(unsigned int i = 0; i < dim; i++) printf(" %f", evalus(i));
            //fflush(stdout);
            // Reject correlated variables - one of the eigenvalues is close to zero
            return false;
        }
    }
*/


    if(picked == NULL)
    {
        for(unsigned int j = 0; j < dim; j++)
        {
            double norm = evalus(j);
            if(norm < minimaleigen) continue;
            norm = sqrt(norm);
            for(unsigned int i = 0; i < dim; i++)
            {
                evctrs(i,j) /= norm;
            }
        }
    }
    else
    {
        for(unsigned int j = 0; j < dim; j++)
        {
            double norm = evalus(j);
// Reject correlated variables in selection - one of the eigenvalues is close to zero
            if(norm < minimaleigen) return false;
            norm = sqrt(norm);
            for(unsigned int i = 0; i < dim; i++)
            {
                evctrs(i,j) /= norm;
            }
        }
    }

/*
///////TEST
    printf("TEST %iD: ", dim);

    if(picked == NULL)
    {
        for(unsigned int f1 = 0; f1 < dim; f1++)
        {
            double means = 0;
            double varns = 0;

            for(unsigned int v = vs; v < vm; v++)
            {

                double d = 0.0;
                for(unsigned int f0 = 0; f0 < dim; f0++)
                {
                    d += ((data->values[data->featurenumber * v + f0] - mean(f0)) * evctrs(f0, f1));
                }
                means += d;
                varns += (d*d);
            }
            means /= vn;
            varns /= vn;
            varns -= (means*means);

            if(fabs(means) > 0.00001 || fabs(varns-1.0) > 0.00001)
            {
                printf("%f(%f)[ev%f] ", (float)means, (float)varns, (float)evalus(f1));
                fflush(stdout);
                fflush(stdout);
            }
        }
    }
    else
    {
        for(unsigned int f1 = 0; f1 < dim; f1++)
        {
            double means = 0;
            double varns = 0;

            for(unsigned int v = vs; v < vm; v++)
            {

                double d = 0.0;
                for(unsigned int f0 = 0; f0 < dim; f0++)
                {
                    d += ((data->values[data->featurenumber * v + picked[f0]] - mean(f0)) * evctrs(f0, f1));
                }
                means += d;
                varns += (d*d);
            }
            means /= vn;
            varns /= vn;
            varns -= (means*means);

            if(fabs(means) > 0.00001 || fabs(varns-1.0) > 0.00001)
            {
                printf("%f(%f)[ev%f] ", (float)means, (float)varns, (float)evalus(f1));
                fflush(stdout);
                fflush(stdout);
            }
        }
    }
    printf("\n\n");

///////TEST_END
*/










// Compute R_MDFs
    vn = data->vectornumber;

    if(picked == NULL)
    {
        for(unsigned int v = 0; v < vn; v++)
        {
            double dd = 0.0;
            for(unsigned int f1 = 0; f1 < dim; f1++)
            {
                double d = 0.0;
                for(unsigned int f0 = 0; f0 < dim; f0++)
                {
                    d += ((data->values[data->featurenumber * v + f0] - mean(f0)) * evctrs(f0, f1));
                }
                dd += (d*d);
            }
            features[v] = sqrt(dd);
        }
    }
    else
    {
        for(unsigned int v = 0; v < vn; v++)
        {
            double dd = 0.0;
            for(unsigned int f1 = 0; f1 < dim; f1++)
            {
                double d = 0.0;
                for(unsigned int f0 = 0; f0 < dim; f0++)
                {
                    d += ((data->values[data->featurenumber * v + picked[f0]] - mean(f0)) * evctrs(f0, f1));
                }
                dd += (d*d);
            }
            features[v] = sqrt(dd);
        }

    }

    return true;
}

void RdaSelectionReduction::GoalFunctionStepFishDyscriminant(double* Q, double* th, bool logar)
{
    int v;
    double meax = 0.0;
    double varx = 0.0;
    double meao = 0.0;
    double varo = 0.0;
    double f;

    int vpx = classtodistinguish > 0 ? data->classendvectorindex[classtodistinguish-1] : 0;
    int vkx = data->classendvectorindex[classtodistinguish];

    int vnx = vkx - vpx;
    int vno = data->vectornumber - vnx;

    int vpo1 = 0;
    int vko1 = vpo1 + vpx;
    int vpo2 = vkx;
    int vko2 = data->vectornumber;

    if(logar)
    {
        for(v = vpx; v < vkx; v++)
        {
            f = log(features[v]+1.0);
            meax += f;
            varx += (f * f);
        }
        for(v = vpo1; v < vko1; v++)
        {
            f = log(features[v]+1.0);
            meao += f;
            varo += (f * f);
        }

        for(v = vpo2; v < vko2; v++)
        {
            f = log(features[v]+1.0);
            meao += f;
            varo += (f * f);
        }
    }
    else
    {
        for(v = vpx; v < vkx; v++)
        {
            f = features[v];
            meax += f;
            varx += (f * f);
        }
        for(v = vpo1; v < vko1; v++)
        {
            f = features[v];
            meao += f;
            varo += (f * f);
        }

        for(v = vpo2; v < vko2; v++)
        {
            f = features[v];
            meao += f;
            varo += (f * f);
        }
    }

    varx /= vnx;
    meax /= vnx;
    varo /= vno;
    meao /= vno;
    varo -= (meao * meao);
    varx -= (meax * meax);

    f = varo + varx;
    if(f < machineepsilon) *Q = -1.0;
    else
    {
        *Q = (meao - meax);
        *Q *= *Q;
        *Q /= f;
    }
    if(varx < machineepsilon && varo < machineepsilon) *th = (meao + meax) / 2.0;
    else
    {
        varx = sqrt(varx) + meax;
        varo = sqrt(varo) + meax;
        *th = varx+varo;
        *th = (meao*varx + meax*varo)/ *th;
        //th = ((meao - meax) * th) / (th + sqrt(varo)) + meax;
    }
    if(logar) *th = exp(*th) - 1.0;
}



void RdaSelectionReduction::GoalFunctionStepFishDyscriminantMin(double *Q, double *th, int *index, bool logar)
{
    int v;
    double meax = 0.0;
    double varx = 0.0;
    double f;
    double tQ = 0.0;
    double meao = 0.0;
    double varo = 0.0;
    bool first = true;

    int vp = classtodistinguish > 0 ? data->classendvectorindex[classtodistinguish-1] : 0;
    int vk = data->classendvectorindex[classtodistinguish];
    int vn = vk - vp;

    if(logar)
    {
        for(v = vp; v < vk; v++)
        {
            f = log(features[v]+1.0);
            meax += f;
            varx += (f * f);
        }
    }
    else
    {
        for(v = vp; v < vk; v++)
        {
            f = features[v];
            meax += f;
            varx += (f * f);
        }
    }
    varx /= vn;
    meax /= vn;
    double smeax = meax * meax;
    varx -= smeax;

    for(int c = 0; c < data->classnumber; c++)
    {
        if(c == classtodistinguish) continue;

        vp = c > 0 ? data->classendvectorindex[c-1] : 0;
        vk = data->classendvectorindex[c];
        vn = vk - vp;

        if(logar)
        {
            for(v = vp; v < vk; v++)
            {
                f = log(features[v]+1.0);
                meao += f;
                varo += (f * f);
            }
        }
        else
        {
            for(v = vp; v < vk; v++)
            {
                f = features[v];
                meao += f;
                varo += (f * f);
            }
        }
        varo /= vn;
        meao /= vn;
        double smeao = meao * meao;
        varo -= smeao;

        f = varo + varx;
        if(f < machineepsilon) tQ = -1.0;
        else
        {
            tQ = (meao - meax);
            tQ *= tQ;
            tQ /= f;
        }

        if(tQ < *Q || first)
        {
            first = false;
            *Q = tQ;
            *index = c;

            if(varx < machineepsilon && varo < machineepsilon) *th = (meao + meax) / 2.0;
            else
            {
                varx = sqrt(varx) + meax;
                varo = sqrt(varo) + meax;
                *th = varx+varo;
                *th = (meao*varx + meax*varo)/ *th;

                //th = sqrt(varx);
                //th = ((meao - meax) * th) / (th + sqrt(varo)) + meax;
            }

            if(logar) *th = exp(*th) - 1.0;
        }
    }
}


void RdaSelectionReduction::GoalFunctionSort(void)
{
    //int vnn = data->vectornumber;
    for(int c = 0; c < data->classnumber; c++)
    {
        int vp = c > 0 ? data->classendvectorindex[c-1] : 0;
        int vk = data->classendvectorindex[c];
        //int vn = vk - vp;

        for(int v = vp; v < vk; v++)
        {
            double dd = features[v];
            int vv;
            for(vv = 0; vv < v; vv++)
            {
                if(dd < fsorted[vv]) break;
            }

//            memmove(fsorted+vv+1, fsorted+vv, (v-vv)*sizeof(double));
//            memmove(csorted+vv+1, csorted+vv, (v-vv)*sizeof(int));;


            for(int vvv = v; vvv > vv; vvv--)
            {
                fsorted[vvv] = fsorted[vvv-1];
                csorted[vvv] = csorted[vvv-1];
            }

            fsorted[vv] = dd;
            csorted[vv] = c;
        }
    }

//    for(int v = 0; v < data->vectornumber; v++)
//    {
//        printf("%i, %f\n", csorted[v], (float)fsorted[v]);
//    }
}


void RdaSelectionReduction::GoalFunctionStepAccDyscriminantMin(double *Q, double *th, int *index, bool balanced)
{
    int cmax = data->classnumber;
    int vallmax = data->vectornumber;
    double lastf = fsorted[0];

    int* maxcount = new int[cmax];
    int* count = new int[cmax];
    double* minacc = new double[cmax];
    double* maxdist = new double[cmax];
    int* vbest = new int[cmax];
    int* doit = new int[cmax];
    double vallmaxvallmax = (double)vallmax*vallmax*vallmax;
    int doneit = cmax-1;

    for(int c = 0; c < cmax; c++)
    {
        count[c]  = 0.0;
        maxdist[c]  = 0.0;
        vbest[c]  = 0;
        minacc[c] = vallmaxvallmax;
        maxcount[c] = data->classendvectorindex[c] - (c > 0 ? data->classendvectorindex[c-1] : 0);
        doit[c] = 0;
    }
    //doit[classtodistinguish] = -1;

    for(int v = 0; v < vallmax-1; v++)
    {
        double dist, acc;
        int cc = csorted[v];
        count[cc]++;
        if(doit[cc] == 0) doit[cc] = 1;

        dist = fsorted[v+1] - lastf;
        if(dist <= machineepsilon) continue;
        cc = csorted[v+1];

        if(cc == classtodistinguish) lastf = fsorted[v+1];

        if(doit[csorted[v+1]] == 0) doit[csorted[v+1]] = 1;

        for(int c = 0; c < cmax; c++)
        {
            if(c == classtodistinguish) continue;
            if(doit[c] <= 0) continue;
            //if(doit[c] == 0 && (!doit[classtodistinguish])) continue;

            if(balanced) acc = (double)(maxcount[classtodistinguish]-count[classtodistinguish])*maxcount[c] + (double)count[c]*maxcount[classtodistinguish];
            else acc = (double)(maxcount[classtodistinguish]-count[classtodistinguish]) + (double)count[c];
            doit[c] = 0;

            if((acc < minacc[c]) || (acc <= minacc[c] && dist > maxdist[c]))
            {
                maxdist[c] = dist;
                minacc[c] = acc;
                vbest[c] = v;
                if(acc <= 0.0)
                {
                    doit[c] = -1;
                    doneit--;
                }
            }
        }
        if(doneit <= 0) break;
        doit[classtodistinguish] = 0;
    }

    double dist = fsorted[vallmax-1] - fsorted[0];
    lastf = -1.0;
    for(int c = 0; c < cmax; c++)
    {
        if(c == classtodistinguish) continue;
        if(minacc[c] > lastf || (minacc[c] == lastf && maxdist[c] < dist))
        {
            dist = maxdist[c];
            lastf = minacc[c];
            doneit = c;
        }
    }

    *Q = (double)minacc[doneit] + (1.0/(fabs(maxdist[doneit])+1.0));
    if(balanced) *Q /= (2 * maxcount[doneit] * maxcount[classtodistinguish]);
    else *Q /= vallmax;
    *Q = 1.0 - *Q;

    *th = fsorted[vbest[doneit]] + maxdist[doneit]/2.0;
    *index = doneit;

    delete[] maxcount;
    delete[] count;
    delete[] minacc;
    delete[] maxdist;
    delete[] vbest;
    delete[] doit;

}

void RdaSelectionReduction::GoalFunctionStepAccDyscriminant(double* Q, double* th, bool balanced)
{
    int vallmax = data->vectornumber;
    double lastf = fsorted[0];

    int vpx = classtodistinguish > 0 ? data->classendvectorindex[classtodistinguish-1] : 0;
    int vkx = data->classendvectorindex[classtodistinguish];
    int vgood = vkx - vpx;
    int vbad = vallmax - vgood;

    int countgood = vgood;
    int countbad = 0;//vallmax + vpx - vkx;
    double minacc = (double)vallmax*vallmax*vallmax;
    double maxdist = 0.0;
    int vbest = 0;

    for(int v = 0; v < vallmax-1; v++)
    {
        double dist, acc;
        if(csorted[v] == classtodistinguish) countgood--;
        else countbad++;

        dist = fsorted[v+1] - lastf;
        if(dist <= machineepsilon) continue;

        lastf = fsorted[v+1];

        if(balanced) acc = (double)countgood*vbad + (double)countbad*vgood;
        else acc = countgood + countbad;



        if((acc < minacc) || (acc <= minacc && dist > maxdist))
        {
            maxdist = dist;
            minacc = acc;
            vbest = v;
        }
        if(minacc <= 0.0) break;
    }


    *Q = (double)minacc + (1.0/(fabs(maxdist)+1.0));
    if(balanced) *Q /= (2*vbad*vgood);
    else *Q /= vallmax;

    *Q = 1.0 - *Q;
    *th = fsorted[vbest] + maxdist/2.0;

}

//------------------------------------------------------------------------------
double RdaSelectionReduction::GoalFunction(unsigned int dim, unsigned int* picked)
{
    real_2d_array evctrs;
    real_1d_array mean;

//    if(csvdat->categories[classtodistinguish].vects <= 2) return -1.0;
    evctrs.setlength(dim, dim);
    mean.setlength(dim);

    if(!GoalFunctionStepTransform(dim, picked, evctrs, mean)) return -1.0;

    double Q, th;
    int cl;
    switch(parameter_goal)
    {
    case 0: GoalFunctionStepFishDyscriminant(&Q, &th, false); break;
    case 1: GoalFunctionStepFishDyscriminant(&Q, &th, true); break;
    case 2: GoalFunctionSort(); GoalFunctionStepAccDyscriminant(&Q, &th, false); break;
    case 3: GoalFunctionSort(); GoalFunctionStepAccDyscriminant(&Q, &th, true); break;
    case 4: GoalFunctionStepFishDyscriminantMin(&Q, &th, &cl, false); break;
    case 5: GoalFunctionStepFishDyscriminantMin(&Q, &th, &cl, true); break;
    case 6: GoalFunctionSort(); GoalFunctionStepAccDyscriminantMin(&Q, &th, &cl, false); break;
    case 7: GoalFunctionSort(); GoalFunctionStepAccDyscriminantMin(&Q, &th, &cl, true); break;
    default: Q = -1.0;
    }
    return Q;
}


//------------------------------------------------------------------------------
void RdaSelectionReduction::StoreClassifier(unsigned int dim, double Q, double th, unsigned int* picked, real_2d_array* evctrs, real_1d_array* mean)
{

    unsigned int r, c;

    vector< vector<double> > vtransformation;
    vtransformation.resize(dim+2);
    for(r = 0; r < dim; r++)
    {
        vector<double> vtrans;
        vtrans.resize(dim);
        for(c = 0; c < dim; c++) vtrans[c] = (*evctrs)(c, r);
        vtransformation[r] = vtrans;
    }
    vector<double> vmeans;
    vmeans.resize(dim);
    for(c = 0; c < dim; c++) vmeans[c] = (*mean)(c);
    vtransformation[r] = vmeans; r++;

    vector<double> vthreshold;
    vthreshold.resize(2);
    vthreshold[0] = th;
    vthreshold[1] = Q;
    vtransformation[r] = vthreshold;

    vector<string> vclassnames;
    vclassnames.resize(1);
    vclassnames[0] = data->classnames[classtodistinguish];

    vector<string> vfeaturenames;
    vfeaturenames.resize(dim);

    if(picked == NULL)
    {
        for(r = 0; r < dim; r++)
        {
            vfeaturenames[r] = data->featurenames[r];
        }
    }
    else
    {
        for(r = 0; r < dim; r++)
        {
            vfeaturenames[r] = data->featurenames[picked[r]];
        }
    }
    Classifier vclassifier;
    vclassifier.classnames = vclassnames;
    vclassifier.featurenames = vfeaturenames;
    vclassifier.values = vtransformation;

    classifier->classifiers.push_back(vclassifier);
}


//------------------------------------------------------------------------------
double RdaSelectionReduction::ClassifierTraining(unsigned int dim, unsigned int* picked)
{
    real_2d_array evctrs;
    real_1d_array mean;

    evctrs.setlength(dim, dim);
    mean.setlength(dim);

    if(!GoalFunctionStepTransform(dim, picked, evctrs, mean)) return -1.0;

    double Q, th;
    int cl;
    switch(parameter_goal)
    {
    case 0: GoalFunctionStepFishDyscriminant(&Q, &th, false); break;
    case 1: GoalFunctionStepFishDyscriminant(&Q, &th, true); break;
    case 2: GoalFunctionSort(); GoalFunctionStepAccDyscriminant(&Q, &th, false); break;
    case 3: GoalFunctionSort(); GoalFunctionStepAccDyscriminant(&Q, &th, true); break;
    case 4: GoalFunctionStepFishDyscriminantMin(&Q, &th, &cl, false); break;
    case 5: GoalFunctionStepFishDyscriminantMin(&Q, &th, &cl, true); break;
    case 6: GoalFunctionSort(); GoalFunctionStepAccDyscriminantMin(&Q, &th, &cl, false); break;
    case 7: GoalFunctionSort(); GoalFunctionStepAccDyscriminantMin(&Q, &th, &cl, true); break;
    default: Q = -1.0;
    }
    StoreClassifier(dim, Q, th, picked, &evctrs, &mean);
    return Q;
}


//------------------------------------------------------------------------------
bool RdaSelectionReduction::featureReindex(int* featurereindex, vector<string> featurenames)
{
    int c = 0;
    for(vector<string>::iterator fn = featurenames.begin(); fn != featurenames.end(); ++fn, c++)
    {
        featurereindex[c] = -1;
        for(int i = 0; i < data->featurenumber; i++)
        {
            if(data->featurenames[i] == *fn) featurereindex[c] = i;
        }
        if(featurereindex[c] < 0) return false;
    }
    return true;
}

//------------------------------------------------------------------------------
bool RdaSelectionReduction::Test(unsigned int* Qsorted, std::ofstream *file)
{
    breakanalysis = false;
    //int step = 0;

    int classifnumber = classifier->classifiers.size();
    if(classifnumber <= 0) return false;

    //unsigned int* Qsorted = new unsigned int[data->classnumber*(classifnumber+1)];
    for(int c = data->classnumber*(classifnumber+1) - 1; c >= 0; c--) Qsorted[c] = 0;

    unsigned int* fired = new unsigned int[data->vectornumber];
    for(int c = data->vectornumber - 1; c >= 0; c--) fired[c] = 0;


    int cc = 0;
    for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c, cc++)
    {
        if(file != NULL) *file << "@Classifier";
        if(c->classnames.size() <= 0) continue;
        if(c->featurenames.size() <= 0) continue;
        if(c->values.size() <= 0) continue;

        //unsigned int classnumber = c->classnames.size();

        if(file != NULL)
        {
           for(vector<string>::iterator cn = c->classnames.begin(); cn != c->classnames.end(); ++cn)
            {
                *file << " " << *cn;
            }
            *file << " (";
            for(vector<string>::iterator fn = c->featurenames.begin(); fn != c->featurenames.end(); ++fn)
            {
                *file << " " << *fn;
            }
            *file << " )\n";
        }
        vector< vector<double> > projection = c->values;
        unsigned int featurenumber = projection.size() - 2;
        if(featurenumber != c->featurenames.size()) continue;
        double threshold = projection[featurenumber+1][0];

        //NotifyProgressStage(BEGINS, featurenumber, 0.0, NULL);

        int* featurereindex = new int[c->featurenames.size()];
        if(!featureReindex(featurereindex, c->featurenames))
        {
            delete[] featurereindex;
            //NotifyProgressStage(FAILED, featurenumber, 0.0, NULL);
            continue;
        }

        for(int ccc = 0; ccc < data->classnumber; ccc++)
        {
            int vs = ccc > 0 ? data->classendvectorindex[ccc-1] : 0;
            int vm = data->classendvectorindex[ccc];

            for(int v = vs; v < vm; v++)
            {
                //bool fir = false;
                double* ptri = data->values + (data->featurenumber * v);

                double dist = 0.0;
                for(unsigned int ff = 0; ff < featurenumber; ff++)
                {
                    double d = 0.0;
                    for(unsigned int f = 0; f < featurenumber; f++)
                    {
                        d += ((ptri[featurereindex[f]] - projection[featurenumber][f])  * projection[ff][f]);
                    }
                    dist += (d*d);
                }
                dist = sqrt(dist) - threshold;
                if(dist < 0)
                {
                    Qsorted[cc + (classifnumber + 1) * ccc]++;
                    if(file != NULL) *file << data->classnames[ccc] << " " << c->classnames.front() << "\n";
                    fired[v]++;
                }
                else
                {
                    if(file != NULL) *file << data->classnames[ccc] << " #\n";
                }

                NotifyProgressStep();
                if(breakanalysis)
                {
                    delete[] featurereindex;
                    return false;
                }
            }
        }
        //NotifyProgressStage(SUCCESS, featurenumber, 0.0, NULL);

        delete[] featurereindex;
    }

    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        int vs = ccc > 0 ? data->classendvectorindex[ccc-1] : 0;
        int vm = data->classendvectorindex[ccc];
        for(int v = vs; v < vm; v++)
        {
            if(fired[v] == 0) Qsorted[classifnumber + (classifnumber + 1) * ccc]++;
        }
    }


/*  TEST
    int cll = 0;
    unsigned int aa[10];
    unsigned int bb[10];
    unsigned int aaa[10];
    unsigned int bbb[10];
    for(int dim = 0; dim < 10; dim++)
    {
        aa[dim] = 0; bb[dim] = 0;
        aaa[dim] = 0; bbb[dim] = 0;
    }

    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        for(int c = 0; c < 11; c++)
        {
            int vmax = data->classendvectorindex[c] - (c > 0 ? data->classendvectorindex[c-1] : 0);

            for(int dim = 0; dim < 10; dim++)
            {
                if(ccc == c)
                {
                    aaa[dim] += vmax;
                    aa[dim] += Qsorted[cll];
                }
                else
                {
                    bbb[dim] += vmax;
                    bb[dim] += Qsorted[cll];
                }
                cll++;
            }
        }
        cll++; //Non classified #
    }

    cout << "=======================\n";
    for(int dim = 0; dim < 10; dim++)
    {
        cout << aa[dim] << "\t" << aaa[dim] << "\t" << bb[dim] << "\t" << bbb[dim] << "\n";
    }
    cout.flush();

*/

    if(file != NULL)
    {
        *file << "@";
        for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c)
        {
            if(c->classnames.size() <= 0) continue;
            if(c->featurenames.size() <= 0) continue;
            if(c->values.size() <= 0) continue;
            *file << "\t" << c->classnames[0];
        }
        *file << "\t#\n";
        for(int ccc = 0; ccc < data->classnumber; ccc++)
        {
            *file << data->classnames[ccc];
            for(int cc = 0; cc <= classifnumber; cc++)
            {

                *file << "\t" << Qsorted[cc + (classifnumber + 1) * ccc];
            }
            *file << "\n";
        }
    }
    //delete[] Qsorted;
    delete[] fired;
    return true;
}


//------------------------------------------------------------------------------
bool RdaSelectionReduction::ComputeMdfs(double* table)
{
    breakanalysis = false;
    //int step = 0;

    int classifnumber = classifier->classifiers.size();
    if(classifnumber <= 0) return false;

    int cc = 0;
    for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c, cc++)
    {
        if(c->classnames.size() <= 0) continue;
        if(c->featurenames.size() <= 0) continue;
        if(c->values.size() <= 0) continue;
/*
        stringstream ss;
        for(vector<string>::iterator cn = c->classnames.begin(); cn != c->classnames.end(); ++cn)
        {
            ss << *cn;
        }
        ss << c->featurenames.size() << "D\n";


        NotifyProgressText(ss.str());
*/

        stringstream ss;
        ss << c->getName() << "\n";
        NotifyProgressText(ss.str());

        vector< vector<double> > projection = c->values;
        unsigned int featurenumber = projection.size() - 2;
        if(featurenumber != c->featurenames.size()) continue;

        int* featurereindex = new int[c->featurenames.size()];
        if(!featureReindex(featurereindex, c->featurenames))
        {
            delete[] featurereindex;
            continue;
        }
        NotifyProgressStep();
        for(int ccc = 0; ccc < data->classnumber; ccc++)
        {
            int vs = ccc > 0 ? data->classendvectorindex[ccc-1] : 0;
            int vm = data->classendvectorindex[ccc];

            for(int v = vs; v < vm; v++)
            {
                double* ptri = data->values + (data->featurenumber * v);

                double dist = 0.0;
                for(unsigned int ff = 0; ff < featurenumber; ff++)
                {
                    double d = 0.0;
                    for(unsigned int f = 0; f < featurenumber; f++)
                    {
                        d += ((ptri[featurereindex[f]] - projection[featurenumber][f])  * projection[ff][f]);
                    }
                    dist += (d*d);
                }
                dist = sqrt(dist);
                table[cc + classifnumber*v] = log(dist + 1.0);

                if(breakanalysis)
                {
                    delete[] featurereindex;
                    return false;
                }
            }
        }
        delete[] featurereindex;
        NotifyProgressStep();
    }

    return true;
}
