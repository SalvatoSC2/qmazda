#ifndef LDAWORKER_H
#define LDAWORKER_H

#include <QObject>
#include <QString>
#include <string>
#include "../MzShared/dataforconsole.h"
//selection.h"
#include "rdaselection.h"
#include "../MzShared/progress.h"


class PluginWorker : public QObject, RdaSelectionReduction
{
    Q_OBJECT
public:
    //void NotifyProgress(int notification, int dimensionality);
    void NotifyProgressStep(void);
    void NotifyProgressText(std::string text);
    void NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected);


    explicit PluginWorker(double* Qtable, unsigned int* Qsorted,
                       DataForSelection* data, Classifiers *classifier,
                       Progress* progressdialog_in,
                       int dimensions_in, int maxtime_in, int goal,
                       int classtodistinguish_in = -1, QObject *parent = 0);
    ~PluginWorker();
    //void initLdaWorker(DataForSelection* data, Progress* progressdialog, int dimensions, int maxtime);
    //void notifyProgress(int notification, int dimensionality);

    ofstream *report;
    //string *featurenames;

    void breakAnalysis(void);

public slots:
    void process_select();
    void process_mdf();
    void process_project();
    void process_test();

signals:
    void finished(bool success);
    void finished();
    void notifyProgressStep(void);
    void notifyProgressText(QString text);


private:
    double* Qtable;
    unsigned int* Qsorted;

    Progress* progressdialog;
    int dimensions;
    int maxtime;
};

#endif // LDAWORKER_H
