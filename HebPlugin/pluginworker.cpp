#include <string>
#include <sstream>
#include <time.h>
#include "pluginworker.h"

PluginWorker::PluginWorker(double* Qtable, unsigned int* Qsorted,
                           DataForSelection* data, Classifiers* classifier,
                           Progress* progressdialog_in, int dimensions_in,
                           int maxtime_in, int goal,
                           int classtodistinguish_in, QObject *parent) :
    QObject(parent)
    ,RdaSelectionReduction(data, classifier, goal, classtodistinguish_in)
{
    progressdialog = progressdialog_in;
    dimensions = dimensions_in;
    maxtime = maxtime_in;

    this->Qtable = Qtable;
    this->Qsorted = Qsorted;
}

PluginWorker::~PluginWorker()
{
}


void PluginWorker::NotifyProgressStep(void)
{
    emit notifyProgressStep();
}
void PluginWorker::NotifyProgressText(std::string text)
{
    emit notifyProgressText(QString::fromStdString(text));
}
void PluginWorker::NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected)
{
    time_t currenttime = time(NULL);
    char* ct = ctime(&currenttime);
    strtok(ct, "\n\r");
    char text[256];

    switch(notification)
    {
    case BEGINS: sprintf(text, "Begins %iD at %s\n", dimensionality, ct); emit notifyProgressText(text); break;
    case STOPPED: sprintf(text, "Stopped %f%%\n", (float)value); emit notifyProgressText(text); break;
    case CANCELED: sprintf(text, "Canceled\n"); emit notifyProgressText(text); break;
    case COMPLETED: sprintf(text, "Completed %iD\n", dimensionality); emit notifyProgressText(text); break;
    case FAILED: sprintf(text,"Failed %iD\n", dimensionality); emit notifyProgressText(text); break;
    case SUCCESS:
        sprintf(text,"D = %i Q = %f\n", dimensionality, (float)value); emit notifyProgressText(text);
        if(data->featurenames != NULL && selected != NULL)
        {
            for(int d = 0; d < dimensionality; d++)
            {
                sprintf(text," %s %i\n", data->featurenames[selected[d]].c_str(), selected[d]);
                emit notifyProgressText(text);
            }
        }
        break;
    }
}

void PluginWorker::breakAnalysis(void)
{
    breakanalysis = true;
}

void PluginWorker::process_test()
{
    emit finished(Test(Qsorted, NULL));
    emit finished();
}

void PluginWorker::process_select()
{
    breakanalysis = false;
    bool ret = true;
    if(classtodistinguish < 0)
    {
        int d;
        int* tempsort = new int[data->classnumber * dimensions];

        int ile = data->classnumber;
        ile = (maxtime+ile-1)/ile;
        for(classtodistinguish = 0; classtodistinguish < data->classnumber; classtodistinguish++)
        {
            bool okl = FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, ile, true);

            if(okl)
            {
                for(d = 0; d < dimensions; d++)
                    tempsort[classtodistinguish * dimensions + d] = Qsorted[d];
            }
            else
            {
                for(d = 0; d < dimensions; d++)
                    tempsort[classtodistinguish * dimensions + d] = -1;
            }
            ret = ret && okl;
        }
        int c = 0;
        for(d = 0; d < data->featurenumber; d++) Qtable[d] = 0.0;
        for(d = 0; d < dimensions*data->classnumber && c < data->featurenumber; d++)
        {
            bool found = false;
            for(int dd = 0; dd < c; dd++)
            {
                if(Qsorted[dd] == tempsort[d])
                {
                    Qtable[Qsorted[dd]] += 1.0;
                    found = true;
                    break;
                }
            }
            if(!found)
            {
                Qsorted[c] =  tempsort[d];
                Qtable[Qsorted[c]] = 1.0;
                c++;
            }
        }
        delete[] tempsort;
        for(; c < data->featurenumber; c++) Qsorted[c] = (unsigned int)(-1);
    }
    else
    {
        ret = FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, maxtime, true);
    }
    emit finished(ret);
    emit finished();
}


void PluginWorker::process_project()
{
    breakanalysis = false;
    bool ret = true;

    NotifyProgressStage(BEGINS, data->featurenumber, 0.0, NULL);
    if(classtodistinguish < 0)
    {
        int ile = data->classnumber;
        ile = (maxtime+ile-1)/ile;
        for(classtodistinguish = 0; classtodistinguish < data->classnumber; classtodistinguish++)
        {
            NotifyProgressText(data->classnames[classtodistinguish]);
            ret = ret && (ClassifierTraining(data->featurenumber, NULL) > 0.0);
            NotifyProgressStep();
            if(breakanalysis) break;
        }
    }
    else
    {
        NotifyProgressText(data->classnames[classtodistinguish]);
        ret = (ClassifierTraining(data->featurenumber, NULL) > 0.0);
        NotifyProgressStep();
    }
    emit finished(ret);
    emit finished();
}

void PluginWorker::process_mdf()
{
    breakanalysis = false;
    int classifnumber = classifier->classifiers.size();
    int vectornumber = data->vectornumber;
    if(classifnumber <= 0 || vectornumber <= 0)
    {
        emit finished(false);
        emit finished();
        return;
    }

    bool res = ComputeMdfs(Qtable);
    if(res)
    {
        delete[] data->values;
        delete[] data->featurenames;

        data->featurenumber = classifnumber;
        data->featurenames = new string[classifnumber];
        data->values = Qtable;
        Qtable = NULL;

        int cc = 0;

        for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c, cc++)
        {
            if(c->classnames.size() <= 0) continue;
            if(c->featurenames.size() <= 0) continue;
            if(c->values.size() <= 0) continue;

            data->featurenames[cc] = c->getName();
/*
            stringstream ss;
            for(vector<string>::iterator cn = c->classnames.begin(); cn != c->classnames.end(); ++cn)
            {
                ss << *cn;
            }
            ss << c->featurenames.size() << "D";
            data->featurenames[cc] = ss.str();
*/

        }
    }
    emit finished(res);
    emit finished();
}

