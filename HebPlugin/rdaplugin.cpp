#include <QtGlobal>
#include <QtPlugin>
#include <QFileDialog>
#include <string>
#include <sstream>

#include "rdaplugin.h"
#include "rdaoptions.h"
#include "../MzShared/multidimselection.h"
#include "../MzShared/selectionlist.h"
#include "../MzShared/testdialog.h"

void* MzNewPluginObject(void)
{
    return new RadPlugin();
}

void MzDeletePluginObject(void* object)
{
    delete static_cast<RadPlugin *> (object);
}

RadPlugin::RadPlugin()
{
    data = NULL;
    currentclassifier = NULL;
    worker = NULL;
    Qtable = NULL;
    Qsorted = NULL;

    dimensionsrestriction = 3;
    timerestriction = 60;
    discriminant = 1;
    mode_index = -1;
    progressdialog = NULL;
    pull_data = NULL;
}

//Uncomment to compile in Qt 4
//    Q_EXPORT_PLUGIN2(RadPlugin, RadPlugin)

RadPlugin::~RadPlugin()
{
    if(data != NULL) delete data;
    if(currentclassifier != NULL) delete currentclassifier;
}

QString RadPlugin::getName(void)
{
    return QString("Heb discrimination");
}


bool RadPlugin::startThreadIn(void)
{
    QObject* par = QObject::sender()->parent();
    pull_data = qobject_cast<MzPullDataInterface*>(par);

    if(data != NULL) delete data;
    data = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;

    data = new DataForSelection();
    pull_data->getData(data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        QMessageBox msgBox;
        msgBox.setText("Invalid parameters");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
        return false;
    }
    return true;
}

bool RadPlugin::startThreadOut(const char* analysisSlot, const char* finishSlot, quint64 maxSteps, int timerestriction, unsigned int sizeQtable, unsigned int sizeQsorted, bool resetClassifier)
{
    int classtodistinguish = -1;
    if(resetClassifier)
    {
        if(currentclassifier != NULL) delete currentclassifier;
        currentclassifier = new Classifiers("MzHebClassifiers2013");
    }
    else
    {
        if(currentclassifier == NULL)
        {
            QMessageBox msgBox;
            msgBox.setText("Invalid classifier");
            msgBox.setWindowTitle("Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
            return false;
        }
    }
    if(sizeQtable > 0) Qtable = new double[sizeQtable];
    if(sizeQsorted > 0) Qsorted = new unsigned int[sizeQsorted];

    for(int c = 0; c < data->classnumber; c++)
    {
        if(data->classnames[c] == classname.toStdString()) classtodistinguish = c;
    }

    QObject* par = QObject::sender()->parent();
    QThread* thread = new QThread;
    QWidget* window = qobject_cast<QWidget*>(par);
    progressdialog = new Progress(window);

    int drn = data->featurenumber < dimensionsrestriction ? data->featurenumber: dimensionsrestriction;
    worker = new PluginWorker(Qtable, Qsorted, data, currentclassifier, progressdialog, drn, timerestriction, discriminant, classtodistinguish);
    worker->report = NULL;

    progressdialog->setMaxValues(timerestriction, maxSteps, worker);
    progressdialog->setWindowModality(Qt::WindowModal);
    progressdialog->show();
    worker->moveToThread(thread);

    connect(thread, SIGNAL(started()), worker, analysisSlot);
    connect(worker, SIGNAL(finished(bool)), this, finishSlot);
    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(worker, SIGNAL(notifyProgressStep(void)), progressdialog, SLOT(NotifyProgressStep(void)));
    connect(worker, SIGNAL(notifyProgressText(QString)), progressdialog, SLOT(NotifyProgressText(QString)));

    window->hide();
    thread->start();
    return true;
}

void RadPlugin::stopThreadIn(void)
{
    QWidget* window = qobject_cast<QWidget*>(progressdialog->parent());
    disconnect(progressdialog, 0, 0, 0);
    delete progressdialog;
    progressdialog = NULL;
    window->show();
}

void RadPlugin::stopThreadOut(void)
{
    if(data != NULL) delete data;
    data = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;
    worker = NULL;

    if(currentclassifier == NULL)
    {
        plugin_save->setEnabled(false);
        plugin_mdfs->setEnabled(false);
        plugin_test->setEnabled(false);
    }
    else
    {
        plugin_save->setEnabled(true);
        plugin_mdfs->setEnabled(true);
        plugin_test->setEnabled(true);
    }
}

bool RadPlugin::initiateMapsPlugin(QObject *parent, QMenu* plugin_menu)
{
    return false;
}


bool RadPlugin::initiateTablePlugin(QObject *parent, QMenu* plugin_menu)
{
    bool ret = true;

    QAction* plugin_load = new QAction(tr("Load classifier..."), parent);
    plugin_load->setStatusTip(tr("Load rules to classify data"));
    plugin_menu->addAction(plugin_load);
    ret = ret && connect(plugin_load, SIGNAL(triggered()), this, SLOT(on_menuLoad_triggered()));

    plugin_save = new QAction(tr("Save classifier..."), parent);
    plugin_save->setStatusTip(tr("Save rules to classify data"));
    plugin_menu->addAction(plugin_save);
    ret = ret && connect(plugin_save, SIGNAL(triggered()), this, SLOT(on_menuSave_triggered()));

    QAction* plugin_options = new QAction(tr("Analysis Options..."), parent);
    plugin_options->setStatusTip(tr("Set options"));
    plugin_menu->addAction(plugin_options);
    ret = ret && connect(plugin_options, SIGNAL(triggered()), this, SLOT(on_menuOptions_triggered()));

    plugin_menu->addSeparator();

    QAction* plugin_select = new QAction(tr("Selection and training..."), parent);
    plugin_select->setStatusTip(tr("Selection of features and training of classifier"));
    plugin_menu->addAction(plugin_select);
    ret = ret && connect(plugin_select, SIGNAL(triggered()), this, SLOT(on_menuSelect_triggered()));

    QAction* plugin_train = new QAction(tr("Projection and training..."), parent);
    plugin_train->setStatusTip(tr("Projection of feature space and training of classifier"));
    plugin_menu->addAction(plugin_train);
    ret = ret && connect(plugin_train, SIGNAL(triggered()), this, SLOT(on_menuProject_triggered()));

    plugin_mdfs = new QAction(tr("Compute MDF's..."), parent);
    plugin_mdfs->setStatusTip(tr("Compute most discriminating features"));
    plugin_menu->addAction(plugin_mdfs);
    ret = ret && connect(plugin_mdfs, SIGNAL(triggered()), this, SLOT(on_menuMdfs_triggered()));

    plugin_test = new QAction(tr("Test classifier..."), parent);
    plugin_test->setStatusTip(tr("Test classifier performance"));
    plugin_menu->addAction(plugin_test);
    ret = ret && connect(plugin_test, SIGNAL(triggered()), this, SLOT(on_menuTest_triggered()));

    plugin_menu->addSeparator();

    plugin_save->setEnabled(false);
    plugin_mdfs->setEnabled(false);
    plugin_test->setEnabled(false);

    return ret;
}

void RadPlugin::on_menuTest_triggered()
{
    if(!startThreadIn()) return;


    quint64 maxSteps = data->vectornumber;
    unsigned int sizeQsorted = data->classnumber * (currentclassifier->classifiers.size()+1);

    startThreadOut(SLOT(process_test()), SLOT(on_menuTest_finished(bool)), maxSteps, 0, 0, sizeQsorted, false);
}
void RadPlugin::on_menuMdfs_triggered()
{
    if(currentclassifier == NULL) return;
    if(!startThreadIn()) return;

    quint64 maxSteps = 2*currentclassifier->classifiers.size();
    startThreadOut(SLOT(process_mdf()), SLOT(on_menuMdfs_finished(bool)), maxSteps, 0,
                   //0, 0,
                   currentclassifier->classifiers.size()*data->vectornumber, 0,
                   //currentclassifier->classifiers.size(), currentclassifier->classifiers.size(),
                   false);
}
void RadPlugin::on_menuProject_triggered()
{
    if(!startThreadIn()) return;
    quint64 maxSteps;
    int classtodistinguish = -1;
    for(int c = 0; c < data->classnumber; c++)
    {
        if(data->classnames[c] == classname.toStdString()) classtodistinguish = c;
    }
    if(classtodistinguish < 0) maxSteps = data->classnumber;
    else maxSteps = 1;

    if(maxSteps < 1) return;
    startThreadOut(SLOT(process_project()), SLOT(on_menuProject_finished(bool)), maxSteps, 0, 0, 0, true);
}
void RadPlugin::on_menuSelect_triggered()
{
    if(!startThreadIn()) return;
    unsigned int sizeQsorted = data->featurenumber;

    quint64 maxSteps = 1;
    quint64 maxstepsold = maxSteps;
    int drn = data->featurenumber < dimensionsrestriction ? data->featurenumber: dimensionsrestriction;
    for(int d = 0; d < drn; d++)
    {
        for(int k = 0; k <= d; k++)
        {
            maxSteps *= (data->featurenumber-k);
            if(maxSteps / (data->featurenumber-k) != maxstepsold)
            {
                maxSteps = 0;
                k = d = drn;
            }
            maxstepsold = maxSteps;
        }
    }

    startThreadOut(SLOT(process_select()), SLOT(on_menuSelect_finished(bool)),
                   maxSteps, timerestriction+data->classnumber,
                   sizeQsorted, sizeQsorted, true);
}


void RadPlugin::on_menuTest_finished(bool success)
{
    stopThreadIn();

    if(success)
    {
        TestDialog testdialog;

        int cla = 1;

        for(vector<Classifier>::iterator c = currentclassifier->classifiers.begin(); c != currentclassifier->classifiers.end(); ++c)
        {
            if(c->classnames.size() <= 0) continue;
            if(c->featurenames.size() <= 0) continue;
            if(c->values.size() <= 0) continue;
            cla++;
        }

        std::string* cn = new std::string[cla];
        cn[cla-1] = "#";

        int cc = 0;
        for(vector<Classifier>::iterator c = currentclassifier->classifiers.begin(); c != currentclassifier->classifiers.end(); ++c, cc++)
        {
            if(c->classnames.size() <= 0) continue;
            if(c->featurenames.size() <= 0) continue;
            if(c->values.size() <= 0) continue;

            stringstream ss;
            for(vector<string>::iterator vs = c->classnames.begin(); vs != c->classnames.end(); ++vs)
            {
                ss << *vs;
            }
            ss << c->featurenames.size() << "D";
            cn[cc] = ss.str();
        }
        testdialog.setTableData(cla, data->classnumber, cn, data->classnames, Qsorted);
        testdialog.exec();
        delete[] cn;
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Test failed");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
    stopThreadOut();
}


void RadPlugin::on_menuProject_finished(bool success)
{
    stopThreadIn();

    if(success)
    {
        QMessageBox msgBox;
        msgBox.setText("Training completed");
        msgBox.setWindowTitle("Information");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Training failed");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
    stopThreadOut();
}


void RadPlugin::on_menuMdfs_finished(bool success)
{
    stopThreadIn();

    if(success)
    {
        SelectionList selectiondialog;
        unsigned int* sorted = new unsigned int[data->featurenumber];
        for(int c = 0; c < data->featurenumber; c++) sorted[c] = c;

        selectiondialog.setList(data, NULL, sorted, dimensionsrestriction);
        if(selectiondialog.exec() == QDialog::Accepted)
        {
            if(selectiondialog.checkedFeatures != NULL)
            {
                int count = 0;

                for(int c = 0; c < data->featurenumber; c++)
                {
                    if(selectiondialog.checkedFeatures[c]) count ++;
                }
                if(count > 0)
                {
                    SelectedFeatures ret;
                    ret.featurenumber = count;
                    ret.featureoriginalindex = new int[count];
                    int cc = 0;
                    for(int c = 0; c < data->featurenumber; c++)
                    {
                        if(selectiondialog.checkedFeatures[c])
                        {
                            ret.featureoriginalindex[cc] = sorted[c];
                            cc ++;
                        }
                    }
                    pull_data->setData(data, &ret);
                }
            }
        }
        delete[] sorted;
        Qtable = NULL;
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("LDA failed");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }

    stopThreadOut();
}


void RadPlugin::on_menuSelect_finished(bool success)
{
    stopThreadIn();

    if(success)
    {
        SelectionList selectiondialog;
        selectiondialog.setList(data, Qtable, Qsorted, dimensionsrestriction);
        if(selectiondialog.exec() == QDialog::Accepted)
        {
            if(selectiondialog.checkedFeatures != NULL)
            {
                int count = 0;

                for(int c = 0; c < data->featurenumber; c++)
                {
                    if(selectiondialog.checkedFeatures[c]) count ++;
                }
                if(count > 0)
                {
                    SelectedFeatures ret;
                    ret.featurenumber = count;
                    ret.featureoriginalindex = new int[count];
                    int cc = 0;
                    for(int c = 0; c < data->featurenumber; c++)
                    {
                        if(selectiondialog.checkedFeatures[c])
                        {
                            ret.featureoriginalindex[cc] = data->featureoriginalindex[Qsorted[c]];
                            cc ++;
                        }
                    }
                    pull_data->setSelection(&ret);
                }
            }
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Selection failed");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }

    stopThreadOut();
}

void RadPlugin::on_menuOptions_triggered()
{
    QObject* par = QObject::sender()->parent();
    pull_data = qobject_cast<MzPullDataInterface*>(par);

    OptionsDialog dialog;
    DataForSelection data;
    pull_data->getData(&data);

    dialog.setOptions(&data, classname, dimensionsrestriction, 32000, timerestriction, mode_index, discriminant);
    if(dialog.exec() == QDialog::Accepted)
    {
        dialog.getOptions(&dimensionsrestriction, &timerestriction, &mode_index, &discriminant);

        if(mode_index < 0 || mode_index >= data.classnumber) classname.clear();
        else classname = QString::fromStdString(data.classnames[mode_index]);
    }
}

void RadPlugin::on_menuSave_triggered()
{
    if(currentclassifier != NULL)
    {
        QString filter;
        bool hexa = false;
        QWidget* window = qobject_cast<QWidget*>(QObject::sender()->parent());
        QString fileName = QFileDialog::getSaveFileName(window,
                                                     tr("Save linear classifier"),
                                                     NULL,//domyslna nazwa pliku
                                                     tr("Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)"),
                                                     &filter);
        if(filter == "Double precision hexadecimals (*.txtv) (*.txtv)") hexa = true;

        if (!fileName.isEmpty())
        {

            if(!currentclassifier->saveClassifier(fileName.toStdString().c_str(), hexa))
            {
                QMessageBox msgBox;
                msgBox.setText("Failed to save classifier");
                msgBox.setWindowTitle("Error");
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.exec();
            }
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("No classifier to save. Train first.");
        msgBox.setWindowTitle("Warning");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
}

bool RadPlugin::openFile(QString filename)
{
    Classifiers* newclassifier = new Classifiers("MzHebClassifiers2013");
    if(!newclassifier->loadClassifier(filename.toStdString().c_str()))
    {
        if(newclassifier != NULL) delete newclassifier;
        return false;
    }
    else
    {
        if(currentclassifier != NULL) delete currentclassifier;
        currentclassifier = newclassifier;
        if(currentclassifier == NULL)
        {
            plugin_save->setEnabled(false);
            plugin_mdfs->setEnabled(false);
            plugin_test->setEnabled(false);
        }
        else
        {
            plugin_save->setEnabled(true);
            plugin_mdfs->setEnabled(true);
            plugin_test->setEnabled(true);
        }
        return true;
    }
}


void RadPlugin::on_menuLoad_triggered()
{
    QString filter;
    QWidget* window = qobject_cast<QWidget*>(QObject::sender()->parent());
    QString fileName = QFileDialog::getOpenFileName(window,
                                                 tr("Load linear classifier"),
                                                 NULL,//domyslna nazwa pliku
                                                 tr("Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)"),
                                                 &filter);
    if (!fileName.isEmpty())
    {

        Classifiers* newclassifier = new Classifiers("MzHebClassifiers2013");
        if(!newclassifier->loadClassifier(fileName.toStdString().c_str()))
        {
            if(newclassifier != NULL) delete newclassifier;
            QMessageBox msgBox;
            msgBox.setText("Failed to load classifier");
            msgBox.setWindowTitle("Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
        }
        else
        {
            if(currentclassifier != NULL) delete currentclassifier;
            currentclassifier = newclassifier;
        }
    }
    if(currentclassifier == NULL)
    {
        plugin_save->setEnabled(false);
        plugin_mdfs->setEnabled(false);
        plugin_test->setEnabled(false);
    }
    else
    {
        plugin_save->setEnabled(true);
        plugin_mdfs->setEnabled(true);
        plugin_test->setEnabled(true);
    }
}
