#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T08:44:48
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#! [0]
TEMPLATE        = lib
CONFIG         += shared
CONFIG         += plugin

SOURCES += \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/multidimselection.cpp \
    ../MzShared/progress.cpp \
    ../MzShared/selectionlist.cpp \
    ../MzShared/testdialog.cpp \
    pluginworker.cpp \
    rdaselection.cpp \
    ../MzShared/classifierio.cpp \
    rdaoptions.cpp \
    rdaplugin.cpp


HEADERS +=\
    ../MzShared/dataforconsole.h \
    ../MzShared/mzselectioninterface.h \
    ../MzShared/multidimselection.h \
    ../MzShared/progress.h \
    ../MzShared/selectionlist.h \
    ../MzShared/testdialog.h \
    pluginworker.h \
    rdaselection.h \
    rdaoptions.h \
    rdaplugin.h \
    ../MzShared/classifierio.h

TARGET          = $$qtLibraryTarget(HebPlugin)
DESTDIR         = ../../Executables
#! [0]

FORMS += \
    ../MzShared/progress.ui \
    ../MzShared/selectionlist.ui \
    ../MzShared/testdialog.ui \
    rdaoptions.ui

include(../Pri/config.pri)
include(../Pri/alglib.pri)
