#ifndef TUTORIALPLUGIN_H
#define TUTORIALPLUGIN_H

#include <QtGlobal>
#include <QObject>
#include <QtPlugin>
#include <QTextStream>
#include <QMessageBox>

#include "../MzShared/mzselectioninterface.h"
#include "../MzShared/progress.h"
#include "pluginworker.h"

class RadPlugin : public QObject, MzSelectionPluginInterface
{
    Q_OBJECT

// The ifdef does not work here !!!???
// #ifdef Q_PLUGIN_METADATA
    //Q_PLUGIN_METADATA(IID "MzSelectionPluginInterface" FILE "RdaPlugin.json")
//#endif
    Q_INTERFACES(MzSelectionPluginInterface)

public:
    RadPlugin();
    ~RadPlugin();
    QString getName(void);
    bool initiateMapsPlugin(QObject* parent, QMenu* plugin_menu);
    bool initiateTablePlugin(QObject* parent, QMenu* plugin_menu);
    bool openFile(QString filename);

public slots:
    void on_menuTest_triggered();
    void on_menuTest_finished(bool success);
    void on_menuProject_triggered();
    void on_menuProject_finished(bool success);
    void on_menuMdfs_triggered();
    void on_menuMdfs_finished(bool success);
    void on_menuSelect_triggered();
    void on_menuSelect_finished(bool success);
    void on_menuOptions_triggered();
    void on_menuSave_triggered();
    void on_menuLoad_triggered();

private:
    QAction* plugin_save;
    QAction* plugin_mdfs;
    QAction* plugin_test;

    bool startThreadIn(void);
    bool startThreadOut(const char *analysisSlot, const char *finishSlot, quint64 maxSteps, int timerestriction, unsigned int sizeQtable, unsigned int sizeQsorted, bool resetClassifier);
    void stopThreadIn(void);
    void stopThreadOut(void);

    double* Qtable;
    unsigned int* Qsorted;
    DataForSelection* data;
    Classifiers* currentclassifier;
    PluginWorker* worker;
    MzPullDataInterface* pull_data;
    Progress* progressdialog;
    int discriminant;
    int dimensionsrestriction;
    int timerestriction;
    QString classname;
    int mode_index;
};

extern "C"
{
    Q_DECL_EXPORT void* MzNewPluginObject(void);
    Q_DECL_EXPORT void MzDeletePluginObject(void* object);
}

//class PluginRoot : public QObject, MzSelectionPluginInterfaceRoot
//{
//    Q_OBJECT

//// The ifdef does not work here !!!???
//// #ifdef Q_PLUGIN_METADATA
//    Q_PLUGIN_METADATA(IID "MzSelectionPluginInterfaceRoot" FILE "HebPlugin.json")
////#endif
//    Q_INTERFACES(MzSelectionPluginInterfaceRoot)

//public:
//    MzSelectionPluginInterface* createInstance(void)
//    {
//        return qobject_cast<MzSelectionPluginInterface*>(new RadPlugin);
//    }
//};


#endif // TUTORIALPLUGIN_H
