/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TUTORIALPLUGIN_H
#define TUTORIALPLUGIN_H

//#include <QtGlobal>
//#include <QObject>
//#include <QThread>
//#include <QTextStream>
#include "../MzShared/mzselectioninterface.h"
// #include "pluginworker.h"
#include "svmselection.h"
//enum LdaNormalization {LN_NONE = 0, LN_STD = 1, LN_MINMAX = 2};

class SvmPlugin : public MzSelectionPluginInterface, SvmSelectionReduction
{
public:
    void NotifyProgressStep(void);
    void NotifyProgressText(std::string text);
    void NotifyProgressStage(const int notification, const int dimensionality,
                             const double value, const unsigned int* selected);

    SvmPlugin();
    ~SvmPlugin();
    const char* getName(void);
    bool initiateTablePlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools);
    bool initiateMapsPlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools);
    void callBack(const unsigned int index);
    bool openFile(std::string* filename);
    void cancelAnalysis(void);

    bool before_training_this(void);
    static bool before_training(void* object);
    void thread_training_this(void);
    static void thread_training(void* object,
                                 void *notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier);
    void after_training_this(void);
    static void after_training(void* object);


    bool before_test_this(void);
    static bool before_test(void* object);
    void thread_test_this(void);
    static void thread_test(void* object,
                                 void *notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier);
    void after_test_this(void);
    static void after_test(void* object);


    bool before_segmentation_this(void);
    void thread_segmentation_this();
    void after_segmentation_this(void);
    static bool before_segmentation(void* object);
    static void thread_segmentation(void* object, void* notifier_object,
                                     MzStepNotificationFunction step_notifier,
                                     MzTextNotificationFunction text_notifier);
    static void after_segmentation(void* object);

    void on_menuAbout_triggered();
    void on_menuTest_triggered();
    void on_menuTrain_triggered();
    void on_menuSave_triggered();
    void on_menuLoad_triggered();
    void on_menuSegment_triggered();

private:
    bool success;
    void* plugin_save;
    void* plugin_test;
    bool SetMachineLearningOptions(void);
//    bool SelectClassifiersOptions(void);

//    bool newTempClassifier(void);
//    bool setTempClassifier(void);
//    bool releaseTempClassifier(void);
//    bool setClassifierFromTemp(void);

    bool startThreadIn(void);
    bool startThreadIn(std::vector<std::string>* featureNames);
    void stopThreadIn(void);
    void stopThreadOut(void);

    int normalizeoption;
    void* notifier_object;
    MzStepNotificationFunction step_notifier;
    MzTextNotificationFunction text_notifier;

    typedef void(SvmPlugin::*OnActionFunctionPointer)(void);
    MzPullDataInterface* pull_data;
    MzGuiRelatedInterface* gui_tools;
    std::vector <OnActionFunctionPointer> onActionTable;
    void* connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function);
};

extern "C"
{
    MZ_DECL_EXPORT void* MzNewPluginObject(void);
    MZ_DECL_EXPORT void MzDeletePluginObject(void* object);
}

#endif // TUTORIALPLUGIN_H
