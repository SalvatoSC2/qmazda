cmake_minimum_required(VERSION 3.1)

project(SvmPlugin LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

add_library(SvmPlugin SHARED
    ../MzShared/dataforconsole.cpp
    ../MzShared/csvio.cpp
    ../MzShared/multidimselection.cpp
    svmplugin.cpp
    ../MzShared/classifierio.cpp
    svmselection.cpp
    libsvm/svm.cpp
)

install(TARGETS SvmPlugin DESTINATION bin)
