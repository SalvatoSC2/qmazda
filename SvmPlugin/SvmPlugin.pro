#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T08:44:48
#
#-------------------------------------------------

#QT       += core

CONFIG -= app_bundle
CONFIG -= qt

TEMPLATE        = lib
CONFIG         += shared
CONFIG         += plugin

TARGET          = $$qtLibraryTarget(SvmPlugin)
DESTDIR         = ../../Executables

SOURCES += \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp \
    ../MzShared/multidimselection.cpp \
    svmplugin.cpp \
    ../MzShared/classifierio.cpp \
    svmselection.cpp \
    libsvm/svm.cpp

HEADERS +=\
    ../MzShared/dataforconsole.h \
    ../MzShared/mzselectioninterface.h \
    ../MzShared/multidimselection.h \
    svmplugin.h \
    ../MzShared/classifierio.h \
    svmselection.h \
    libsvm/svm.h

include(../Pri/config.pri)

