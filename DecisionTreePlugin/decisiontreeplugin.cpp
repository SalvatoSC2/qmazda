/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//#include <QtGlobal>
#include "../MzShared/mzdefines.h"

#include "decisiontreeplugin.h"
#include "../MzShared/multidimselection.h"

#include <string>
#include <sstream>
#include <stdlib.h>
#include <limits>
#include <time.h>

void* MzNewPluginObject(void)
{
    return new DecisionTreePlugin();
}

void MzDeletePluginObject(void* object)
{
    delete static_cast<DecisionTreePlugin *> (object);
}

void DecisionTreePlugin::NotifyProgressStep(void)
{
    (*step_notifier)(notifier_object);
}
void DecisionTreePlugin::NotifyProgressText(std::string text)
{
    (*text_notifier)(notifier_object, text);
}
void DecisionTreePlugin::NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected)
{
    time_t currenttime = time(NULL);
    char* ct = ctime(&currenttime);
    strtok(ct, "\n\r");
    char text[256];

    switch(notification)
    {
    case BEGINS: sprintf(text, "Begins %iD at %s\n", dimensionality, ct); (*text_notifier)(notifier_object, text); break;
    case STOPPED: sprintf(text, "Stopped %.2f%%\n", (float)(value*100.0)); (*text_notifier)(notifier_object, text); break;
    case CANCELED: sprintf(text, "Canceled\n"); (*text_notifier)(notifier_object, text); break;
    case COMPLETED: sprintf(text, "Completed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case FAILED: sprintf(text,"Failed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case SUCCESS:
        sprintf(text,"D = %i Q = %f\n", dimensionality, (float)value);
        (*text_notifier)(notifier_object, text);
        break;
    }
}

bool DecisionTreePlugin::before_test_this(void)
{
    std::vector<std::string> featureNames = getFeatureNames();
    if(!startThreadIn(&featureNames))
        return false;
    featureNames.clear();
    mz_uint64 maxSteps = data->vectornumber;
    gui_tools->openProgressDialog(0, maxSteps, this, "Linear classifier test");
    return true;
}

void DecisionTreePlugin::thread_test_this()
{
    breakanalysis = false;

    vector<string> classnames;
    classnames = getClassNames();
    vector<string> featurenames;
    featurenames = getFeatureNames();
    unsigned int classcount = classnames.size();
    unsigned int cfeaturecount = featurenames.size();
    if(classcount < 1 || cfeaturecount < 1)
    {
        success = false;
        return;
    }
    featurenames.clear();
    classcount++;

    for(int f = 0; f < data->featurenumber; f++)
    {
        featurenames.push_back(data->featurenames[f]);
    }
    if(!configureForClassification(&featurenames))
    {
        success = false;
        return;
    }

    unsigned int* fire_table = new unsigned int[data->classnumber*classcount];
    memset(fire_table, 0, sizeof(unsigned int)*(data->classnumber*classcount));

    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        int vs = ccc > 0 ? data->classendvectorindex[ccc-1] : 0;
        int vm = data->classendvectorindex[ccc];
        for(int v = vs; v < vm; v++)
        {
            double* ptri = data->values + (data->featurenumber * v);
            unsigned int cc = classifyFeatureVector(ptri);
            if(cc >= classcount)
                cc = 0;
            fire_table[cc + classcount * ccc]++;
            NotifyProgressStep();
        }
    }

    std::stringstream ss;
    for(unsigned int cc = 0; cc < classcount-1; cc++)
        ss << "\t" << classnames[cc];
    ss << "\t!" << std::endl;

    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        ss << data->classnames[ccc];
        for(unsigned int cc = 1; cc < classcount; cc++)
        {
            ss << "\t" << fire_table[cc + classcount * ccc];
        }
        ss << "\t" << fire_table[classcount * ccc] << std::endl;
    }
    confusionMatrixText = ss.str();
    delete[] fire_table;

    success = true;
}
void DecisionTreePlugin::after_test_this(void)
{
    stopThreadIn();
    if(success)
    {
        gui_tools->showTestResults(&confusionMatrixText, "Test results");
    }
    else
    {
        gui_tools->showMessage("Error", "Test failed. Features required by the classifier may be missing.", 3);
    }
    stopThreadOut();
}
bool DecisionTreePlugin::before_test(void* object)
{
    return ((DecisionTreePlugin*)object)->before_test_this();
}
void DecisionTreePlugin::thread_test(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((DecisionTreePlugin*)object)->notifier_object = notifier_object;
    ((DecisionTreePlugin*)object)->step_notifier = step_notifier;
    ((DecisionTreePlugin*)object)->text_notifier = text_notifier;
    ((DecisionTreePlugin*)object)->thread_test_this();
}
void DecisionTreePlugin::after_test(void* object)
{
    ((DecisionTreePlugin*)object)->after_test_this();
}
void DecisionTreePlugin::on_menuTest_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_test, &thread_test, &after_test);
}



bool DecisionTreePlugin::before_segmentation_this(void)
{
    vector<string> classnames;
    classnames = getClassNames();
    vector<string> featurenames;
    featurenames = getFeatureNames();
    unsigned int classcount = classnames.size();
    unsigned int featurecount = featurenames.size();
    if(classcount < 1 || featurecount < 1)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }


    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();

    for(unsigned int k = 0; k < featurecount; k++)
    {
        dataMap.featurenames.push_back(featurenames[k]);
    }
    dataMap.resultnames.push_back("DecisionTree");

    if(! pull_data->getData(&dataMap))
    {
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    gui_tools->openProgressDialog(0, dataMap.vectornumber, this, "DecisionTree segmentation");

    if(! configureForClassification(& dataMap.featurenames))
    {
        return false;
    }
    return true;
}

void DecisionTreePlugin::thread_segmentation_this()
{
    segmentImage(dataMap.vectornumber, dataMap.values, dataMap.result[0]);
}
void DecisionTreePlugin::after_segmentation_this(void)
{
    stopThreadIn();
    stopThreadOut();
}
bool DecisionTreePlugin::before_segmentation(void* object)
{
    return ((DecisionTreePlugin*)object)->before_segmentation_this();
}
void DecisionTreePlugin::thread_segmentation(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((DecisionTreePlugin*)object)->notifier_object = notifier_object;
    ((DecisionTreePlugin*)object)->step_notifier = step_notifier;
    ((DecisionTreePlugin*)object)->text_notifier = text_notifier;
    ((DecisionTreePlugin*)object)->thread_segmentation_this();
}
void DecisionTreePlugin::after_segmentation(void* object)
{
    ((DecisionTreePlugin*)object)->after_segmentation_this();
}
void DecisionTreePlugin::on_menuSegment_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_segmentation, &thread_segmentation, &after_segmentation);
}




void DecisionTreePlugin::cancelAnalysis(void)
{
    breakanalysis = true;
}

DecisionTreePlugin::DecisionTreePlugin()
    :DecisionTreeClassifier()
{
    pull_data = NULL;
    gui_tools = NULL;
    plugin_test = NULL;
    data = NULL;
    breakanalysis = false;
}

DecisionTreePlugin::~DecisionTreePlugin()
{
}

const char* DecisionTreePlugin::getName(void)
{
    return "Decision tree";
}



bool DecisionTreePlugin::startThreadIn(void)
{
    if(data != NULL) delete data;
    data = NULL;
    data = new DataForSelection();
    pull_data->getData(data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    return true;
}

bool DecisionTreePlugin::startThreadIn(std::vector<std::string>* featureNames)
{
    if(data != NULL) delete data;
    data = new DataForSelection();
    pull_data->getData(featureNames, data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    return true;
}

void DecisionTreePlugin::stopThreadIn(void)
{
    gui_tools->closeProgressDialog();
}

void DecisionTreePlugin::stopThreadOut(void)
{
    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();
    if(data != NULL) delete data;
    data = NULL;
//    gui_tools->menuEnable(plugin_test, true);
}

void* DecisionTreePlugin::connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function)
{
    void* ret = gui_tools->addMenuAction(name, tip, onActionTable.size());
    if(name != NULL && function != NULL)
        onActionTable.push_back(function);
    return ret;
}

bool DecisionTreePlugin::initiateTablePlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    connectMenuAction("Load classifier...", "Load rules to classify data", &DecisionTreePlugin::on_menuLoad_triggered);
    //connectMenuAction(NULL, NULL, NULL);
    plugin_test = connectMenuAction("Test classifier...", "Test classifier performance", &DecisionTreePlugin::on_menuTest_triggered);
    //connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &DecisionTreePlugin::on_menuAbout_triggered);
    stopThreadOut();
    return true;
}

bool DecisionTreePlugin::initiateMapsPlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    connectMenuAction("Load classifier...", "Load rules to classify data", &DecisionTreePlugin::on_menuLoad_triggered);
    //connectMenuAction(NULL, NULL, NULL);
    plugin_test = connectMenuAction("Segment image...", "Image segmentation by the linear classifier", &DecisionTreePlugin::on_menuSegment_triggered);
    //connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &DecisionTreePlugin::on_menuAbout_triggered);
    stopThreadOut();
    return true;
}

void DecisionTreePlugin::callBack(const unsigned int index)
{
    if(index < onActionTable.size())
        if(onActionTable[index] != NULL)
            (this->*(onActionTable[index]))();
}

void DecisionTreePlugin::on_menuAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>qmazda DecisionTreePlugin</h2> " << std::endl;
    ss << QMAZDA_VERSION << "<br>" << std::endl;
    ss << QMAZDA_COPYRIGHT << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." << std::endl;
    ss << "Home: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html </a> <br>" << std::endl;
    ss << "Sources: <a href=\"https://gitlab.com/qmazda/qmazda\"> https://gitlab.com/qmazda/qmazda </a> <br>" << std::endl;
    gui_tools->showAbout("About DecisionTreePlugin", ss.str().c_str());
}

bool DecisionTreePlugin::openFile(string *filename)
{
    return loadClassifierFromFile(filename->c_str());
}

void DecisionTreePlugin::on_menuLoad_triggered()
{
    std::string fileName;
    if(gui_tools->getOpenFile(&fileName))
    {
        if(! loadClassifierFromFile(fileName.c_str()))
        {
            gui_tools->showMessage("Error", "Failed to load classifier", 3);
        }
    }
    stopThreadOut();
}
