/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013-2019  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DecisionTree_H
#define DecisionTree_H
#include "../MzShared/dataforconsole.h"
#include "../MzShared/classifierio.h"
#include "dectree.h"

const char DecisionTreeClassifierName[] = "MzDecisionTreeClassifier2019";

class DecisionTreeClassifier : public ClassifierAccessInterface, DecisionTree
{
public:
    DecisionTreeClassifier();
    ~DecisionTreeClassifier();
    const char* getClassifierMagic(void){return DecisionTreeClassifierName;}
    bool loadClassifierFromFile(const char* filename);
    bool configureForClassification(vector<string>* input_feature_names);
    unsigned int classifyFeatureVector(double* featureVector);
    void segmentImage(unsigned int vectornumber, MazdaMapPixelType **values, MazdaMapPixelType *result);
    std::vector<std::string> getFeatureNames(void);
    std::vector<std::string> getClassNames(void);

    std::string Serialize(const char indentChar)
    {
        return serialize(indentChar);
    }


private:
    unsigned int input_feature_names_count;
    double* input_feature_names_buffer;
    std::vector<std::string> class_names;

};

#endif // DecisionTree_H
