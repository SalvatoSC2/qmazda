#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T08:44:48
#
#-------------------------------------------------

#QT       += core
#QT       += gui
#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG -= app_bundle
CONFIG -= qt

TEMPLATE        = lib
CONFIG         += shared
CONFIG         += plugin

TARGET          = $$qtLibraryTarget(DecTreePlugin)
DESTDIR         = ../../Executables

SOURCES += \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp \
    ../MzShared/multidimselection.cpp \
    decisiontreeplugin.cpp \
    dectree.cpp \
    ../MzShared/classifierio.cpp \
    ../LdaPlugin/ldaselection.cpp \
    ../VschPlugin/vschselection.cpp \
    ../MzShared/parametertreetemplate.cpp \
    ../SvmPlugin/svmselection.cpp \
    ../SvmPlugin/libsvm/svm.cpp \
    decisiontree.cpp

HEADERS +=\
    ../MzShared/dataforconsole.h \
    ../MzShared/mzselectioninterface.h \
    ../MzShared/multidimselection.h \
    decisiontreeplugin.h \
    dectree.h \
    ../MzShared/classifierio.h \
    decisiontree.h

include(../Pri/config.pri)
include(../Pri/alglib.pri)
include(../Pri/qhull.pri)
