/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOGFEATUREOPTIONS_H
#define DIALOGFEATUREOPTIONS_H

#include <QDialog>
#include <QTreeWidgetItem>
#include "../MzShared/parametertreetemplate.h"
#include "../MzShared/parametertreewidget.h"

namespace Ui {
class DialogFeatureOptions;
}

class DialogFeatureOptions : public QDialog
{
    Q_OBJECT

public:
    explicit DialogFeatureOptions(NameStubTree* roiFeaturesTemplate, NameStubTree* localFeaturesTemplate, QWidget *parent = 0);
    ~DialogFeatureOptions();

    static std::vector<std::string> loadFeatureList(const char *filename);
    static bool saveFeatureList(const char *filename, QStringList *list);

    void setFeatureListRoi(QStringList* list);
    void setFeatureListLocal(QStringList* list);
    void getFeatureListRoi(QStringList* list);
    void getFeatureListLocal(QStringList* list);
protected:
    void dragEnterEvent(QDragEnterEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);
    void dropEvent(QDropEvent* event);

private slots:
//    void on_toolButtonReset_clicked();
//    void on_toolButtonInfo_clicked();
    void on_toolButtonPlus_clicked();
    void on_toolButtonMinus_clicked();
    void on_toolButtonExpand_clicked();

//    void on_treeWidgetRoi_itemChanged(QTreeWidgetItem *item, int column);

//    void on_treeWidgetLocal_itemChanged(QTreeWidgetItem *item, int column);
    void loadOptions(const char *filename);

    void on_toolButton_Load_clicked();

    void on_toolButton_Save_clicked();

private:
    Ui::DialogFeatureOptions *ui;
//    NameStubTree* roiFeaturesTemplate;
//    NameStubTree* localFeaturesTemplate;

//    void removeItem(QTreeWidgetItem* item);
//    void removeUnmatchedChildren(QTreeWidgetItem* item, NameStubTree* featuresTemplate);
//    void addMatchedChildren(QTreeWidgetItem* item, NameStubTree* featuresTemplate, ParameterTreeWidget *treeWidget);

};

#endif // DIALOGFEATUREOPTIONS_H
