/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RANDOMNAMES_H
#define RANDOMNAMES_H
#include <string>
#include <vector>

class RandomNames
{
public:
    RandomNames();
    ~RandomNames();
    std::string getTempName(std::string prefix, std::string sufix, bool toRemove = true);
    void deleteFile(std::string fname);

private:
    std::vector <std::string> filesToremove;
};

extern RandomNames randomNames;

#endif // RANDOMNAMES_H
