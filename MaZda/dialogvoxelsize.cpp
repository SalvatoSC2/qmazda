/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dialogvoxelsize.h"
#include "ui_dialogvoxelsize.h"

DialogVoxelSize::DialogVoxelSize(double *voxels, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogVoxelSize)
{
    voxelsize = voxels;
    ui->setupUi(this);

    ui->voxelSizeX->setText(QString::number(voxelsize[0]));
    ui->voxelSizeY->setText(QString::number(voxelsize[1]));
    ui->voxelSizeZ->setText(QString::number(voxelsize[2]));

    //voxelsize[0] = ui->voxelSizeX->text().toDouble();
    // = getParameter(QString("multiscaleVesselness"), QString("MinStd")).toFloat();
}

DialogVoxelSize::~DialogVoxelSize()
{
    delete ui;
}

void DialogVoxelSize::on_voxelSizeX_editingFinished()
{
    voxelsize[0] = ui->voxelSizeX->text().toDouble();
    if(voxelsize[0] < 0.00000001) voxelsize[0] = 1.0;
    ui->voxelSizeX->setText(QString::number(voxelsize[0]));
}

void DialogVoxelSize::on_voxelSizeY_editingFinished()
{
    voxelsize[1] = ui->voxelSizeY->text().toDouble();
    if(voxelsize[1] < 0.00000001) voxelsize[1] = 1.0;
    ui->voxelSizeY->setText(QString::number(voxelsize[1]));
}

void DialogVoxelSize::on_voxelSizeZ_editingFinished()
{
    voxelsize[2] = ui->voxelSizeZ->text().toDouble();
    if(voxelsize[2] < 0.00000001) voxelsize[2] = 1.0;
    ui->voxelSizeZ->setText(QString::number(voxelsize[2]));
}
