/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "renderarea.h"

RenderArea::RenderArea(QWidget *parent) :
    QScrollArea(parent)
{
}

void RenderArea::wheelEvent(QWheelEvent *ev)
{
    if(widget() == NULL) return;
    Renderer2D* renderer2d = qobject_cast<Renderer2D*>(widget());
    if(renderer2d != NULL)
    {
        if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        {
            renderer2d->wheelEvent(ev);
        }
        else
        {
            QScrollArea::wheelEvent(ev);
        }
    }
    else
    {
        Renderer3D* renderer3d = qobject_cast<Renderer3D*>(widget());
        if(renderer3d != NULL) renderer3d->wheelEvent(ev);
    }
}

void RenderArea::wheelEventSuperclass(QWheelEvent *ev)
{
    QScrollArea::wheelEvent(ev);
}

