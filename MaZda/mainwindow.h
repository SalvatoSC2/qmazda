/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../MzShared/mzdefines.h"

#include <QMainWindow>
#include <QListWidgetItem>
#include <QScrollArea>
#include <QScrollBar>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QListWidget>
#include <QComboBox>
#include <QSlider>
#include <QSpinBox>
#include <QFileDialog>
#include <QMessageBox>
#include <QProcess>
#include <QTextStream>
#include <QPainter>
#include <QColorDialog>
#include <QToolButton>

#include <QDial>
//#include <QTemporaryFile>

#include "renderarea.h"
#include "painter2d.h"
#include "painter3d.h"
#include "getsetslice.h"

#include "dockeditoptions.h"


//typedef enum {UNKNOWN, NONEXISTING_NAME, NONEXISTING_COMBO_ITEM, NONEXISTING_SPIN_VALUE, FEATURE_ADDED, FEATURE_GROUP_ADDED, COMBO_BRANCH_ADDED, SPIN_BRANCH_ADDED, BRANCH_ADDED} OptionParserResult;

const int histogram_size = 256;
const int feature_names_length = 12;
const int combo_names_length = 16;
const int undo_roi_levels = 8;

typedef MultiChannelPixel <MazdaImagePixelType, 3> PixelRGBType;
typedef MazdaImage<MazdaImagePixelType, 2> MIType2D;
typedef MazdaImage<PixelRGBType , 2> MIType2DRGB;
typedef MazdaImage<MazdaImagePixelType, 3> MIType3D;
typedef MazdaImage<PixelRGBType , 3> MIType3DRGB;



namespace Ui {
    class MainWindow;
}

//class MainWindowEventHandler : public QObject
//{
//    Q_OBJECT
//protected:
//    bool eventFilter(QObject* obj, QEvent* e);
//};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

    bool loadRoi(const QString fileName, bool erase = false);
    bool loadRoiAsk(const QString fileName, bool erase = false);
    bool saveRoi(const char *fileName);
    bool saveImage(const char *fileName);

    bool loadImage(const QString *fileName);
    bool loadImages(const QStringList* fileNames);


    //bool loadOptions(QString* fileName);
protected:
    void dragEnterEvent(QDragEnterEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);
    void dropEvent(QDropEvent* event);

public slots:
public slots:
//    void keyPressEvent(QKeyEvent*);
//    void keyReleaseEvent(QKeyEvent*);

    void setZoomRequest(double zoom);
    void setRoiListRequest(void);
    void setStatusTextRequest(QString* txt);

//    void setRedoUndoRequest(void);
    //void enableUndoRedoOptions(bool u, bool r);

private slots:
//    void contour_nodes_Changed(int i);
//    void contour_neighborhood_Changed(int i);
//    void contour_gradient_Changed(int i);
//    void contour_tension_Changed(int i);
//    void morphology_depth_Changed(int i);
//    void line_thickness_Changed(int i);
//    void aangle_spinb_Changed(int i);
//    void resize_spinb_Changed(int i);
//    void stretch_spinb_Changed(int i);
//    void sangle_spinb_Changed(int i);
//    void contour_mode_Changed(int i);
//    void morphology_mode_Changed(int i);
//    void morphology_shape_Changed(int i);
    void zoomSliderValueChanged(int value);
    void zoomResetClicked();
    void on_actionLoadImage_triggered();
    void on_actionZoom_in_triggered();
    void on_actionZoom_out_triggered();
    void on_actionZoom_1_triggered();
    void on_actionLoadRoi_triggered();
    void on_actionPencil_triggered();
    void on_actionMove_triggered();
    void on_actionFloodFill_triggered();
    void on_actionRectangle_triggered();
    void on_actionElipse_triggered();
    void on_actionActiveContour_triggered();
    void on_actionLine_triggered();
    void on_actionErase_triggered();
    void on_addRoiButton_clicked();
    void on_removeRoiButton_clicked();
    void on_changeRoiVisibilityButton_clicked();
    void on_comboBox_currentIndexChanged(const QString &arg1);
    void on_imageLevelsButton_clicked();
    void on_imageOffButton_clicked();
    void on_ImageGrayButton_clicked();
    void on_horizontalSliderMin_valueChanged(int value);
    void on_horizontalSliderMax_valueChanged(int value);
//    void on_addOptionsButton_clicked();
//    void on_removeOptionsButton_clicked();
//    void on_defaultOptionsButton_clicked();
//    void on_expandCollapseButton_clicked();
//    void on_actionSaveOptions_triggered();
//    void on_actionLoadOptions_triggered();
//    void on_optionsInfoButton_clicked();
    void on_toolButton_clicked();
    void on_actionEraseRegion_triggered();
    void on_roiListWidget_itemClicked(QListWidgetItem *item);
    void on_roiListWidget_itemChanged(QListWidgetItem *item);
    void on_roiListWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);
    void on_actionSaveRoi_triggered();
    void on_actionRoi_from_sliders_triggered();
    void on_actionOpen_triggered();
    void on_actionClose_triggered();
    void on_actionDilate_triggered();
    void on_actionErode_triggered();
    void on_actionMedian_triggered();
    void on_actionSplit_roi_triggered();
//    void on_zoomReset_clicked();
//    void on_horizontalSliderZoom_valueChanged(int value);
    void on_actionRoi_based_computation_triggered();
    void on_actionMap_computation_triggered();
    void on_actionPoint_driven_computation_triggered();
    bool on_actionSaveImage_triggered();
    void on_actionRemove_small_triggered();
    void on_actionUndo_triggered();
    void on_actionRedo_triggered();
    void on_actionAbout_triggered();
    void on_actionContents_triggered();
    void on_actionRender_2D_triggered(bool checked);
    void on_actionRender_3D_triggered(bool checked);
    void on_actionBackground_color_triggered();
    void on_spinBox_X_valueChanged(int arg1);
    void on_spinBox_Y_valueChanged(int arg1);
    void on_spinBox_Z_valueChanged(int arg1);
//    void on_toolButton_Alfa_toggled(bool checked);
//    void on_horizontalSlider_Alfa_valueChanged(int value);
    void on_checkBox_X_clicked(bool checked);
    void on_checkBox_Y_clicked(bool checked);
    void on_checkBox_Z_clicked(bool checked);
//    void on_toolButton_Alfa_clicked();
    void on_toolButton_Alfa_clicked(bool checked);
    void on_toolButton_Afla_clicked(bool checked);
    void on_actionImage_info_triggered();
    void on_toolButton_Color_clicked();

    void on_actionMerge_visible_triggered();

//    void on_actionOptionsTools_triggered();

    void on_actionFeatureOptions_triggered();

    void on_actionMaZda_new_instance_triggered();

    void on_actionMap_viewer_triggered();

    void on_actionReport_editor_triggered();

    void on_actionColor_overlay_triggered();

    void on_actionGray_levels_triggered();

    void on_actionText_triggered();

    void on_actionVoxel_size_triggered();

    void on_actionExit_triggered();

private:

    NameStubTree roiFeaturesTemplate;
    NameStubTree localFeaturesTemplate;
    QStringList featureListRoi;
    QStringList featureListLocal;

    DockEditOptions* dockEditOptions;
//    ImageUtils* imageutils;
    std::string imageInfo;
    RenderArea* renderArea;
    RenderingContainer2D* container2d;
    RenderingContainer3D* container3d;
    RenderingContainerInterface* container;
    GetSetSlice getSetSlice;
    Painter2D* renderer2d;
    Painter3D* renderer3d;
//    bool setZoomRequested;
//    RendererInterface* renderer;
    void initializeFeatures(void);


    void onMorphology(int mode);
    //void updateRenderer(void);
    void updateXYZASliders(void);
    void switchRender2D(void);
    void switchRender3D(void);

    void rendererRepaint(void);
    void adjustScrollBar(QScrollBar *scrollBar, double factor);


//    void defaultTextureOptionsSubBranch(QTreeWidgetItem *parent);
    //QPalette createRoiPalette(QColor color);
    void setColorToRoiItem(QListWidgetItem *item, QColor color);

    void redrawHistogram(void);
//    void createDrawingToolsTree(void);
    void uncheckDrawingTools(QAction* checked, DrawingTool tool = DT_NONE);
    //void setEmptyOptions(void);
//    OptionParserResult addBranchOrOption(QString fromgen, QTreeWidgetItem* parent_item, QTreeWidgetItem** created_item = NULL);
//    OptionParserResult addBranchOrOption(QTreeWidgetItem* current, QTreeWidgetItem** item = NULL);



//    void insertLeafOfName(QString name, int root);
//    void getUniqueNames(QStringList* list, QTreeWidgetItem* branch);
//    void createMethodsSubtree(QTreeWidgetItem *parent_item, int method_index);
    //OptionParserResult selectComboOptionFirstCharMatch(QTreeWidgetItem *parent_item, QString* stub);
//    OptionParserResult createMethodsComboOrOption(QTreeWidgetItem *parent_item, int method_index, QString* stub = NULL, QTreeWidgetItem** item = NULL);
//    OptionParserResult createFeatureOrFeaturesGroup(QTreeWidgetItem *parent_item, const char feature_names[][feature_names_length], int features_number, QString *stub = NULL, QTreeWidgetItem** new_item = NULL);
    //OptionParserResult createOptionsCombo(QTreeWidgetItem *parent_item, QString branch_name, const char combo_names[][combo_names_length], int combo_items_number, QTreeWidgetItem** new_item = NULL, int current = 0, QString* current_str = NULL);
//    OptionParserResult createMethodsCombo(QTreeWidgetItem *parent_item, char method_names[][combo_names_length], int methods_number, QTreeWidgetItem** new_item = NULL, int current = 0, QString* current_str = NULL);
//    OptionParserResult createOptionsSpinBox(QTreeWidgetItem *parent_item, QString branch_name, int min, int value, int max, QTreeWidgetItem** new_item);
//    void updatePrefixesAndNamesRecurrent(QTreeWidgetItem* branch);
    //void removeInvalidNameRecurrent(QTreeWidgetItem* branch, int mode);
//    bool queryGenerator(QString* result, QString featurename, int mode);
    bool queryGenerator(QString* result, const char* mode);

//    void validateNamesUniqueness(bool show = false);
//    bool namesToTree(QString mode, QTextStream *names, QString* tree);
//    QTreeWidgetItem* insertTreeItem(QString* nameplus, QTreeWidgetItem* parent);
//    void insertTreeItems(QTextStream *stream, QTreeWidgetItem* parent);
//    void clearOptions(void);
//    int getRootParentOfAnItem(QTreeWidgetItem** item);
//    void removeChildrenOfAnItem(QTreeWidgetItem* item);
//    void getFeatureNameCandidatesFromChildrenRecurently(QTreeWidgetItem* branch, QString upperstubtext, QString *textstream);
//    void addInitialItemToTheBranch(QTreeWidgetItem* current);
//    void updateItemName(QTreeWidgetItem* item);
//    void getItemStubOrName(QTreeWidgetItem* item, QString* stub, QString* name);
//    void setLeafNameFromStub(QTreeWidgetItem* item, QString stub);



    void recreateRoiControls(void);
//    void loadOptionsPrivate(QString *fileName);
//    void saveOptions(QString fileName);

    Ui::MainWindow *ui;
    int histogram[histogram_size];
//    QString tempFtsFile;
//    QString tempImgFile;
//    QString tempRoiFile;
//    QStringList undoRoiFiles2d;
//    QStringList undoRoiFiles3d;
//    void setTemporatyFileNames(void);
//    void deleteTemporatyFileNames(void);

    QSlider zoomSlider;
    QToolButton zoomReset;

//    QTreeWidgetItem* draw_filled;
//    QTreeWidgetItem* draw_transform;
//    QTreeWidgetItem* draw_active;

//    QComboBox* contour_mode;
//    QSpinBox* contour_nodes;
//    QSlider* contour_neighborhood;
    //QSlider* contour_size;
    //QSlider* contour_threshold;
    //QSlider* contour_grey_level;
//    QSlider* contour_gradient;
//    QSlider* contour_tension;

//    QComboBox* morphology_mode;
//    QComboBox* morphology_shape;
//    QSpinBox* morphology_depth;

//    QSpinBox* line_thickness;

//    QSlider* aangle_spinb;
//    QSlider* resize_spinb;
//    QSlider* stretch_spinb;
//    QSlider* sangle_spinb;

//    QTreeWidgetItem *generate_roi;
//    QTreeWidgetItem *generate_local;

    QString getExecutableWithPath(const char* name, bool use_quotation);


//    void setToolOptions(void);

//protected:
//    bool eventFilter(QObject* obj, QEvent* e);
};

#endif // MAINWINDOW_H
