/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "mzroidock.h"

MzRoiDock::MzRoiDock(const QString &title, QWidget *parent, Qt::WindowFlags flags) :
    QDockWidget(title, parent, flags){}

MzRoiDock::MzRoiDock(QWidget *parent, Qt::WindowFlags flags) :
    QDockWidget(parent, flags){}

void MzRoiDock::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}

void MzRoiDock::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}

void MzRoiDock::dragLeaveEvent(QDragLeaveEvent* event)
{
    event->accept();
}

void MzRoiDock::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();

    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();

        if(urlList.size() >= 1)
        {
            QString filename = urlList.at(0).toLocalFile();
            ((MainWindow*)parent())->loadRoiAsk(filename, event->possibleActions() & Qt::MoveAction);
            event->acceptProposedAction();
        }
    }
}
