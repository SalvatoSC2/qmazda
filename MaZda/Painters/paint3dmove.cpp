/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paint3dmove.h"

Paint3DMove::Paint3DMove(RenderingContainer3D *icontainer, Painter3D *ipainter, unsigned int list, GLdouble *modelMat, GLdouble *volumeMat, bool *ierase) :
    Paint3dBlock(icontainer, ipainter, list, modelMat, volumeMat, ierase)
{
    container = icontainer;
    renderer = ipainter;
    erase = ierase;
    unsigned int color = container->getActiveRoi()->GetColor();
    colorpixel.channel[2] = color&0xff; color >>= 8;
    colorpixel.channel[1] = color&0xff; color >>= 8;
    colorpixel.channel[0] = color&0xff;
    state = 0;
    this->modelMat = modelMat;
    this->volumeMat = volumeMat;
    this->list = list;
}

//AfterPaintAction Paint3DMove::imageDataChanged(void)
//{
//    return DONOTHING;
//}

//MzBounds Paint3DMove::getModifiedRegion()
//{
//    return cube;
//}

//AfterPaintAction Paint3DMove::mouseMoveEvent(QMouseEvent *ev)
//{
//    if(ev->buttons() & Qt::LeftButton)
//    {
//        if(QApplication::keyboardModifiers() & Qt::AltModifier)
//        {
//            int x = ev->x();
//            int y = ev->y();
//            GLdouble mat_1[16];
//            GLdouble matt[16];
//            GLdouble majj[16];
//            GLdouble makk[16];
//            MatrixUtils3D<GLdouble>::invert3x3(mat_1, volumeMat);
//            matt[1] = 0; matt[2] = 0; matt[3] = 0;
//            matt[4] = 0; matt[6] = 0; matt[7] = 0;
//            matt[8] = 0; matt[9] = 0; matt[10] = 1; matt[11] = 0;
//            matt[12] = 0; matt[13] = 0; matt[14] = 0; matt[15] = 1;
//            matt[0] = pow(1.002, (double)(x-mousex));
//            matt[5] = pow(1.002, (double)(mousey-y));
//            MatrixUtils3D<GLdouble>::multiply3x3(majj, modelMat, volumeMat);
//            MatrixUtils3D<GLdouble>::multiply3x3(makk, majj, matt);
//            MatrixUtils3D<GLdouble>::multiply3x3(modelMat, makk, mat_1);
//            mousex = x;
//            mousey = y;
//            return REDRAW;
//        }
//        else if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
//        {
//            int input[2];
//            input[0] = mousex;
//            input[1] = mousey;
//            GLdouble outputemp[3];
//            GLdouble outputr[3];
//            GLdouble output[3];
//            renderer->Coordinates2Dto3D(input, &(modelMat[12]), outputr, outputemp);
//            input[0] = mousex = ev->x();
//            input[1] = mousey = ev->y();
//            renderer->Coordinates2Dto3D(input, &(modelMat[12]), output, outputemp);

//            modelMat[12] += (output[0] - outputr[0]);
//            modelMat[13] += (output[1] - outputr[1]);
//            modelMat[14] += (output[2] - outputr[2]);
//            return REDRAW;
//        }
//        else
//        {
//            int x = ev->x();
//            int y = ev->y();
//            MatrixUtils3D<GLdouble>::rotatemodel((double)(x-mousex)/100.0, 0, 2, modelMat, volumeMat);
//            MatrixUtils3D<GLdouble>::rotatemodel((double)(mousey-y)/100.0, 1, 2, modelMat, volumeMat);
//            mousex = x;
//            mousey = y;
//            return REDRAW;
//        }
//        return DONOTHING;
//    }
//    return DONOTHING;
//}

//AfterPaintAction Paint3DMove::mousePressEvent(QMouseEvent *ev)
//{
//    mousex = ev->x();
//    mousey = ev->y();
//    if(state == 0)
//    {
//        updateShape();
//        state = 1;
//    }
//    return REDRAW;
//}

//AfterPaintAction Paint3DMove::mouseReleaseEvent(QMouseEvent *ev){return DONOTHING;}


AfterPaintAction Paint3DMove::changeShape(int /*delta*/, bool /*pressed*/)
{
    return DONOTHING;
}


void Paint3DMove::rasterizeBlock(void)
{
//    if(ev->buttons() != Qt::LeftButton) return DONOTHING;
//    glNewList(list, GL_COMPILE);
//    glEndList();
//    renderer->showModel(false);
    if(image == NULL)
        return;// CANCEL;
    RenderingContainer3D::MzRoi* roi = container->getActiveRoi();
    if(roi == NULL)
        return;// CANCEL;
    unsigned int roi_size[RenderingContainer3D::Dimensions];
    roi->GetSize(roi_size);
    int begin_roi[RenderingContainer3D::Dimensions];
    int end_roi[RenderingContainer3D::Dimensions];
    int begin_image[RenderingContainer3D::Dimensions];
    int end_image[RenderingContainer3D::Dimensions];

//    RenderingContainer3D::MazdaRGBPixelType8 colorpixel2;
//    colorpixel2.channel[0] = 22;
//    colorpixel2.channel[1] = 22;
//    colorpixel2.channel[2] = 122;

    roi->GetBegin(begin_roi);
    roi->GetEnd(end_roi);

    double pi[RenderingContainer3D::Dimensions];
    double po[RenderingContainer3D::Dimensions];

    for(int k = 0; k < 8; k++)
    {
        for(int d = 0; d < RenderingContainer3D::Dimensions; d++)
        {
            pi[d] = (k&(1<<d) ? (double)roi_size[d]/2.0 : -(double)roi_size[d]/2.0)*voxelsize[d];
        }
        Transform(pi, po, modelMat);
        ImageToVoxelSpace(po, po);
        if(k)
        {
            for(int d = 0; d < RenderingContainer3D::Dimensions; d++)
            {
                int rpd = round(po[d]);
                if(begin_image[d] > rpd) begin_image[d] = rpd;
                if(end_image[d] < rpd) end_image[d] = rpd;
            }
        }
        else
        {
            for(int d = 0; d < RenderingContainer3D::Dimensions; d++)
            {
                begin_image[d] = end_image[d] = round(po[d]);
            }
        }
    }

    for(int d = 0; d < RenderingContainer3D::Dimensions; d++)
    {
        if(begin_image[d] < 0) begin_image[d] = 0;
        else if(begin_image[d] >= (int)size[d]) begin_image[d] = (int)size[d] - 1;
        if(end_image[d] >= (int)size[d]) end_image[d] = (int)size[d] - 1;
        else if(end_image[d] < 0) end_image[d] = 0;
    }

    GLdouble modelMatO[16];
    MatrixUtils3D<GLdouble>::invert3x3(modelMatO, modelMat);

    MazdaImageRegionIterator< RenderingContainer3D::MzRGBImage8 > iiter(image, (unsigned int*) begin_image, (unsigned int*) end_image);

//    double a[3], b[3];
//    a[0] = 53;
//    a[1] = 111;
//    a[2] = -5;
//    printf("Ispace %f %f %f", (float) a[0], (float) a[1], (float) a[2]);
//    ImageToVoxelSpace(a, b);
//    printf("Vspace %f %f %f", (float) b[0], (float) b[1], (float) b[2]);
//    Transform(b, a, modelMat);
//    printf("Tspace %f %f %f", (float) a[0], (float) a[1], (float) a[2]);
//    DeTransform(a, b, modelMatO, modelMat);
//    printf("Vspace %f %f %f", (float) b[0], (float) b[1], (float) b[2]);
//    VoxelToImageSpace(b, a);
//    printf("Ispace %f %f %f", (float) a[0], (float) a[1], (float) a[2]);
//    fflush(stdout);

    bool set = false;
    while(! iiter.IsBehind())
    {
        unsigned int* index = iiter.GetIndex();
        int rpd[RenderingContainer3D::Dimensions];
        VoxelToImageSpace((int*) index, pi);
        DeTransform(pi, po, modelMatO, modelMat);
        //ImageToVoxelSpace(po, rpd);

        for(int d = 0; d < RenderingContainer3D::Dimensions; d++)
            rpd[d] = round(po[d]/voxelsize[d] + (double)roi_size[d]/2.0);// + begin_roi[d];

//        printf("Rpd %i %i %i\n", rpd[0], rpd[1], rpd[2]);
//        iiter.SetPixel(colorpixel2);////////
//        if(rpd[0] <= end_roi[0] && rpd[1] <= end_roi[1] && rpd[2] <= end_roi[2] &&
//           rpd[0] >= begin_roi[0] && rpd[1] >= begin_roi[1] && rpd[2] >= begin_roi[2])

        if((unsigned int)rpd[0] < roi_size[0] && (unsigned int)rpd[1] < roi_size[1] && (unsigned int)rpd[2] < roi_size[2])
        {
//            iiter.SetPixel(colorpixel);////////
//            rpd[0] -= begin_roi[0];
//            rpd[1] -= begin_roi[1];
//            rpd[2] -= begin_roi[2];

            if(roi->GetPixel(rpd))
            {
                iiter.SetPixel(colorpixel);
                if(set)
                {
                    for(int d = 0; d < RenderingContainer3D::Dimensions; d++)
                    {
                        if(begin_image[d] > (int)index[d]) begin_image[d] = index[d];
                        if(end_image[d] < (int)index[d]) end_image[d] = index[d];
                    }
                }
                else
                {
                    set = true;
                    for(int d = 0; d < RenderingContainer3D::Dimensions; d++)
                        begin_image[d] = end_image[d] = index[d];
                }
            }
        }
        ++iiter;
    }
//    renderer->MzImage2MzRoi(begin_image, end_image, colorpixel, *erase);
    for(int d = 0; d < RenderingContainer3D::Dimensions; d++)
    {
        cube.min[d] = begin_image[d];
        cube.max[d] = end_image[d];
    }
//    printf("Paint3DMove::rasterizeBlock %i %i %i %i %i %i\n", cube.min[0], cube.min[1], cube.min[2], cube.max[0], cube.max[1], cube.max[2]);
//    fflush(stdout);
    return;// FINALIZE;
}


//AfterPaintAction Paint3DMove::mouseDoubleClickEvent(QMouseEvent *ev)
//{

//}

//AfterPaintAction Paint3DMove::wheelEvent(QWheelEvent *ev){return DONOTHING;}

//void Paint3DMove::updateShape(void)
//{
//    glNewList(list, GL_COMPILE);
//    glColor4f(((float)colorpixel.channel[0])/255.0f, ((float)colorpixel.channel[1])/255.0f, ((float)colorpixel.channel[2])/255.0f, 1.0f);
//    createBlock();
//    glEndList();
//    renderer->showModel(true);
//}

void Paint3DMove::createBlock(void)
{
    RenderingContainer3D::MzRoi* roi = container->getActiveRoi();
    if(roi == NULL) return;
    if(roi->IsEmpty()) return;
    char* pbuffer;
    unsigned int roi_size[RenderingContainer3D::Dimensions];
    unsigned long int buf_size[RenderingContainer3D::Dimensions];
    int begin_roi[RenderingContainer3D::Dimensions];
    float x0, x1, x2;
    float sh[3];

    roi->GetSize(roi_size);
    buf_size[0] = roi_size[0]+1;
    buf_size[1] = buf_size[0]*(roi_size[1]+1);
    buf_size[2] = buf_size[1]*(roi_size[2]+1);
    char* buffer = new char[buf_size[2]];

    memset(buffer, 0, buf_size[2]);
    MazdaRoiIterator< RenderingContainer3D::MzRoi > iter(roi);
    unsigned int x[3];
    for(x[2] = 0; x[2] < roi_size[2]; x[2]++)
    {
        for(x[1] = 0; x[1] < roi_size[1]; x[1]++)
        {
            pbuffer = buffer + buf_size[0]*x[1] + buf_size[1]*x[2];
            for(x[0] = 0; x[0] < roi_size[0]; x[0]++)
            {
                if(iter.GetPixel())
                {
                    *pbuffer ^= 7;
                    *(pbuffer+1) ^= 9;
                    *(pbuffer+buf_size[0]) ^= 18;
                    *(pbuffer+buf_size[1]) ^= 36;
                }
                pbuffer++;
                ++iter;
            }
        }
    }

    roi->GetBegin(begin_roi);
    sh[0] = (float)roi_size[0]/2.0;
    sh[1] = (float)roi_size[1]/2.0;
    sh[2] = (float)roi_size[2]/2.0;

    MatrixUtils3D<GLdouble>::unit4x4(modelMat);
    modelMat[12] = ((GLdouble)begin_roi[0]-(GLdouble)size[0]/2.0+sh[0])*voxelsize[0];
    modelMat[13] = ((GLdouble)begin_roi[1]-(GLdouble)size[1]/2.0+sh[1])*voxelsize[1];
    modelMat[14] = ((GLdouble)begin_roi[2]-(GLdouble)size[2]/2.0+sh[2])*voxelsize[2];

    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glFrontFace(GL_CCW);
    glBegin(GL_QUADS);

    pbuffer = buffer;
    for(x[2] = 0; x[2] <= roi_size[2]; x[2]++)
    {
        x2 = (x[2]-sh[2])*voxelsize[2];
        for(x[1] = 0; x[1] <= roi_size[1]; x[1]++)
        {
            x1 = (x[1]-sh[1])*voxelsize[1];
            char* pbuffer = buffer + buf_size[0]*x[1] + buf_size[1]*x[2];
            for(x[0] = 0; x[0] <= roi_size[0]; x[0]++)
            {
                x0 = (x[0]-sh[0])*voxelsize[0];
                if (*pbuffer & 1)
                {
                    if(*pbuffer & 8)
                    {
                        glNormal3f(1, 0, 0);
                        glVertex3f(x0, x1, x2);
                        glVertex3f(x0, x1+voxelsize[1], x2);
                        glVertex3f(x0, x1+voxelsize[1], x2+voxelsize[2]);
                        glVertex3f(x0, x1, x2+voxelsize[2]);
                    }
                    else
                    {
                        glNormal3f(-1, 0, 0);
                        glVertex3f(x0, x1, x2+voxelsize[2]);
                        glVertex3f(x0, x1+voxelsize[1], x2+voxelsize[2]);
                        glVertex3f(x0, x1+voxelsize[1], x2);
                        glVertex3f(x0, x1, x2);
                    }
                }
                if (*pbuffer & 2)
                {
                    if(*pbuffer & 16)
                    {
                        glNormal3f(0, 1, 0);
                        glVertex3f(x0, x1, x2+voxelsize[2]);
                        glVertex3f(x0+voxelsize[0], x1, x2+voxelsize[2]);
                        glVertex3f(x0+voxelsize[0], x1, x2);
                        glVertex3f(x0, x1, x2);
                    }
                    else
                    {
                        glNormal3f(0, -1, 0);
                        glVertex3f(x0, x1, x2);
                        glVertex3f(x0+voxelsize[0], x1, x2);
                        glVertex3f(x0+voxelsize[0], x1, x2+voxelsize[2]);
                        glVertex3f(x0, x1, x2+voxelsize[2]);
                    }
                }
                if (*pbuffer & 4)
                {
                    if(*pbuffer & 32)
                    {
                        glNormal3f(0, 0, 1);
                        glVertex3f(x0, x1, x2);
                        glVertex3f(x0+voxelsize[0], x1, x2);
                        glVertex3f(x0+voxelsize[0], x1+voxelsize[1], x2);
                        glVertex3f(x0, x1+voxelsize[1], x2);
                    }
                    else
                    {
                        glNormal3f(0, 0, -1);
                        glVertex3f(x0, x1+voxelsize[1], x2);
                        glVertex3f(x0+voxelsize[0], x1+voxelsize[1], x2);
                        glVertex3f(x0+voxelsize[0], x1, x2);
                        glVertex3f(x0, x1, x2);
                    }
                }
                pbuffer++;
            }
        }
    }
    glEnd();
    delete[] buffer;
}

/*
void Paint3DMove::createBlock(void)
{
    RenderingContainer3D::MzRoi* roi = container->getActiveRoi();
    if(roi == NULL) return;
    if(roi->IsEmpty()) return;
    unsigned int roi_size[RenderingContainer3D::Dimensions];
    roi->GetSize(roi_size);
    int begin_roi[RenderingContainer3D::Dimensions];
    int end_roi[RenderingContainer3D::Dimensions];
    roi->GetBegin(begin_roi);
    roi->GetEnd(end_roi);

    unsigned d;
    float x0, x1, x2;
    float sh[3];
    sh[0] = (float)roi_size[0]/2.0;
    sh[1] = (float)roi_size[1]/2.0;
    sh[2] = (float)roi_size[2]/2.0;
    float shi[3];
    shi[0] = (float)size[0]/2.0;
    shi[1] = (float)size[1]/2.0;
    shi[2] = (float)size[2]/2.0;

    MatrixUtils3D<GLdouble>::unit4x4(modelMat);
    modelMat[12] = ((GLfloat)begin_roi[0]-shi[0]+sh[0])*voxelsize[0];
    modelMat[13] = ((GLfloat)begin_roi[1]-shi[1]+sh[1])*voxelsize[1];
    modelMat[14] = ((GLfloat)begin_roi[2]-shi[2]+sh[2])*voxelsize[2];

    for(d = 0; d < 3; d++)
        sh[d] += begin_roi[d];

    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glFrontFace(GL_CCW);
    glBegin(GL_QUADS);
    bool previous = false;
    int xx0max = end_roi[0]-begin_roi[0];
    int xx1max = end_roi[1]-begin_roi[1]-1;
    int begin_reg[RenderingContainer3D::Dimensions];
    int end_reg[RenderingContainer3D::Dimensions];

    begin_reg[0] = begin_roi[0]; end_reg[0] = end_roi[0];
    begin_reg[1] = begin_roi[1]; end_reg[1] = end_roi[1];
    begin_reg[2] = begin_roi[2]; end_reg[2] = begin_roi[2];
    MazdaRoiRegionIterator< RenderingContainer3D::MzRoi > iterf(roi, begin_reg, end_reg);

    for(d = 0; d < 3; d++)
        shi[d] = sh[d]-begin_reg[d];
    while(! iterf.IsBehind())
    {
        if(iterf.GetPixel())
        {
            x0 =  iterf.GetIndex(0) - shi[0];
            x1 =  iterf.GetIndex(1) - shi[1];
            x2 =  iterf.GetIndex(2) - shi[2];
            glNormal3f(0, 0, -1);
            glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
            glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
            glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
            glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
        }
        ++iterf;
    }

    begin_reg[0] = begin_roi[0]; end_reg[0] = end_roi[0];
    begin_reg[1] = begin_roi[1]; end_reg[1] = begin_roi[1];
    begin_reg[2] = begin_roi[2]; end_reg[2] = end_roi[2];
    MazdaRoiRegionIterator< RenderingContainer3D::MzRoi > itert(roi, begin_reg, end_reg);
    for(d = 0; d < 3; d++)
        shi[d] = sh[d]-begin_reg[d];
    while(! itert.IsBehind())
    {
        if(itert.GetPixel())
        {
            x0 =  itert.GetIndex(0) - shi[0];
            x1 =  itert.GetIndex(1) - shi[1];
            x2 =  itert.GetIndex(2) - shi[2];
            glNormal3f(0, -1, 0);
            glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
            glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
            glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
            glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);

        }
        ++itert;
    }


    if(end_roi[1]-begin_roi[1] > 0)
    {
        begin_reg[1] = begin_roi[1]; end_reg[1] = end_roi[1]-1;
        begin_reg[2] = end_roi[2]; end_reg[2] = end_roi[2];
        MazdaRoiRegionIterator< RenderingContainer3D::MzRoi > iterb(roi, begin_reg, end_reg);
        begin_reg[1] = begin_roi[1]+1; end_reg[1] = end_roi[1];
        MazdaRoiRegionIterator< RenderingContainer3D::MzRoi > iterb1y(roi, begin_reg, end_reg);
        begin_reg[1] = begin_roi[1]; end_reg[1] = end_roi[1]-1;

        previous = false;
        bool previousy = false;
        for(d = 0; d < 3; d++)
            shi[d] = sh[d]-begin_reg[d];
        while(! iterb.IsBehind())
        {
            bool i = iterb.GetPixel();
            bool iy = iterb1y.GetPixel();
            int xx0 = iterb.GetIndex(0);
            int xx1 = iterb.GetIndex(1);
            int xx2 = iterb.GetIndex(2);
            x0 =  xx0 - shi[0];
            x1 =  xx1 - shi[1];
            x2 =  xx2 - shi[2];
            if(i)
            {
                glNormal3f(0, 0, 1);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            if(iy && (!i))
            {
                glNormal3f(0, -1, 0);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            else if((!iy) && i)
            {
                glNormal3f(0, 1, 0);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
            }
            if(previous && (!i))
            {
                glNormal3f(1, 0, 0);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            else if((!previous) && i)
            {
                glNormal3f(-1, 0, 0);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
            }
            if(xx0 >= xx0max)
            {
                if(i)
                {
                    glNormal3f(1, 0, 0);
                    glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                }
                previous = false;
            }
            else
            {
                previous = i;
            }
            if(xx1 >= xx1max)
            {
                if(iy)
                {
                    glNormal3f(0, 1, 0);
                    glVertex3f((x0  )*voxelsize[0], (x1+2)*voxelsize[1], (x2+1)*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+2)*voxelsize[1], (x2+1)*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+2)*voxelsize[1], (x2  )*voxelsize[2]);
                    glVertex3f((x0  )*voxelsize[0], (x1+2)*voxelsize[1], (x2  )*voxelsize[2]);

                    glNormal3f(0, 0, 1);
                    glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+2)*voxelsize[1], (x2+1)*voxelsize[2]);
                    glVertex3f((x0  )*voxelsize[0], (x1+2)*voxelsize[1], (x2+1)*voxelsize[2]);

                    if(!previousy)
                    {
                        glNormal3f(-1, 0, 0);
                        glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                        glVertex3f((x0  )*voxelsize[0], (x1+2)*voxelsize[1], (x2+1)*voxelsize[2]);
                        glVertex3f((x0  )*voxelsize[0], (x1+2)*voxelsize[1], (x2  )*voxelsize[2]);
                        glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                    }
                    if(xx0 >= xx0max)
                    {
                        glNormal3f(1, 0, 0);
                        glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                        glVertex3f((x0+1)*voxelsize[0], (x1+2)*voxelsize[1], (x2  )*voxelsize[2]);
                        glVertex3f((x0+1)*voxelsize[0], (x1+2)*voxelsize[1], (x2+1)*voxelsize[2]);
                        glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                    }
                }
                else if(previousy)
                {
                    glNormal3f(1, 0, 0);
                    glVertex3f((x0+1)*voxelsize[0], (x1+0)*voxelsize[1], (x2  )*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+0)*voxelsize[1], (x2+1)*voxelsize[2]);
                }
                previousy = iy;
            }
            ++iterb;
            ++iterb1y;
        }
    }

    if(end_roi[2]-begin_roi[2] > 0)
    {

        begin_reg[2] = begin_roi[2]; end_reg[2] = end_roi[2]-1;
        begin_reg[1] = end_roi[1]; end_reg[1] = end_roi[1];
        MazdaRoiRegionIterator< RenderingContainer3D::MzRoi > iterd(roi, begin_reg, end_reg);
        begin_reg[2] = begin_roi[2]+1; end_reg[2] = end_roi[2];
        MazdaRoiRegionIterator< RenderingContainer3D::MzRoi > iterd1z(roi, begin_reg, end_reg);
        begin_reg[2] = begin_roi[2]; end_reg[2] = end_roi[2]-1;

        previous = false;
        for(d = 0; d < 3; d++)
            shi[d] = sh[d]-begin_reg[d];
        while(! iterd.IsBehind())
        {
            bool i = iterd.GetPixel();
            bool iz = iterd1z.GetPixel();
            int xx0 = iterd.GetIndex(0);
            int xx1 = iterd.GetIndex(1);
            int xx2 = iterd.GetIndex(2);
            x0 =  xx0 - shi[0];
            x1 =  xx1 - shi[1];
            x2 =  xx2 - shi[2];

            if(i)
            {
                glNormal3f(0, 1, 0);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
            }
            if(iz && (!i))
            {
                glNormal3f(0, 0, -1);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            else if((!iz) && i)
            {
                glNormal3f(0, 0, 1);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            if(previous && (!i))
            {
                glNormal3f(1, 0, 0);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            else if((!previous) && i)
            {
                glNormal3f(-1, 0, 0);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
            }
            if(xx0 >= xx0max)
            {
                if(i)
                {
                    glNormal3f(1, 0, 0);
                    glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                }
                previous = false;
            }
            else
            {
                previous = i;
            }
            ++iterd;
            ++iterd1z;
        }
    }

    if(end_roi[1]-begin_roi[1] > 0 && end_roi[2]-begin_roi[2] > 0)
    {
        begin_reg[0] = begin_roi[0]; end_reg[0] = end_roi[0];
        begin_reg[1] = begin_roi[1]; end_reg[1] = end_roi[1]-1;
        begin_reg[2] = begin_roi[2]; end_reg[2] = end_roi[2]-1;
        MazdaRoiRegionIterator< RenderingContainer3D::MzRoi > iter(roi, begin_reg, end_reg);
        begin_reg[1] = begin_roi[1]+1; end_reg[1] = end_roi[1];
        begin_reg[2] = begin_roi[2]; end_reg[2] = end_roi[2]-1;
        MazdaRoiRegionIterator< RenderingContainer3D::MzRoi > iter1y(roi, begin_reg, end_reg);
        begin_reg[1] = begin_roi[1]; end_reg[1] = end_roi[1]-1;
        begin_reg[2] = begin_roi[2]+1; end_reg[2] = end_roi[2];
        MazdaRoiRegionIterator< RenderingContainer3D::MzRoi > iter1z(roi, begin_reg, end_reg);
        begin_reg[1] = begin_roi[1]; end_reg[1] = end_roi[1]-1;
        begin_reg[2] = begin_roi[2]; end_reg[2] = end_roi[2]-1;
        previous = false;
        for(d = 0; d < 3; d++)
            shi[d] = sh[d]-begin_reg[d];
        while(! iter.IsBehind())
        {
            bool i;
            bool iy;
            bool iz;
            int xx0 = iter.GetIndex(0);
            int xx1 = iter.GetIndex(1);
            int xx2 = iter.GetIndex(2);

            i = iter.GetPixel();
            iy = iter1y.GetPixel();
            iz = iter1z.GetPixel();

            x0 =  xx0 - shi[0];
            x1 =  xx1 - shi[1];
            x2 =  xx2 - shi[2];
            if(iy && (!i))
            {
                glNormal3f(0, -1, 0);

                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            else if((!iy) && i)
            {
                glNormal3f(0, 1, 0);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
            }
            if(iz && (!i))
            {
                glNormal3f(0, 0, -1);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            else if((!iz) && i)
            {
                glNormal3f(0, 0, 1);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            if(previous && (!i))
            {
                glNormal3f(1, 0, 0);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
            }
            else if((!previous) && i)
            {
                glNormal3f(-1, 0, 0);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                glVertex3f((x0  )*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
            }
            if(xx0 >= xx0max)
            {
                if(i)
                {
                    glNormal3f(1, 0, 0);
                    glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2  )*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2  )*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1+1)*voxelsize[1], (x2+1)*voxelsize[2]);
                    glVertex3f((x0+1)*voxelsize[0], (x1  )*voxelsize[1], (x2+1)*voxelsize[2]);
                }
                previous = false;
            }
            else
            {
                previous = i;
            }
            ++iter;
            ++iter1y;
            ++iter1z;
        }
    }
    glEnd();
}
*/
