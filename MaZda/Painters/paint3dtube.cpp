/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paint3dtube.h"

Paint3DTube::Paint3DTube(RenderingContainer3D *icontainer, Painter3D *ipainter, unsigned int list, GLdouble* modelMat, GLdouble *volumeMat, bool *ierase) :
    Paint3dBlock(icontainer, ipainter, list, modelMat, volumeMat, ierase)
{
    shaper = 0;
}

void Paint3DTube::createTube(float bottomRadius, float topRadius, float halfHeight)
{
    float f = (bottomRadius - topRadius)/2.0f;
    float g = sqrt(halfHeight*halfHeight + f*f);
    float nz = f/g;
    float nn = halfHeight/g;
    const int nnn = 256;

    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glFrontFace(GL_CCW);
    glBegin(GL_TRIANGLES);
    for(int k = 0; k < nnn; k++)
    {
        int kp = (k+1)%nnn;
        float kkk = k*2*M_PI/nnn;
        float kkp = kp*2*M_PI/nnn;
        float cc = cos(kkk);
        float cp = cos(kkp);
        float ss = sin(kkk);
        float sp = sin(kkp);
        glNormal3f(0, 0, 1);
        glVertex3f(topRadius*sp, topRadius*cp, halfHeight);
        glVertex3f(topRadius*ss, topRadius*cc, halfHeight);
        glVertex3f(0.0f, 0.0f, halfHeight);
        glNormal3f(0, 0, -1);
        glVertex3f(bottomRadius*ss, bottomRadius*cc, -halfHeight);
        glVertex3f(bottomRadius*sp, bottomRadius*cp, -halfHeight);
        glVertex3f(0.0f, 0.0f, -halfHeight);
    }
    glEnd();

    glBegin(GL_QUADS);
    for(int k = 0; k < nnn; k++)
    {
        int kp = (k+1)%nnn;
        float kkk = k*2*M_PI/nnn;
        float kkp = kp*2*M_PI/nnn;
        float cc = cos(kkk);
        float cp = cos(kkp);
        float ss = sin(kkk);
        float sp = sin(kkp);

        glNormal3f(sp*nn, cp*nn, nz);
        glVertex3f(topRadius*sp, topRadius*cp, halfHeight);
        glVertex3f(bottomRadius*sp, bottomRadius*cp, -halfHeight);
        glNormal3f(ss*nn, cc*nn, nz);
        glVertex3f(bottomRadius*ss, bottomRadius*cc, -halfHeight);
        glVertex3f(topRadius*ss, topRadius*cc, halfHeight);
    }
    glEnd();
}



void Paint3DTube::tubeFill(float bottomRadius, float topRadius, float halfHeight)
{
    const int nnn = 256;
    double inp[3];
    double po[4][3];

    Transform(inp, po[0], modelMat);
    ImageToVoxelSpace(po[0], po[0]);
    for(int iii = 0; iii < 3; iii++)
        cube.min[iii] = cube.max[iii] = round(po[0][iii]);

    for(int k = 0; k < nnn; k++)
    {
        int kp = (k+1)%nnn;
        float kkk = k*2*M_PI/nnn;
        float kkp = kp*2*M_PI/nnn;
        float cc = cos(kkk);
        float cp = cos(kkp);
        float ss = sin(kkk);
        float sp = sin(kkp);
        double p1[4][3]={
            {topRadius*sp, topRadius*cp, halfHeight},    //a
            {topRadius*ss, topRadius*cc, halfHeight},    //b
            {0.0f, 0.0f, halfHeight},        //c
            {0.0f, 0.0f, -halfHeight}};      //f
        TransformTetrahedronAndFill(p1, po, modelMat);
        for(int iii = 0; iii < 3; iii++)
        {
            int b = round(po[0][iii]);
            if(cube.min[iii] > b) cube.min[iii] = b;
            if(cube.max[iii] < b) cube.max[iii] = b;
        }
        double p2[4][3]={
            {bottomRadius*ss, bottomRadius*cc, -halfHeight},   //e
            {bottomRadius*sp, bottomRadius*cp, -halfHeight},   //d
            {0.0f, 0.0f, -halfHeight},       //f
            {topRadius*ss, topRadius*cc, halfHeight}};   //b
        TransformTetrahedronAndFill(p2, po, modelMat);
        for(int iii = 0; iii < 3; iii++)
        {
            int b = round(po[1][iii]);
            if(cube.min[iii] > b) cube.min[iii] = b;
            if(cube.max[iii] < b) cube.max[iii] = b;
        }
        double p3[4][3]={
            {topRadius*sp, topRadius*cp, halfHeight},    //a
            {topRadius*ss, topRadius*cc, halfHeight},    //b
            {bottomRadius*sp, bottomRadius*cp, -halfHeight},   //d
            {0.0f, 0.0f, -halfHeight}};      //f
        TransformTetrahedronAndFill(p3, po, modelMat);
    }
}

AfterPaintAction Paint3DTube::changeShape(int delta, bool pressed)
{
    if(pressed)
    {
        if(delta > 0) shaper+=10;
        else if(delta < 0) shaper-=10;
    }
    else
    {
        if(delta > 0) shaper++;
        else if(delta < 0) shaper--;
    }
    if(shaper > 100) shaper = 100;
    if(shaper < -100) shaper = -100;
    updateShape();
    return REDRAW;
}

void Paint3DTube::createBlock(void)
{
    createTube(10 + (double)shaper/10.0, 10 - (double)shaper/10.0, 10);
}
void Paint3DTube::rasterizeBlock(void)
{
    tubeFill(10 + (double)shaper/10.0, 10 - (double)shaper/10.0, 10);
}
