/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paint3dcube.h"

Paint3DCube::Paint3DCube(RenderingContainer3D *icontainer, Painter3D *ipainter, unsigned int list, GLdouble* modelMat, GLdouble *volumeMat, bool *ierase) :
    Paint3dBlock(icontainer, ipainter, list, modelMat, volumeMat, ierase)
{
    shaper = 0;
}

void Paint3DCube::createCube(float bottomRadius, float topRadius, float halfHeight)
{
    float f = (bottomRadius - topRadius)/2.0f;
    float g = sqrt(halfHeight*halfHeight + f*f);
    float nz = f/g;
    float nn = halfHeight/g;
    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glFrontFace(GL_CCW);
    glBegin(GL_QUADS);
        glNormal3f(nn, 0, 0);
        glVertex3f(bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(topRadius, topRadius, halfHeight);
        glVertex3f(topRadius, -topRadius, halfHeight);
        glVertex3f(bottomRadius, -bottomRadius, -halfHeight);
        glNormal3f(0, 0, -1.0f);
        glVertex3f(-bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(bottomRadius, -bottomRadius, -halfHeight);
        glVertex3f(-bottomRadius, -bottomRadius, -halfHeight);
        glNormal3f(0, nn, nz);
        glVertex3f(-topRadius, topRadius, halfHeight);
        glVertex3f(topRadius, topRadius, halfHeight);
        glVertex3f(bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(-bottomRadius, bottomRadius, -halfHeight);
        glNormal3f(-nn, 0, nz);
        glVertex3f(-topRadius, topRadius, halfHeight);
        glVertex3f(-bottomRadius, bottomRadius, -halfHeight);
        glVertex3f(-bottomRadius, -bottomRadius, -halfHeight);
        glVertex3f(-topRadius, -topRadius, halfHeight);
        glNormal3f(0, 0, 1.0f);
        glVertex3f(topRadius, topRadius, halfHeight);
        glVertex3f(-topRadius, topRadius, halfHeight);
        glVertex3f(-topRadius, -topRadius, halfHeight);
        glVertex3f(topRadius, -topRadius, halfHeight);
        glNormal3f(0, -nn, nz);
        glVertex3f(topRadius, -topRadius, halfHeight);
        glVertex3f(-topRadius, -topRadius, halfHeight);
        glVertex3f(-bottomRadius, -bottomRadius, -halfHeight);
        glVertex3f(bottomRadius, -bottomRadius, -halfHeight);
    glEnd();
}



void Paint3DCube::cubeFill(float bottomRadius, float topRadius, float halfHeight)
{
    double pi[5][4][3]={
        {{topRadius, -topRadius, halfHeight},
        {-topRadius, topRadius, halfHeight},
        {-topRadius, -topRadius, halfHeight},
        {-bottomRadius, -bottomRadius, -halfHeight}},
        {{topRadius, -topRadius, halfHeight},
        {-topRadius, topRadius, halfHeight},
        {topRadius, topRadius, halfHeight},
        {bottomRadius, bottomRadius, -halfHeight}},
        {{-topRadius, topRadius, halfHeight},
        {-bottomRadius, bottomRadius, -halfHeight},
        {-bottomRadius, -bottomRadius, -halfHeight},
        {bottomRadius, bottomRadius, -halfHeight}},
        {{topRadius, -topRadius, halfHeight},
        {bottomRadius, -bottomRadius, -halfHeight},
        {-bottomRadius, -bottomRadius, -halfHeight},
        {bottomRadius, bottomRadius, -halfHeight}},
        {{topRadius, -topRadius, halfHeight},
        {-topRadius, topRadius, halfHeight},
        {-bottomRadius, -bottomRadius, -halfHeight},
        {bottomRadius, bottomRadius, -halfHeight}}};
    double po[4][3];
//    double mato[16] = {
//        1.0, 0.0, 0.0, 0.0,
//        0.0, 1.0, 0.0, 0.0,
//        0.0, 0.0, 1.0, 0.0,
//        0.0, 0.0, 0.0, 1.0
//    };

    for(int i = 0; i < 5; i++)
    {
        TransformTetrahedronAndFill(pi[i], po, modelMat);
        for(int ii = 0; ii < 4; ii++)
        {
            if(i == 0 && ii == 0)
            {
                for(int iii = 0; iii < 3; iii++)
                {
                    int b = round(po[ii][iii]);
                    cube.min[iii] = cube.max[iii] = b;
                }
            }
            else
            {
                for(int iii = 0; iii < 3; iii++)
                {
                    int b = round(po[ii][iii]);
                    if(cube.min[iii] > b) cube.min[iii] = b;
                    if(cube.max[iii] < b) cube.max[iii] = b;
                }
            }
        }
    }
}

AfterPaintAction Paint3DCube::changeShape(int delta, bool pressed)
{
    if(pressed)
    {
        if(delta > 0) shaper+=10;
        else if(delta < 0) shaper-=10;
    }
    else
    {
        if(delta > 0) shaper++;
        else if(delta < 0) shaper--;
    }
    if(shaper > 100) shaper = 100;
    if(shaper < -100) shaper = -100;
    updateShape();
    return REDRAW;
}

void Paint3DCube::createBlock(void)
{
    createCube(10 + (double)shaper/10.0, 10 - (double)shaper/10.0, 10);
}
void Paint3DCube::rasterizeBlock(void)
{
    cubeFill(10 + (double)shaper/10.0, 10 - (double)shaper/10.0, 10);
}

