/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paintelli.h"
#include <QApplication>
#include <QPainter>

//#include <math.h>
#include <cmath>


#define sqrt2rec 0.70710678
#define pi4 0.78539816
#define pi2 1.5707963
#define pi 3.1415927
#define twopi 6.2831853
#define elipointsno 1024

AfterPaintAction PaintElli::imageDataChanged(void)
{
    return DONOTHING;
}

AfterPaintAction PaintElli::mouseMoveEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        //QPoint pl[4];

        int radia, radib;
        if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
        {
            QPoint newpoint;
            newpoint.setX(ev->x() / zoom);
            newpoint.setY(ev->y() / zoom);
            double x = endpoint.x()-point.x();
            double y = endpoint.y()-point.y();
            double xn = newpoint.x()-point.x();
            double yn = newpoint.y()-point.y();
            if((xn != 0 || yn != 0) && (x != 0 || y != 0))
            {
                angle += (atan2(yn, xn) - atan2(y, x));
                if(angle > pi) angle -= twopi;
                if(angle < -pi) angle += twopi;
            }
            endpoint = newpoint;
        }
        else
        {
            endpoint.setX(ev->x() / zoom);
            endpoint.setY(ev->y() / zoom);
        }

        if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        {
            double x = endpoint.x()-point.x();
            double y = endpoint.y()-point.y();
            radia = radib = sqrt(x*x + y*y);
        }
        else
        {
            double x = endpoint.x()-point.x();
            double y = endpoint.y()-point.y();
            double a = atan2(y, x)-angle;
            double dd = sqrt(x*x + y*y);
            radia = cos(a)*dd;
            a += pi2;
            radib = cos(a)*dd;
        }

        QPainter pixPaint(canvas);
        QPen pen(color);
        pen.setWidth(1);
        pen.setCapStyle(Qt::RoundCap);
        pen.setJoinStyle(Qt::RoundJoin);
        pixPaint.setPen(pen);
        QBrush brush(color);
        pixPaint.setBrush(brush);

        pixPaint.drawImage(rectp.left(), rectp.top(), background, rectp.left(), rectp.top(), rectp.width(), rectp.height());
        //pixPaint.drawPolygon(pp, elipointsno);

        QTransform transform;
        transform.translate(point.x(), point.y());
        transform.rotate(angle*180.0/pi);
        pixPaint.setTransform(transform);

        pixPaint.drawEllipse(QPointF(0.0, 0.0), radia, radib);
        QRect rectn;

        double cosangle = cos(angle);
        double sinangle = sin(angle);
        double m = fabs(cosangle*radia)+fabs(sinangle*radib);
        rectn.setLeft(point.x()-m+0.5);
        rectn.setRight(point.x()+m+0.5);
        m = fabs(cosangle*radib)+fabs(sinangle*radia);
        rectn.setTop(point.y()-m+0.5);
        rectn.setBottom(point.y()+m+0.5);

        *rect = rectn.united(rectp);
        rectp = rectn;

        info = QString("%1,%2 R[%3,%4]").arg(point.x()).arg(point.y()).arg(radia).arg(radib);//.arg(int(180*angle/3.1415927));
        return REDRAW;
    }
    return DONOTHING;
}


AfterPaintAction PaintElli::mousePressEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        point.setX(ev->x() / zoom);
        point.setY(ev->y() / zoom);
        endpoint = point;
        rectp.setBottomRight(point);
        rectp.setTopLeft(point);

        QPainter pixPaint(canvas);
        QPen pen(color);
        //pen.setWidth(thick);
        //pen.setCapStyle(Qt::RoundCap);
        pixPaint.setPen(pen);
        pixPaint.drawPoint(point);
        *rect = rectp;
        //rect->adjust(-thick/2, -thick/2, thick/2, thick/2);
        return REDRAW;
    }
    return DONOTHING;
}

AfterPaintAction PaintElli::mouseReleaseEvent(QMouseEvent *ev)
{
//    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        return FINALIZE;
    }
    return DONOTHING;
}

AfterPaintAction PaintElli::mouseDoubleClickEvent(QMouseEvent *ev){return DONOTHING;}
AfterPaintAction PaintElli::wheelEvent(QWheelEvent *ev){return DONOTHING;}

PaintElli::PaintElli(QColor icolor, double izoom, QImage *icanvas, QRect *irect)
{
    zoom = izoom;
    color = icolor;
    canvas = icanvas;
    rect = irect;
    background = icanvas->copy();
    angle = 0.0;
}

