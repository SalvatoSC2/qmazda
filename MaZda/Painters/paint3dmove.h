/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAINT3DMOVE_H
#define PAINT3DMOVE_H

#include "painter3d.h"
#include <QColor>
#include "paint3dblock.h"

class Paint3DMove : public Paint3dBlock
{
public:
//    AfterPaintAction mouseMoveEvent(QMouseEvent *ev);
//    AfterPaintAction mousePressEvent(QMouseEvent *ev);
//    AfterPaintAction mouseReleaseEvent(QMouseEvent *ev);
//    AfterPaintAction mouseDoubleClickEvent(QMouseEvent *ev);
//    AfterPaintAction wheelEvent(QWheelEvent *ev);
//    AfterPaintAction imageDataChanged(void);
//    MzBounds getModifiedRegion(void);
//    QString getStatus(void) {return info;}
    Paint3DMove(RenderingContainer3D* icontainer, Painter3D* ipainter, unsigned int list, GLdouble* modelMat, GLdouble *volumeMat, bool* ierase);

private:
//    void initiateCube(MzBounds *cube, double po[3]);
//    void expandCube(MzBounds *cube, double po[3]);
//    void clipCube(MzBounds *cube);
    void updateShape(void);
    void createBlock(void);
    void rasterizeBlock(void);
    AfterPaintAction changeShape(int delta, bool pressed);
    int state;
    GLdouble* volumeMat;
    GLdouble* modelMat;
    int mousex, mousey;
    bool* erase;
    Painter3D* renderer;
    RenderingContainer3D* container;
    RenderingContainer3D::MazdaRGBPixelType8 colorpixel;
    QString info;
    unsigned int list;
};


#endif // PAINT3DMOVE_H
