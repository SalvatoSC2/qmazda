/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAINT3DFLOODFILL_H
#define PAINT3DFLOODFILL_H


#include "ipaint.h"
#include "painter3d.h"

#include <QColor>
struct stackElem
{
    unsigned int voxel[RenderingContainer3D::Dimensions];
};

class Paint3DFloodFill : public iPaint
{
public:
    AfterPaintAction keyPressEvent(QKeyEvent*){return DONOTHING;}
    AfterPaintAction keyReleaseEvent(QKeyEvent*){return DONOTHING;}
    AfterPaintAction mouseMoveEvent(QMouseEvent *ev);
    AfterPaintAction mousePressEvent(QMouseEvent *ev);
    AfterPaintAction mouseReleaseEvent(QMouseEvent *ev);
    AfterPaintAction mouseDoubleClickEvent(QMouseEvent *ev);
    AfterPaintAction wheelEvent(QWheelEvent *ev);
    AfterPaintAction imageDataChanged(void);
    MzBounds getModifiedRegion(void);
    QString* getStatus(void) {return &info;}
    Paint3DFloodFill(RenderingContainer3D* icontainer, Painter3D* ipainter, bool* ierase);

private:
    bool* erase;
    Painter3D* renderer;
    RenderingContainer3D* container;
    QString info;
    MzBounds cube;
    RenderingContainer3D::MazdaRGBPixelType8 colorpixel;
    void FloodFill(unsigned int seed[], MzBounds *bounds);
};

#endif // PAINT3DFLOODFILL_H
