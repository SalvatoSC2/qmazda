/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paint3dball.h"
#include "sphear.h"

Paint3DBall::Paint3DBall(RenderingContainer3D* icontainer, Painter3D* ipainter, unsigned int list,  GLdouble* modelMat, GLdouble *volumeMat, bool *ierase) :
    Paint3dBlock(icontainer, ipainter, list, modelMat, volumeMat, ierase)
{
}

void Paint3DBall::createBall(float radius, int divider)
{
    Sphear sphear(divider);
    unsigned int tn = sphear.trianglesNumber();
    unsigned int* tr = new unsigned int[tn*3];
    sphear.triangles(tr);
    double vertex[3];

    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glFrontFace(GL_CCW);
    glBegin(GL_TRIANGLES);

    for(unsigned int k = 0; k < tn; k++)
    {
        for(unsigned int v = 2; v < 3; v--)
        {
            sphear.vertex(tr[k*3+v], vertex);
            glNormal3f(vertex[0], vertex[1], vertex[2]);
            glVertex3f(radius*vertex[0], radius*vertex[1], radius*vertex[2]);
        }
    }
    glEnd();
    delete[] tr;
}

void Paint3DBall::ballFill(float radius, int divider)
{
    Sphear sphear(divider);
    unsigned int tn = sphear.trianglesNumber();
    unsigned int* tr = new unsigned int[tn*3];
    sphear.triangles(tr);
    double pi[4][3];
    double po[4][3];

    pi[3][0] = 0.0;
    pi[3][1] = 0.0;
    pi[3][2] = 0.0;

    for(unsigned int k = 0; k < tn; k++)
    {
        for(unsigned int v = 2; v < 3; v--)
        {
            sphear.vertex(tr[k*3+v], pi[v]);
            pi[v][0] *= radius;
            pi[v][1] *= radius;
            pi[v][2] *= radius;
        }
        TransformTetrahedronAndFill(pi, po, modelMat);
        for(int ii = 0; ii < 4; ii++)
        {
            if(k == 0 && ii == 0)
            {
                for(int iii = 0; iii < 3; iii++)
                {
                    int b = round(po[ii][iii]);
                    cube.min[iii] = cube.max[iii] = b;
                }
            }
            else
            {
                for(int iii = 0; iii < 3; iii++)
                {
                    int b = round(po[ii][iii]);
                    if(cube.min[iii] > b) cube.min[iii] = b;
                    if(cube.max[iii] < b) cube.max[iii] = b;
                }
            }
        }
    }
}

AfterPaintAction Paint3DBall::changeShape(int /*delta*/, bool /*pressed*/)
{
    return DONOTHING;
}

void Paint3DBall::createBlock(void)
{
    createBall(10, 8);
}
void Paint3DBall::rasterizeBlock(void)
{
    ballFill(10, 8);
}
