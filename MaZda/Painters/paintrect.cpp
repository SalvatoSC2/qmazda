/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paintrect.h"

#include "paintrect.h"
#include <QApplication>
#include <QPainter>

//#include <math.h>
#include <cmath>


#define sqrt2rec 0.70710678
#define pi4 0.78539816
#define pi2 1.5707963
#define pi 3.1415927
#define twopi 6.2831853

AfterPaintAction PaintRect::imageDataChanged(void)
{
    return DONOTHING;
}

AfterPaintAction PaintRect::mouseMoveEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        QPoint pl[4];

        if(QApplication::keyboardModifiers() & Qt::ShiftModifier)
        {
            QPoint newpoint;
            newpoint.setX(ev->x() / zoom);
            newpoint.setY(ev->y() / zoom);
            double x = endpoint.x()-point.x();
            double y = endpoint.y()-point.y();
            double xn = newpoint.x()-point.x();
            double yn = newpoint.y()-point.y();
            if((xn != 0 || yn != 0) && (x != 0 || y != 0))
            {
                angle += (atan2(yn, xn) - atan2(y, x));
                if(angle > pi) angle -= twopi;
                if(angle < -pi) angle += twopi;
            }
            endpoint = newpoint;
        }
        else
        {
            endpoint.setX(ev->x() / zoom);
            endpoint.setY(ev->y() / zoom);
        }

        if(QApplication::keyboardModifiers() & Qt::ControlModifier)
        {

            double x = endpoint.x()-point.x();
            double y = endpoint.y()-point.y();
            double a = atan2(y, x);
            double d = sqrt2rec*sqrt(x*x + y*y);
            pl[0] = point;
            pl[2] = endpoint;
            pl[1] = QPoint(d*cos(a+pi4)+point.x()+0.5, d*sin(a+pi4)+point.y()+0.5);
            pl[3] = QPoint(d*cos(a-pi4)+point.x()+0.5, d*sin(a-pi4)+point.y()+0.5);
        }
        else
        {
            pl[0] = point;
            pl[2] = endpoint;
            double x = endpoint.x()-point.x();
            double y = endpoint.y()-point.y();
//            pl[1] = QPoint(point.x(), endpoint.y());
//            pl[3] = QPoint(endpoint.x(), point.y());
            double a = atan2(y, x)-angle;
            double dd = sqrt(x*x + y*y);
            double d = cos(a)*dd;

            pl[1] = QPoint(d*cos(angle)+point.x()+0.5, d*sin(angle)+point.y()+0.5);
            a += pi2;
            d = cos(a)*dd;
            pl[3] = QPoint(d*cos(angle-pi2)+point.x()+0.5, d*sin(angle-pi2)+point.y()+0.5);
        }



        QPainter pixPaint(canvas);
        QPen pen(color);
        pen.setWidth(1);
        pen.setCapStyle(Qt::RoundCap);
        pen.setJoinStyle(Qt::RoundJoin);
        pixPaint.setPen(pen);
        QBrush brush(color);
        pixPaint.setBrush(brush);

        //pixPaint.drawPixmap(rectp.left(), rectp.top(), background, rectp.left(), rectp.top(), rectp.width(), rectp.height());
        pixPaint.drawImage(rectp.left(), rectp.top(), background, rectp.left(), rectp.top(), rectp.width(), rectp.height());
        pixPaint.drawPolygon(pl, 4);

        QRect rectn;
        rectn.setTopLeft(pl[0]);
        rectn.setBottomRight(pl[0]);

        for(int n = 1; n < 4; n++)
        {
            if(pl[n].x() < rectn.left()) rectn.setLeft(pl[n].x());
            if(pl[n].x() > rectn.right()) rectn.setRight(pl[n].x());
            if(pl[n].y() < rectn.top()) rectn.setTop(pl[n].y());
            if(pl[n].y() > rectn.bottom()) rectn.setBottom(pl[n].y());
        }
        *rect = rectn.united(rectp);
        rectp = rectn;

        info = QString("%1,%2 - %3,%4").arg(point.x()).arg(point.y()).arg(endpoint.x()).arg(endpoint.y());//.arg(int(180*angle/3.1415927));
        return REDRAW;
    }
    return DONOTHING;
}

AfterPaintAction PaintRect::mousePressEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        point.setX(ev->x() / zoom);
        point.setY(ev->y() / zoom);
        endpoint = point;
        rectp.setBottomRight(point);
        rectp.setTopLeft(point);

        QPainter pixPaint(canvas);
        QPen pen(color);
        //pen.setWidth(thick);
        //pen.setCapStyle(Qt::RoundCap);
        pixPaint.setPen(pen);
        pixPaint.drawPoint(point);
        *rect = rectp;
        //rect->adjust(-thick/2, -thick/2, thick/2, thick/2);
        return REDRAW;
    }
    return DONOTHING;
}

AfterPaintAction PaintRect::mouseReleaseEvent(QMouseEvent *ev)
{
//    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        return FINALIZE;
    }
}

AfterPaintAction PaintRect::mouseDoubleClickEvent(QMouseEvent *ev){return DONOTHING;}
AfterPaintAction PaintRect::wheelEvent(QWheelEvent *ev){return DONOTHING;}

PaintRect::PaintRect(QColor icolor, double izoom, QImage *icanvas, QRect *irect)
{
    zoom = izoom;
    color = icolor;
    canvas = icanvas;
    rect = irect;
    background = icanvas->copy();
    angle = 0.0;
}
