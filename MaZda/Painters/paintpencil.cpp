/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paintpencil.h"
#include <QPainter>


AfterPaintAction PaintPencil::imageDataChanged(void)
{
    return DONOTHING;
}

AfterPaintAction PaintPencil::mouseMoveEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        QPoint endp;
        endp.setX(ev->x() / zoom);
        endp.setY(ev->y() / zoom);
        QPainter pixPaint(canvas);
        QPen pen(color);
        pen.setWidth(thick);
        pen.setCapStyle(Qt::RoundCap);
        pixPaint.setPen(pen);
        pixPaint.drawLine(point, endp);
        if(endp.x() < intrect.left()) intrect.setLeft(endp.x());
        if(endp.x() > intrect.right()) intrect.setRight(endp.x());
        if(endp.y() < intrect.top()) intrect.setTop(endp.y());
        if(endp.y() > intrect.bottom()) intrect.setBottom(endp.y());

        if(point.x() < endp.x())
        {
            rect->setLeft(point.x());
            rect->setRight(endp.x());
        }
        else
        {
            rect->setRight(point.x());
            rect->setLeft(endp.x());
        }
        if(point.y() < endp.y())
        {
            rect->setTop(point.y());
            rect->setBottom(endp.y());
        }
        else
        {
            rect->setBottom(point.y());
            rect->setTop(endp.y());
        }
        rect->adjust(-thick/2, -thick/2, thick/2, thick/2);
        point = endp;

        info = QString("%1,%2").arg(point.x()).arg(point.y());
        return REDRAW;
    }
    return DONOTHING;
}

AfterPaintAction PaintPencil::mousePressEvent(QMouseEvent *ev)
{
    if(ev->buttons() & Qt::LeftButton)
    {
        ev->accept();
        point.setX(ev->x() / zoom);
        point.setY(ev->y() / zoom);
        intrect.setBottomRight(point);
        intrect.setTopLeft(point);

        QPainter pixPaint(canvas);
        QPen pen(color);
        pen.setWidth(thick);
        pen.setCapStyle(Qt::RoundCap);
        pixPaint.setPen(pen);
        pixPaint.drawPoint(point);
        *rect = intrect;
        rect->adjust(-thick/2, -thick/2, thick/2, thick/2);
        return REDRAW;
    }
    return DONOTHING;
}

AfterPaintAction PaintPencil::mouseReleaseEvent(QMouseEvent *ev)
{
//    if(ev->buttons() & Qt::LeftButton)
//    {
        ev->accept();
        *rect = intrect;
        rect->adjust(-thick/2, -thick/2, thick/2, thick/2);
        return FINALIZE;
//    }
//    return DONOTHING;
}

AfterPaintAction PaintPencil::mouseDoubleClickEvent(QMouseEvent *ev){return DONOTHING;}
AfterPaintAction PaintPencil::wheelEvent(QWheelEvent *ev){return DONOTHING;}

PaintPencil::PaintPencil(QColor icolor, int ithick, double izoom, QImage *icanvas, QRect *irect)
{
    thick = ithick;
    zoom = izoom;
    color = icolor;
    canvas = icanvas;
    rect = irect;
}
