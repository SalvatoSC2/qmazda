/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IPAINT_H
#define IPAINT_H

#include <QMouseEvent>
#include <QWheelEvent>
#include <QString>

#define MZDIMENSIONS 4
struct MzBounds
{
    int min[MZDIMENSIONS];
    int max[MZDIMENSIONS];
};
typedef enum {NOACTION, DONOTHING, REDRAW, FINALIZE, CANCEL, NEWROI, REFRESH} AfterPaintAction;
typedef enum {DT_IGNORE, DT_NONE, DT_PEN, DT_POLY, DT_ELLI, DT_RECT, DT_FILL, DT_SNAKE, DT_MOVE} DrawingTool;

class iPaint
{
public:
    virtual AfterPaintAction keyPressEvent(QKeyEvent *ev) = 0;
    virtual AfterPaintAction keyReleaseEvent(QKeyEvent *ev) = 0;
    virtual AfterPaintAction mouseMoveEvent(QMouseEvent *ev) = 0;
    virtual AfterPaintAction mousePressEvent(QMouseEvent *ev) = 0;
    virtual AfterPaintAction mouseReleaseEvent(QMouseEvent *ev) = 0;
    virtual AfterPaintAction mouseDoubleClickEvent(QMouseEvent *ev) = 0;
    virtual AfterPaintAction wheelEvent(QWheelEvent *ev) = 0;
    virtual AfterPaintAction imageDataChanged(void) = 0;
    virtual MzBounds getModifiedRegion(void) = 0;
    virtual QString* getStatus(void) = 0;

    virtual ~iPaint(){}
};


#endif // IPAINT_H
