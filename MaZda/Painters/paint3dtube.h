/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAINT3DTUBE_H
#define PAINT3DTUBE_H

#include "paint3dblock.h"

class Paint3DTube : public Paint3dBlock
{
public:
    Paint3DTube(RenderingContainer3D* icontainer, Painter3D* ipainter, unsigned int list, GLdouble *modelMat, GLdouble *volumeMat, bool *ierase);
private:
    void createBlock(void);
    void rasterizeBlock(void);
    AfterPaintAction changeShape(int delta, bool pressed);
    void createTube(float bottomRadius, float topRadius, float halfHeight);
    void tubeFill(float bottomRadius, float topRadius, float halfHeight);
    int shaper;
};

#endif // PAINT3DTUBE_H
