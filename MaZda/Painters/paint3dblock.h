/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAINT3DBLOCK_H
#define PAINT3DBLOCK_H

#include "ipaint.h"
#include "painter3d.h"
#include <QColor>

/*
  Stan 0:
  ML - wstaw domyślny blok

  Stan 1:
  DML, Enter, Spacja - akceptuj
  MR, Esc - przerwij
  MM - dokładność rolki

  Ctrl - tryb obrotu i powiększenia całej przestrzeni
  Ctrl+Shift - tryb przesunięcia całej przestrzeni

  Brak - obrót bryły, (powiększanie rolka)
  Shift - przesunięcie bryły, (przybliżenie-oddalenie rolka)

  Alt, Alt+Shift - powiększenie kierunkowe (ustożkowienie rolka)
*/


class Paint3dBlock : public iPaint
{
public:
    AfterPaintAction keyPressEvent(QKeyEvent *ev);
    AfterPaintAction keyReleaseEvent(QKeyEvent *ev);
    AfterPaintAction mouseMoveEvent(QMouseEvent *ev);
    AfterPaintAction mousePressEvent(QMouseEvent *ev);
    AfterPaintAction mouseReleaseEvent(QMouseEvent *ev);
    AfterPaintAction mouseDoubleClickEvent(QMouseEvent *ev);
    AfterPaintAction wheelEvent(QWheelEvent *ev);
    AfterPaintAction imageDataChanged(void);
    MzBounds getModifiedRegion(void);
    QString* getStatus(void) {return &info;}
    void updateShape();
    Paint3dBlock(RenderingContainer3D* icontainer, Painter3D* ipainter, unsigned int list, GLdouble *modelMat, GLdouble *volumeMat, bool *ierase);

protected:
    virtual void createBlock(void) = 0;
    virtual void rasterizeBlock(void) = 0;
    virtual AfterPaintAction changeShape(int delta, bool pressed) = 0;
    RenderingContainer3D* container;
    RenderingContainer3D::MazdaRGBPixelType8 colorpixel;
    MzBounds cube;
    GLdouble* modelMat;
    GLdouble* volumeMat;
    unsigned int list;
    int mousex, mousey;
    Painter3D* painter;
    bool initial;
    bool* erase;
    bool middlepressed;
    QString info;
    int lastcoords[RenderingContainer3D::Dimensions];

    RenderingContainer3D::MzRGBImage8* image;
    unsigned int size[RenderingContainer3D::Dimensions];
    double voxelsize[RenderingContainer3D::Dimensions];

    static void DeTransform(const double ip[3], double op[3], const double mato[16], const double mat[16]);
    static void Transform(double ip[3], double op[3], double mat[16]);
    void ImageToVoxelSpace(const double ip[3], int op[3]);
    void ImageToVoxelSpace(const double ip[3], double op[3]);
    void VoxelToImageSpace(const int ip[3], double op[3]);
    void VoxelToImageSpace(const double ip[3], double op[3]);
    void TransformTetrahedronAndFill(double ip[4][3], double op[4][3], double mat[16]);
private:
    void FillSliceTriangle(unsigned int z, double punkty[3][2]);
    void FillTetrahedron(double punkty[4][3]);



};

#endif // PAINT3DBLOCK_H
