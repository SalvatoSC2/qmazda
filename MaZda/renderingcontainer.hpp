/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RENDERING_CONTAINER_HPP
#define RENDERING_CONTAINER_HPP

#include "renderingcontainer.h"
#include "randomnames.h"

template< unsigned int VDimensions >
RenderingContainer< VDimensions >::RenderingContainer()
{
    whattoupdate = 0;
    region_update_valid = false;
    active_roi = -1;
    channel = 'Y';
    minwindow = std::numeric_limits<MazdaImagePixelType>::min();
    maxwindow = std::numeric_limits<MazdaImagePixelType>::max();
    visibility = NORMAL;
    image_source_color = NULL;
    image_source_gray = NULL;
    image_grayscale_windowed = NULL;
    roi_overlay = NULL;
    canvas_image = NULL;

    for(int u = 0; u < MaxUndoSteps; u++)
        undoFileList.push_back(randomNames.getTempName("/undo", ".roi", false));
    redolevels = 0;
    undolevels = -1;
    currentundo = 0;
}

template< unsigned int VDimensions >
RenderingContainer< VDimensions >::~RenderingContainer()
{
    //std::cout << "~RenderingContainer() <"<< VDimensions << ">" << std::endl;
    for(unsigned int i = 0; i < roitable.size(); i++)  if(roitable[i] != NULL) delete roitable[i];
    if(image_source_color != NULL) delete image_source_color;
    if(image_source_gray != NULL) delete image_source_gray;
    if(image_grayscale_windowed != NULL) delete image_grayscale_windowed;
    if(roi_overlay != NULL) delete roi_overlay;
    if(canvas_image != NULL) delete canvas_image;

    for(int r = 0; r < undoFileList.size(); r++)
        randomNames.deleteFile(undoFileList[r]);
    undoFileList.clear();
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::canUndo()
{
    if(undolevels > 0) return true;
    else return false;
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::canRedo()
{
    if(redolevels > 0) return true;
    else return false;
}


template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::undo()
{
    if(undolevels <= 0) return false;
    undolevels--;
    redolevels++;
    currentundo--;
    currentundo = (undoFileList.size()+currentundo)%undoFileList.size();
    std::vector < MzRoi*> rois  = MazdaRoiIO< MzRoi>::Read(undoFileList[currentundo]);
    removeRois();
    appendRois(rois);
    return true;
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::redo()
{
    if(redolevels <= 0) return false;
    undolevels++;
    redolevels--;
    currentundo++;
    currentundo = currentundo%undoFileList.size();
    std::vector < MzRoi*> rois  = MazdaRoiIO< MzRoi>::Read(undoFileList[currentundo]);
    removeRois();
    appendRois(rois);
    return true;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::setModified()
{
    redolevels = 0;
    undolevels++;
    currentundo++;
    if(undolevels > undoFileList.size()-1) undolevels = undoFileList.size()-1;
    currentundo = currentundo%undoFileList.size();
    MazdaRoiIO< MzRoi >::Write(undoFileList[currentundo], &roitable, NULL);
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::setImageChannel(char ichannel)
{
    channel = ichannel;
    whattoupdate |= IMAGE_CHANNEL_CHANGED;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::setImageVisibilityMode(ImageVisibilityMode ivisibility)
{
    visibility = ivisibility;
    whattoupdate |= IMAGE_SLIDERS_CHANGED;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::setImageGrayWindowLow(double iminwindow)
{
    minwindow = iminwindow;
    whattoupdate |= IMAGE_SLIDERS_CHANGED;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::setImageGrayWindowHigh(double imaxwindow)
{
    maxwindow = imaxwindow;
    whattoupdate |= IMAGE_SLIDERS_CHANGED;
}

template< unsigned int VDimensions >
ImageVisibilityMode RenderingContainer< VDimensions >::getImageVisibilityMode(void)
{
    return visibility;
}

template< unsigned int VDimensions >
double RenderingContainer< VDimensions >::getImageGrayWindowLow(void)
{
    return minwindow;
}

template< unsigned int VDimensions >
double RenderingContainer< VDimensions >::getImageGrayWindowHigh(void)
{
    return maxwindow;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::getImageHistogram(int* histogram, int bins, int *minmax)
{
    if(whattoupdate & FROMLOADED2CHANNEL)
    {
        unsigned int oldwtu = whattoupdate;
        whattoupdate = FROMLOADED2CHANNEL;
        assembleImage();
        whattoupdate = oldwtu - FROMLOADED2CHANNEL;
    }
    memset(histogram, 0, bins * sizeof(int));
    if(image_source_gray == NULL) return;
    int max = std::numeric_limits<MazdaImagePixelType>::max();
    int bin = bins - 1;
    MazdaImageIterator<MzImage> iterator(image_source_gray);

    if(minmax != NULL)
    {
        minmax[0] = std::numeric_limits<int>::max();
        minmax[1] = std::numeric_limits<int>::min();
        while(!iterator.IsBehind())
        {
            int val = iterator.GetPixel();
            if(val > minmax[1]) minmax[1] = val;
            if(val < minmax[0]) minmax[0] = val;
            unsigned int i = (val*bin)/max;
            if(i < bins) histogram[i]++;
            ++iterator;
        }
    }
    else
    {
        while(!iterator.IsBehind())
        {
            int val = iterator.GetPixel();
            unsigned int i = (val*bin)/max;
            if(i < bins) histogram[i]++;
            ++iterator;
        }
    }
}


template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::getActiveRegionHistogram(int* histogram, int bins, unsigned int *minmaxarea)
{
    if(active_roi<0 || active_roi>=roitable.size()) return false;
    if(roitable[active_roi] == NULL) return false;
    if(roitable[active_roi]->IsEmpty()) return false;
    if(whattoupdate & FROMLOADED2CHANNEL)
    {
        unsigned int oldwtu = whattoupdate;
        whattoupdate = FROMLOADED2CHANNEL;
        assembleImage();
        whattoupdate = oldwtu - FROMLOADED2CHANNEL;
    }
    memset(histogram, 0, bins * sizeof(int));
    if(image_source_gray == NULL) return false;

    unsigned int size[VDimensions];
    double spacing[VDimensions];
    int begin[VDimensions];
    int end[VDimensions];

    image_source_gray->GetSize(size);
    image_source_gray->GetSpacing(spacing);

    roitable[active_roi]->GetBegin(begin);
    roitable[active_roi]->GetEnd(end);
    bool quit = false;
    for(unsigned int d = 0; d < VDimensions; d++)
    {
        if(begin[d] >= (int)size[d]) {quit = true; break;}
        if(end[d] < 0) {quit = true; break;}
        if(begin[d] < 0) begin[d] = 0;
        if(end[d] >= (int)size[d]) end[d] = size[d]-1;
    }
    if(quit) return false;

    int max = std::numeric_limits<MazdaImagePixelType>::max();
    int bin = bins - 1;
    MazdaImageRegionIterator<MzImage> iterator(image_source_gray, (unsigned int*) begin, (unsigned int*) end);
    MazdaRoiRegionIterator<MzRoi> riterator(roitable[active_roi], begin, end, true);

    if(minmaxarea != NULL)
    {
        minmaxarea[0] = std::numeric_limits<unsigned int>::max();
        minmaxarea[1] = std::numeric_limits<unsigned int>::min();
        minmaxarea[2] = 0;
        while(!iterator.IsBehind())
        {
            if(riterator.GetPixel())
            {
                unsigned int val = iterator.GetPixel();
                if(val > minmaxarea[1]) minmaxarea[1] = val;
                if(val < minmaxarea[0]) minmaxarea[0] = val;
                unsigned int i = (val*bin)/max;
                if(i < bins) histogram[i]++;
                minmaxarea[2]++;
            }
            ++iterator;
            ++riterator;
        }
    }
    else return false;
    return true;
}



template< unsigned int VDimensions >
unsigned int RenderingContainer< VDimensions >::getNumberOfRois(void)
{
    return roitable.size();
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::addRoi(void)
{
    roitable.push_back(new MzRoi());
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::removeRoi(unsigned int index)
{
    if(index >= roitable.size()) return;
    if(roitable[index] != NULL) delete roitable[index];
    roitable[index] = NULL;
    roitable.erase(roitable.begin()+index);
    if(active_roi >= roitable.size()) active_roi = roitable.size() - 1;
    whattoupdate |= ROI_CHANGED;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::clearRoi(unsigned int index)
{
    if(index >= roitable.size()) return;
    if(roitable[index] != NULL)
    {
        roitable[index]->Erase();
    }
    whattoupdate |= ROI_CHANGED;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::setActiveRoiIndex(unsigned int index)
{
    if(index >= roitable.size()) return;
    active_roi = index;
}

template< unsigned int VDimensions >
int RenderingContainer< VDimensions >::getActiveRoiIndex(void)
{
    return active_roi;
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::setRoiName(unsigned int index, std::string name)
{
    if(index >= roitable.size()) return false;
    if(roitable[index] == NULL)  return false;
    roitable[index]->SetName(name);
    return true;
}

template< unsigned int VDimensions >
std::string RenderingContainer< VDimensions >::getRoiName(unsigned int index)
{
    std::string ret;
    if(index >= roitable.size()) return ret;
    if(roitable[index] == NULL)  return ret;
    return roitable[index]->GetName();
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::setRoiColor(unsigned int index, unsigned int color)
{
    if(index >= roitable.size()) return false;
    if(roitable[index] == NULL)  return false;
    roitable[index]->SetColor(color);
    whattoupdate |= ROI_CHANGED;
    return true;
}

template< unsigned int VDimensions >
unsigned int RenderingContainer< VDimensions >::getRoiColor(unsigned int index)
{
    if(index >= roitable.size()) return 0;
    if(roitable[index] == NULL)  return 0;
    return roitable[index]->GetColor();
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::setRoiVisibility(unsigned int index, int visibility)
{
    if(index >= roitable.size()) return false;
    if(roitable[index] == NULL)  return false;
    roitable[index]->SetVisibility(visibility);
    whattoupdate |= ROI_CHANGED;
    return true;
}

template< unsigned int VDimensions >
int RenderingContainer< VDimensions >::getRoiVisibility(unsigned int index)
{
    if(index >= roitable.size()) return 0;
    if(roitable[index] == NULL)  return 0;
    return roitable[index]->GetVisibility();
}
template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::getSize(unsigned int* size)
{
    unsigned int d;
    unsigned int s[VDimensions];
    if(image_source_color != NULL) image_source_color->GetSize(s);
    else if(image_source_gray != NULL) image_source_gray->GetSize(s);
    else for(d = 0; d < VDimensions; d++) s[d] = 0;
    for(d = 0; d < VDimensions; d++) size[d] = s[d];
}
template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::getSpacing(double* spacing)
{
    unsigned int d;
    double s[VDimensions];
    if(image_source_color != NULL) image_source_color->GetSpacing(s);
    else if(image_source_gray != NULL) image_source_gray->GetSpacing(s);
    else for(d = 0; d < VDimensions; d++) s[d] = 0.0;
    for(d = 0; d < VDimensions; d++) spacing[d] = s[d];
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::setSpacing(double* spacing)
{
    if(image_source_color != NULL) image_source_color->SetSpacing(spacing);
    if(image_source_gray != NULL) image_source_gray->SetSpacing(spacing);
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::update()
{
    if(whattoupdate) assembleImage();
    whattoupdate = 0;
    region_update_valid = false;
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::setImage(MzRGBImage* image)
{
    if(image == NULL) return false;
    cleanImageData();
    image_source_color = image;
    roiResizeForImage();
    whattoupdate |= IMAGE_LOADED;
    return true;
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::setImage(MzImage* image)
{
    if(image == NULL) return false;
    cleanImageData();
    image_source_gray = image;
    roiResizeForImage();
    whattoupdate |= IMAGE_LOADED;
    return true;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::removeRois()
{
    for(unsigned int i = 0; i < roitable.size(); i++)
        if(roitable[i] != NULL) delete roitable[i];
    roitable.clear();
    active_roi = -1;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::setRoisRedraw(void)
{
    whattoupdate |= ROI_CHANGED;
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::setRois(std::vector<MzRoi*> rois)
{
    removeRois();
    whattoupdate |= ROI_CHANGED;
    return appendRois(rois);
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::appendRoi(MzRoi* roi)
{
    roitable.push_back(roi);
    whattoupdate |= ROI_CHANGED;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::appendRois(std::vector<MzRoi*> rois)
{
    for(unsigned int i = 0; i < rois.size(); i++)
        if(rois[i] != NULL) roitable.push_back(rois[i]);
    if(active_roi < 0 && roitable.size() > 0) active_roi = 0;
    whattoupdate |= ROI_CHANGED;

}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::setActiveRoi(MzRoi* roi)
{
    if(active_roi < 0 || active_roi >= roitable.size()) return false;
    if(roitable[active_roi] == roi) return true;
    delete roitable[active_roi];
    roitable[active_roi] = roi;
    return true;
}

template< unsigned int VDimensions >
bool RenderingContainer< VDimensions >::setRoi(unsigned int index, MzRoi* roi)
{
    if(index < 0 || index >= roitable.size()) return false;
    if(roitable[index] == roi) return true;
    delete roitable[index];
    roitable[index] = roi;
    whattoupdate |= ROI_CHANGED;
    return true;
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::setUpdateRegion(unsigned int begin[VDimensions], unsigned int end[VDimensions])
{
    for(unsigned int i = 0; i < VDimensions; i++)
    {
        update_begin[i] = begin[i];
        update_end[i] = end[i];
    }
    region_update_valid = true;
}

//===========PRIVATE===========

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::cleanImageData()
{
    if(image_source_color != NULL) delete image_source_color;
    if(image_source_gray != NULL) delete image_source_gray;
    if(image_grayscale_windowed != NULL) delete image_grayscale_windowed;
    if(roi_overlay != NULL) delete roi_overlay;
    if(canvas_image != NULL) delete canvas_image;
    image_source_color = NULL;
    image_source_gray = NULL;
    image_grayscale_windowed = NULL;
    roi_overlay = NULL;
    canvas_image = NULL;
    whattoupdate |= IMAGE_LOADED;
}
template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::roiResizeForImage()
{
    // Intentionally no resize
    // Resized after the first edit or move
}

template< unsigned int VDimensions >
void RenderingContainer< VDimensions >::assembleImage(void)
{
    if(image_source_gray == NULL && image_source_color == NULL) return;

    //image_source_color >> image_source_gray
    if(image_source_color != NULL && (whattoupdate & FROMLOADED2CHANNEL))
    {
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        image_source_color->GetSize(size);
        image_source_color->GetSpacing(spacing);

        bool same = true;
        if(image_source_gray == NULL)
        {
            same = false;
        }
        else
        {
            unsigned int sizeg[VDimensions];
            image_source_gray->GetSize(sizeg);
            for(unsigned int d = 0; d < VDimensions; d++)
            {
                if(sizeg[d] != size[d])
                {
                    same = false;
                    break;
                }
            }
        }
        if(!same)
        {
            if(image_source_gray != NULL)
                delete image_source_gray;
            image_source_gray = new MzImage(size, spacing, false);
            if(image_grayscale_windowed != NULL)
                delete image_grayscale_windowed;
            image_grayscale_windowed = new MzImage8(size, spacing, false);
            if(roi_overlay != NULL)
                delete roi_overlay;
            roi_overlay = new MzRGBImage8(size, spacing, false);
            if(canvas_image != NULL)
                delete canvas_image;
            canvas_image = new MzRGBImage8(size, spacing, false);
        }
        MzImageColorConvert(channel, image_source_color, image_source_gray);
    }
    else if(whattoupdate & FROMLOADED2CHANNEL)
    {
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        image_source_gray->GetSize(size);
        image_source_gray->GetSpacing(spacing);
        bool same = true;
        if(image_grayscale_windowed == NULL)
        {
            same = false;
        }
        else
        {
            unsigned int sizeg[VDimensions];
            image_grayscale_windowed->GetSize(sizeg);
            for(unsigned int d = 0; d < VDimensions; d++)
            {
                if(sizeg[d] != size[d])
                {
                    same = false;
                    break;
                }
            }
        }
        if(!same)
        {
            if(image_grayscale_windowed != NULL)
                delete image_grayscale_windowed;
            image_grayscale_windowed = new MzImage8(size, spacing, false);
            if(roi_overlay != NULL)
                delete roi_overlay;
            roi_overlay = new MzRGBImage8(size, spacing, false);
            if(canvas_image != NULL)
                delete canvas_image;
            canvas_image = new MzRGBImage8(size, spacing, false);
        }
    }

    //image_source_gray >> image_grayscale_windowed
    if(whattoupdate & FROMCHANNEL2GRAYSCALE)
    {
        if(image_grayscale_windowed == NULL || image_source_gray == NULL) return;
        MazdaImageIterator<MzImage> srci = MazdaImageIterator<MzImage>(image_source_gray);
        MazdaImageIterator<MzImage8> dsti = MazdaImageIterator<MzImage8>(image_grayscale_windowed);
        switch(visibility)
        {
        case NORMAL:
        {
            //double sub =  (double)minwindow/256.0;
            double mul =  255.0 / ((double)maxwindow - minwindow);
            double pix;
            while (!srci.IsBehind())
            {
                pix = srci.GetPixel();
                pix -= minwindow;
                pix *= mul;
                if(pix < 0.0) pix = 0.0;
                if(pix > 255.0) pix = 255.0;
                dsti.SetPixel((MazdaImagePixelType8)pix);
                ++srci;
                ++dsti;
            }
        }break;

        case THREELEVEL:
        {
            int pix;
            if(maxwindow > minwindow)
            {
                while (!srci.IsBehind())
                {
                    pix = srci.GetPixel();// >> 8;
                    dsti.SetPixel((MazdaImagePixelType8) (pix >= maxwindow ? 254 : (pix < minwindow ? 0 : 127)));
                    ++srci;
                    ++dsti;
                }
            }
            else
            {
                while (!srci.IsBehind())
                {
                    pix = srci.GetPixel();// >> 8;
                    dsti.SetPixel((MazdaImagePixelType8) (pix >= minwindow ? 0 : (pix < maxwindow ? 254 : 127)));
                    ++srci;
                    ++dsti;
                }
            }
        }break;

        default: break;
        }
    }

    //roitable >> roi_overlay
    if(whattoupdate & FROMROI2ROIOVERLAY)
    {
        MzRoisToRgb(&roitable, roi_overlay);
    }

    //roi_overlay+image_grayscale >> canvas_image
    if(whattoupdate & COMBINEROIANDGRAYSCALE)
    {
        if(visibility == OFF)
        {
            unsigned int d;
            MazdaImageIterator<MzRGBImage8> srci = MazdaImageIterator<MzRGBImage8>(roi_overlay);
            MazdaImageIterator<MzRGBImage8> dsti = MazdaImageIterator<MzRGBImage8>(canvas_image);
            while (!srci.IsBehind())
            {
                MazdaRGBPixelType8* spix = srci.GetPixelPointer();
                MazdaRGBPixelType8* dpix = dsti.GetPixelPointer();
                for(d = 0; d < MazdaRGBPixelType8::Channels; d++)
                {
                    if(spix->channel[d] > 0) dpix->channel[d] = spix->channel[d]-1;
                    else dpix->channel[d] = 0;
                }
                ++srci;
                ++dsti;
            }
        }
        else
        {
            MazdaImageIterator<MzImage8> srcgi = MazdaImageIterator<MzImage8>(image_grayscale_windowed);
            MazdaImageIterator<MzRGBImage8> srci = MazdaImageIterator<MzRGBImage8>(roi_overlay);
            MazdaImageIterator<MzRGBImage8> dsti = MazdaImageIterator<MzRGBImage8>(canvas_image);
            while (!srci.IsBehind())
            {
                unsigned int d;
                MazdaImagePixelType8 spigx = srcgi.GetPixel();
                MazdaRGBPixelType8* spix = srci.GetPixelPointer();
                MazdaRGBPixelType8* dpix = dsti.GetPixelPointer();
                bool clean = true;
                for(d = 0; d < MazdaRGBPixelType8::Channels; d++)
                {
                    if(spix->channel[d] > 0)
                    {
                        clean = false;
                        break;
                    }
                }
                if(clean)
                {
                    for(d = 0; d < MazdaRGBPixelType8::Channels; d++)
                    {
                        dpix->channel[d] = spigx;
                    }
                }
                else
                {
                    spigx /= 2;
                    for(d = 0; d < MazdaRGBPixelType8::Channels; d++)
                    {
                        dpix->channel[d] = (spix->channel[d]>>1) + spigx;
                    }
                }
                ++srcgi;
                ++srci;
                ++dsti;
            }
        }
    }
}

#endif // RENDERING_CONTAINER_HPP
