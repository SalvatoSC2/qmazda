/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RENDERER_INTERFACE_H
#define RENDERER_INTERFACE_H
#include "renderingcontainer.h"

class RendererInterface
{
public:
    virtual void ResetView(void) = 0;
    virtual bool SetContainer(RenderingContainerInterface* container) = 0;
    virtual void SetZoom(double zoom) = 0;
    virtual double GetZoom(void) = 0;
    virtual void SetBackground(unsigned int  color) = 0;
    virtual unsigned int GetBackground(void) = 0;
    virtual void Repaint() = 0;

    virtual ~RendererInterface(){}
};

#endif // RENDERER_INTERFACE_H
