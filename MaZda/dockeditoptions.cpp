/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dockeditoptions.h"
#include "ui_dockeditoptions.h"
#include <QMenu>

DockEditOptions::DockEditOptions(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::DockEditOptions)
{
    ui->setupUi(this);
    tree = ui->treeWidget;
    QTreeWidgetItem* item;

    item = tree->createMethod("Pen", "Line drawing properties");
    tree->createProperty(item, "Width", "i;1;1;35", "Line width");
    item = tree->createMethod("Morphology", "Mathematical morphology parameters");
    tree->createProperty(item, "Apply", "t;visible;current;visible;all", "Apply for current region or for a group of regions");
    tree->createProperty(item, "Radius", "f;5.0;1.0;15.0", "Radius of structuring element");
    item = tree->createMethod("Remove", "Remove regions of small size");
    tree->createProperty(item, "Size", "i;1000;1;2000000000");

    tree->expandAll();
    expanded = true;
//Sets the first column of parameter tree not editable
    tree->setItemDelegateForColumn(0, new NoEditDelegate(this));
    tree->setItemDelegateForColumn(1, new EditDelegate(this, tree, 0));
}

DockEditOptions::~DockEditOptions()
{
    delete ui;
}

QString DockEditOptions::getValue(QString name)
{
    return tree->getValue(name);
}

//QTreeWidgetItem* DockEditOptions::createMethod(QString name, QString tooltip)
//{
//    QTreeWidgetItem* methoditem = new QTreeWidgetItem();
//    methoditem->setText(0, name);
//    methoditem->setToolTip(0, tooltip);
//    tree->addTopLevelItem(methoditem);
//    return methoditem;
//}

//QTreeWidgetItem*  DockEditOptions::createProperty(QTreeWidgetItem *parent, QString name, QString value, QString tooltip)
//{
//    QTreeWidgetItem* parameteritem = new QTreeWidgetItem();
//    parameteritem->setText(0, name);
//    parameteritem->setToolTip(0, tooltip);
//    parameteritem->setToolTip(1, tooltip);
//    parameteritem->setData(1, Qt::UserRole, QString(value));
//    QStringList valist = value.split(";");
//    if(valist.size() > 1)
//    {
//        if(valist.size() >= 2)
//        {
//            parameteritem->setText(1, valist[1]);
//        }
//    }
//    else parameteritem->setText(1, value);
//    parameteritem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsEditable);
//    parent->addChild(parameteritem);
//    return parameteritem;
//}
/*
void DockEditOptions::onItemClicked(QTreeWidgetItem *item, int column)
{
    if (item == NULL) return;
    if(column == 1)
    {
        QStringList valist = item->data(1, Qt::UserRole).toString().split(";");
        if(valist.size() >= 3)
        {
            if(valist[0] == "t")
            {
                QMenu menu(tree);
                for(int i = 2; i < valist.size(); i++)
                {
                    menu.addAction(new QAction(valist[i], this));
                }
                QRect r = tree->visualItemRect(item);
                QPoint w = tree->mapToGlobal(r.bottomLeft());
                w.setX(QCursor::pos().x());
                QAction* a = menu.exec(w);//(QCursor::pos());
                if(a != NULL) item->setText(1, a->text());
            }
        }
    }
}

void DockEditOptions::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    onItemClicked(item, column);
}

*/

//void DockEditOptions::onItemChanged(QTreeWidgetItem *item, int column)
//{
//    if(column == 1)
//    {
//        QStringList valist = item->data(1, Qt::UserRole).toString().split(";");
//        if(valist.size() >= 2)
//        {
//            if(valist[0] == "f")
//            {
//                float v = item->text(1).toFloat();
//                if(valist.size() >= 3)
//                {
//                    float m = valist[2].toFloat();
//                    if(v < m) v = m;
//                }
//                if(valist.size() >= 4)
//                {
//                    float m = valist[3].toFloat();
//                    if(v > m) v = m;
//                }
//                item->setText(1, QString::number(v));
//            }
//            else if(valist[0] == "i")
//            {
//                int v = item->text(1).toFloat();
//                if(valist.size() >= 3)
//                {
//                    int m = valist[2].toFloat();
//                    if(v < m) v = m;
//                }
//                if(valist.size() >= 4)
//                {
//                    int m = valist[3].toFloat();
//                    if(v > m) v = m;
//                }
//                item->setText(1, QString::number(v));
//            }
//        }
//    }
//}


//void DockEditOptions::on_treeWidget_itemChanged(QTreeWidgetItem *item, int column)
//{
//    onItemChanged(item, column);
//}

void DockEditOptions::on_toolButtonExpand_clicked()
{
    if(expanded) tree->collapseAll();
    else tree->expandAll();
    expanded = ! expanded;
}

void DockEditOptions::on_toolButtonDefault_clicked()
{
    tree->resetValues(1);
}
