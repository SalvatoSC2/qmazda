/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RENDERING_CONTAINER_H
#define RENDERING_CONTAINER_H

#include <QObject>
#include <QRgb>
#include <QColor>
#include <QPixmap>
#include <QImage>
#include <QStringList>
//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc/imgproc_c.h"

#include "../SharedImage/mazdadummy.h"
#include "../SharedImage/mazdaimage.h"
#include "../SharedImage/mazdaimageio.h"
#include "../SharedImage/mazdaroi.h"
#include "../SharedImage/mazdaroiio.h"
#include "../SharedImage/imagedefs.h"

const int MaxUndoSteps = 12;

//typedef MazdaRoi<MazdaRoiBlockType, 2> MzRoi2D;
//typedef MazdaRoi<MazdaRoiBlockType, 3> MzRoi3D;
//typedef MultiChannelPixel <MazdaImagePixelType, 3> MazdaRGBPixelType;
//typedef MazdaImage<MazdaImagePixelType, 2> MzImage2D;
//typedef MazdaImage<MazdaImagePixelType, 3> MzImage3D;
//typedef MazdaImage<MazdaRGBPixelType, 2> MzRGBImage2D;
//typedef MazdaImage<MazdaRGBPixelType, 3> MzRGBImage3D;
//typedef MazdaDummy MzRoi;
//typedef MazdaDummy MzImage;


#define FROMLOADED2CHANNEL 1
#define FROMCHANNEL2GRAYSCALE 2
#define FROMROI2ROIOVERLAY 4
#define COMBINEROIANDGRAYSCALE 8
//#define COMBINE4SCREEN 16
//#define UPDATESCREEN 32

#define ROI_CHANGED (FROMROI2ROIOVERLAY | COMBINEROIANDGRAYSCALE )
#define IMAGE_LOADED (FROMLOADED2CHANNEL | FROMROI2ROIOVERLAY | FROMCHANNEL2GRAYSCALE | COMBINEROIANDGRAYSCALE )
#define IMAGE_CHANNEL_CHANGED (FROMLOADED2CHANNEL | FROMCHANNEL2GRAYSCALE | COMBINEROIANDGRAYSCALE )
#define IMAGE_SLIDERS_CHANGED (FROMCHANNEL2GRAYSCALE | COMBINEROIANDGRAYSCALE )

typedef enum {NORMAL, THREELEVEL, OFF} ImageVisibilityMode;
//typedef enum {DT_IGNORE, PEN, POLY, ELLI, DTRECT, FILL, SNAK, MOVE} DrawingTool;
//typedef enum {FROMGREY, MEDIAN, CLOSE, OPEN, ERODE, DILATE, SPLIT, REMOVESMALL, REMOVEBIG} RoiModificationAlgorithm;

class RenderingContainerInterface
{
public:
    virtual bool undo() = 0;
    virtual bool redo() = 0;
    virtual bool canUndo() = 0;
    virtual bool canRedo() = 0;
    virtual void setModified() = 0;

    virtual void setImageChannel(char ichannel) = 0;
    virtual void setImageVisibilityMode(ImageVisibilityMode ivisibility) = 0;
    virtual void setImageGrayWindowLow(double iminwindow) = 0;
    virtual void setImageGrayWindowHigh(double imaxwindow) = 0;

    virtual ImageVisibilityMode getImageVisibilityMode(void) = 0;
    virtual double getImageGrayWindowLow(void) = 0;
    virtual double getImageGrayWindowHigh(void) = 0;
    virtual void getImageHistogram(int* histogram, int bins, int* minmax) = 0;
    virtual bool getActiveRegionHistogram(int* histogram, int bins, unsigned int *minmaxarea) = 0;

    virtual unsigned int getNumberOfRois(void) = 0;
    virtual void addRoi(void) = 0;
    virtual void removeRoi(unsigned int index) = 0;
    virtual void clearRoi(unsigned int index) = 0;

    virtual void setActiveRoiIndex(unsigned int index) = 0;
    virtual int getActiveRoiIndex(void) = 0;

    virtual bool setRoiName(unsigned int index, std::string name) = 0;
    virtual std::string getRoiName(unsigned int index) = 0;
    virtual bool setRoiColor(unsigned int index, unsigned int color) = 0;
    virtual unsigned int getRoiColor(unsigned int index) = 0;

    virtual bool setRoiVisibility(unsigned int index, int visible) = 0;
    virtual int getRoiVisibility(unsigned int index) = 0;

    virtual void getSize(unsigned int* size) = 0;
    virtual void getSpacing(double* spacing) = 0;
    virtual void setSpacing(double* spacing) = 0;

    virtual void update() = 0;

    virtual ~RenderingContainerInterface(){}
};


template< unsigned int VDimensions >
class RenderingContainer : public RenderingContainerInterface
{
    friend class GetSetSlice;
public:
    typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoi;
    typedef MultiChannelPixel <MazdaImagePixelType, 3> MazdaRGBPixelType;
    typedef MazdaImage<MazdaImagePixelType, VDimensions> MzImage;
    typedef MazdaImage<MazdaRGBPixelType, VDimensions> MzRGBImage;

    typedef unsigned char MazdaImagePixelType8;
    typedef MultiChannelPixel <MazdaImagePixelType8, 3> MazdaRGBPixelType8;
    typedef MazdaImage<MazdaImagePixelType8, VDimensions> MzImage8;
    typedef MazdaImage<MazdaRGBPixelType8, VDimensions> MzRGBImage8;
    const static int Dimensions = VDimensions;

    RenderingContainer();
    ~RenderingContainer();

    bool canUndo();
    bool canRedo();
    bool undo();
    bool redo();
    void setModified();

    void setImageChannel(char ichannel);
    void setImageVisibilityMode(ImageVisibilityMode ivisibility);
    void setImageGrayWindowLow(double iminwindow);
    void setImageGrayWindowHigh(double imaxwindow);

    ImageVisibilityMode getImageVisibilityMode(void);
    double getImageGrayWindowLow(void);
    double getImageGrayWindowHigh(void);

    void getImageHistogram(int* histogram, int bins, int* minmax);
    bool getActiveRegionHistogram(int* histogram, int bins, unsigned int *minmaxarea);
    unsigned int getNumberOfRois(void);
    void addRoi(void);
    void removeRoi(unsigned int index);
    void clearRoi(unsigned int index);
    void setActiveRoiIndex(unsigned int index);
    int getActiveRoiIndex(void);
    bool setRoiName(unsigned int index, std::string name);
    std::string getRoiName(unsigned int index);
    bool setRoiColor(unsigned int index, unsigned int color);
    unsigned int getRoiColor(unsigned int index);

    bool setRoiVisibility(unsigned int index, int visible);
    int getRoiVisibility(unsigned int index);
    void getSize(unsigned int* size);
    void getSpacing(double* spacing);
    void setSpacing(double* spacing);

    void update();

    void appendRois(std::vector<MzRoi*> rois);
    void appendRoi(MzRoi* roi);

    bool setImage(MzRGBImage* image);
    bool setImage(MzImage* image);
    void removeRois();
    bool setRois(std::vector<MzRoi*> rois);
    void setRoisRedraw(void);

    MzRGBImage* getRGBImage()
    {
        return image_source_color;
    }
    MzImage* getGrayImage()
    {
        return image_source_gray;
    }
    MzRGBImage8* getRGBCanvas()
    {
        return canvas_image;
    }
    MzImage8* getGrayCanvas()
    {
        return image_grayscale_windowed;
    }
    MzRoi* getActiveRoi()
    {
        if(active_roi < 0 || active_roi >= roitable.size()) return NULL;
        return roitable[active_roi];
    }
    bool setActiveRoi(MzRoi* roi);

    std::vector<MzRoi*>* getRoitable(void)
    {
        return &roitable;
    }
    MzRoi* getRoi(unsigned int index)
    {
        if(index < 0 || index >= roitable.size()) return NULL;
        return roitable[index];
    }
    bool setRoi(unsigned int index, MzRoi* roi);
    void setUpdateRegion(unsigned int begin[VDimensions], unsigned int end[VDimensions]);

private:
    void cleanImageData(void);
    void roiResizeForImage(void);
    void assembleImage(void);

    unsigned int update_begin[VDimensions];
    unsigned int update_end[VDimensions];
    unsigned int whattoupdate;
    bool region_update_valid;
    int active_roi;
    char channel;
    double minwindow;
    double maxwindow;
    ImageVisibilityMode visibility;
    std::vector<MzRoi*> roitable;
    MzRGBImage* image_source_color;
    MzImage* image_source_gray;
    MzImage8* image_grayscale_windowed;
    MzRGBImage8* roi_overlay;
    MzRGBImage8* canvas_image;

    std::vector <std::string> undoFileList;
    int redolevels;
    int undolevels;
    int currentundo;

};


#include "renderingcontainer.hpp"

#endif // RENDERING_CONTAINER_H
