/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dialogfeatureoptions.h"
#include "ui_dialogfeatureoptions.h"
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QDropEvent>
#include <QMimeData>
#include "stdio.h"
#include "../MzShared/filenameremap.h"

DialogFeatureOptions::DialogFeatureOptions(NameStubTree *roiFeaturesTemplate, NameStubTree *localFeaturesTemplate, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogFeatureOptions)
{
    ui->setupUi(this);

    ui->treeWidgetRoi->setSeparator(' ');
    ui->treeWidgetLocal->setSeparator(' ');

    ui->treeWidgetRoi->setTemplate(roiFeaturesTemplate);
    ui->treeWidgetLocal->setTemplate(localFeaturesTemplate);

    ui->treeWidgetRoi->setItemDelegateForColumn(0, new EditDelegate(this, ui->treeWidgetRoi, ' '));
    ui->treeWidgetLocal->setItemDelegateForColumn(0, new EditDelegate(this, ui->treeWidgetLocal, ' '));

    QTreeWidgetItem* root;
    root = ui->treeWidgetRoi->createMethod("", "Options for region feature extraction");
    root->setIcon(0, QIcon(":/icons/icons/configure.png"));
    ui->treeWidgetRoi->setCurrentItem(root);
    ui->treeWidgetRoi->expandAll();

    root = ui->treeWidgetLocal->createMethod("", "Options for local feature extraction");
    root->setIcon(0, QIcon(":/icons/icons/configure.png"));
    ui->treeWidgetLocal->setCurrentItem(root);
    ui->treeWidgetLocal->expandAll();

}

DialogFeatureOptions::~DialogFeatureOptions()
{
    QStringList list;
    ui->treeWidgetRoi->getUniqueLeafs(&list);

    std::ofstream file;
    file.open("/home/piotr/Program/qmazda/MaZda/fr.txt");
    if (!file.is_open()) return;
    if (!file.good()) return;
    for (int i = 0; i < list.size(); ++i)
        file << list.at(i).toStdString() << std::endl;

    delete ui;
}

std::vector<std::string> DialogFeatureOptions::loadFeatureList(const char* filename)
{
    std::string name;
    std::ifstream file;
    std::vector<std::string> result;
    file.open(filename);
    if (!file.is_open()) return result;
    if (!file.good()) return result;
    do
    {
        std::string sline;
        std::getline(file, sline);
        std::replace(sline.begin(), sline.end(), ',', ' ');
        std::replace(sline.begin(), sline.end(), ';', ' ');
        std::replace(sline.begin(), sline.end(), '\"', ' ');
        std::stringstream ss(sline);
        while(!ss.eof())
        {
            name.clear();
            ss >> std::skipws >> name;
            if(name.length() > 0) result.push_back(name);
        }
    }
    while(!file.eof());
    file.close();
    return result;
}

bool DialogFeatureOptions::saveFeatureList(const char *filename, QStringList* lista)
{
    QFile f;
    if(lista->size() <= 0) return false;
    f.setFileName(filename);
    if(f.open(QFile::WriteOnly))
    {
        QTextStream textstream(&f);
        for (int i = 0; i < lista->size(); ++i) textstream << lista->at(i) << '\n';
        f.close();
        return true;
    }
    else return false;
}

/*
void DialogFeatureOptions::on_toolButtonReset_clicked()
{
    QTreeWidgetItem* root = NULL;
    switch(ui->tabWidget->currentIndex())
    {
    case 0:
        ui->treeWidgetRoi->clear();
        root = ui->treeWidgetRoi->createMethod("", "Options for region feature extraction");
        root->setIcon(0, QIcon(":/icons/icons/configure.png"));
        ui->treeWidgetRoi->setCurrentItem(root);
        ui->treeWidgetRoi->expandAll();
        break;
    case 1:
        ui->treeWidgetLocal->clear();
        root = ui->treeWidgetLocal->createMethod("Local", "Options for local feature extraction and map computation");
        root->setIcon(0, QIcon(":/icons/icons/configure.png"));
        ui->treeWidgetLocal->setCurrentItem(root);
        ui->treeWidgetLocal->expandAll();
        break;
    default: break;
    }
}

void DialogFeatureOptions::on_toolButtonInfo_clicked()
{

}
*/

void DialogFeatureOptions::setFeatureListRoi(QStringList* list)
{
    ui->treeWidgetRoi->setUniqueLeafs(list);
}

void DialogFeatureOptions::setFeatureListLocal(QStringList* list)
{
    ui->treeWidgetLocal->setUniqueLeafs(list);
}

void DialogFeatureOptions::getFeatureListRoi(QStringList* list)
{
    ui->treeWidgetRoi->getUniqueLeafs(list);
}

void DialogFeatureOptions::getFeatureListLocal(QStringList* list)
{
    ui->treeWidgetLocal->getUniqueLeafs(list);
}

void DialogFeatureOptions::on_toolButtonPlus_clicked()
{
//    NameStubTree* featuresTemplate;
    ParameterTreeWidget* treeWidget;

    switch(ui->tabWidget->currentIndex())
    {
    case 0:
//        featuresTemplate = &roiFeaturesTemplate;
        treeWidget = ui->treeWidgetRoi;
        break;
    case 1:
//        featuresTemplate = &localFeaturesTemplate;
        treeWidget = ui->treeWidgetLocal;
        break;
    default: return;
    }

// Not elagent way to force finish editing
    blockSignals(true);
    treeWidget->setEnabled(false);
    treeWidget->setEnabled(true);
    blockSignals(false);

    QTreeWidgetItem* item = treeWidget->currentItem();
    treeWidget->addMatchedChildren(item);
}

void DialogFeatureOptions::on_toolButtonMinus_clicked()
{
    ParameterTreeWidget* treeWidget;
    switch(ui->tabWidget->currentIndex())
    {
    case 0:
        treeWidget = ui->treeWidgetRoi;
        break;
    case 1:
        treeWidget = ui->treeWidgetLocal;
        break;
    default: return;
    }
    QTreeWidgetItem* item = treeWidget->currentItem();
    treeWidget->removeItem(item);
}

void DialogFeatureOptions::on_toolButtonExpand_clicked()
{
    ParameterTreeWidget* treeWidget;
    switch(ui->tabWidget->currentIndex())
    {
    case 0:
        treeWidget = ui->treeWidgetRoi;
        break;
    case 1:
        treeWidget = ui->treeWidgetLocal;
        break;
    default: return;
    }
    QTreeWidgetItem* item = treeWidget->topLevelItem(0);
    if(item->isExpanded()) treeWidget->collapseAll();
    else treeWidget->expandAll();
}


void DialogFeatureOptions::loadOptions(const char* filename)
{
    std::vector<std::string> lista = loadFeatureList(filename);
    if(lista.size() <= 0)
        return;
    QStringList qlista;

    for (unsigned int i = 0; i < lista.size(); ++i)
        qlista.push_back(lista[i].c_str());

    switch(ui->tabWidget->currentIndex())
    {
    case 0:
        ui->treeWidgetRoi->setUniqueLeafs(&qlista);
        break;
    case 1:
        ui->treeWidgetLocal->setUniqueLeafs(&qlista);
        break;
    default: break;
    }

}

void DialogFeatureOptions::on_toolButton_Load_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open feature list"),  NULL, tr("Text file (*.txt);;All files (*)"));
    if (fileName.isEmpty())
        return;
    loadOptions(fileNameRemapRead(&fileName).c_str());
}

void DialogFeatureOptions::on_toolButton_Save_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save feature list"),  NULL, tr("Text file (*.txt);;All files (*)"));
    if (fileName.isEmpty()) return;
    QStringList qlista;
    switch(ui->tabWidget->currentIndex())
    {
    case 0:
        ui->treeWidgetRoi->getUniqueLeafs(&qlista);
        break;
    case 1:
        ui->treeWidgetLocal->getUniqueLeafs(&qlista);
        break;
    default: break;
    }
    saveFeatureList(fileNameRemapWrite(&fileName).c_str(), &qlista);
}



void DialogFeatureOptions::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}

void DialogFeatureOptions::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}

void DialogFeatureOptions::dragLeaveEvent(QDragLeaveEvent* event)
{
    event->accept();
}

void DialogFeatureOptions::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();

    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();

        int ulsize = urlList.size();

        if(ulsize >= 1)
        {
            QString filename = urlList.at(0).toLocalFile();
            loadOptions(fileNameRemapRead(&filename).c_str());
        }
    }
}
