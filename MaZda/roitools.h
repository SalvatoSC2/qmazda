/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ROITOOLS_H
#define ROITOOLS_H

#include "../SharedImage/mazdaroi.h"
#include "../SharedImage/mazdaimage.h"
#include "renderingcontainer.h"

template< int VDimensions >
struct StackElem
{
    int voxel[VDimensions];
};


template< class RenderingContainer >
class RoiTools
{
public:
    /**
     * @brief roiFromSliders creates region from two-thresholding of image
     * @param container
     * @return pointer to region object
     */
    static void roiFromSliders(RenderingContainer* container)
    {
        if(container->getActiveRoiIndex() < 0) return;
        int begin[RenderingContainer::Dimensions];
        int end[RenderingContainer::Dimensions];
        unsigned int size[RenderingContainer::Dimensions];
        if(container->getGrayCanvas() == NULL) return;
        container->getGrayCanvas()->GetSize(size);
        for(int d = 0; d < RenderingContainer::Dimensions; d++)
        {
            begin[d] = 0; end[d] = size[d]-1;
        }
        typename RenderingContainer::MzRoi* roi = new typename RenderingContainer::MzRoi(begin, end);

        MazdaImageIterator <typename RenderingContainer::MzImage> iiter(container->getGrayImage());
        MazdaRoiIterator <typename RenderingContainer::MzRoi> riter(roi);

        double low = container->getImageGrayWindowLow();
        double hi = container->getImageGrayWindowHigh();
        bool itis = false;
        while(! riter.IsBehind())
        {
            double pixel = iiter.GetPixel();
            if(pixel >= low && pixel < hi)
            {
                riter.SetPixel();
                itis = true;
            }
            ++riter;
            ++iiter;
        }
        if(!itis)
        {
            delete roi;
            return;
        }
        typename RenderingContainer::MzRoi* rroi = MazdaRoiResizer< typename RenderingContainer::MzRoi >::Crop(roi);
        delete roi;
        rroi->SetColor(container->getActiveRoi()->GetColor());
        rroi->SetName(container->getActiveRoi()->GetName());
        rroi->SetVisibility(container->getActiveRoi()->GetVisibility());
        container->setActiveRoi(rroi);
    }

    /**
     * @brief createBallElement creates circle or ball shaped region used as structuring element
     * @param radius
     * @return pointer to region object
     */
    static typename RenderingContainer::MzRoi* createBallElement(double radius)
    {
        int begin[RenderingContainer::Dimensions];
        int end[RenderingContainer::Dimensions];
        for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
        {
            begin[d] = -(int)radius; end[d] = (int)radius;
        }

        int radxrad = radius*radius;
        typename RenderingContainer::MzRoi* rroi = new typename RenderingContainer::MzRoi(begin, end);
        MazdaRoiRegionIterator <typename RenderingContainer::MzRoi> riter(rroi, begin, end);


        while(! riter.IsBehind())
        {
            int x, dist;
            dist = 0;
            for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
            {
                x = riter.GetIndex(d)-((int)radius);
                x *= x;
                dist += x;
            }
            if(dist <= radxrad) riter.SetPixel();
            ++riter;
        }

        MazdaRoiRegionIterator <typename RenderingContainer::MzRoi> riter2(rroi, begin, end);
        while(! riter2.IsBehind())
        {
            if(riter2.GetIndex(0) == 0) printf("\n");
            if(riter2.GetPixel()) printf("*");
            else printf(".");
            ++riter2;
        }
        fflush(stdout);

        return rroi;
    }

    /**
     * @brief roiMorphology computes new region after morphological erosion, dilation or median
     * @param container container with region list
     * @param sel structurying element
     * @param mode -1 erode, 0 median, +1 dilate
     * @param forwhat 0 apply for current roi, 1 apply for visible rois, 2 apply for all the rois
     */
    static void roiMorphology(RenderingContainer* container, typename RenderingContainer::MzRoi* sel, int mode, int forwhat)
    {
        int begin[RenderingContainer::Dimensions];
        int end[RenderingContainer::Dimensions];
        int sbegin[RenderingContainer::Dimensions];
        int send[RenderingContainer::Dimensions];
        int rbegin[RenderingContainer::Dimensions];
        int rend[RenderingContainer::Dimensions];
        int srbegin[RenderingContainer::Dimensions];
        int srend[RenderingContainer::Dimensions];
        int obegin[RenderingContainer::Dimensions];
        int oend[RenderingContainer::Dimensions];
        unsigned int white;
        unsigned int black;
        unsigned int maxwhite;
        unsigned int maxblack;
        sel->GetBegin(sbegin);
        sel->GetEnd(send);
        white = 0;
        MazdaRoiRegionIterator <typename RenderingContainer::MzRoi> siter(sel, sbegin, send);
        while(! siter.IsBehind())
        {
            if(siter.GetPixel()) white++;
            ++siter;
        }
        switch(mode)
        {
        case -1: maxwhite = white-1; maxblack = 0; break;
        case 1: maxblack = white-1; maxwhite = 0; break;
        default: maxwhite = maxblack = white/2; break;
        }

        int ar = container->getActiveRoiIndex();
        int nr = container->getNumberOfRois();
        if(ar < 0 || nr <= 0) return;
        for(int r = (forwhat ? 0 : ar); r < (forwhat ? nr: ar+1); r++)
        {
            typename RenderingContainer::MzRoi* roi = container->getRoi(r);
            if(forwhat == 1 && (!roi->GetVisibility())) continue;
            if(roi->IsEmpty()) continue;
            roi->GetBegin(begin);
            roi->GetEnd(end);
            for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
            {
                obegin[d] = begin[d] + sbegin[d];
                oend[d] = end[d] + send[d];
            }
            typename RenderingContainer::MzRoi* oroi = new typename RenderingContainer::MzRoi(obegin, oend);

            MazdaRoiRegionIterator <typename RenderingContainer::MzRoi> oiter(oroi, obegin, oend);
            while(! oiter.IsBehind())
            {
                //oiter.SetPixel();
                for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
                {
                    int x = oiter.GetIndex(d)+obegin[d];
                    rbegin[d] = x+sbegin[d];
                    rend[d] = x+send[d];

                    if(rbegin[d] < begin[d]) rbegin[d] = begin[d];
                    if(rend[d] > end[d]) rend[d] = end[d];
                    srbegin[d] = rbegin[d]-x;
                    srend[d] = rend[d]-x;
                }
//                printf("J %i %i P %i %i\n", rbegin[0], rbegin[1], begin[0], begin[1]);
//                fflush(stdout);
                MazdaRoiRegionIterator <typename RenderingContainer::MzRoi> iter(roi, rbegin, rend);
                MazdaRoiRegionIterator <typename RenderingContainer::MzRoi> siter(sel, srbegin, srend);
                white = black = 0;
                while(! siter.IsBehind())
                {
                    if(siter.GetPixel())
                    {
//                        printf("s");
                        if(iter.GetPixel()) white++;
                        else black++;

                        if(white > maxwhite)
                        {
                            oiter.SetPixel();
                            break;
                        }
                        if(black > maxblack) break;
                    }
                    ++iter;
                    ++siter;
                }
                ++oiter;
            }
            typename RenderingContainer::MzRoi* rroi = MazdaRoiResizer< typename RenderingContainer::MzRoi >::Crop(oroi);
            delete oroi;
            rroi->SetName(roi->GetName());
            rroi->SetColor(roi->GetColor());
            rroi->SetVisibility(roi->GetVisibility());
            container->setRoi(r, rroi);
        }
    }

    /**
     * @brief floodFillSplit flood-fills input roi with zeros and sets the same pixels in output roi
     * @param input
     * @param output
     * @param seed
     */
    static void floodFillSplit(typename RenderingContainer::MzRoi* input, typename RenderingContainer::MzRoi* output, StackElem <RenderingContainer::Dimensions> seed)
    {
        std::vector <StackElem <RenderingContainer::Dimensions> > voxelstack;
        unsigned int size[RenderingContainer::Dimensions];
        input->GetSize(size);
//        for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
//        {
//            if(seed.voxel[d] >= size[d]) return;
//        }
        StackElem <RenderingContainer::Dimensions> current = seed;
        try{
            voxelstack.push_back(current);
            do
            {
                current = voxelstack.back();
                voxelstack.pop_back();

                for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
                {
                    current.voxel[d]++;
                    if(current.voxel[d] < size[d])
                    {
                        bool color = input->GetPixel(current.voxel);
                        if(color)
                        {
                            voxelstack.push_back(current);
                            input->ClrPixel(current.voxel);
                            output->SetPixel(current.voxel);
                        }
                    }
                    current.voxel[d]-=2;
                    if(current.voxel[d] < size[d])
                    {
                        bool color = input->GetPixel(current.voxel);
                        if(color)
                        {
                            voxelstack.push_back(current);
                            input->ClrPixel(current.voxel);
                            output->SetPixel(current.voxel);
                        }
                    }
                    current.voxel[d]++;
                }
             }while(voxelstack.size() > 0);
        }
        catch(...){}
    }

    /**
     * @brief splitRois splits active roi into separate regions
     * @param container
     */
    static void splitRois(RenderingContainer* container)
    {
        typename RenderingContainer::MzRoi* roi = container->getActiveRoi();
        if(roi == NULL) return;
        if(roi->IsEmpty()) return;

        unsigned int color = container->getNumberOfRois();
        typename RenderingContainer::MzRoi* temproi;
        MazdaRoiRegionCopier< typename RenderingContainer::MzRoi, RoiAddOperation<typename RenderingContainer::MzRoi::BlockType> > copier;
        temproi = copier.Copy(roi);
        int begin[RenderingContainer::Dimensions];
        int end[RenderingContainer::Dimensions];
        temproi->GetBegin(begin);
        temproi->GetEnd(end);

        MazdaRoiRegionIterator <typename RenderingContainer::MzRoi> iter(temproi, begin, end);
        while(! iter.IsBehind())
        {
            if(iter.GetPixel())
            {
                typename RenderingContainer::MzRoi* droi = new typename RenderingContainer::MzRoi(begin, end);
                StackElem <RenderingContainer::Dimensions> seed;
                for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
                {
                    seed.voxel[d] = iter.GetIndex(d);
                }
                floodFillSplit(temproi, droi, seed);

                typename RenderingContainer::MzRoi* rroi = MazdaRoiResizer< typename RenderingContainer::MzRoi >::Crop(droi);
                delete droi;
                rroi->SetName(roi->GetName());
                rroi->SetColor(RoiDefaultColors[color%(sizeof(RoiDefaultColors)/sizeof(unsigned int))]);
                color++;
                rroi->SetVisibility(roi->GetVisibility());
                container->appendRoi(rroi);
            }
            ++iter;
        }
        delete temproi;
    }


    /**
     * @brief mergeRois merges visible regions in one
     * @param container
     */
    static void mergeRois(RenderingContainer* container)
    {
        int begin[RenderingContainer::Dimensions];
        int end[RenderingContainer::Dimensions];

        bool set = false;
        for(unsigned int r = 0; r < container->getNumberOfRois(); r++)
        {
            typename RenderingContainer::MzRoi* sroi = container->getRoi(r);
            if(sroi == NULL) continue;
            if(sroi->IsEmpty()) continue;
            if(! sroi->GetVisibility()) continue;
            if(! set)
            {
                set = true;
                sroi->GetBegin(begin);
                sroi->GetEnd(end);
            }
            else
            {
                int sb[RenderingContainer::Dimensions];
                int se[RenderingContainer::Dimensions];
                sroi->GetBegin(sb);
                sroi->GetEnd(se);
                for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
                {
                    if(begin[d] > sb[d]) begin[d] = sb[d];
                    if(end[d] < se[d]) end[d] = se[d];
                }
            }
        }
        if(!set) return;
        for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
        {
            if(begin[d] > end[d]) return;
        }
        typename RenderingContainer::MzRoi* droi = new typename RenderingContainer::MzRoi(begin, end);
        set = false;
        for(unsigned int r = 0; r < container->getNumberOfRois(); r++)
        {
            typename RenderingContainer::MzRoi* sroi = container->getRoi(r);
            if(sroi == NULL) continue;
            if(sroi->IsEmpty()) continue;
            if(! sroi->GetVisibility()) continue;
            if(! set)
            {
                set = true;
                droi->SetName(sroi->GetName());
                droi->SetColor(RoiDefaultColors[container->getNumberOfRois()%(sizeof(RoiDefaultColors)/sizeof(unsigned int))]);
                droi->SetVisibility(true);
            }
            int sb[RenderingContainer::Dimensions];
            int se[RenderingContainer::Dimensions];
            sroi->GetBegin(sb);
            sroi->GetEnd(se);

            MazdaRoiRegionIterator <typename RenderingContainer::MzRoi> siter(sroi, sb, se);
            MazdaRoiRegionIterator <typename RenderingContainer::MzRoi> diter(droi, sb, se);
            while(! siter.IsBehind())
            {
                if(siter.GetPixel()) diter.SetPixel();
                ++siter;
                ++diter;
            }
        }
        container->appendRoi(droi);
    }

    /**
     * @brief removeSmall removes regions smaller then volume
     * @param container
     * @param volume the minimum volume
     * @return list of removed region indices
     */
    static std::vector<unsigned int> removeSmall(RenderingContainer* container, unsigned int volume)
    {
        std::vector<unsigned int> toReturn;
        unsigned int count = container->getNumberOfRois();
        for(unsigned int r = count-1; r < count; r--)
        {
            typename RenderingContainer::MzRoi* roi = container->getRoi(r);
            if(roi == NULL)
            {
                container->removeRoi(r);
                toReturn.push_back(r);
            }
            else if(roi->IsEmpty())
            {
                container->removeRoi(r);
                toReturn.push_back(r);
            }
            else
            {
                int sb[RenderingContainer::Dimensions];
                int se[RenderingContainer::Dimensions];
                roi->GetBegin(sb);
                roi->GetEnd(se);

                unsigned int v = 1;
                for(unsigned int d = 0; d < RenderingContainer::Dimensions; d++)
                {
                    int s = se[d]-sb[d];
                    if(s < 0)
                    {
                        v = 0;
                        break;
                    }
                    v *= s;
                }
                if(v > volume)
                {
                    v = 0;
                    MazdaRoiIterator <typename RenderingContainer::MzRoi> iter(roi);
                    while(! iter.IsBehind())
                    {
                        if(iter.GetPixel()) v++;
                        ++iter;
                    }
                }
                if(v <= volume)
                {
                    container->removeRoi(r);
                    toReturn.push_back(r);
                }
            }
        }
        return toReturn;
    }
};

#endif // ROITOOLS_H
