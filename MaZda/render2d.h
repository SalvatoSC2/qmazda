/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef IMAGEROI_H
#define IMAGEROI_H

//#include <QScrollArea>
#include <QWidget>
#include <QWheelEvent>
#include "renderingcontainer.h"
#include "rendererinterface.h"

typedef RenderingContainer<2> RenderingContainer2D;

class Renderer2DState
{
public:
    Renderer2DState(void)
    {
        color = 0x17;
        Reset();
    }
    void Reset(void)
    {
        zoom_factor = 1.0;
        x_shift = 0.0;
        y_shift = 0.0;
    }
    double zoom_factor;
    double x_shift;
    double y_shift;
    unsigned int color;
};

class Renderer2D : public QWidget, RendererInterface
{
    Q_OBJECT
public:
    void paintEvent(QPaintEvent *e);
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *ev);
    explicit Renderer2D(RenderingContainer2D* container, QWidget *parent = 0);
    ~Renderer2D(void);
    void ResetView(void);
    bool SetContainer(RenderingContainerInterface *container);
    void SetZoom(double zoom);
    double GetZoom(void);
    void SetBackground(unsigned int  color);
    unsigned int GetBackground(void);
    void Repaint();
    static void initColor(unsigned int color);
signals:
    void setZoomRequest(double zoom);
    void setRoiListRequest(void);
    void setStatusTextRequest(QString* string);
protected:
    Renderer2DState state;
    RenderingContainer2D* container;
    QImage screen_image;
private:
    static Renderer2DState persistent_state;
    void zoomKeepLocation(int x, int y, double previous_zoom, double current_zoom);

};

#endif // IMAGEROI_H
