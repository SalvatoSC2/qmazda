cmake_minimum_required(VERSION 3.1)

project(MaZda LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5 COMPONENTS Widgets REQUIRED)

add_executable(MaZda 
    main.cpp
    mainwindow.cpp
    render2d.cpp
    render3d.cpp
    renderarea.cpp
    roilistwidget.cpp
    Painters/paintpencil.cpp
    painter2d.cpp
    painter3d.cpp
    Painters/paintelli.cpp
    Painters/paintfloodfill.cpp
    Painters/paintmove.cpp
    Painters/paintpoly.cpp
    Painters/paintrect.cpp
    Painters/paint3dpen.cpp
    Painters/paint3dball.cpp
    Painters/paint3dblock.cpp
    Painters/paint3dcube.cpp
    Painters/paint3dfloodfill.cpp
    Painters/paint3dmove.cpp
    Painters/paint3dtube.cpp
    Painters/sphear.cpp
    randomnames.cpp
    dockeditoptions.cpp
    ../MzShared/parametertreewidget.cpp
    dialogfeatureoptions.cpp
    ../MzShared/parametertreetemplate.cpp
    ../MzShared/filenameremap.cpp
    dialogvoxelsize.cpp
    mzroidock.cpp
    mainwindow.ui
    dockeditoptions.ui
    dialogfeatureoptions.ui
    dialogvoxelsize.ui
    mazdares.qrc
    mzimage.icns
    MZMain.rc
)

target_include_directories(MaZda PRIVATE ../MzShared)

# target_compile_definitions(MaZda PRIVATE USE_DEBUG_LOG)
# target_compile_definitions(MaZda PRIVATE USE_NOTIFICATIONS)
# target_compile_definitions(MaZda PRIVATE MAZDA_IMAGE_8BIT_PIXEL)
# target_compile_definitions(MaZda PRIVATE MAZDA_CONVERT_BGR_IMAGE)
# target_compile_definitions(MaZda PRIVATE MAZDA_HIDE_ON_PROGRESS)
target_compile_definitions(MaZda PRIVATE MAZDA_IN_MACOS_BUNDLE)

find_package(ITK REQUIRED)
include(${ITK_USE_FILE})
if( "${ITK_VERSION_MAJOR}" LESS 4 )
  target_link_libraries(MaZda ITKReview ${ITK_LIBRARIES})
else( "${ITK_VERSION_MAJOR}" LESS 4 )
  target_link_libraries(MaZda PRIVATE ${ITK_LIBRARIES})
endif( "${ITK_VERSION_MAJOR}" LESS 4 )

find_package(OpenCV 3 REQUIRED)
target_link_libraries(MaZda PRIVATE ${OpenCV_LIBS} )

find_package(TIFF REQUIRED)
target_link_libraries(MaZda PRIVATE TIFF::TIFF)

find_package(OpenGL REQUIRED)
target_link_libraries(MaZda PRIVATE ${OPENGL_LIBRARY} )

target_link_libraries(MaZda PRIVATE Qt5::Widgets)

# find_package(dlib REQUIRED)
# target_link_libraries(MaZda PRIVATE dlib::dlib)

install(TARGETS MaZda DESTINATION bin)
