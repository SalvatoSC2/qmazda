/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAINTER3D_H
#define PAINTER3D_H

#include "Painters/ipaint.h"
#include "render3d.h"

class Painter3D : public Renderer3D
{
public:
    Painter3D(RenderingContainer3D *container, QWidget *parent);
    ~Painter3D();

    void paintEvent(QPaintEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *ev);
//    void keyPressEvent(QKeyEvent *ev);
//    void keyReleaseEvent(QKeyEvent *ev);

    void setDrawingTool(DrawingTool idrawingtool, bool ieraser);

    void getSize(unsigned int* size);
    void getSpacing(double* spacing);


    void MzImage2MzRoi(int beginr[], int endr[], RenderingContainer3D::MzRGBImage8::PixelType color, bool erase);
//    bool Coordinates3Dto2D(double input[3], double output[3], bool update_matrices);
    bool Coordinates2Dto3D(int input[2], int output[3]);
    bool Coordinates2Dto3D(const int input[2], const GLdouble reference[3], GLdouble output[3], GLdouble outputref[]);
    void showModel(bool show);

private:
    void deletePaintTools(void);
    void repaintAction(AfterPaintAction action);
    iPaint* painttoolobject;
    DrawingTool drawingtool;
    bool eraser;
protected:
    bool eventFilter(QObject* obj, QEvent* e);
    //class MainWindowEventHandler : public QObject
    //{
    //    Q_OBJECT
    //protected:
    //    bool eventFilter(QObject* obj, QEvent* e);
    //};
};

#endif // PAINTER3D_H
