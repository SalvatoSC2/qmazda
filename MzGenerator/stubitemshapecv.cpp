/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemshapecv.h"
#include "stubitemshape.h"

StubItemShapeCv::~StubItemShapeCv()
{
}
StubItem* StubItemShapeCv::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemShapeCv* item = new StubItemShapeCv;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();

    item->ProfileArea = (unsigned int) -1;
    item->Perimeter = (unsigned int) -1;

    item->X = (unsigned int) -1;
    item->Y = (unsigned int) -1;
    item->Width = (unsigned int) -1;
    item->Height = (unsigned int) -1;
    item->ProfileArea = (unsigned int) -1;
    item->EquivDiameter = (unsigned int) -1;
    item->Perimeter = (unsigned int) -1;
    item->Roundness = (unsigned int) -1;
    item->ElipsAngle = (unsigned int) -1;
    item->ElipsHeight = (unsigned int) -1;
    item->ElipsWidth = (unsigned int) -1;
    item->ElipsElong = (unsigned int) -1;
    item->MomTheta = (unsigned int) -1;
    item->MomElong = (unsigned int) -1;
    item->HuMom1 = (unsigned int) -1;
    item->HuMom2 = (unsigned int) -1;
    item->HuMom3 = (unsigned int) -1;
    item->HuMom4 = (unsigned int) -1;
    item->HuMom5 = (unsigned int) -1;
    item->HuMom6 = (unsigned int) -1;
    item->HuMom7 = (unsigned int) -1;
    item->ChRectWidth = (unsigned int) -1;
    item->ChRectHeight = (unsigned int) -1;
    item->ChRectArea = (unsigned int) -1;
    item->ChArea = (unsigned int) -1;
    item->ChPerim = (unsigned int) -1;
    item->ChMinFeret = (unsigned int) -1;
    item->ChMaxFeret = (unsigned int) -1;
    item->RadDist = (unsigned int) -1;
    item->RadDistsqrd = (unsigned int) -1;
    item->RadDistdev = (unsigned int) -1;
    item->Radbb = (unsigned int) -1;

    item->compute_radii = false;
    item->compute_elips = false;
    item->compute_convex = false;
    item->compute_moments = false;
    item->compute_humoments = false;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemShapeCv::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    if(itemf->Name() == "X") {X = itemf->Index(); compute_moments = true;}
    else if(itemf->Name() == "Y") {Y = itemf->Index(); compute_moments = true;}
    else if(itemf->Name() == "PixelsCount") {PixelsCount = itemf->Index();}
    else if(itemf->Name() == "ProfileArea") {ProfileArea = itemf->Index();}
    else if(itemf->Name() == "EquivDiameter") {EquivDiameter = itemf->Index();}
    else if(itemf->Name() == "Perimeter") {Perimeter = itemf->Index();}
    else if(itemf->Name() == "Roundness") {Roundness = itemf->Index();}
    else if(itemf->Name() == "ElipsAngle") {ElipsAngle = itemf->Index(); compute_elips = true;}
    else if(itemf->Name() == "ElipsHeight") {ElipsHeight = itemf->Index(); compute_elips = true;}
    else if(itemf->Name() == "ElipsWidth") {ElipsWidth = itemf->Index(); compute_elips = true;}
    else if(itemf->Name() == "ElipsElong") {ElipsElong = itemf->Index(); compute_elips = true;}
    else if(itemf->Name() == "MomTheta") {MomTheta = itemf->Index(); compute_moments = true;}
    else if(itemf->Name() == "MomElong") {MomElong = itemf->Index(); compute_moments = true;}
    else if(itemf->Name() == "HuMom1") {HuMom1 = itemf->Index(); compute_humoments = true;}
    else if(itemf->Name() == "HuMom2") {HuMom2 = itemf->Index(); compute_humoments = true;}
    else if(itemf->Name() == "HuMom3") {HuMom3 = itemf->Index(); compute_humoments = true;}
    else if(itemf->Name() == "HuMom4") {HuMom4 = itemf->Index(); compute_humoments = true;}
    else if(itemf->Name() == "HuMom5") {HuMom5 = itemf->Index(); compute_humoments = true;}
    else if(itemf->Name() == "HuMom6") {HuMom6 = itemf->Index(); compute_humoments = true;}
    else if(itemf->Name() == "HuMom7") {HuMom7 = itemf->Index(); compute_humoments = true;}
    else if(itemf->Name() == "ChRectWidth") {ChRectWidth = itemf->Index(); compute_convex = true;}
    else if(itemf->Name() == "ChRectHeight") {ChRectHeight = itemf->Index(); compute_convex = true;}
    else if(itemf->Name() == "ChRectArea") {ChRectArea = itemf->Index(); compute_convex = true;}
    else if(itemf->Name() == "ChArea") {ChArea = itemf->Index(); compute_convex = true;}
    else if(itemf->Name() == "ChPerim") {ChPerim = itemf->Index(); compute_convex = true;}
    else if(itemf->Name() == "ChMinFeret") {ChMinFeret = itemf->Index(); compute_convex = true;}
    else if(itemf->Name() == "ChMaxFeret") {ChMaxFeret = itemf->Index(); compute_convex = true;}
    else if(itemf->Name() == "RadDist") {RadDist = itemf->Index(); compute_radii = true;}
    else if(itemf->Name() == "RadDistsqrd") {RadDistsqrd = itemf->Index(); compute_radii = true;}
    else if(itemf->Name() == "RadDistdev") {RadDistdev = itemf->Index(); compute_radii = true;}
    else if(itemf->Name() == "Radbb") {Radbb = itemf->Index(); compute_radii = true;}

    children.push_back(item);
}

void StubItemShapeCv::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);

    if(datas->roi != NULL)
    {
        try
        {
            if(dimensions == 2) inRoi2D(datas);
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}

void StubItemShapeCv::inRoi2D(StubItemDataEntryRoi* datas)
{
    MzRoi2D* roi = static_cast<MzRoi2D*> (datas->roi);
    double* table = datas->table;
    WykonajOliczenia(roi, table);
}

double StubItemShapeCv::segmentGetMaxAreaContour(CvSeq* contours, CvSeq** contour)
{
    double maxArea = -1;
    *contour = contours;
    CvSeq* cr = contours;
    while(cr != NULL)
    {
        double area = cvContourArea(cr);
        if(area > maxArea)
        {
            *contour = cr;
            maxArea = area;
        }
        cr = cr->h_next;
    }
    return maxArea;
}

void StubItemShapeCv::WykonajOliczenia(MzRoi2D* roi, double* table)
{
    unsigned int size[4];
    roi->GetSize(size);
    IplImage* img = cvCreateImage(cvSize(size[0]+2, size[1]+2), IPL_DEPTH_8U, 1);
    MazdaRoiIterator<MzRoi2D> rit(roi);
    int begin[2];
    int end[2];
    roi->GetBegin(begin);
    roi->GetEnd(end);
    memset(img->imageData, 0, img->imageSize);

    rit.GoToBegin();
//    for(int y = begin[1]; y <= end[1]; y++)
//    {
//        unsigned char* ip = (unsigned char*)(img->imageData) + img->widthStep*(y+1) + 1;
//        for(int x = begin[0]; x <= end[0]; x++)
//        {
//            if(rit.GetPixel())
//                *ip = 255;
//            else
//                *ip = 0;
//            ip++;
//            ++rit;
//        }
//    }

    unsigned int pixcount = 0;
    for(int y = 1; y < img->height-1; y++)
    {
        unsigned char* ip = (unsigned char*)(img->imageData) + img->widthStep*y +1;
        for(int x = 1; x < img->width-1; x++)
        {
            if(rit.GetPixel())
            {
                pixcount ++;
                *ip = 255;
            }
            else
                *ip = 0;
            ip++;
            ++rit;
        }
    }
    if(PixelsCount != (unsigned int)-1) table[PixelsCount] = pixcount;

//    for(int y = 0; y < img->height; y++)
//    {
//        unsigned char* ip = (unsigned char*)(img->imageData) + img->widthStep*y;
//        for(int x = 0; x < img->width; x++)
//        {
//            if(*ip == 0) printf(".");
//            else if(*ip == 255) printf("X");
//            else printf("?");
//            fflush(stdout);
//            ip++;
//        }
//        printf("\n");
//    }
//    fflush(stdout);

    CvMemStorage* storage = NULL;
    CvSeq* contours = NULL;
    CvSeq* contour = NULL;
    storage = cvCreateMemStorage(0);

//    cvNamedWindow("Window");
//    cvShowImage("Window", img);
//    cvWaitKey(10000);

    cvFindContours(img, storage, &contours, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));
    if(contours == NULL)
    {
        cvReleaseMemStorage(&storage);
        cvReleaseImage(&img);
        return;
    }
    double area = segmentGetMaxAreaContour(contours, &contour);
    if(area < 1 || contour->total < 6)
    {
        cvReleaseMemStorage(&storage);
        cvReleaseImage(&img);
        return;
    }

    double perim = 1.0;
    if(Perimeter != (unsigned int)-1 || Roundness != (unsigned int)-1)
         perim = cvContourPerimeter(contour);
    if(ProfileArea != (unsigned int)-1) table[ProfileArea] = area;
    if(Perimeter != (unsigned int)-1) table[Perimeter] = perim;
    if(Roundness != (unsigned int)-1) table[Roundness] = (4 * M_PI * area)/(perim*perim);
    if(EquivDiameter != (unsigned int)-1) table[EquivDiameter] = 2.0*sqrt(area/3.1415927);

    if(compute_elips)
    {
        CvBox2D box = cvFitEllipse2(contour);
        if(ElipsAngle != (unsigned int)-1) table[ElipsAngle] = box.angle*M_PI/180.0;
        if(ElipsHeight != (unsigned int)-1) table[ElipsHeight] = box.size.height;
        if(ElipsWidth != (unsigned int)-1) table[ElipsWidth] = box.size.width;
        if(ElipsElong != (unsigned int)-1)
        {
            double ellheight = box.size.height;
            double ellwidth = box.size.width;
            double ellelong;
            if(ellheight < ellwidth)
            {
                ellelong = ellwidth;
                ellwidth = ellheight;
                ellheight = ellelong;
            }
            ellelong = ellheight/ellwidth;
            table[ElipsElong] = ellelong;
        }
    }

    double centerx = 0;
    double centery = 0;
    if(compute_moments || compute_humoments || compute_radii)
    {
        CvMoments ms;
        cvMoments(contour, &ms);
        centerx = ms.m10/ms.m00;
        centery = ms.m01/ms.m00;
        if(X != (unsigned int)-1) table[X] = centerx + begin[0] - 1;
        if(Y != (unsigned int)-1) table[Y] = centery + begin[1] - 1;
        if(MomTheta != (unsigned int)-1) table[MomTheta] = atan2(ms.mu11, ms.mu20 - ms.mu02)/2.0;
        if(MomElong != (unsigned int)-1)
        {
            double elx = ms.mu20 + ms.mu02;
            double ely = sqrt(4.0 * ms.mu11 * ms.mu11 + (ms.mu20 - ms.mu02)*(ms.mu20 - ms.mu02));
            table[MomElong] = (elx+ely)/(elx-ely);
        }
        if(compute_humoments)
        {
            CvHuMoments hms;
            cvGetHuMoments(&ms, &hms);
            if(HuMom1 != (unsigned int)-1) table[HuMom1] = hms.hu1;
            if(HuMom2 != (unsigned int)-1) table[HuMom2] = hms.hu2;
            if(HuMom3 != (unsigned int)-1) table[HuMom3] = hms.hu3;
            if(HuMom4 != (unsigned int)-1) table[HuMom4] = hms.hu4;
            if(HuMom5 != (unsigned int)-1) table[HuMom5] = hms.hu5;
            if(HuMom6 != (unsigned int)-1) table[HuMom6] = hms.hu6;
            if(HuMom7 != (unsigned int)-1) table[HuMom7] = hms.hu7;
        }
    }

    if(compute_convex)
    {
        CvMemStorage* hullStorage = cvCreateMemStorage(0);
        CvSeq *hcontour = cvConvexHull2(contour, hullStorage, CV_CLOCKWISE, 1);

        if(ChArea != (unsigned int)-1) table[ChArea] = cvContourArea(hcontour);
        if(ChPerim != (unsigned int)-1) table[ChPerim] = cvContourPerimeter(hcontour);

        if(ChRectArea != (unsigned int)-1 || ChRectHeight != (unsigned int)-1 || ChRectWidth != (unsigned int)-1)
        {
            CvBox2D box = cvMinAreaRect2(hcontour);
            if(box.size.width <= box.size.height)
            {
                if(ChRectHeight != (unsigned int)-1) table[ChRectHeight] = box.size.height;
                if(ChRectWidth != (unsigned int)-1) table[ChRectWidth] = box.size.width;
            }
            else
            {
                if(ChRectHeight != (unsigned int)-1) table[ChRectHeight] = box.size.width;
                if(ChRectWidth != (unsigned int)-1) table[ChRectWidth] = box.size.height;
            }
            if(ChRectArea != (unsigned int)-1) table[ChRectArea] = box.size.height*box.size.width;
        }

        if(ChMinFeret != (unsigned int)-1 || ChMaxFeret != (unsigned int)-1)
        {
            double minfer = -1;
            double maxfer = -1;
            int imax = hcontour->total;
            for(int i = 0; i < imax; i++)
            {
                double maxnn = 0;
                unsigned int j = (i+1) % imax;
                CvPoint* ppi = (CvPoint*) cvGetSeqElem(hcontour, i);
                CvPoint* ppj = (CvPoint*) cvGetSeqElem(hcontour, j);

                double xn = ppi->y - ppj->y;
                double yn = ppj->x - ppi->x;
                double nn = sqrt(xn*xn + yn*yn);
                xn /= nn;
                yn /= nn;

                for(int k = 0; k < imax; k++)
                {
                    CvPoint* ppk = (CvPoint*) cvGetSeqElem(contour, k);
                    nn = xn*(ppk->x - ppi->x) + yn*(ppk->y - ppi->y);
                    nn = fabs(nn);
                    if(maxnn < nn) maxnn = nn;
                }
                if(minfer < 0 || minfer > maxnn) minfer = maxnn;
                if(maxfer < maxnn) maxfer = maxnn;
            }
            if(ChMinFeret != (unsigned int)-1) table[ChMinFeret] = minfer;
            if(ChMaxFeret != (unsigned int)-1) table[ChMaxFeret] = maxfer;
        }
        cvReleaseMemStorage(&hullStorage);
    }

    if(compute_radii)
    {
        double area = 0.0;
        double dist = 0.0;
        double dist2 = 0.0;
        int imax = contour->total;
        for(int i = 0; i < imax; i++)
        {
            unsigned int j = (i+1) % imax;
            CvPoint* ppi = (CvPoint*) cvGetSeqElem(contour, i);
            CvPoint* ppj = (CvPoint*) cvGetSeqElem(contour, j);

            double x1 = ppi->x - centerx;
            double x2 = ppj->x - centerx;
            double y1 = ppi->y - centery;
            double y2 = ppj->y - centery;
            double tarea2_t = (x1*y2 -x2*y1);
            double dist2_t = sqrt(x1*x1+y1*y1);
            dist2_t += sqrt(x2*x2+y2*y2);
            area += tarea2_t;
            dist += tarea2_t*dist2_t;
            dist2 += tarea2_t*dist2_t*dist2_t;
        }
        area /= 2.0;
        dist /= 4.0;
        dist2 /= 8.0;
        dist /= area;
        dist2 /= area;
        if(RadDist != (unsigned int)-1) table[RadDist] = dist;
        if(RadDistsqrd != (unsigned int)-1) table[RadDistsqrd] = dist2;
        if(RadDistdev != (unsigned int)-1) table[RadDistdev] = sqrt(dist2)-dist;
        if(Radbb != (unsigned int)-1) table[Radbb] = area / sqrt(dist2);
    }

    cvReleaseMemStorage(&storage);
    cvReleaseImage(&img);
}
