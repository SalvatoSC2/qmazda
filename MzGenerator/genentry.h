/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENENTRY_H
#define GENENTRY_H

#include "stubitem.h"

#ifdef USE_DEBUG_LOG
extern std::ofstream debugfile;
#endif

enum GentryMode {roi, local};

extern char* execute_command;
extern char* modestring;
extern char* input_regions;
extern char* output_data;

extern unsigned long int notifications;
extern unsigned long int maxnotifications;

extern void notify(void);
extern void notifyText(const char *message);
extern int gentryScan(int argc, char* argv[]);
extern int gentryRun(void);


#endif // GENENTRY_H
