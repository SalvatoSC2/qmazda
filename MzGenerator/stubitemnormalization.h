/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMNORMALIZATION_H
#define STUBITEMNORMALIZATION_H


#define N_sigma 3.0
#include "genentry.h"
#include "math.h"

const int WhiteValue = std::numeric_limits<MazdaImagePixelType>::max();
const int WhitePlusValue = WhiteValue+1;
const int HalfValue = WhiteValue/2;

class StubItemNormalization : public StubItem
{
public:
    ~StubItemNormalization();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Execute(StubItemData* data);
    bool IsNotifier(){return false;}

private:
    template <unsigned int VDimensions>
    MazdaDummy* inRoi(StubItemDataEntry* datas)
    {
        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return NULL;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
        }
        if(quit) return NULL;

        int min = 0;
        int max = 0;
        MazdaImageRegionIterator<MITypeLocal> ci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        if(normalization == 'M')
        {
            min = WhiteValue;
            max = 0;
            while(!ri.IsBehind())
            {
                if(ri.GetPixel())
                {
                    int ii = ci.GetPixel();
                    if(ii > max) max = ii;
                    if(ii < min) min = ii;
                }
                ++ri;
                ++ci;
            }
        }
        else if(normalization == 'S')
        {
            unsigned long int area = 0;
            double mean = 0.0;
            double stdev = 0.0;
            while(!ri.IsBehind())
            {
                if(ri.GetPixel())
                {
                    double ii = ci.GetPixel();
                    mean += ii;
                    stdev += (ii * ii);
                    area++;
                }
                ++ri;
                ++ci;
            }
            mean /= area;
            stdev /= area;
            stdev -= (mean * mean);
            stdev = sqrt(stdev);
            min = (mean - (N_sigma*stdev));
            max = (mean + (N_sigma*stdev));
        }
        else if(normalization == 'N')
        {
            unsigned long int area = 0;
            unsigned long int* histogram = new unsigned long int[WhitePlusValue];
            memset(histogram, 0, sizeof(unsigned long int)*WhitePlusValue);
            while(!ri.IsBehind())
            {
                if(ri.GetPixel())
                {
                    histogram[ci.GetPixel()] ++;
                    area++;
                }
                ++ri;
                ++ci;
            }
            unsigned int i;
            for(i = 1; i < WhitePlusValue; i++) histogram[i] += histogram[i-1];
            double k01=(double)area/100;
            double k99=(double)area-k01;
            for(i = 0; i < WhitePlusValue; i++)
            {
                if(histogram[i]>=k01) break;
            }
            min  = i + 1;
            for(; i < WhitePlusValue; i++)
            {
                if(histogram[i]>=k99) break;
            }
            max = i;
            delete[] histogram;
        }
        else return NULL;

        ci.GoToBegin();
        ri.GoToBegin();
        MITypeLocal* normalized = new MITypeLocal(size, spacing, true);
        MazdaImageRegionIterator<MITypeLocal> co = MazdaImageRegionIterator<MITypeLocal>(normalized, (unsigned int*) begin, (unsigned int*) end);
        int maxmin = max - min + 1;

        if(maxmin == 0)
        {
            while(!ri.IsBehind())
            {
                if(ri.GetPixel())
                {
                    co.SetPixel(HalfValue);
                }
                ++ri;
                ++co;
            }
        }
        else
        {
            while(!ri.IsBehind())
            {
                if(ri.GetPixel())
                {
                    long int p = ci.GetPixel();
                    co.SetPixel((p>max ?maxmin :(p<min ?0 :p-min)) * (long int)WhitePlusValue / maxmin);
                }
                ++ri;
                ++ci;
                ++co;
            }
        }
        return normalized;
    }

    template <unsigned int VDimensions>
    MazdaDummy* noRoi(StubItemDataEntry* datas)
    {
        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        int min = 0;
        int max = 0;
        MazdaImageIterator<MITypeLocal> ci = MazdaImageIterator<MITypeLocal>(image);
        if(normalization == 'M')
        {
            min = WhiteValue;
            max = 0;
            while(!ci.IsBehind())
            {
                int ii = ci.GetPixel();
                if(ii > max) max = ii;
                if(ii < min) min = ii;
                ++ci;
            }
        }
        else if(normalization == 'S')
        {
            unsigned long int area = 0;
            double mean = 0.0;
            double stdev = 0.0;
            while(!ci.IsBehind())
            {
                double ii = ci.GetPixel();
                mean += ii;
                stdev += (ii * ii);
                area++;
                ++ci;
            }
            mean /= area;
            stdev /= area;
            stdev -= (mean * mean);
            stdev = sqrt(stdev);
            min = (mean - (N_sigma*stdev));
            max = (mean + (N_sigma*stdev));
        }
        else if(normalization == 'N')
        {
            unsigned long int area = 0;
            unsigned long int* histogram = new unsigned long int[WhitePlusValue];
            memset(histogram, 0, sizeof(unsigned long int)*WhitePlusValue);
            while(!ci.IsBehind())
            {
                histogram[ci.GetPixel()] ++;
                area++;
                ++ci;
            }
            unsigned int i;
            for(i = 1; i < WhitePlusValue; i++) histogram[i] += histogram[i-1];
            double k01=(double)area/100;
            double k99=(double)area-k01;
            for(i = 0; i < WhitePlusValue; i++)
            {
                if(histogram[i]>=k01) break;
            }
            min  = i + 1;
            for(; i < WhitePlusValue; i++)
            {
                if(histogram[i]>=k99) break;
            }
            max = i;
            delete[] histogram;
        }
        else return NULL;

        ci.GoToBegin();
        MITypeLocal* normalized = new MITypeLocal(size, spacing, true);
        MazdaImageIterator<MITypeLocal> co = MazdaImageIterator<MITypeLocal>(normalized);
        int maxmin = max - min + 1;

        if(maxmin == 0)
        {
            while(!ci.IsBehind())
            {

                co.SetPixel(HalfValue);
                ++ci;
                ++co;
            }
        }
        else
        {
            while(!ci.IsBehind())
            {
                long int p = ci.GetPixel();
                co.SetPixel((p>max ?maxmin :(p<min ?0 :p-min)) * WhitePlusValue / maxmin);
                ++ci;
                ++co;
            }
        }
        return normalized;
    }

    char normalization;
};



#endif // STUBITEMNORMALIZATION_H
