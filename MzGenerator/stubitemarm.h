/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMARM_H
#define STUBITEMARM_H

#include "genentry.h"
#include <math.h>

class StubItemArm : public StubItem
{
public:
    ~StubItemArm();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
private:
    static double wyzn4(double W[4][4]);
    static double pod3wyzn4(double W[4][4], int w, int k);
    static void ma_wekwek4(double W[4][4], double W1[4], double W2[4]);
    static double li_wekwek4(double W1[4], double W2[4]);
    static void ma_ma4(double W[4][4], double W1[4][4], double W2[4][4]);
    static void wek_mawek4(double W[4], double W1[4][4], double W2[4]);
    static void sum_ma4(double W[4][4], double W1[4][4]);
    static void sum_wek4(double W[4], double W1[4]);
    static int invert4(double W[4][4], double W1[4][4]);

    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas)
    {
        if(bits < 1) return;
        if(bits > sizeof(MazdaImagePixelType)*8) return;
        unsigned int shift = sizeof(MazdaImagePixelType)*8 - bits;

        double* table = datas->table;

        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
        }
        if(quit) return;
        if(end[0] - begin[0] < 2) return;
        if(end[1] - begin[1] < 1) return;

        MazdaImageRegionIterator<MITypeLocal> ci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);

        long int area;
        double vrnc, mean, ds;

        double M1[4][4];
        double M2[4][4];
        double V1[4];
        double V2[4];
        double V3[4];

        area = 0;
        mean = 0.0;
        vrnc = 0.0;

        while(!ri.IsBehind())
        {
            if(ri.GetPixel())
            {
                double p = ci.GetPixel()>>shift;
                mean += p;
                vrnc += p*p;
                area++;
            }
            ++ri;
            ++ci;
        }
        if(area <= 0)
        {
            if(Area != (unsigned int)-1) table[Area] = 0;
            return;
        }

        mean /= area;
        vrnc /= area;
        vrnc = vrnc - (mean*mean);

        if (vrnc <= 0) return;
        ds=sqrt(vrnc);
        for(int i=0; i<4; i++)
        {
            for(int j=0; j<4; j++) M1[j][i]=0;
            V1[i]=0;
        }
        area = 0;

        // a b c
        // d e
        begin[1]++;
        begin[0]++; end[0]--;
        MazdaImageRegionIterator<MITypeLocal> cei = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rei = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[0]--; end[0]--;
        MazdaImageRegionIterator<MITypeLocal> cdi = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rdi = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[1]--; end[1]--;
        MazdaImageRegionIterator<MITypeLocal> cai = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rai = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[0]++; end[0]++;
        MazdaImageRegionIterator<MITypeLocal> cbi = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rbi = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[0]++; end[0]++;
        MazdaImageRegionIterator<MITypeLocal> cci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rci = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);

        while(!rei.IsBehind())
        {
            if(rai.GetPixel() && rbi.GetPixel() && rci.GetPixel() && rdi.GetPixel() && rei.GetPixel())
            {
                double fs;
                fs = ((double) (cei.GetPixel()>>shift) - mean)/ds;
                V2[0] = ((double)(cdi.GetPixel()>>shift) - mean)/ds;
                V2[1] = ((double)(cai.GetPixel()>>shift) - mean)/ds;
                V2[2] = ((double)(cbi.GetPixel()>>shift) - mean)/ds;
                V2[3] = ((double)(cci.GetPixel()>>shift) - mean)/ds;

                V3[0]=V2[0]*fs;
                V3[1]=V2[1]*fs;
                V3[2]=V2[2]*fs;
                V3[3]=V2[3]*fs;

                sum_wek4(V1, V3);
                ma_wekwek4(M2, V2, V2);
                sum_ma4(M1, M2);
                area++;
            }
            ++rai; ++rbi; ++rci; ++rdi; ++rei;
            ++cai; ++cbi; ++cci; ++cdi; ++cei;
        }
        if(invert4(M2, M1))  return;
        wek_mawek4(V2, M2, V1);
        if(Teta1 != (unsigned int)-1) table[Teta1] = V2[0];
        if(Teta2 != (unsigned int)-1) table[Teta2] = V2[1];
        if(Teta3 != (unsigned int)-1) table[Teta3] = V2[2];
        if(Teta4 != (unsigned int)-1) table[Teta4] = V2[3];
        if(Area != (unsigned int)-1) table[Area] = area;

        if(Sigma != (unsigned int)-1)
        {
            double sigma = 0.0;
            rai.GoToBegin(); rbi.GoToBegin(); rci.GoToBegin(); rdi.GoToBegin(); rei.GoToBegin();
            cai.GoToBegin(); cbi.GoToBegin(); cci.GoToBegin(); cdi.GoToBegin(); cei.GoToBegin();
            while(!rei.IsBehind())
            {
                if(rai.GetPixel() && rbi.GetPixel() && rci.GetPixel() && rdi.GetPixel() && rei.GetPixel())
                {
                    double fs;
                    fs = ((double) (cei.GetPixel()>>shift) - mean)/ds;
                    V1[0] = ((double)(cdi.GetPixel()>>shift) - mean)/ds;
                    V1[1] = ((double)(cai.GetPixel()>>shift) - mean)/ds;
                    V1[2] = ((double)(cbi.GetPixel()>>shift) - mean)/ds;
                    V1[3] = ((double)(cci.GetPixel()>>shift) - mean)/ds;

                    double si = fs-li_wekwek4(V1, V2);
                    sigma += (si*si);
                }
                ++rai; ++rbi; ++rci; ++rdi; ++rei;
                ++cai; ++cbi; ++cci; ++cdi; ++cei;
            }
            table[Sigma] = sqrt(sigma/(double)area);
        }
    }
    unsigned int bits;
    unsigned int Area;
    unsigned int Teta1;
    unsigned int Teta2;
    unsigned int Teta3;
    unsigned int Teta4;
    unsigned int Sigma;
};


#endif // STUBITEMARM_H
