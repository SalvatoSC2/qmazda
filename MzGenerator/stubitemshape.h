/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMSHAPE_H
#define STUBITEMSHAPE_H

#include "genentry.h"
#include <math.h>

#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkConnectedComponentImageFilter.h>
#include <itkLabelImageToShapeLabelMapFilter.h>


class StubItemShape : public StubItem
{
public:
    ~StubItemShape();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
private:
    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas)
    {
        double* table = datas->table;

        typedef unsigned char PixelType;
        typedef itk::Image<PixelType, VDimensions> ImageType;
        typedef itk::ShapeLabelObject< PixelType, VDimensions > ShapeLabelObjectType;
        typedef itk::LabelMap< ShapeLabelObjectType >  LabelMapType;

//Convert MzRoi to ITK format
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        int begin[VDimensions];
        int end[VDimensions];
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        roi->GetBegin(begin);
        roi->GetEnd(end);
        for(int d = 0; d < VDimensions; d++)
        {
            if(end[d] <= begin[d]) return;
        }
        typename ImageType::Pointer image;
        image = ImageType::New();
        typename ImageType::IndexType start;
        typename ImageType::SizeType size;
        for(int d = 0; d < VDimensions; d++)
        {
            size[d] = end[d]-begin[d]+1;
            start[d] = 0;
        }
        typename ImageType::RegionType region;
        region.SetSize(size);
        region.SetIndex(start);
        image->SetRegions(region);
        image->Allocate();
        itk::ImageRegionIterator<ImageType> imageIterator(image, image->GetRequestedRegion());
        MazdaRoiRegionIterator<MzRoiLocal> roiIterator = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        while(!imageIterator.IsAtEnd())
        {
            if(roiIterator.GetPixel()) imageIterator.Set(1);
            else imageIterator.Set(0);
            ++imageIterator;
            ++roiIterator;
        }

//Connectivity computation
        if(compute_connected)
        {
            typedef itk::ConnectedComponentImageFilter <ImageType, ImageType > ConnectedComponentImageFilterType;
            typename ConnectedComponentImageFilterType::Pointer connected = ConnectedComponentImageFilterType::New ();
            connected->SetInput(image);
            if(FullyConnectedFalse != (unsigned int)-1)
            {
                connected->SetFullyConnected(false);
                connected->Update();
                table[FullyConnectedFalse] = connected->GetObjectCount();
            }
            if(FullyConnectedTrue != (unsigned int)-1)
            {
                connected->SetFullyConnected(true);
                connected->Update();
                table[FullyConnectedTrue] = connected->GetObjectCount();
            }
        }

//Shape properties computation
        if(compute_shape)
        {
            typedef itk::LabelImageToShapeLabelMapFilter< ImageType, LabelMapType> I2LType;
            typename I2LType::Pointer i2l = I2LType::New();
            i2l->SetInput( image );
            i2l->SetComputeFeretDiameter(compute_feret);
            i2l->SetComputePerimeter(compute_perimeter);
            i2l->Update();

            LabelMapType *labelMap = i2l->GetOutput();
            if(labelMap->GetNumberOfLabelObjects() != 1) return;
            ShapeLabelObjectType *labelObject = labelMap->GetNthLabelObject(0);
            if(Area != (unsigned int)-1)
            {
                table[Area] = labelObject->GetNumberOfPixels();
            }
            if(SizeX != (unsigned int)-1)
            {
                table[SizeX] = labelObject->GetBoundingBox().GetSize()[0];
            }
            if(SizeY != (unsigned int)-1)
            {
                table[SizeY] = labelObject->GetBoundingBox().GetSize()[1];
            }
            if(FeretDiameter != (unsigned int)-1)
            {
               table[FeretDiameter] = labelObject->GetFeretDiameter();
            }
            if(CentroidX != (unsigned int)-1)
            {
               table[CentroidX] = labelObject->GetCentroid()[0]+begin[0];
            }
            if(CentroidY != (unsigned int)-1)
            {
               table[CentroidY] = labelObject->GetCentroid()[1]+begin[1];
            }
            if(PrincipalMomentX != (unsigned int)-1)
            {
                table[PrincipalMomentX] = labelObject->GetPrincipalMoments()[0];
            }
            if(PrincipalMomentY != (unsigned int)-1)
            {
                table[PrincipalMomentY] = labelObject->GetPrincipalMoments()[1];
            }
            if(Tilt != (unsigned int)-1)
            {
                if(VDimensions >= 3)
                {
                    double d = fabs(labelObject->GetPrincipalAxes()[2][2]);
                    if(d <= 1.0 && d >= -1.0)
                    {
                        table[Tilt] = acos(d);
                    }
                }
                else
                {
                    double x = labelObject->GetPrincipalAxes()[0][0];
                    double y = labelObject->GetPrincipalAxes()[0][1];
                    if(x < 0.0)
                    {
                        x = -x;
                        y = -y;
                    }
                    table[Tilt] = atan2(y, x);
                }
            }
            if(Elongation != (unsigned int)-1)
            {
                table[Elongation] = labelObject->GetElongation();
            }
            if(Perimeter != (unsigned int)-1)
            {
                table[Perimeter] = labelObject->GetPerimeter();
            }
            if(Roundness != (unsigned int)-1)
            {
                table[Roundness] = labelObject->GetRoundness();
            }
            if(EquivalentSphericalRadius != (unsigned int)-1)
            {
                table[EquivalentSphericalRadius] = labelObject->GetEquivalentSphericalRadius();
            }
            if(EquivalentSphericalPerimeter != (unsigned int)-1)
            {
                table[EquivalentSphericalPerimeter] = labelObject->GetEquivalentSphericalPerimeter();
            }
            if(EquivalentEllipsoidDiameterX != (unsigned int)-1)
            {
                table[EquivalentEllipsoidDiameterX] = labelObject->GetEquivalentEllipsoidDiameter()[0];
            }
            if(EquivalentEllipsoidDiameterY != (unsigned int)-1)
            {
                table[EquivalentEllipsoidDiameterY] = labelObject->GetEquivalentEllipsoidDiameter()[1];
            }
            if(VDimensions >= 3)
            {
                if(SizeZ != (unsigned int)-1)
                {
                    table[SizeZ] = labelObject->GetBoundingBox().GetSize()[2];
                }
                if(CentroidZ != (unsigned int)-1)
                {
                    table[CentroidZ] = labelObject->GetCentroid()[2]+begin[2];
                }
                if(PrincipalMomentZ != (unsigned int)-1)
                {
                    table[PrincipalMomentZ] = labelObject->GetPrincipalMoments()[2];
                }
                if(EquivalentEllipsoidDiameterZ != (unsigned int)-1)
                {
                    table[EquivalentEllipsoidDiameterZ] = labelObject->GetEquivalentEllipsoidDiameter()[2];
                }
            }
        }
    }

    unsigned int Area;
    unsigned int FullyConnectedFalse;
    unsigned int FullyConnectedTrue;
    unsigned int SizeX;
    unsigned int SizeY;
    unsigned int SizeZ;
    unsigned int FeretDiameter;
    unsigned int CentroidX;
    unsigned int CentroidY;
    unsigned int CentroidZ;
    unsigned int PrincipalMomentX;
    unsigned int PrincipalMomentY;
    unsigned int PrincipalMomentZ;
    unsigned int Tilt;
    unsigned int Elongation;
    unsigned int Perimeter;
    unsigned int Roundness;
    unsigned int EquivalentSphericalRadius;
    unsigned int EquivalentSphericalPerimeter;
    unsigned int EquivalentEllipsoidDiameterX;
    unsigned int EquivalentEllipsoidDiameterY;
    unsigned int EquivalentEllipsoidDiameterZ;

    bool compute_shape;
    bool compute_perimeter;
    bool compute_feret;
    bool compute_connected;
    bool compute_skeleton;
};


#endif // STUBITEMSHAPE_H
/*
References:
https://itk.org/Wiki/ITK/Examples/ImageProcessing/ShapeAttributes
https://itk.org/Wiki/ITK/Examples/ImageProcessing/ConnectedComponentImageFilter
*/
