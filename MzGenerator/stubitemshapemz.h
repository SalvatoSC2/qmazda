/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMSHAPEMZ_H
#define STUBITEMSHAPEMZ_H

#include "genentry.h"
#include <math.h>

#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkConnectedComponentImageFilter.h>
#include <itkLabelImageToShapeLabelMapFilter.h>


class StubItemShapeMz : public StubItem
{
public:
    ~StubItemShapeMz();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
private:
    void inRoi2D(StubItemDataEntryRoi* datas);
    void WykonajOliczenia(MzRoi2D* roi, double *table);

    bool compute_cokolwiek;
    bool compute_profil;
    bool compute_szkielet;
    bool compute_profil_obwod;
    bool compute_martin;
    bool compute_martinr;
    bool compute_kontur_spojny;
    bool compute_feret;
    bool compute_obwod_wypukly;
    bool compute_prostokat_opisany;
    bool compute_okrag_wpisany;
    bool compute_okrag_opisany;
    bool compute_elipsa;
    bool compute_najw_sred;
//----------------------------------------------
//Wielkosci podstawowe
//----------------------------------------------
    unsigned int X;
    unsigned int Y;
    unsigned int F;
    unsigned int Spol;
    unsigned int Smax;
    unsigned int Aox;
    unsigned int Ug;
    unsigned int Uw;
    unsigned int Fmax;
    unsigned int Fmin;
    unsigned int Fh;
    unsigned int Fv;
    unsigned int Mmin;
    unsigned int Mmax;
    unsigned int Maver;
    unsigned int Ft;
    unsigned int Ul;
    unsigned int S1;
    unsigned int S2;
    unsigned int Lsz;
    unsigned int S;
    unsigned int L;
    unsigned int SxL;
    unsigned int D1;
    unsigned int D2;
    unsigned int Fd2;
    unsigned int LmaxE;
    unsigned int LminE;
    unsigned int FE;
//----------------------------------------------
//Wspolczynniki ksztaltu
//----------------------------------------------
    unsigned int W1;
    unsigned int W2;
    unsigned int W6;
    unsigned int W3;
    unsigned int W4;
    unsigned int W5;
    unsigned int W5b;
    unsigned int W7;
    unsigned int Rs;
    unsigned int Rf;
    unsigned int Rff;
    unsigned int Rc;
    unsigned int Rc1;
    unsigned int Rc2;
    unsigned int Rm;
    unsigned int Rb;
    unsigned int Rd;
    unsigned int Rh;
    unsigned int W8;
    unsigned int W9;
    unsigned int W10;
    unsigned int SigR;
    unsigned int W11;
    unsigned int W12;
    unsigned int W13;
    unsigned int W14;
    unsigned int W15;
//----------------------------------------------
//Momenty
//----------------------------------------------
    unsigned int M2x;
    unsigned int M2y;
    unsigned int M2xy;
//----------------------------------------------
//Dodatkowe wielkosci
//----------------------------------------------
    unsigned int Er;
    unsigned int Er2;
    unsigned int El;
    unsigned int El2;
    unsigned int Nc;
    unsigned int Nv;
    unsigned int Nl;
    unsigned int Nsz;
    unsigned int Ni;
    unsigned int Nx;
    unsigned int No;
    unsigned int Xo;
    unsigned int Yo;
    unsigned int XYo;
};


#endif // STUBITEMSHAPEMZ_H
