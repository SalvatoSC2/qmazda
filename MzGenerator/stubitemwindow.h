/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMWINDOW_H
#define STUBITEMWINDOW_H

#define N_sigma 3.0
#include "genentry.h"

class StubItemWindow : public StubItem
{
public:
    ~StubItemWindow();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Execute(StubItemData* data);
    bool IsNotifier(){return false;}
    void Connecting(unsigned int index);

private:
    template <unsigned int VDimensions>
    /**
     * @brief createRoi
     * @return
     */
    MazdaDummy* createRoi(void)
    {
       int begin[VDimensions];
       int end[VDimensions];
       for(unsigned int d = 0; d < VDimensions; d++)
       {
           begin[d] = 0;
           end[d] = 2*radius;
       }
       typedef MazdaRoi<MazdaRoiBlockType, VDimensions> MRType;
       MRType* roi = new MRType(begin, end);

       switch(shape)
       {
       case 'c':
       case 'C':
       {
           double radrad = ((double)radius+0.5);
           radrad *= radrad;
           MazdaRoiRegionIterator<MRType> iterator(roi, begin, end);
           while(! iterator.IsBehind())
           {
               double dd = 0;
                for(unsigned int d = 0; d < VDimensions; d++)
                {
                    int c = iterator.GetIndex(d);
                    c -= radius;
                    dd += (c*c);

                }
               if(dd <= radrad) iterator.SetPixel();
               else iterator.ClrPixel();
               ++iterator;
           }
           break;
       }
       default:
       {
           MazdaRoiIterator<MRType> iterator(roi);
           while(! iterator.IsBehind())
           {
               iterator.SetPixel();
               ++iterator;
           }
           break;
       }
       }
       return roi;
    }

    template <unsigned int VDimensions>
    /**
     * @brief createMaps
     * @param datas
     * @param roi
     */
    void createMaps(StubItemDataEntryLocal* datas, MazdaDummy* roi)
    {
        unsigned int d;
        unsigned int size[VDimensions];
        unsigned int begin[VDimensions];
        unsigned int v[VDimensions];
        int vrb[VDimensions];

        typedef MazdaImage<MazdaImagePixelType, VDimensions> MIType;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoi;
        typedef MazdaImage <MazdaMapPixelType, VDimensions> MzMap;

        unsigned int nomap = (*(datas->maps)).size();
        if(nomap <= 0) return;
        double* table = new double[nomap];

        MIType* image = static_cast<MIType*>(datas->image);
        image->GetSize(size);
        unsigned int step = datas->step;
        for(d = 0; d < VDimensions; d++)
        {
            v[d] = begin[d] = ((size[d]-1)%step)/2;
            vrb[d] = (int)v[d] - (int)radius;
        }
        StubItemDataEntryRoi newdata;
//        (static_cast<StubItemDataEntry> (newdata))  = (static_cast<StubItemDataEntry> (*datas));
//        (StubItemDataEntry)newdata  = (StubItemDataEntry)(*datas);

        newdata.image = datas->image;
        newdata.roi = NULL;
//        newdata.imageid = datas->imageid;
//        newdata.roiid = datas->roiid;
//        newdata.is3D = datas->is3D;
        newdata.isRGB = datas->isRGB;
        newdata.roi = roi;
        newdata.table = table;
        do
        {
            static_cast<MzRoi*>(roi)->SetBegin(vrb);
            for(unsigned int i = 0; i < indices.size(); i++)
            {
                table[indices[i]] = mznan;
            }
            for(unsigned int i = 0; i < Connected(); i++)
                Connected(i)->Execute(&newdata);
            for(unsigned int i = 0; i < indices.size(); i++)
            {
                unsigned int mapindex = indices[i];
                MzMap* map = static_cast<MzMap*>((*(datas->maps))[mapindex]);
                map->SetPixel(v, table[mapindex]);
            }
            for(d = 0; d < VDimensions; d++)
            {
                v[d] += step;
                vrb[d] += step;
                if((int)v[d] < size[d]) break;
                else
                {
                    v[d] = begin[d];
                    vrb[d] = (int)v[d] - (int)radius;
                }
            }
        }while(d < VDimensions);

        delete[] table;

        if(step > 1)
        {
            for(unsigned int i = 0; i < indices.size(); i++)
            {
                unsigned int mapindex = indices[i];
                MzMap* map = static_cast<MzMap*>((*(datas->maps))[mapindex]);
                interpolateMap<VDimensions>(map, step);
            }
        }

    }


    template <unsigned int VDimensions>
    /**
     * @brief createMaps
     * @param datas
     * @param roi
     */
    void interpolateMap(MazdaImage <MazdaMapPixelType, VDimensions>* map, unsigned int mapstep)
    {
        int d, j;
        unsigned int x[VDimensions];
        int xmin[VDimensions];
        int xmax[VDimensions];
        unsigned int size[VDimensions];
        map->GetSize(size);

        for(unsigned int direction = 0; direction < VDimensions; direction++)
        {
            if(size[direction] <= mapstep) continue;
            for(d = 0; d < VDimensions; d++)
            {
                xmin[d] = x[d] = ((size[d]-1)%mapstep)/2;
                xmax[d] = size[d];
            }
            xmax[direction] -= mapstep;
            do
            {
                double a0 = map->GetPixel(x);
                x[direction] += mapstep;
                double a1 = map->GetPixel(x);
                x[direction] -= mapstep;
                double bs = 1.0/(double)mapstep;
                double b0 = 1.0-bs;
                double b1 = bs;

                if(x[direction] == xmin[direction])
                {
                    x[direction] = 0;
                    for(x[direction] = 0; x[direction] < xmin[direction]; x[direction]++)
                    {
                        map->SetPixel(x, a0);
                    }
                }

                for(unsigned int i = 1; i < mapstep; i++)
                {
                    x[direction]++;
                    map->SetPixel(x, b0*a0 + b1*a1);
                    b0 -= bs;
                    b1 += bs;
                }
                x[direction] ++;
                x[direction] -= mapstep;

                for(j = 0; j < VDimensions; j++)
                {
                    if(j < direction) x[j] ++;
                    else x[j] += mapstep;

                    if((int)x[j] < (int)xmax[j])
                    {
                        break;
                    }
                    else
                    {
                        if(j == direction)
                        {
                            double a0 = map->GetPixel(x);
                            for(; x[direction] < size[direction]; x[direction]++)
                            {
                                map->SetPixel(x, a0);
                            }
                        }
                        x[j] = xmin[j];
                    }
                }
            }while(j < VDimensions);
        }
    }


    template <unsigned int VDimensions>
    /**
     * @brief createVectors
     * @param datas
     * @param roi
     */
    void createVectors(StubItemDataEntryLocal* datas, MazdaDummy* roi)
    {
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoi;
        MzRoi* roiin = static_cast<MzRoi*>(datas->roi);
        StubItemDataEntryRoi newdata;
        static_cast<StubItemDataEntry> (newdata)  = static_cast<StubItemDataEntry> (*datas);

        newdata.image = datas->image;
        newdata.roi = NULL;
//        newdata.imageid = datas->imageid;
//        newdata.roiid = datas->roiid;
//        newdata.is3D = datas->is3D;
        newdata.isRGB = datas->isRGB;

        unsigned int line = 0;
        unsigned int d;
        int begin[VDimensions];
        int end[VDimensions];
        int vrb[VDimensions];
        roiin->GetBegin(begin);
        roiin->GetEnd(end);

        MazdaRoiRegionIterator<MzRoi> iterator(roiin, begin, end);
        while(! iterator.IsBehind())
        {
             if(iterator.GetPixel())
             {
                 for(d = 0; d < VDimensions; d++) vrb[d] = iterator.GetIndex(d) + begin[d] - radius;
                 static_cast<MzRoi*>(roi)->SetBegin(vrb);
                 newdata.roi = roi;
                 newdata.table = (*(datas->tables))[line];
                 for(unsigned int i = 0; i < Connected(); i++) Connected(i)->Execute(&newdata);
                 line++;
             }
             ++iterator;
        }
    }
    std::vector <unsigned int> indices;
    unsigned int radius;
    char shape;
};


#endif // STUBITEMWINDOW_H
