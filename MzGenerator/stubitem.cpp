/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitem.h"

//double* StubItem::table;




bool StubItemBuilder::addFeature(const std::string name, unsigned int index, NotifyType* notify)
{
//    printf("addFeature(%s, %i) %s\n", name.c_str(), index);
//    fflush(stdout);
    std::vector <std::string> list;
    ParameterTreeItem<NameStub>* matched = NULL;
    char* leftc = (char*) name.c_str();
    char* leftp;
    if(featuresTemplate->doesItMatch(leftc, &matched) != 0)
        return false;
    StubItem* item = topLevel;
    int k;
    int match;
    matched = NULL;
    while(true)
    {
        leftp = leftc;
        match = featuresTemplate->doesItMatch(leftp, &matched, &leftc, 1);
        if(match < 0) return false;
        int length = leftc-leftp;
        if(length <= 0) return false;
        int kmax = item->Connected();
        for(k = 0; k < kmax; k++)
        {
            if(item->Connected(k)->Name().compare(0, item->Connected(k)->Name().length(), leftp, length) == 0)
            {
                item = item->Connected(k);
//                printf("item->Connecting(%i) %s\n", index, item->Name().c_str());
//                fflush(stdout);
                item->Connecting(index);
                if(match == 0) return false;
                break;
            }
        }
        if(k >= kmax) break;
        std::string nn = std::string(leftp).substr(0, leftc-leftp);
        list.push_back(nn);
    }

    while(true)
    {
        std::string nn = std::string(leftp).substr(0, leftc-leftp);
        list.push_back(nn);
        item = (*(MethodNewType*)(matched->data.ptr))(dimensions, item, &list, index, notify);
//        printf("item2->Connecting(%i) %s\n", index, item->Name().c_str());
//        fflush(stdout);
        item->Connecting(index);
        if(item->IsNotifier()) notificationsNumber++;
        //item->SetProperties(&list, index);
        leftp = leftc;
        if(match <= 0) break;
        match = featuresTemplate->doesItMatch(leftp, &matched, &leftc, 1);
    }
    return true;
}


