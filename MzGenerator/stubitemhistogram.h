/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMHISTOGRAM_H
#define STUBITEMHISTOGRAM_H

#include "genentry.h"
#include <math.h>

class StubItemHistogram : public StubItem
{
public:
    ~StubItemHistogram();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
private:
    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas)
    {
        if(bits < 1) return;
        if(bits > sizeof(MazdaImagePixelType)*8) return;
        unsigned int shift = sizeof(MazdaImagePixelType)*8 - bits;


#ifdef USE_DEBUG_LOG
        if(debugfile.is_open())
        {
            debugfile << "Bits: " << bits << " Shift: " << shift << " Pixel: " << sizeof(MazdaImagePixelType) << std::endl;
        }
#endif

        double* table = datas->table;
        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
        }
        if(quit) return;

        MazdaImageRegionIterator<MITypeLocal> ci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        unsigned long int area = 0;

        unsigned int i;
        if(compute_var)
        {
            double i1 = 0.0;
            double i2 = 0.0;
            double i3 = 0.0;
            double i4 = 0.0;
            while(!ri.IsBehind())
            {
                if(ri.GetPixel())
                {
                    double p = ci.GetPixel()>>shift;
                    double pp = p*p;
                    i1 += p;
                    i2 += pp;
                    i3 += pp*p;
                    i4 += pp*pp;
                    area++;
                }
                ++ri;
                ++ci;
            }
            if(Area != (unsigned int)-1) table[Area] = (double)area;

            if(area > 0)
            {
                i1 /= area;
                i2 /= area;
                i3 /= area;
                i4 /= area;
                double var = i2 - (i1*i1);
                if(Mean != (unsigned int)-1) table[Mean] = i1;
                if(Variance != (unsigned int)-1) table[Variance] = var;
                if(var > 0)
                {
                    if(StdDev != (unsigned int)-1) table[StdDev] = sqrt(var);
                    if(Skewness != (unsigned int)-1)
                    {
                        double mi3 = - 2.0*i1*i1*i1 + 3.0*i1*i2 - i3;
                        table[Skewness] = mi3/(sqrt(var)*var);
                    }
                    if(Kurtosis != (unsigned int)-1)
                    {
                        double mi4 = -3.0*i1*i1*i1*i1 + 6.0*i1*i1*i2 - 4.0*i1*i3 + i4;
                        table[Kurtosis] = (mi4/(var*var)) - 3;
                    }
                }
                else
                {
                    if(StdDev != (unsigned int)-1) table[StdDev] = 0.0;
                    if(Skewness != (unsigned int)-1) table[Skewness] = 0.0;
                    if(Kurtosis != (unsigned int)-1) table[Kurtosis] = 0.0;
                }
            }
        }
        if(compute_01 || compute_perc || compute_10)
        {
            unsigned int hisize = (unsigned int)1 << bits;
            unsigned long int* histogram = new unsigned long int[hisize];
            memset(histogram, 0, sizeof(unsigned long int)*hisize);
            ri.GoToBegin();
            ci.GoToBegin();
            area = 0;

//#ifdef USE_DEBUG_LOG
//            int logi = 0;
//#endif
            while(!ri.IsBehind())
            {
//#ifdef USE_DEBUG_LOG
//                int end_begin_1 = end[0] - begin[0] + 1;
//                if(debugfile.is_open())
//                {
//                    if(ri.GetPixel())
//                    {
//                       debugfile << "\t" << (int)ci.GetPixel();
//                    }
//                    else
//                    {
//                        debugfile << "\t*";
//                    }
//                    logi++;
//                    if(logi%end_begin_1 == 0)
//                    {
//                        debugfile << std::endl;
//                    }
//                }
//#endif

                if(ri.GetPixel())
                {
                    histogram[ci.GetPixel()>>shift]++;
                    area++;
                }
                ++ri;
                ++ci;
            }

#ifdef USE_DEBUG_LOG
                if(debugfile.is_open())
                {
                    debugfile << std::endl;
                    debugfile << "Histogram:";
                    for(i = 0; i < hisize; i++)
                    {
                        if(i%32 == 0)
                            debugfile << std::endl;
                        debugfile << " " << histogram[i];
                    }
                    debugfile << std::endl;
                }
#endif

            if(area > 0)
            {
                if(compute_01)
                {
                    unsigned long int maxim01 = 0;
                    unsigned int domin01 = 0;
                    for(i = 0; i < hisize; i++)
                    {
                        if(histogram[i] > maxim01)
                        {
                            maxim01 = histogram[i];
                            domin01 = i;
                        }
                    }
                    if(Maxm01 != (unsigned int)-1) table[Maxm01] = (double)maxim01 / (double)area;
                    if(Domn01 != (unsigned int)-1) table[Domn01] = (double)(domin01);
                }
                if(compute_perc || compute_10)
                {
                    for(i = 1; i < hisize; i++) histogram[i] += histogram[i-1];
                    unsigned long int maxim10 = 0;
                    unsigned int domin10 = 0;
                    if(compute_10)
                    {
                        for(i = 0; i < (hisize-0x10); i++)
                        {
                            if(histogram[i+0x10]-histogram[i] > maxim10)
                            {
                                maxim10 = histogram[i+0x10]-histogram[i];
                                domin10 = i;
                            }
                        }
                        if(Maxm10 != (unsigned int)-1) table[Maxm10] = (double)maxim10 / (double)area;
                        if(Domn10 != (unsigned int)-1) table[Domn10] = (double)(domin10);
                    }
                    if(compute_perc)
                    {
                        double k01 =(double)area/100;
                        double k10 =(double)area/10;
                        double k50 =(double)area/2;
                        double k90 =(double)area-k10;
                        double k99 =(double)area-k01;
                        for(i = 0; i < hisize; i++)
                        {
                            if(histogram[i]>=k01) break;
                        }
                        if(Perc01 != (unsigned int)-1) table[Perc01] = i;
                        for(; i < hisize; i++)
                        {
                            if(histogram[i]>=k10) break;
                        }
                        if(Perc10 != (unsigned int)-1) table[Perc10] = i;
                        for(; i < hisize; i++)
                        {
                            if(histogram[i]>=k50) break;
                        }
                        if(Perc50 != (unsigned int)-1) table[Perc50] = i;
                        for(; i < hisize; i++)
                        {
                            if(histogram[i]>=k90) break;
                        }
                        if(Perc90 != (unsigned int)-1) table[Perc90] = i;
                        for(; i < hisize; i++)
                        {
                            if(histogram[i]>=k99) break;
                        }
                        if(Perc99 != (unsigned int)-1) table[Perc99] = i;
                    }
                }
            }
            delete[] histogram;
        }

    }
    bool compute_perc;
    bool compute_10;
    bool compute_01;
    bool compute_var;
    unsigned int bits;
    unsigned int Area;
    unsigned int Mean;
    unsigned int Variance;
    unsigned int StdDev;
    unsigned int Skewness;
    unsigned int Kurtosis;
    unsigned int Perc01;
    unsigned int Perc10;
    unsigned int Perc50;
    unsigned int Perc90;
    unsigned int Perc99;
    unsigned int Maxm01;
    unsigned int Domn01;
    unsigned int Maxm10;
    unsigned int Domn10;
};


#endif // STUBITEMHISTOGRAM_H
