/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2018 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemhaar.h"

StubItemHaar::~StubItemHaar()
{
}
StubItem* StubItemHaar::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemHaar* item = new StubItemHaar;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();
    item->bits = atoi((*nameStubs)[nameStubs->size()-3].c_str());
    item->scale = -1;

    for(unsigned int s = 0; s < 8; s++)
        for(unsigned int n = 0; n < 3; n++)
            item->Wavelet[s][n] = (unsigned int) -1;
    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemHaar::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    if(itemf->Name() == "S1HH") { Wavelet[0][0] = itemf->Index(); if(scale < 0) scale = 0; }
    else if(itemf->Name() == "S1HL") { Wavelet[0][1] = itemf->Index(); if(scale < 0) scale = 0; }
    else if(itemf->Name() == "S1LH") { Wavelet[0][2] = itemf->Index(); if(scale < 0) scale = 0; }
    else if(itemf->Name() == "S2HH") { Wavelet[1][0] = itemf->Index(); if(scale < 1) scale = 1; }
    else if(itemf->Name() == "S2HL") { Wavelet[1][1] = itemf->Index(); if(scale < 1) scale = 1; }
    else if(itemf->Name() == "S2LH") { Wavelet[1][2] = itemf->Index(); if(scale < 1) scale = 1; }
    else if(itemf->Name() == "S3HH") { Wavelet[2][0] = itemf->Index(); if(scale < 2) scale = 2; }
    else if(itemf->Name() == "S3HL") { Wavelet[2][1] = itemf->Index(); if(scale < 2) scale = 2; }
    else if(itemf->Name() == "S3LH") { Wavelet[2][2] = itemf->Index(); if(scale < 2) scale = 2; }
    else if(itemf->Name() == "S4HH") { Wavelet[3][0] = itemf->Index(); if(scale < 3) scale = 3; }
    else if(itemf->Name() == "S4HL") { Wavelet[3][1] = itemf->Index(); if(scale < 3) scale = 3; }
    else if(itemf->Name() == "S4LH") { Wavelet[3][2] = itemf->Index(); if(scale < 3) scale = 3; }
    else if(itemf->Name() == "S5HH") { Wavelet[4][0] = itemf->Index(); if(scale < 4) scale = 4; }
    else if(itemf->Name() == "S5HL") { Wavelet[4][1] = itemf->Index(); if(scale < 4) scale = 4; }
    else if(itemf->Name() == "S5LH") { Wavelet[4][2] = itemf->Index(); if(scale < 4) scale = 4; }
    else if(itemf->Name() == "S6HH") { Wavelet[5][0] = itemf->Index(); if(scale < 5) scale = 5; }
    else if(itemf->Name() == "S6HL") { Wavelet[5][1] = itemf->Index(); if(scale < 5) scale = 5; }
    else if(itemf->Name() == "S6LH") { Wavelet[5][2] = itemf->Index(); if(scale < 5) scale = 5; }
    else if(itemf->Name() == "S7HH") { Wavelet[6][0] = itemf->Index(); if(scale < 6) scale = 6; }
    else if(itemf->Name() == "S7HL") { Wavelet[6][1] = itemf->Index(); if(scale < 6) scale = 6; }
    else if(itemf->Name() == "S7LH") { Wavelet[6][2] = itemf->Index(); if(scale < 6) scale = 6; }
    else if(itemf->Name() == "S8HH") { Wavelet[7][0] = itemf->Index(); if(scale < 7) scale = 7; }
    else if(itemf->Name() == "S8HL") { Wavelet[7][1] = itemf->Index(); if(scale < 7) scale = 7; }
    else if(itemf->Name() == "S8LH") { Wavelet[7][2] = itemf->Index(); if(scale < 7) scale = 7; }

    children.push_back(item);
}

void StubItemHaar::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);

    if(datas->roi != NULL)
    {
        try
        {
            switch(dimensions)
            {
            case 3: inRoi<3> (datas); break;
            case 2: inRoi<2> (datas); break;
            default: break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}


