/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemgradient.h"


StubItemGradient::~StubItemGradient()
{
}
StubItem* StubItemGradient::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemGradient* item = new StubItemGradient;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();
    item->bits = atoi((*nameStubs)[nameStubs->size()-2].c_str());
    item->Area= (unsigned int) -1;
    item->Mean= (unsigned int) -1;
    item->Variance= (unsigned int) -1;
    item->StdDev= (unsigned int) -1;
    item->Skewness= (unsigned int) -1;
    item->Kurtosis= (unsigned int) -1;
    item->NonZeros= (unsigned int) -1;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemGradient::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    if(itemf->Name() == "Mean") Mean= itemf->Index();
    else if(itemf->Name() == "Variance") Variance= itemf->Index();
    else if(itemf->Name() == "StdDev") StdDev= itemf->Index();
    else if(itemf->Name() == "Skewness") Skewness= itemf->Index();
    else if(itemf->Name() == "Kurtosis") Kurtosis= itemf->Index();
    else if(itemf->Name() == "NonZeros") NonZeros= itemf->Index();
    else if(itemf->Name() == "Area") Area= itemf->Index();

    children.push_back(item);
}

void StubItemGradient::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);
    if(datas->roi != NULL)
    {
        try
        {
            switch(dimensions)
            {
            case 3: inRoi<3> (datas); break;
            case 2: inRoi<2> (datas); break;
            default: break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}


