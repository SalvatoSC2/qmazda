/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEMPLATES_H
#define TEMPLATES_H

#include "stubitem.h"
#include "stubitemcolortransform.h"
#include "stubitemnormalization.h"
#include "stubitemhistogram.h"
#include "stubitemgradient.h"
#include "stubitemarm.h"
#include "stubitemlbp.h"
#include "stubitemhog.h"
#include "stubitemgrlm.h"
#include "stubitemglcm.h"
#include "stubitemhaar.h"
#include "stubitemgabor.h"
#include "stubitemshape.h"
#include "stubitemshapemz.h"
#include "stubitemshapecv.h"
#include "stubitemwindow.h"

//typedef StubItemThrough StubItemWindow;
typedef StubItemThrough StubItemPreWindow;

// typedef StubItemThrough StubItemShape;
typedef StubItemFinal StubItemShapeFeature;
//typedef StubItemColorTransform StubItemColorTransform;
//typedef StubItemThrough StubItemNormalization;
typedef StubItemThrough StubItemBits;
//typedef StubItemThrough StubItemGabor;
typedef StubItemFinal StubItemGaborFeature;
//typedef StubItemThrough StubItemGrlm;
typedef StubItemFinal StubItemGrlmFeature;
//typedef StubItemThrough StubItemGlcm;
typedef StubItemFinal StubItemGlcmFeature;
//typedef StubItemThrough StubItemArm;
typedef StubItemFinal StubItemArmFeature;
//typedef StubItemThrough StubItemGradient;
typedef StubItemFinal StubItemGradientFeature;
//typedef StubItemThrough StubItemHistogram;
typedef StubItemFinal StubItemHistogramFeature;

typedef StubItemFinal StubItemLbpFeature;
typedef StubItemFinal StubItemHogFeature;

extern ItemTemplate localTreeTemplate[214];
extern ItemTemplate roiTreeTemplate[246];

#endif // TEMPLATES_H
