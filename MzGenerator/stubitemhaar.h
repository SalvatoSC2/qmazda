/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2018 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef StubItemHaar_H
#define StubItemHaar_H

#include "genentry.h"
#include <math.h>


#include "../SharedImage/mazdaimageio.h"

class StubItemHaar : public StubItem
{
public:
    ~StubItemHaar();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
private:
    const MazdaMapPixelType mapnan = std::numeric_limits<MazdaMapPixelType>::quiet_NaN();

    template <unsigned int VDimensions>
    void computeScale(int s, MazdaImage <MazdaMapPixelType, VDimensions>* m, double* table)
    {
        typedef MazdaImage <MazdaMapPixelType, VDimensions> MzMap;
        unsigned int size[VDimensions];
        unsigned int sizem[VDimensions];
        double spacing[VDimensions];
        unsigned int size0[3];
        unsigned int size1[3];

        m->GetSize(sizem);
        m->GetSize(size);
        m->GetSpacing(spacing);

        size0[0] = size[0];
        size0[1] = 1;
        for(unsigned int d = 1; d < VDimensions; d++)
            size0[1] *= size[d];

        size[0] /= 2;
        size1[0] = size[0];
        size1[1] = size0[1];

        MzMap* ml = NULL;
        MzMap* mh = NULL;
        MzMap* mll = NULL;

        bool doh = (Wavelet[s][0] != (unsigned int)-1) || (Wavelet[s][1] != (unsigned int)-1);
        bool dol = (Wavelet[s][2] != (unsigned int)-1) || (scale > s);

        if(doh && dol)
        {
            ml = new MzMap(size, spacing);
            mh = new MzMap(size, spacing);

            MazdaMapPixelType* pm = m->GetDataPointer();
            MazdaMapPixelType* pml = ml->GetDataPointer();
            MazdaMapPixelType* pmh = mh->GetDataPointer();
            MazdaMapPixelType* pmhm = pmh;
            MazdaMapPixelType* pmm = pm;
            for(unsigned int y = 0; y < size0[1]; y++)
            {
                pmhm += size1[0];
                pmm += size0[0];
                for(; pmh < pmhm; pmh++, pml++)
                {
                    MazdaMapPixelType a1 = *pm;
                    pm++;
                    MazdaMapPixelType a2 = *pm;
                    pm++;
                    *pmh = ((a1 - a2)/2.0);
                    *pml = ((a1 + a2)/2.0);
                }
                pm = pmm;
            }
        }
        else if(doh)
        {
            mh = new MzMap(size, spacing);
            MazdaMapPixelType* pm = m->GetDataPointer();
            MazdaMapPixelType* pmh = mh->GetDataPointer();
            MazdaMapPixelType* pmhm = pmh;
            MazdaMapPixelType* pmm = pm;
            for(unsigned int y = 0; y < size0[1]; y++)
            {
                pmhm += size1[0];
                pmm += size0[0];
                for(; pmh < pmhm; pmh++)
                {
                    MazdaMapPixelType a1 = *pm;
                    pm++;
                    MazdaMapPixelType a2 = *pm;
                    pm++;
                    *pmh = ((a1 - a2)/2.0);
                }
                pm = pmm;
            }
        }
        else if(dol)
        {
            ml = new MzMap(size, spacing);
            MazdaMapPixelType* pm = m->GetDataPointer();
            MazdaMapPixelType* pml = ml->GetDataPointer();
            MazdaMapPixelType* pmlm = pml;
            MazdaMapPixelType* pmm = pm;
            for(unsigned int y = 0; y < size0[1]; y++)
            {
                pmlm += size1[0];
                pmm += size0[0];
                for(; pml < pmlm; pml++)
                {
                    MazdaMapPixelType a1 = *pm;
                    pm++;
                    MazdaMapPixelType a2 = *pm;
                    pm++;
                    *pml = ((a1 + a2)/2.0);
                }
                pm = pmm;
            }
        }

        size0[0] = size[0];
        size0[1] = size[1];
        size0[2] = 1;
        for(unsigned int d = 2; d < VDimensions; d++)
            size0[1] *= size[d];

        size[1] /= 2;
        size1[0] = size[0];
        size1[1] = size[1];
        size1[2] = size0[2];

        double elh = 0.0;
        double ehl = 0.0;
        double ehh = 0.0;
        unsigned int elhc = 0;
        unsigned int ehlc = 0;
        unsigned int ehhc = 0;

        if(ml != NULL)
        {
            mll = new MzMap(size, spacing);
            MazdaMapPixelType* pmlo = ml->GetDataPointer();
            MazdaMapPixelType* pmo = mll->GetDataPointer();
            MazdaMapPixelType* pml;
            MazdaMapPixelType* pm;

            for(unsigned int z = 0; z < size0[2]; z++)
            {
                for(unsigned int x = 0; x < size0[0]; x++)
                {
                    pml = pmlo + x;
                    pm = pmo + x;
                    MazdaMapPixelType* pmm = pm + (size1[1]*size1[0]);
                    for(; pm < pmm; pm += size1[0])
                    {
                        MazdaMapPixelType l1 = *pml;
                        pml += size0[0];
                        MazdaMapPixelType l2 = *pml;
                        pml += size0[0];
                        *pm = ((l1 + l2)/2.0);
                        MazdaMapPixelType lh = ((l1 - l2)/2.0);
                        if(lh == lh)
                        {
                            elh += ((double)lh*lh);
                            elhc ++;
                        }
                    }
                }
                pmlo += z*(size0[1]*size0[0]);
                pmo += z*(size1[1]*size1[0]);
            }
//            std::vector<MzMap*> mmm;
//            mmm.push_back(ml);
//            MazdaMapIO<MzMap>::Write("__ml.tif", &mmm);

            delete ml;
            ml = NULL;
        }

        if(mh != NULL)
        {
            MazdaMapPixelType* pmh;
            MazdaMapPixelType* pmho = mh->GetDataPointer();
            for(unsigned int z = 0; z < size0[2]; z++)
            {
                for(unsigned int x = 0; x < size0[0]; x++)
                {
                    pmh = pmho + x;
                    MazdaMapPixelType* pmm = pmh + ((size[1]*size[0])/2)*2;
                    for(; pmh < pmm; pmh += size[0])
                    {
                        MazdaMapPixelType h1 = *pmh;
                        pmh += size0[0];
                        MazdaMapPixelType h2 = *pmh;
                        MazdaMapPixelType hl = ((h1 + h2)/2.0);
                        MazdaMapPixelType hh = ((h1 - h2)/2.0);
                        if(hl == hl)
                        {
                            ehl += ((double)hl*hl);
                            ehlc ++;
                        }
                        if(hh == hh)
                        {
                            ehh += ((double)hh*hh);
                            ehhc ++;
                        }
                    }
                }
                pmho += z*(size[1]*size[0]);
            }

//            std::vector<MzMap*> mmm;
//            mmm.push_back(mh);
//            MazdaMapIO<MzMap>::Write("__mh.tif", &mmm);

            delete mh;
            mh = NULL;
        }

        if((Wavelet[s][0] != (unsigned int)-1) && ehhc > 0)
        {
            table[Wavelet[s][0]] = ehh/ehhc;
        }
        if((Wavelet[s][1] != (unsigned int)-1) && ehlc > 0)
        {
            table[Wavelet[s][1]] = ehl/ehlc;
        }
        if((Wavelet[s][2] != (unsigned int)-1) && elhc > 0)
        {
            table[Wavelet[s][2]] = elh/elhc;
        }

        if(mll != NULL)
        {
//            std::vector<MzMap*> mmm;
//            mmm.push_back(mll);
//            MazdaMapIO<MzMap>::Write("__mll.tif", &mmm);

            if(scale > s)
                computeScale(s+1, mll, table);
            delete mll;
        }
    }

    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas)
    {
        if(bits < 1) return;
        if(bits > sizeof(MazdaImagePixelType)*8) return;
        unsigned int shift = sizeof(MazdaImagePixelType)*8 - bits;
        double* table = datas->table;
        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
        }
        if(quit) return;

        typedef MazdaImage <MazdaMapPixelType, VDimensions> MzMap;
        unsigned int sizem[VDimensions];
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            sizem[d] = end[d]-begin[d]+1;
        }
        MzMap* mapa = new MzMap(sizem, spacing);

        MazdaImageRegionIterator<MITypeLocal> ci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        MazdaImageIterator<MzMap> mi = MazdaImageIterator<MzMap>(mapa);

        while(!ri.IsBehind())
        {
            if(ri.GetPixel())
            {
                mi.SetPixel(ci.GetPixel()>>shift);
            }
            else
            {
                mi.SetPixel(mapnan);
            }
            ++ri;
            ++ci;
            ++mi;
        }
        if(scale >= 0) computeScale(0, mapa, table);
        delete mapa;
    }
    int scale;
    unsigned int bits;
    //[scale][hh, hl, lh]
    unsigned int Wavelet[8][3];
};


#endif // StubItemHaar_H
