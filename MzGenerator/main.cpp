/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "genentry.h"
#include <algorithm>
#include <cstdio>

void notifyText(const char*)
{}
void notify(void)
{
    static unsigned int perclast = (unsigned int)-1;
    notifications++;
    unsigned int perc = 100*notifications/maxnotifications;
    if(perc != perclast)
    {
        perclast = perc;
        printf("\r%i%%", perc);
        fflush(stdout);
    }
    if(perc >= 100) printf("\n");
}

int main(int argc, char* argv[])
{
    int ret = gentryScan(argc, argv);
    if(ret != 0) return ret;
    ret = gentryRun();
    if(ret != 0) return ret;
    if(execute_command != NULL) return system(execute_command);
    return 0;
}
