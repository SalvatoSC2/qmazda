/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iostream>
#include <string.h>
#include <locale.h>
#include <stdlib.h>

#include "featureio.h"
/**
 * @brief loadFeatureList loads strings from file and separates them in vector of strings
 * @param filename
 * @param line if true reads the first line of a file, if false reads the file to the end
 * @return
 */
std::vector<std::string> loadFeatureList(const char* fileName, bool firstLine)
{
    std::string name;
    std::ifstream file;
    std::vector<std::string> result;
    file.open(fileName);
    if (!file.is_open()) return result;
    if (!file.good()) return result;
//    const char* c;
    char *prevloc = setlocale(LC_ALL, NULL);
    if(prevloc) prevloc = strdup(prevloc);
    setlocale(LC_ALL, "C");
    do
    {
        std::string sline;
        std::getline(file, sline);
//        c = sline.c_str();
        std::replace(sline.begin(), sline.end(), ',', ' ');
        std::replace( sline.begin(), sline.end(), ';', ' ');
        std::replace( sline.begin(), sline.end(), '\"', ' ');
//        c = sline.c_str();
        std::stringstream ss(sline);
        while(!ss.eof())
        {
            name.clear();
            ss >> std::skipws >> name;
//            c = name.c_str();
            if(name.length() > 0)
                result.push_back(name);
        }
    }
    while(!firstLine && !file.eof());
    file.close();
//    for(int i = 0; i < result.size(); i++)
//    {
//        std::cout << result[i] << std::endl;
//    }
    setlocale(LC_ALL, prevloc);
    free(prevloc);
    return result;
}
