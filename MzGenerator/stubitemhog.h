/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2018 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef StubItemHog_H
#define StubItemHog_H

#include "genentry.h"
#include <math.h>

class StubItemHog : public StubItem
{
public:
    ~StubItemHog();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}

private:
    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas, unsigned int bins)
    {
        unsigned int n;
        double* table = datas->table;
        if(bits < 1) return;
        if(bits > sizeof(MazdaImagePixelType)*8) return;
        unsigned int shift = sizeof(MazdaImagePixelType)*8 - bits;

        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
        }
        if(quit) return;
        if(end[0] - begin[0] < 2) return;
        if(end[1] - begin[1] < 2) return;

        long int area = 0;
        double vals[32];
        for(n = 0; n < 32; n++) vals[n] = 0;

        begin[1]++; end[1]--;
        begin[0]++; end[0]--;
        MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[0]++; end[0]++;
        MazdaImageRegionIterator<MITypeLocal> cli = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rli = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[0] -= 2; end[0] -= 2;
        MazdaImageRegionIterator<MITypeLocal> cri = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[0]++; end[0]++;
        begin[1]++; end[1]++;
        MazdaImageRegionIterator<MITypeLocal> cti = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rti = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[1] -= 2; end[1] -= 2;
        MazdaImageRegionIterator<MITypeLocal> cbi = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
        MazdaRoiRegionIterator<MzRoiLocal> rbi = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
        begin[1]++; end[1]++;

        double add = M_PI/bins;
        double add2 = 2.0*add;

        while(!ri.IsBehind())
        {
            if(ri.GetPixel() && rli.GetPixel() && rri.GetPixel() && rti.GetPixel() && rbi.GetPixel())
            {
                int xx = ((int)cli.GetPixel()>>shift) - ((int)cri.GetPixel()>>shift);
                int yy = ((int)cbi.GetPixel()>>shift) - ((int)cti.GetPixel()>>shift);
//                int xx = ((int)cli.GetPixel() - (int)cri.GetPixel());
//                int yy = ((int)cbi.GetPixel() - (int)cti.GetPixel());
                double p = sqrt(xx*xx+yy*yy);
                double a = atan2(yy, xx);
                a += add;
                a += (2.0*M_PI);
                unsigned int da = a / add2;
                da %= bins;
                vals[da] += p;
                area++;
            }
            ++ri; ++rli; ++rri; ++rti; ++rbi;
            ++cli; ++cri; ++cti; ++cbi;
        }

        for(n = 0; n < bins; n++)
            if(Count[n] != (unsigned int)-1)
                table[Count[n]] = vals[n]/(double)area;
    }

    unsigned int Count[32];
    unsigned int bins;
    unsigned int bits;
};


#endif // StubItemHog_H
