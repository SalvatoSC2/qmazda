/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemgabor.h"

StubItem* StubItemGabor::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemGabor* item = new StubItemGabor;
    item->NewCommon(dimensions, parent, nameStubs, notify);
    return item;
}
StubItem* StubItemGaborLocal::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemGaborLocal* item = new StubItemGaborLocal;
    item->NewCommon(dimensions, parent, nameStubs, notify);
    return item;
}

void StubItemGabor::NewCommon(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, NotifyType* notify)
{
    const double mult = 0.00174532925199432957692369076849;// M_PI*2.0/3600;

    this->dimensions = dimensions;
    this->notify = notify;

    std::string direction = (*nameStubs)[nameStubs->size()-2];

    double period;

    if(direction == "A")
    {
        name = nameStubs->back();
        bits = atoi((*nameStubs)[nameStubs->size()-7].c_str());
        stddev  = atoi((*nameStubs)[nameStubs->size()-5].c_str());
        kernelsize = stddev * 3;

        double angl = atoi((*nameStubs)[nameStubs->size()-3].c_str());
        angl *= mult;
        angle[0] = cos(angl);
        angle[1] = sin(angl);
        angle[2] = 0.0;

        period = atoi((*nameStubs)[nameStubs->size()-1].c_str());
    }
    else
    {
        name = nameStubs->back();
        bits = atoi((*nameStubs)[nameStubs->size()-5].c_str());
        stddev  = atoi((*nameStubs)[nameStubs->size()-3].c_str());
        kernelsize = stddev * 3;

        if(direction == "O")
        {
            angle[0] = angle[1] = angle[2] = 0.0;
        }
        else if(direction == "X")
        {
            angle[0] = 0.0;
            angle[1] = 0.0;
            angle[2] = 1.0;
        }
        else if(direction == "H")
        {
            angle[0] = 1.0;
            angle[1] = 0.0;
            angle[2] = 0.0;
        }
        else if(direction == "U")
        {
            angle[0] = 0.92387953;
            angle[1] = 0.38268343;
            angle[2] = 0.0;
        }
        else if(direction == "Z")
        {
            angle[0] = 0.70710678;
            angle[1] = 0.70710678;
            angle[2] = 0.0;
        }
        else if(direction == "S")
        {
            angle[0] = 0.38268343;
            angle[1] = 0.92387953;
            angle[2] = 0.0;
        }
        else if(direction == "G")
        {
            angle[0] = -0.38268343;
            angle[1] = 0.92387953;
            angle[2] = 0.0;
        }
        else if(direction == "N")
        {
            angle[0] = -0.70710678;
            angle[1] = 0.70710678;
            angle[2] = 0.0;
        }
        else if(direction == "J")
        {
            angle[0] = -0.92387953;
            angle[1] = 0.38268343;
            angle[2] = 0.0;
        }
        else //"V"
        {
            angle[0] = 0.0;
            angle[1] = 1.0;
            angle[2] = 0.0;
        }
        period = atoi((*nameStubs)[nameStubs->size()-1].c_str());
    }

    freq = 6.2831853/period;
    Phase = (unsigned int) -1;
    Mag = (unsigned int) -1;
    Area = (unsigned int) -1;
    Re = (unsigned int) -1;
    Im = (unsigned int) -1;

    switch(dimensions)
    {
    case 3:
        kernel3re = itk::Image < double, 3 >::New();
        kernel3im = itk::Image < double, 3 >::New();
        CreateKernels < itk::Image< double, 3 > > (kernel3im, kernel3re);
        break;
    case 2:
        kernel2re = itk::Image < double, 2 >::New();
        kernel2im = itk::Image < double, 2 >::New();
        CreateKernels < itk::Image< double, 2 > > (kernel2im, kernel2re);
        break;
    default: break;
    }

    if(parent != NULL) parent->Connect(this);
}


void StubItemGabor::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    if(itemf->Name() == "Area") Area = itemf->Index();
    if(itemf->Name() == "Mag") Mag = itemf->Index();
    if(itemf->Name() == "Phase") Phase = itemf->Index();
    if(itemf->Name() == "Re") Re = itemf->Index();
    if(itemf->Name() == "Im") Im = itemf->Index();

    children.push_back(item);
}

void StubItemGabor::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);
    if(datas->roi != NULL)
    {
        try
        {
            switch(dimensions)
            {
            case 3: inRoi<3> (datas, kernel3im, kernel3re); break;
            case 2: inRoi<2> (datas, kernel2im, kernel2re); break;
            default: break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}


void StubItemGaborLocal::Execute(StubItemData* data)
{
    StubItemDataEntryLocal* datas = static_cast<StubItemDataEntryLocal*> (data);

    if(datas->roi == NULL && datas->maps != NULL)
    {
        switch(dimensions)
        {
        case 3: createMaps<3>(datas, kernel3im, kernel3re); break;
        case 2: createMaps<2>(datas, kernel2im, kernel2re); break;
        default: break;
        }
    }
    else if(datas->roi != NULL && datas->tables != NULL)
    {
        switch(dimensions)
        {
        case 3: createVectors<3> (datas, kernel3im, kernel3re); break;
        case 2: createVectors<2> (datas, kernel2im, kernel2re); break;
        default: break;
        }
    }
    if(notify!=NULL) (*notify)();
    return;
}


