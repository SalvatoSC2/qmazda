/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemnormalization.h"

StubItemNormalization::~StubItemNormalization()
{
}
StubItem* StubItemNormalization::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemNormalization* item = new StubItemNormalization;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();
    item->normalization = nameStubs->back().at(0);
    if(parent != NULL) parent->Connect(item);
    return item;
}

void StubItemNormalization::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datasr = dynamic_cast<StubItemDataEntryRoi*> (data);
    StubItemDataEntryLocal* datasl = dynamic_cast<StubItemDataEntryLocal*> (data);
    StubItemDataEntry* datas = static_cast<StubItemDataEntry*> (data);

    if(normalization == 'D')
    {
        //if(notify!=NULL) (*notify)();
        for(unsigned int i = 0; i < Connected(); i++) Connected(i)->Execute(data);
        return;
    }
    MazdaDummy* normalized = NULL;

    if(datasl != NULL)
    {
        switch(dimensions)
        {
        case 3: normalized = noRoi<3> (datas); break;
        case 2: normalized = noRoi<2> (datas); break;
        default: break;
        }
    }
    else if(datasr != NULL)
    {
        switch(dimensions)
        {
        case 3: normalized = inRoi<3> (datas); break;
        case 2: normalized = inRoi<2> (datas); break;
        default: break;
        }
    }
    if(normalized == NULL) return;
    if(datasl != NULL)
    {
        StubItemDataEntryLocal newdata = *datasl;
        newdata.image = normalized;
        newdata.isRGB = false;
        for(unsigned int i = 0; i < Connected(); i++) Connected(i)->Execute(&newdata);
    }
    else if(datasr != NULL)
    {
        StubItemDataEntryRoi newdata = *datasr;
        newdata.image = normalized;
        newdata.isRGB = false;
        for(unsigned int i = 0; i < Connected(); i++) Connected(i)->Execute(&newdata);
    }
    delete normalized;
    return;
}

