#ifndef FEATUREIO_H
#define FEATUREIO_H

#include <string>
#include <vector>

std::vector<std::string> loadFeatureList(const char* fileName, bool firstLine = false);


#endif // FEATUREIO_H
