/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemgrlm.h"


StubItemGrlm::~StubItemGrlm()
{
}
StubItem* StubItemGrlm::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemGrlm* item = new StubItemGrlm;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();
    item->bits = atoi((*nameStubs)[nameStubs->size()-3].c_str());
    item->RLNonUni = (unsigned int) -1;
    item->GLevNonUn = (unsigned int) -1;
    item->LngREmph = (unsigned int) -1;
    item->ShrtREmp = (unsigned int) -1;
    item->Fraction = (unsigned int) -1;
    item->MRLNonUni = (unsigned int) -1;
    item->MGLevNonUn = (unsigned int) -1;

    if(item->name == "H") item->direction = H;
    else if(item->name == "Z") item->direction = Z;
    else if(item->name == "N") item->direction = N;
    else if(item->name == "X") item->direction = X;
    else item->direction = V;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemGrlm::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    if(itemf->Name() == "RLNonUni") RLNonUni= itemf->Index();
    else if(itemf->Name() == "GLevNonUn") GLevNonUn= itemf->Index();
    else if(itemf->Name() == "LngREmph") LngREmph= itemf->Index();
    else if(itemf->Name() == "ShrtREmp") ShrtREmp= itemf->Index();
    else if(itemf->Name() == "Fraction") Fraction= itemf->Index();
    else if(itemf->Name() == "MRLNonUni") MRLNonUni= itemf->Index();
    else if(itemf->Name() == "MGLevNonUn") MGLevNonUn= itemf->Index();
    children.push_back(item);
}

void StubItemGrlm::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);

#ifdef USE_DEBUG_LOG
    if(debugfile.is_open())
    {
        debugfile << "########  Grlm::Execute()  ########" << std::endl;
        //debugfile << " ImageId: " << datas->imageid;
        //debugfile << " RoiName: " << datas->roiid <<" ########"<< std::endl;
    }
#endif
    if(datas->roi != NULL)
    {
        try
        {
            switch(dimensions)
            {
            case 3: inRoi<3> (datas); break;
            case 2: inRoi<2> (datas); break;
            default: break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}

