/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemhog.h"

StubItemHog::~StubItemHog()
{
}
StubItem* StubItemHog::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemHog* item = new StubItemHog;

    for(unsigned int n = 0; n < 32; n++)
        item->Count[n] = (unsigned int) -1;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();
    item->bits = atoi((*nameStubs)[nameStubs->size()-3].c_str());

    if(item->name == "O4b") item->bins = 4;
    else if(item->name == "O8b") item->bins = 8;
    else if(item->name == "O16b") item->bins = 16;
    else if(item->name == "O32b") item->bins = 32;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemHog::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);
    int index = atoi( itemf->Name().c_str());
    if(index >= 0 && index < 32)
        Count[index] = itemf->Index();
    children.push_back(item);
}

void StubItemHog::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);

    if(datas->roi != NULL)
    {
        try
        {
            switch(dimensions)
            {
            case 3: inRoi<3> (datas, bins); break;
            case 2: inRoi<2> (datas, bins); break;
            default: break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}


