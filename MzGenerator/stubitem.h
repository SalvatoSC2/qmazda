/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEM_H
#define STUBITEM_H

#include <vector>
#include <string>
#include <limits>
#include "../MzShared/parametertreetemplate.h"
#include "../SharedImage/mazdaimage.h"
#include "../SharedImage/mazdaroi.h"
#include "../SharedImage/imagedefs.h"

typedef MultiChannelPixel <MazdaImagePixelType, 3> PixelRGBType;
typedef MazdaImage<MazdaImagePixelType, 2> MIType2D;
typedef MazdaImage<PixelRGBType , 2> MIType2DRGB;
typedef MazdaImage<MazdaImagePixelType, 3> MIType3D;
typedef MazdaImage<PixelRGBType , 3> MIType3DRGB;
typedef MazdaRoi <MazdaRoiBlockType, 2> MzRoi2D;
typedef MazdaRoi <MazdaRoiBlockType, 3> MzRoi3D;

typedef MazdaImage<MazdaMapPixelType, 2> MapType2D;
typedef MazdaImage<MazdaMapPixelType, 3> MapType3D;

class StubItemBuilder;

const double mznan = std::numeric_limits<double>::quiet_NaN();

/**
 * @brief The StubItemData class dummy to be inherited by data storages
 */
class StubItemData
{
public:
    virtual ~StubItemData(){}
};

class StubItemDataEntry : public StubItemData
{
public:
    MazdaDummy* image;
    MazdaDummy* roi;
//    unsigned int imageid;
//    unsigned int roiid;
//    bool is3D;
    bool isRGB;
};

class StubItemDataEntryRoi : public StubItemDataEntry
{
public:
    double* table;
};

class StubItemDataEntryLocal : public StubItemDataEntry
{
public:
    std::vector<MazdaDummy*>* maps;
    std::vector<double*>* tables;
    unsigned int step;
};

typedef void NotifyType(void);

/**
 * @brief The StubItem class abstract to be inherited by algorithm modules, enables modules connectivity
 */
class StubItem
{
public:
//    static double* table;
    virtual void Execute(StubItemData* data) = 0;
    virtual bool IsNotifier(void) = 0;
//    virtual void SetProperties(std::vector <std::string>* properties, unsigned int index) = 0;
    virtual void Connecting(unsigned int /*index*/){}

    virtual void Connect(StubItem* item)
    {
        children.push_back(item);
    }
    std::size_t Connected(void)
    {
        return children.size();
    }
    StubItem* Connected(std::size_t i)
    {
        return children[i];
    }
    virtual ~StubItem()
    {
        for(unsigned int i = 0; i < children.size(); i++) delete children[i];
        children.clear();
    }
    std::string Name(void)
    {
        return name;
    }

protected:
    unsigned int dimensions;
    std::string name;
    std::vector < StubItem* > children;
    NotifyType* notify;
};

/**
 * @brief The StubItemThrough class dummy algorithm module just to forward data to another module
 */
class StubItemThrough : public StubItem
{
public:
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* /*notify*/)
    {
        StubItemThrough* item = new StubItemThrough;
        item->dimensions = dimensions;
        if(nameStubs != NULL) item->name = nameStubs->back();
        if(parent != NULL) parent->Connect(item);
        return item;
    }
    void Execute(StubItemData* data)
    {
        for(unsigned int i = 0; i < Connected(); i++) Connected(i)->Execute(data);
    }
    bool IsNotifier(){return false;}
};

/**
 * @brief The StubItemFinal class dummy feature computation modules. Actually if used, the features should be computed by the preceding module.
 */
class StubItemFinal : public StubItem
{
public:
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType* notify)
    {
        StubItemFinal* item = new StubItemFinal;
        item->dimensions = dimensions;
        item->name = nameStubs->back();
        item->index = index;
        item->notify = notify;
        if(parent != NULL) parent->Connect(item);
        return item;
    }
    void Execute(StubItemData* /*data*/)
    {
    }
    bool IsNotifier(){return false;}
    unsigned int Index(void)
    {
        return index;
    }
private:
    unsigned int index;
};

typedef StubItem* MethodNewType(unsigned int, StubItem*, std::vector<std::string>*, unsigned int, NotifyType*);

struct ItemTemplate
{
    std::string codeLine;
    MethodNewType* creator;
};


/**
 * @brief The StubItemBuilder class to create connection of algorithms that computes features.
 */
class StubItemBuilder
{
public:

    static void CreateTemplate(NameStubTree* featuresTemplate, const ItemTemplate* itemTemplate, const unsigned int itemTemplateSize)
    {
        ParameterTreeItem<NameStub>* item;
        item = featuresTemplate->beginBuildFromLine();
        item->data.ptr = (void*)(&StubItemThrough::New);
        for(unsigned int i = 0; i < itemTemplateSize; i++)
        {
            ParameterTreeItem<NameStub>* item = featuresTemplate->buildFromLine(itemTemplate[i].codeLine, '.');
            if(item != NULL)
            {
                item->data.ptr = (void*)(itemTemplate[i].creator);
            }
        }
        featuresTemplate->endBuildFromLine();
    }

    StubItemBuilder(unsigned int dimensions, NameStubTree* featuresTemplate)
    {
        notificationsNumber = 0;
        this->dimensions = dimensions;
        this->featuresTemplate = featuresTemplate;
        topLevel = StubItemThrough::New(dimensions, NULL, NULL, 0, NULL);
    }
//    StubItemBuilder(unsigned int dimensions, NameStubTree* featuresTemplate, const ItemTemplate* itemTemplate, const unsigned int itemTemplateSize)
//    {
//        ParameterTreeItem<NameStub>* item;
//        item = featuresTemplate->beginBuildFromLine();
//        item->data.ptr = (void*)(&StubItemThrough::New);
//        for(unsigned int i = 0; i < itemTemplateSize; i++)
//        {
//            ParameterTreeItem<NameStub>* item = featuresTemplate->buildFromLine(itemTemplate[i].codeLine, '.');
//            if(item != NULL)
//            {
//                item->data.ptr = (void*)(itemTemplate[i].creator);
//            }
//        }
//        featuresTemplate->endBuildFromLine();

//        notificationsNumber = 0;
//        this->dimensions = dimensions;
//        this->featuresTemplate = featuresTemplate;
//        topLevel = StubItemThrough::New(dimensions, NULL, NULL, 0, NULL);
//    }
    ~StubItemBuilder()
    {
        if(topLevel != NULL) delete topLevel;
        topLevel = NULL;
    }
    void Execute(StubItemData* data)
    {
        if(topLevel != NULL)
            static_cast<StubItemThrough*>(topLevel)->Execute(data);
    }
    bool addFeature(const std::string name, unsigned int index, NotifyType *notify);

    unsigned int getNotificationsNumber(void)
    {
        return notificationsNumber;
    }

private:
    unsigned int dimensions;
    unsigned int notificationsNumber;
    NameStubTree* featuresTemplate;
    StubItem* topLevel;
};

#endif // STUBITEM_H
