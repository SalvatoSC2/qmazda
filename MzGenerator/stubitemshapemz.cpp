/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemshapemz.h"
#include "stubitemshape.h"

StubItemShapeMz::~StubItemShapeMz()
{
}
StubItem* StubItemShapeMz::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemShapeMz* item = new StubItemShapeMz;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();

//----------------------------------------------
//Wielkosci podstawowe
//----------------------------------------------
    item->X = (unsigned int) -1;
    item->Y = (unsigned int) -1;
    item->F = (unsigned int) -1;
    item->Spol = (unsigned int) -1;
    item->Smax = (unsigned int) -1;
    item->Aox = (unsigned int) -1;
    item->Ug = (unsigned int) -1;
    item->Uw = (unsigned int) -1;
    item->Fmax = (unsigned int) -1;
    item->Fmin = (unsigned int) -1;
    item->Fh = (unsigned int) -1;
    item->Fv = (unsigned int) -1;
    item->Mmin = (unsigned int) -1;
    item->Mmax = (unsigned int) -1;
    item->Maver = (unsigned int) -1;
    item->Ft = (unsigned int) -1;
    item->Ul = (unsigned int) -1;
    item->S1 = (unsigned int) -1;
    item->S2 = (unsigned int) -1;
    item->Lsz = (unsigned int) -1;
    item->S = (unsigned int) -1;
    item->L = (unsigned int) -1;
    item->SxL = (unsigned int) -1;
    item->D1 = (unsigned int) -1;
    item->D2 = (unsigned int) -1;
    item->Fd2 = (unsigned int) -1;
    item->LmaxE = (unsigned int) -1;
    item->LminE = (unsigned int) -1;
    item->FE = (unsigned int) -1;
//----------------------------------------------
//Wspolczynniki ksztaltu
//----------------------------------------------
    item->W1 = (unsigned int) -1;
    item->W2 = (unsigned int) -1;
    item->W6 = (unsigned int) -1;
    item->W3 = (unsigned int) -1;
    item->W4 = (unsigned int) -1;
    item->W5 = (unsigned int) -1;
    item->W5b = (unsigned int) -1;
    item->W7 = (unsigned int) -1;
    item->Rs = (unsigned int) -1;
    item->Rf = (unsigned int) -1;
    item->Rff = (unsigned int) -1;
    item->Rc = (unsigned int) -1;
    item->Rc1 = (unsigned int) -1;
    item->Rc2 = (unsigned int) -1;
    item->Rm = (unsigned int) -1;
    item->Rb = (unsigned int) -1;
    item->Rd = (unsigned int) -1;
    item->Rh = (unsigned int) -1;
    item->W8 = (unsigned int) -1;
    item->W9 = (unsigned int) -1;
    item->W10 = (unsigned int) -1;
    item->SigR = (unsigned int) -1;
    item->W11 = (unsigned int) -1;
    item->W12 = (unsigned int) -1;
    item->W13 = (unsigned int) -1;
    item->W14 = (unsigned int) -1;
    item->W15 = (unsigned int) -1;
//----------------------------------------------
//Momenty
//----------------------------------------------
    item->M2x = (unsigned int) -1;
    item->M2y = (unsigned int) -1;
    item->M2xy = (unsigned int) -1;
//----------------------------------------------
//Dodatkowe wielkosci
//----------------------------------------------
    item->Er = (unsigned int) -1;
    item->Er2 = (unsigned int) -1;
    item->El = (unsigned int) -1;
    item->El2 = (unsigned int) -1;
    item->Nc = (unsigned int) -1;
    item->Nv = (unsigned int) -1;
    item->Nl = (unsigned int) -1;
    item->Nsz = (unsigned int) -1;
    item->Ni = (unsigned int) -1;
    item->Nx = (unsigned int) -1;
    item->No = (unsigned int) -1;
    item->Xo = (unsigned int) -1;
    item->Yo = (unsigned int) -1;
    item->XYo = (unsigned int) -1;

    item->compute_cokolwiek = false;
    item->compute_profil = false;
    item->compute_szkielet = false;
    item->compute_profil_obwod = false;
    item->compute_martin = false;
    item->compute_martinr = false;
    item->compute_kontur_spojny = false;
    item->compute_feret = false;
    item->compute_obwod_wypukly = false;
    item->compute_prostokat_opisany = false;
    item->compute_okrag_wpisany = false;
    item->compute_okrag_opisany = false;
    item->compute_elipsa = false;
    item->compute_najw_sred = false;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemShapeMz::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    if(itemf->Name() == "X") {X = itemf->Index();}
    else if(itemf->Name() == "Y") {Y = itemf->Index();}
    else if(itemf->Name() == "F") {F = itemf->Index();}
    else if(itemf->Name() == "Spol") {Spol = itemf->Index();}
    else if(itemf->Name() == "Rc1") {Rc1 = itemf->Index();}

    else if(itemf->Name() == "Smax") {Smax = itemf->Index(); compute_cokolwiek = true; compute_najw_sred = true;}
    else if(itemf->Name() == "Aox") {Aox = itemf->Index(); compute_cokolwiek = true; compute_najw_sred = true;}
    else if(itemf->Name() == "Ug") {Ug = itemf->Index(); compute_cokolwiek = true; compute_profil_obwod = true;}
    else if(itemf->Name() == "Uw") {Uw = itemf->Index(); compute_cokolwiek = true; compute_obwod_wypukly = true;}
    else if(itemf->Name() == "Fmax") {Fmax = itemf->Index(); compute_cokolwiek = true; compute_feret = true;}
    else if(itemf->Name() == "Fmin") {Fmin = itemf->Index(); compute_cokolwiek = true; compute_feret = true;}
    else if(itemf->Name() == "Fh") {Fh = itemf->Index(); compute_cokolwiek = true; compute_feret = true;}
    else if(itemf->Name() == "Fv") {Fv = itemf->Index(); compute_cokolwiek = true; compute_feret = true;}
    else if(itemf->Name() == "Mmin") {Mmin = itemf->Index(); compute_cokolwiek = true; compute_martin = true;}
    else if(itemf->Name() == "Mmax") {Mmax = itemf->Index(); compute_cokolwiek = true; compute_martin = true;}
    else if(itemf->Name() == "Maver") {Maver = itemf->Index(); compute_cokolwiek = true; compute_martin = true;}
    else if(itemf->Name() == "Ft") {Ft = itemf->Index(); compute_cokolwiek = true; compute_profil = true;}
    else if(itemf->Name() == "Ul") {Ul = itemf->Index(); compute_cokolwiek = true; compute_kontur_spojny = true;}
    else if(itemf->Name() == "S1") {S1 = itemf->Index(); compute_cokolwiek = true; compute_szkielet = true;}
    else if(itemf->Name() == "S2") {S2 = itemf->Index(); compute_cokolwiek = true; compute_szkielet = true;}
    else if(itemf->Name() == "Lsz") {Lsz = itemf->Index(); compute_cokolwiek = true; compute_szkielet = true;}
    else if(itemf->Name() == "S") {S = itemf->Index(); compute_cokolwiek = true; compute_prostokat_opisany = true;}
    else if(itemf->Name() == "L") {L = itemf->Index(); compute_cokolwiek = true; compute_prostokat_opisany = true;}
    else if(itemf->Name() == "SxL") {SxL = itemf->Index(); compute_cokolwiek = true; compute_prostokat_opisany = true;}
    else if(itemf->Name() == "D1") {D1 = itemf->Index(); compute_cokolwiek = true; compute_okrag_wpisany = true;}
    else if(itemf->Name() == "D2") {D2 = itemf->Index(); compute_cokolwiek = true; compute_okrag_opisany = true;}
    else if(itemf->Name() == "Fd2") {Fd2 = itemf->Index(); compute_cokolwiek = true; compute_okrag_opisany = true;}
    else if(itemf->Name() == "LmaxE") {LmaxE = itemf->Index(); compute_cokolwiek = true; compute_elipsa = true;}
    else if(itemf->Name() == "LminE") {LminE = itemf->Index(); compute_cokolwiek = true; compute_elipsa = true;}
    else if(itemf->Name() == "FE") {FE = itemf->Index(); compute_cokolwiek = true; compute_elipsa = true;}
    else if(itemf->Name() == "W1") {W1 = itemf->Index(); compute_cokolwiek = true; compute_elipsa = true;}
    else if(itemf->Name() == "W2") {W2 = itemf->Index(); compute_cokolwiek = true; compute_kontur_spojny = true; compute_profil = true;}
    else if(itemf->Name() == "W6") {W6 = itemf->Index(); compute_cokolwiek = true; compute_kontur_spojny = true; compute_profil = true;}
    else if(itemf->Name() == "W3") {W3 = itemf->Index(); compute_cokolwiek = true; compute_kontur_spojny = true; compute_profil = true;}
    else if(itemf->Name() == "W4") {W4 = itemf->Index(); compute_cokolwiek = true; compute_kontur_spojny = true;}
    else if(itemf->Name() == "W5") {W5 = itemf->Index(); compute_cokolwiek = true; compute_szkielet = true;}
    else if(itemf->Name() == "W5b") {W5b = itemf->Index(); compute_cokolwiek = true; compute_szkielet = true;}
    else if(itemf->Name() == "W7") {W7 = itemf->Index(); compute_cokolwiek = true; compute_okrag_opisany = true; compute_okrag_wpisany = true;}
    else if(itemf->Name() == "Rs") {Rs = itemf->Index(); compute_cokolwiek = true; compute_profil_obwod = true;}
    else if(itemf->Name() == "Rf") {Rf = itemf->Index(); compute_cokolwiek = true; compute_feret = true;}
    else if(itemf->Name() == "Rff") {Rff = itemf->Index(); compute_cokolwiek = true; compute_feret = true;}
    else if(itemf->Name() == "Rc") {Rc = itemf->Index(); compute_cokolwiek = true; compute_profil_obwod = true;}
    else if(itemf->Name() == "Rc2") {Rc2 = itemf->Index(); compute_cokolwiek = true; compute_profil_obwod = true;}
    else if(itemf->Name() == "Rm") {Rm = itemf->Index(); compute_cokolwiek = true; compute_profil_obwod = true;}
    else if(itemf->Name() == "Rb") {Rb = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "Rd") {Rd = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "Rh") {Rh = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "W8") {W8 = itemf->Index(); compute_cokolwiek = true; compute_prostokat_opisany = true;}
    else if(itemf->Name() == "W9") {W9 = itemf->Index(); compute_cokolwiek = true; compute_prostokat_opisany = true;}
    else if(itemf->Name() == "W10") {W10 = itemf->Index(); compute_cokolwiek = true; compute_martin = true;}
    else if(itemf->Name() == "SigR") {SigR = itemf->Index(); compute_cokolwiek = true; compute_martin = true;}
    else if(itemf->Name() == "W11") {W11 = itemf->Index(); compute_cokolwiek = true; compute_feret = true; compute_najw_sred = true;}
    else if(itemf->Name() == "W12") {W12 = itemf->Index(); compute_cokolwiek = true; compute_najw_sred = true;}
    else if(itemf->Name() == "W13") {W13 = itemf->Index(); compute_cokolwiek = true; compute_najw_sred = true;}
    else if(itemf->Name() == "W14") {W14 = itemf->Index(); compute_cokolwiek = true; compute_najw_sred = true;}
    else if(itemf->Name() == "W15") {W15 = itemf->Index(); compute_cokolwiek = true; compute_najw_sred = true; compute_najw_sred = true;}
    else if(itemf->Name() == "M2x") {M2x = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "M2y") {M2y = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "M2xy") {M2xy = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "Er") {Er = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "Er2") {Er2 = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "El") {El = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "El2") {El2 = itemf->Index(); compute_cokolwiek = true; compute_martinr = true;}
    else if(itemf->Name() == "Nc") {Nc = itemf->Index(); compute_cokolwiek = true; compute_profil_obwod = true;}
    else if(itemf->Name() == "Nv") {Nv = itemf->Index(); compute_cokolwiek = true; compute_feret = true;}
    else if(itemf->Name() == "Nl") {Nl = itemf->Index(); compute_cokolwiek = true; compute_kontur_spojny = true;}
    else if(itemf->Name() == "Nsz") {Nsz = itemf->Index(); compute_cokolwiek = true; compute_szkielet = true;}
    else if(itemf->Name() == "Ni") {Ni = itemf->Index(); compute_cokolwiek = true; compute_szkielet = true;}
    else if(itemf->Name() == "Nx") {Nx = itemf->Index(); compute_cokolwiek = true; compute_szkielet = true;}
    else if(itemf->Name() == "No") {No = itemf->Index(); compute_cokolwiek = true; compute_szkielet = true;}
    else if(itemf->Name() == "Xo") {Xo = itemf->Index(); compute_cokolwiek = true; compute_okrag_wpisany = true;}
    else if(itemf->Name() == "Yo") {Yo = itemf->Index(); compute_cokolwiek = true; compute_okrag_wpisany = true;}
    else if(itemf->Name() == "XYo") {XYo = itemf->Index(); compute_cokolwiek = true; compute_okrag_wpisany = true;}
    children.push_back(item);
}

void StubItemShapeMz::Execute(StubItemData* data)
{
    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);

    if(datas->roi != NULL)
    {
        try
        {
            if(dimensions == 2) inRoi2D(datas);
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();
    return;
}



void StubItemShapeMz::inRoi2D(StubItemDataEntryRoi* datas)
{
    MzRoi2D* roi = static_cast<MzRoi2D*> (datas->roi);
    double* table = datas->table;
    WykonajOliczenia(roi, table);
}


#define ffpop(x, y)\
{\
    if(stackPointer > 0)\
    {\
        stackPointer--;\
        unsigned int p = stack[stackPointer];\
        x = p & 0xffff;\
        y = (p >> 16) & 0xffff;\
    }\
    else break;\
};

#define ffpush(x, y)\
{\
    if(stackPointer < max_area_fill-1)\
    {\
        stack[stackPointer] = (x) | ((y) << 16);\
        stackPointer++;\
    }\
    else break;\
};

//---------------------------------------------------------------------------
void StubItemShapeMz::WykonajOliczenia(MzRoi2D* roi, double* table)
{
    const double norma = 1.0;
    const int liczba_katow = 256;  //Podzielne przez 8
    const double duzy_double = 10000000000000000.0;
    double p2 = sqrt(2.0);

    //Obliczenia obszaru i zakresow roi
    //Obliczanie srodka ciezkosci
    double X_sred = 0.0;
    double Y_sred = 0.0;
    int area = 0;
    int begin[2];
    int end[2];
    roi->GetBegin(begin);
    roi->GetEnd(end);
    MazdaRoiIterator<MzRoi2D> iterator(roi);
    for(int y = begin[1]; y <= end[1]; y++)
    {
        for(int x = begin[0]; x <= end[0]; x++)
        {

            if(iterator.GetPixel())
            {
                area++;
                X_sred+=(double)x;
                Y_sred+=(double)y;
            }
            ++iterator;
        }
    }
    if(area>0)
    {
        X_sred/=area;
        Y_sred/=area;
    }
    else
    {
        if(F != (unsigned int)-1) table[F] = 0.0;
        return;
    }
    if(X != (unsigned int)-1) table[X] = X_sred;
    if(Y != (unsigned int)-1) table[Y] = Y_sred;
    if(F != (unsigned int)-1) table[F] = area;
    if(Spol != (unsigned int)-1) table[Spol] = sqrt((double)area/M_PI_4)*norma;
    if(Rc1 != (unsigned int)-1) table[Rc1] = sqrt(area/M_PI_4)*norma;


    if(compute_cokolwiek)
    {
        unsigned char* obrazek_tymczasowy = NULL;
        int obrazek_tymczasowy_x;
        int obrazek_tymczasowy_y;

        double X_sred_ = X_sred-begin[0]+1;
        double Y_sred_ = Y_sred-begin[1]+1;
        int area_profil = 0;

        //Tworzenie obrazka tymczasowego
        if(obrazek_tymczasowy!=NULL) delete[]obrazek_tymczasowy;
        obrazek_tymczasowy = new unsigned char[(end[0]-begin[0]+3)*(end[1]-begin[1]+3)+2];

        obrazek_tymczasowy_x = end[0]-begin[0]+3;
        obrazek_tymczasowy_y = end[1]-begin[1]+3;
        for(int y = 0; y < obrazek_tymczasowy_y; y++)
        {
            obrazek_tymczasowy[obrazek_tymczasowy_x*y] = 0;
            obrazek_tymczasowy[obrazek_tymczasowy_x*y+obrazek_tymczasowy_x-1] = 0;
        }
        for(int x = 0; x < obrazek_tymczasowy_x; x++)
        {
            obrazek_tymczasowy[x] = 0;
            obrazek_tymczasowy[x + obrazek_tymczasowy_x*(obrazek_tymczasowy_y-1)] = 0;
        }

        iterator.GoToBegin();
        for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
        {
            for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
            {
                if(iterator.GetPixel())
                    obrazek_tymczasowy[x + y * obrazek_tymczasowy_x] = 9;
                else
                    obrazek_tymczasowy[x + y * obrazek_tymczasowy_x] = 0;
                ++iterator;
            }
        }


        if(compute_feret || compute_obwod_wypukly || compute_okrag_opisany || compute_elipsa || compute_prostokat_opisany || compute_profil || compute_szkielet)
        {
            //Wypelnianie dziur w obiektach (uspojnianie)
            int max_area_fill = obrazek_tymczasowy_x*obrazek_tymczasowy_y;
            unsigned int* stack = new unsigned int[max_area_fill];
            {
                int x, y;
                x = 0; y = 0;
                int stackPointer = 0;
                if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]!=0) return;
                do
                {
                    ffpush (x, y);
                }while(false);

                while(true)
                {
                    ffpop (x, y);
                    obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=2;
                    if(x>0)
                        if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x-1]==0)
                            ffpush (x-1, y);
                    if(y>0)
                        if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]==0)
                            ffpush (x, y-1);
                    if(x<obrazek_tymczasowy_x-1)
                        if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x+1]==0)
                            ffpush (x+1, y);
                    if(y<obrazek_tymczasowy_y-1)
                        if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]==0)
                            ffpush (x, y+1);
                }
            }

            for(int y = 0; y < obrazek_tymczasowy_y; y++)
                for(int x = 0; x < obrazek_tymczasowy_x; x++)
                {
                    if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&3)
                        obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=16;

                    if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&2)
                        obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&=0xfd;
                    else
                        obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=2;
                }


            //Obliczenia obszaru profilu
            for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                {
                    if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&2)
                        area_profil++;
                }


            if(Ft != (unsigned int)-1) table[Ft] = area_profil*norma*norma;

            //Poszukiwanie liczby dziur w obiekcie
            int oczek_szkieletu = 0;
            for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                {
                    int stackPointer = 0;
                    if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&16) continue;
                    oczek_szkieletu++;
                    ffpush (x, y);
                    while(true)
                    {
                        ffpop (x, y);
                        obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=16;
                        if(x>0)
                            if((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x-1]&16)==0)
                                ffpush (x-1, y);
                        if(y>0)
                            if((obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&16)==0)
                                ffpush (x, y-1);
                        if(x<obrazek_tymczasowy_x-1)
                            if((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x+1]&16)==0)
                                ffpush (x+1, y);
                        if(y<obrazek_tymczasowy_y-1)
                            if((obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&16)==0)
                                ffpush (x, y+1);
                    }
                }
            delete[] stack;


            if(compute_szkielet)
            {
                //Szkieletyzacja oryginalu
                int max_while = 0;
                int min_while = -1;
                bool to_do = true;
                while(to_do&&(max_while<2048))
                {
                    to_do = false;

                    for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                        for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                        {
                            if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 8)!=0) &&
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x+1] & 8)==0))
                            {
                                int sa = 0;
                                int sb = 0;
                                // int sc = 0;
                                bool sl = false;
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&12) {sa++; sl=true;}
                                else {sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x+1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x+1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&12) {sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if((sb>1)||(sa<4))
                                {
                                    obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=4;
                                    if(min_while<0) min_while = max_while;
                                }
                            }
                        }

                    for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                        for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                        {
                            if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 8)!=0) &&
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x+1] & 8)==0))
                            {
                                obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&=0xf7;
                                to_do = true;
                                if((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x-1] & 8)==0)
                                {
                                    obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=4;
                                    if(min_while<0) min_while = max_while;
                                }
                            }
                        }

                    for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                        for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                        {
                            if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 8)!=0) &&
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x] & 8)==0))
                            {
                                int sa = 0;
                                int sb = 0;
                                //int sc = 0;
                                bool sl = false;
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&12) {sa++; sl=true;}
                                else {sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x+1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x+1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&12) {sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if((sb>1)||(sa<4))
                                {
                                    obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=4;
                                    if(min_while<0) min_while = max_while;
                                }
                            }
                        }
                    for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                        for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                        {
                            if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 8)!=0) &&
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x] & 8)==0))
                            {
                                obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&=0xf7;
                                to_do = true;
                                if((obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x] & 8)==0)
                                {
                                    obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=4;
                                    if(min_while<0) min_while = max_while;
                                }
                            }
                        }


                    for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                        for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                        {
                            if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 8)!=0) &&
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x-1] & 8)==0))
                            {
                                int sa = 0;
                                int sb = 0;
                                //int sc = 0;
                                bool sl = false;
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&12) {sa++; sl=true;}
                                else {sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x+1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x+1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&12) {sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if((sb>1)||(sa<4))
                                {
                                    obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=4;
                                    if(min_while<0) min_while = max_while;
                                }
                            }
                        }
                    for(int y = obrazek_tymczasowy_y-2; y > 0; y--)
                        for(int x = obrazek_tymczasowy_x-2; x > 0; x--)
                        {
                            if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 8)!=0) &&
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x-1] & 8)==0))
                            {
                                obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&=0xf7;
                                to_do = true;
                                if((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x+1] & 8)==0)
                                {
                                    obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=4;
                                    if(min_while<0) min_while = max_while;
                                }
                            }
                        }
                    for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                        for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                        {
                            if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 8)!=0) &&
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x] & 8)==0))
                            {
                                int sa = 0;
                                int sb = 0;
                                //int sc = 0;
                                bool sl = false;
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&12) {sa++; sl=true;}
                                else {sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x+1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x-1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x+1]&12) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&12) {sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if((sb>1)||(sa<4))
                                {
                                    obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=4;
                                    if(min_while<0) min_while = max_while;
                                }
                            }
                        }
                    for(int y = obrazek_tymczasowy_y-2; y > 0; y--)
                        for(int x = obrazek_tymczasowy_x-2; x > 0; x--)
                        {
                            if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 8)!=0) &&
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x] & 8)==0))
                            {
                                obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&=0xf7;
                                to_do = true;
                                if((obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x] & 8)==0)
                                {
                                    obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=4;
                                    if(min_while<0) min_while = max_while;
                                }
                            }
                        }
                    max_while++;
                }
                max_while--;
                if(min_while<0) min_while = max_while;

                //Pocienianie szkieletu
                int punktow_szkieletu=0;
                for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                    for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                    {
                        if((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 4)!=0)
                        {
                            obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]|=8;
                            punktow_szkieletu++;
                        }
                    }

                for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                    for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                    {
                        if((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] & 8)!=0)
                        {
                            if((obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x  ]&8) &&
                                    (obrazek_tymczasowy[obrazek_tymczasowy_x*(y  )+x-1]&8) &&
                                    (!(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x+1]&8)))
                                obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] &= (0xff-8);
                            if((obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x  ]&8) &&
                                    (obrazek_tymczasowy[obrazek_tymczasowy_x*(y  )+x-1]&8) &&
                                    (!(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x+1]&8)))
                                obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] &= (0xff-8);
                            if((obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x  ]&8) &&
                                    (obrazek_tymczasowy[obrazek_tymczasowy_x*(y  )+x+1]&8) &&
                                    (!(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x-1]&8)))
                                obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] &= (0xff-8);
                            if((obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x  ]&8) &&
                                    (obrazek_tymczasowy[obrazek_tymczasowy_x*(y  )+x+1]&8) &&
                                    (!(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x-1]&8)))
                                obrazek_tymczasowy[obrazek_tymczasowy_x*y+x] &= (0xff-8);
                        }
                    }

                //Obliczanie dlugosci szkieletu i poszukiwanie punktow wierzcholkowych
                double dlugosc_szkieletu = 0.0;
                int punktow_wierzcholkowych_szkieletu = 0;
                for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                    for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                    {
                        if((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&8)!=0)
                        {
                            int sa = 0;
                            int sc = 0;
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&8) sa++;
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x+1]&8) sc++;
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&8) sa++;
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x-1]&8) sc++;
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x-1]&8) sa++;
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x-1]&8) sc++;
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&8) sa++;
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x+1]&8) sc++;
                            dlugosc_szkieletu+=(sc*p2+sa*1.0);
                            if(sa+sc < 2)
                            {
                                punktow_wierzcholkowych_szkieletu++;
                            }
                        }
                    }
                dlugosc_szkieletu/=2;
                int rozgalezien_szkieletu = 2*oczek_szkieletu + punktow_wierzcholkowych_szkieletu - 2;

                if(S1 != (unsigned int)-1) table[S1] = (double)max_while*norma; //szkielet max grubosc
                if(S2 != (unsigned int)-1) table[S2] = (double)min_while*norma; //szkielet min grubosc
                if(Lsz != (unsigned int)-1) table[Lsz] = dlugosc_szkieletu*norma;
                if(Nsz != (unsigned int)-1) table[Nsz] = (double)punktow_szkieletu;
                if(Ni != (unsigned int)-1) table[Ni] = (double)punktow_wierzcholkowych_szkieletu;
                if(Nx != (unsigned int)-1) table[Nx] = (double)rozgalezien_szkieletu;
                if(No != (unsigned int)-1) table[No] = (double)oczek_szkieletu;


                if(W5 != (unsigned int)-1) table[W5] = area_profil*norma/dlugosc_szkieletu;
                if(W5b != (unsigned int)-1) table[W5b] = area_profil/(dlugosc_szkieletu*dlugosc_szkieletu); //bezwymiarowy
            }
        }

        //Szukanie punktow konturu i obliczanie obwodu (obiekt niespojny)
        if(compute_profil_obwod || compute_martin || compute_martinr || compute_kontur_spojny
                || compute_najw_sred || compute_feret || compute_obwod_wypukly || compute_okrag_wpisany
                || compute_okrag_opisany || compute_elipsa || compute_prostokat_opisany)
        {
            int kontur_max = 0;
            unsigned short *kontur_x = NULL;
            unsigned short *kontur_y = NULL;

            for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                {
                    if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&1)
                    {
                        if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x+1]&1)==0)||
                                ((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x-1]&1)==0)||
                                ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&1)==0)||
                                ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&1)==0))
                        {
                            kontur_max++;
                        }
                    }
                }

            if(kontur_x!=NULL) delete[] kontur_x;
            kontur_x = new unsigned short[kontur_max+2];
            if(kontur_y!=NULL) delete[] kontur_y;
            kontur_y = new unsigned short[kontur_max+2];
            kontur_max = 0;
            double obwod = 0.0;

            for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                {
                    if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&1)
                    {
                        if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x+1]&1)==0)||
                                ((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x-1]&1)==0)||
                                ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&1)==0)||
                                ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&1)==0))
                        {
                            kontur_x[kontur_max] = x;
                            kontur_y[kontur_max] = y;
                            kontur_max++;
                            int sa = 0;
                            int sb = 0;
                            int sc = 0;

                            bool sl = false;

                            if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&1) {sa++; sl=true;}
                            else {sl=false;};
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x+1]&1) {sc++; sl=true;}
                            else {if(sl)sb++; sl=false;};
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&1) {sa++; sl=true;}
                            else {if(sl)sb++; sl=false;};
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x-1]&1) {sc++; sl=true;}
                            else {if(sl)sb++; sl=false;};
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x-1]&1) {sa++; sl=true;}
                            else {if(sl)sb++; sl=false;};
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x-1]&1) {sc++; sl=true;}
                            else {if(sl)sb++; sl=false;};
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&1) {sa++; sl=true;}
                            else {if(sl)sb++; sl=false;};
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x+1]&1) {sc++; sl=true;}
                            else {if(sl)sb++; sl=false;};
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&1) {sl=true;}
                            else {if(sl)sb++; sl=false;};

                            if(sb>1) sb=2;
                            obwod+=((double)sb*(sc*p2+sa*1.0));
                        }
                    }
                }
            obwod/=2;

            if(Ug != (unsigned int)-1) table[Ug] = obwod*norma;
            if(Rs != (unsigned int)-1) table[Rs] = obwod*obwod/(4*M_PI*area);
            if(Rc != (unsigned int)-1) table[Rc] = sqrt(area/M_PI_4)/(obwod/M_PI); //Rc
            if(Rc2 != (unsigned int)-1) table[Rc2] = (obwod/M_PI)*norma;
            if(Rm != (unsigned int)-1) table[Rm] = (obwod/sqrt(M_PI_4*area))-1; //Rm
            if(Nc != (unsigned int)-1) table[Nc] = (double)kontur_max;

            //Obliczanie promieni Martina, odleglosc kontur - srodek
            if(compute_martin || compute_martinr)
            {
                double Mmin_t, Mmax_t, Maver_t, Maver2;
                Maver_t = 0.0;
                Maver2 = 0.0;
                Mmin_t = duzy_double;
                Mmax_t = 0.0;
                for(int f = 0; f<kontur_max; f++)
                {
                    double d = ((double)kontur_x[f]-X_sred_)*((double)kontur_x[f]-X_sred_)
                            +((double)kontur_y[f]-Y_sred_)*((double)kontur_y[f]-Y_sred_);

                    Maver2+=d;
                    d = sqrt(d);
                    Maver_t+=d;
                    if(Mmin_t>d) Mmin_t=d;
                    if(Mmax_t<d) Mmax_t=d;
                }
                if(kontur_max>0)
                {
                    Maver_t/=kontur_max;
                    Maver2/=kontur_max;
                }
                else
                {
                    Maver_t = 0.0;
                    Maver2 = 0.0;
                    Mmin_t = 0.0;
                    Mmax_t = 0.0;
                }

                double Msigma = 0.0;
                for(int f = 0; f<kontur_max; f++)
                {
                    double d = ((double)kontur_x[f]-X_sred_)*((double)kontur_x[f]-X_sred_)
                            +((double)kontur_y[f]-Y_sred_)*((double)kontur_y[f]-Y_sred_);

                    Msigma += ((sqrt(d)-Maver_t)*(sqrt(d)-Maver_t));
                }
                if(kontur_max>0)
                {
                    Msigma/=kontur_max;
                }
                else
                {
                    Msigma = 0.0;
                }
                if(SigR != (unsigned int)-1) table[SigR] = Msigma;
                if(Mmin != (unsigned int)-1) table[Mmin] = Mmin_t*norma;
                if(Mmax != (unsigned int)-1) table[Mmax] = Mmax_t*norma;
                if(Maver != (unsigned int)-1) table[Maver] = Maver_t*norma;
                if(W10 != (unsigned int)-1) table[W10] = Mmin_t/Mmax_t;

                if(compute_martinr)
                {
                    //Obliczanie wielkosci potrzebnych do obliczen wspolczynnikow RB, RD i momentow
                    double Er_t = 0.0;
                    double Er2_t = 0.0;
                    double El_t = 0.0;
                    double El2_t = 0.0;
                    double M2x_t = 0.0;
                    double M2y_t = 0.0;
                    double M2xy_t = 0.0;

                    for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                        for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                        {
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&1)
                            {
                                double dmin = duzy_double;
                                for(int f = 0; f < kontur_max; f++)
                                {
                                    double d = 	(kontur_x[f]-x)*(kontur_x[f]-x)+
                                            (kontur_y[f]-y)*(kontur_y[f]-y);
                                    if(dmin > d) dmin = d;
                                }
                                El2_t+=dmin;
                                El_t+=sqrt(dmin);
                                dmin = ((double)x-X_sred_)*((double)x-X_sred_)
                                        +((double)y-Y_sred_)*((double)y-Y_sred_);
                                Er2_t+=dmin;
                                Er_t+=sqrt(dmin);
                                M2x_t += (((double)x-X_sred_)*((double)x-X_sred_));
                                M2y_t += (((double)y-Y_sred_)*((double)y-Y_sred_));
                                M2xy_t += (((double)x-X_sred_)*((double)y-Y_sred_));
                            }
                        }
                    Er_t /= area;
                    Er2_t /= area;
                    El_t /= area;
                    El2_t /= area;
                    M2x_t /= area;
                    M2y_t /= area;
                    M2xy_t /= area;

                    if(M2x != (unsigned int)-1) table[M2x] = M2x_t*norma*norma;
                    if(M2y != (unsigned int)-1) table[M2y] = M2y_t*norma*norma;
                    if(M2xy != (unsigned int)-1) table[M2xy] = M2xy_t*norma*norma;

                    if(Er != (unsigned int)-1) table[Er] = Er_t*norma;
                    if(Er2 != (unsigned int)-1) table[Er2] = Er2_t*norma*norma;
                    if(El != (unsigned int)-1) table[El] = El_t*norma;
                    if(El2 != (unsigned int)-1) table[El2] = El2_t*norma*norma;

                    if(Rb != (unsigned int)-1) table[Rb] = (Er2_t>0.0 ?area*norma/sqrt(2*M_PI*Er2_t) :0.0);
                    if(Rd != (unsigned int)-1) table[Rd] = (El_t>0.0 ?(double)area*area*area*norma/(El_t*El_t) :0.0);
                    if(Rh != (unsigned int)-1) table[Rh] = ((Maver2-(1.0/(double)kontur_max))>0.0 ?Maver_t/sqrt(Maver2-(1.0/(double)kontur_max)) :0.0);
                }
            }
            //Szukanie punktow konturu i obliczanie obwodu (obiekt spojny)
            if(compute_kontur_spojny || compute_najw_sred || compute_feret || compute_obwod_wypukly
                    || compute_okrag_opisany || compute_elipsa || compute_prostokat_opisany || compute_okrag_wpisany)
            {
                double diameter = 0.0;
                int kontur_profil_max = 0;
                double obwod_profil = 0.0;
                double r_opisany = duzy_double;

                for(int y = 1; y < obrazek_tymczasowy_y-1; y++)
                    for(int x = 1; x < obrazek_tymczasowy_x-1; x++)
                    {
                        if(obrazek_tymczasowy[obrazek_tymczasowy_x*y+x]&2)
                        {
                            if(((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x+1]&2)==0)||
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*y+x-1]&2)==0)||
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&2)==0)||
                                    ((obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&2)==0))
                            {
                                kontur_x[kontur_profil_max] = x;
                                kontur_y[kontur_profil_max] = y;

                                kontur_profil_max++;
                                int sa = 0;
                                int sb = 0;
                                int sc = 0;

                                bool sl = false;

                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&2) {sa++; sl=true;}
                                else {sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x+1]&2) {sc++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x]&2) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y+1)+x-1]&2) {sc++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x-1]&2) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x-1]&2) {sc++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x]&2) {sa++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x*(y-1)+x+1]&2) {sc++; sl=true;}
                                else {if(sl)sb++; sl=false;};
                                if(obrazek_tymczasowy[obrazek_tymczasowy_x* y+   x+1]&2) {sl=true;}
                                else {if(sl)sb++; sl=false;};

                                if(sb>1) sb=2;
                                obwod_profil+=((double)sb*(sc*p2+sa*1.0));
                            }
                        }
                    }
                obwod_profil/=2;
                if(Ul != (unsigned int)-1) table[Ul] = obwod_profil*norma;
                if(W2 != (unsigned int)-1) table[W2] = (obwod_profil>0 ?4*M_PI*area_profil/(obwod_profil*obwod_profil) :0.0);
                if(W6 != (unsigned int)-1) table[W6] = (area_profil>0 ?(obwod_profil/(4*M_PI*area_profil*norma)) :0.0);
                if(W3 != (unsigned int)-1) table[W3] = (area_profil>0 ?(obwod_profil*obwod_profil/area_profil) :0.0);
                if(Nl != (unsigned int)-1) table[Nl] = (double)kontur_profil_max;

                //Obliczanie najwiekszej srednicy
                if(compute_najw_sred)
                {

                    int wychylony_a = 0;
                    int wychylony_b = 0;

                    for(int f = 0; f<kontur_profil_max; f++)
                    {
                        for(int ff = f+1; ff<kontur_profil_max; ff++)
                        {
                            double d = (((double)kontur_x[ff]-(double)kontur_x[f]) * ((double)kontur_x[ff]-(double)kontur_x[f]))+
                                    (((double)kontur_y[ff]-(double)kontur_y[f]) * ((double)kontur_y[ff]-(double)kontur_y[f]));
                            if(diameter<d)
                            {
                                diameter = d;
                                wychylony_a = ff;
                                wychylony_b = f;
                            }
                        }
                    }
                    diameter = sqrt(diameter);
                    double Aox_t;
                    if(kontur_max<2) Aox_t = 0;
                    else
                        Aox_t = atan2((double)kontur_x[wychylony_a]-(double)kontur_x[wychylony_b],
                                      (double)kontur_y[wychylony_a]-(double)kontur_y[wychylony_b]);
                    Aox_t *= (180.0/M_PI);

                    if(Aox != (unsigned int)-1) table[Aox] = Aox;
                    if(Smax != (unsigned int)-1) table[Smax] = diameter*norma;
                    if(W12 != (unsigned int)-1) table[W12] = (diameter>0.0 ?4*area/(diameter*diameter) :0.0);
                    if(W13 != (unsigned int)-1) table[W13] = diameter/(area*norma);
                    if(W14 != (unsigned int)-1) table[W14] = (diameter>0.0 ?area/(diameter*diameter*diameter*norma) :0.0);
//                    if(W15 != (unsigned int)-1) table[W15] = (diameter*feret_mind>0.0 ?area/(M_PI_4*diameter*feret_mind) :0.0);
                }
                if(compute_feret || compute_obwod_wypukly || compute_okrag_opisany || compute_elipsa || compute_prostokat_opisany)
                {
                    int feret[liczba_katow];
                    double feret_proj[liczba_katow];
                    int feret_rzeczywisty[liczba_katow];
                    //Poszukiwanie wystajacych punktow konturu co (360/liczba_katow) stopni katowych
                    int rzeczywistych_wystajacych = 0;
                    for(int f = 0; f<liczba_katow; f++)
                    {
                        double cosa = cos(2*M_PI*(double)f/liczba_katow);
                        double sina = sin(2*M_PI*(double)f/liczba_katow);
                        int i = 0;
                        double d = -duzy_double;

                        for(int ff = 0; ff<kontur_profil_max; ff++)
                        {
                            double dd = (double)(kontur_x[ff])*cosa + (double)(kontur_y[ff])*sina;
                            if(dd>d) {d = dd; i = ff;};
                        }
                        feret[f] = i;
                        feret_proj[f] = d;
                        if(f>0)
                        {
                            if(feret[f]!=feret[f-1])
                            {
                                feret_rzeczywisty[rzeczywistych_wystajacych]=i;
                                rzeczywistych_wystajacych++;
                            }
                        }
                        else
                        {
                            feret_rzeczywisty[rzeczywistych_wystajacych]=i;
                            rzeczywistych_wystajacych++;
                        }
                    }
                    if(Nv != (unsigned int)-1) table[Nv] = (double)rzeczywistych_wystajacych;

                    //Obliczanie srednic Fereta
                    double feret_maxd;
                    double feret_mind;
                    double feret_h;
                    double feret_v;
                    feret_maxd = feret_mind = feret_h = feret_proj[0] + feret_proj[liczba_katow/2];
                    feret_v = feret_proj[liczba_katow/4] + feret_proj[liczba_katow/2+liczba_katow/4];

                    for(int f = 1; f<(liczba_katow/2); f++)
                    {
                        double fm;
                        fm = feret_proj[f] + feret_proj[liczba_katow/2+f];
                        if(fm > feret_maxd) feret_maxd = fm;
                        if(fm < feret_mind) feret_mind = fm;
                    }

                    if(Fmax != (unsigned int)-1) table[Fmax] = feret_maxd*norma;
                    if(Fmin != (unsigned int)-1) table[Fmin] = feret_mind*norma;
                    if(Fh != (unsigned int)-1) table[Fh] = feret_h*norma;
                    if(Fv != (unsigned int)-1) table[Fv] = feret_v*norma;
                    if(Rf != (unsigned int)-1) table[Rf] = feret_h/feret_v;

                    if(Rff != (unsigned int)-1) table[Rff] = feret_mind/feret_maxd; //sensowniejszy
                    if(W11 != (unsigned int)-1) table[W11] = (diameter-feret_mind)*norma;
                    if(W15 != (unsigned int)-1) table[W15] = (diameter*feret_mind>0.0 ?area/(M_PI_4*diameter*feret_mind) :0.0);


                    if(compute_obwod_wypukly)
                    {
                        //Obliczanie obwodu wypuklego obiektu
                        double obwod_wypukly = 0.0;
                        for(int f = 0; f<liczba_katow; f++)
                        {
                            int ff = (f+1)%liczba_katow;
                            double xx = (double)kontur_x[feret[f]]-(double)kontur_x[feret[ff]];
                            double yy = (double)kontur_y[feret[f]]-(double)kontur_y[feret[ff]];
                            obwod_wypukly+=sqrt(xx*xx+yy*yy);
                        }
                        if(Uw != (unsigned int)-1) table[Uw] = obwod_wypukly*norma;
                        if(W4 != (unsigned int)-1) table[W4] = obwod_profil/obwod_wypukly;

                    }

                    //Obliczanie wymiarow prostokata opisanego
                    if(compute_prostokat_opisany)
                    {
                        double pow_prost = duzy_double;
                        double szer_prost, wys_prost;
                        for(int f = 0; f<(liczba_katow/4); f++)
                        {
                            double fm, fn;
                            fm = feret_proj[f] + feret_proj[liczba_katow/2+f];
                            fn = feret_proj[liczba_katow/4+f] + feret_proj[liczba_katow/2+liczba_katow/4+f];
                            if(fn*fm < pow_prost)
                            {
                                pow_prost = fn*fm;
                                szer_prost = fn;
                                wys_prost = fm;
                            }
                        }
                        if(pow_prost>=duzy_double)
                        {
                            pow_prost = 0.0;
                            szer_prost = 0.0;
                            wys_prost = 0.0;
                        }
                        else if(szer_prost>wys_prost)
                        {
                            double p = szer_prost;
                            szer_prost = wys_prost;
                            wys_prost = p;
                        }

                        if(W8 != (unsigned int)-1) table[W8] = szer_prost/wys_prost;
                        if(W9 != (unsigned int)-1) table[W9] = szer_prost*wys_prost/area;
                        if(S != (unsigned int)-1) table[S] = szer_prost*norma;
                        if(L != (unsigned int)-1) table[L] = wys_prost*norma;
                        if(SxL != (unsigned int)-1) table[SxL] = pow_prost*norma*norma;
                    }

                    //Obliczanie promieni okregu opisanego na obiekcie
                    if(compute_okrag_opisany || compute_elipsa)
                    {
                        bool jeszcze = true;
                        int max_while_okregu = 10000;
                        double x_opisany = obrazek_tymczasowy_x/2;
                        double y_opisany = obrazek_tymczasowy_y/2;
                        while(jeszcze && max_while_okregu)
                        {
                            double xa;
                            double ya;
                            jeszcze = false;
                            max_while_okregu--;

                            for(int i = 0; i < 4; i++)
                            {
                                if(i&2){xa = x_opisany; if(i&1) ya=y_opisany+0.5; else ya=y_opisany-0.5;}
                                else{ya = y_opisany; if(i&1) xa=x_opisany+0.5; else xa=x_opisany-0.5;}
                                if(ya>=obrazek_tymczasowy_y) continue;
                                if(xa>=obrazek_tymczasowy_x) continue;
                                if(ya<=0.0) continue;
                                if(xa<=0.0) continue;
                                double rmax = 0.0;
                                double d;
                                for(int f = 0; f < rzeczywistych_wystajacych; f++)
                                {
                                    d =	(kontur_x[feret_rzeczywisty[f]]-xa)*(kontur_x[feret_rzeczywisty[f]]-xa)+
                                            (kontur_y[feret_rzeczywisty[f]]-ya)*(kontur_y[feret_rzeczywisty[f]]-ya);
                                    if(rmax < d) rmax = d;
                                }

                                if(r_opisany > rmax)
                                {
                                    r_opisany = rmax;
                                    x_opisany = xa;
                                    y_opisany = ya;
                                    jeszcze = true;
                                }
                            }
                        }
                        double pole_kola_opisanego = M_PI*r_opisany;
                        r_opisany = sqrt(r_opisany);

                        if(D2 != (unsigned int)-1) table[D2] = r_opisany*norma;
                        if(Fd2 != (unsigned int)-1) table[Fd2] = pole_kola_opisanego*norma*norma;


                        //Obliczanie osi elipsy opisanej na obiekcie
                        if(compute_elipsa)
                        {
                            jeszcze = true;
                            int max_while_elipsy = 10000;
                            double exa = x_opisany;
                            double exb = x_opisany;
                            double eya = y_opisany;
                            double eyb = y_opisany;
                            double powierzchnia_elipsy = duzy_double;
                            double duza_os_elipsy = 0.0;
                            double mala_os_elipsy = 0.0;

                            while(jeszcze && max_while_elipsy)
                            {
                                double xa;
                                double xb;
                                double ya;
                                double yb;

                                jeszcze = false;
                                max_while_elipsy--;
                                for(int i = 0; i < 16; i++)
                                {
                                    if(i&2){xa = exa; if(i&1) ya=eya+0.5; else ya=eya-0.5;}
                                    else{ya = eya; if(i&1) xa=exa+0.5; else xa=exa-0.5;}

                                    if(i&4){xb = exb; if(i&3) yb=eyb+0.5; else yb=eyb-0.5;}
                                    else{yb = eyb; if(i&3) xb=exb+0.5; else xb=exb-0.5;}

                                    if(yb>=obrazek_tymczasowy_y) continue;
                                    if(ya>=obrazek_tymczasowy_y) continue;
                                    if(xb>=obrazek_tymczasowy_x) continue;
                                    if(xa>=obrazek_tymczasowy_x) continue;

                                    if(yb<=0.0) continue;
                                    if(ya<=0.0) continue;
                                    if(xb<=0.0) continue;
                                    if(xa<=0.0) continue;

                                    double lasso = 0.0;//duzy_double;
                                    double d, p;

                                    for(int f = 0; f < rzeczywistych_wystajacych; f++)
                                    {
                                        d =sqrt(
                                                    (kontur_x[feret_rzeczywisty[f]]-xa)*(kontur_x[feret_rzeczywisty[f]]-xa)+
                                                (kontur_y[feret_rzeczywisty[f]]-ya)*(kontur_y[feret_rzeczywisty[f]]-ya));
                                        d += sqrt(
                                                    (kontur_x[feret_rzeczywisty[f]]-xb)*(kontur_x[feret_rzeczywisty[f]]-xb)+
                                                (kontur_y[feret_rzeczywisty[f]]-yb)*(kontur_y[feret_rzeczywisty[f]]-yb));
                                        if(lasso < d) lasso = d;
                                    }
                                    d = (xa-xb)*(xa-xb) + (ya-yb)*(ya-yb);
                                    p = sqrt(lasso*lasso - d)*lasso;

                                    if(powierzchnia_elipsy > p)
                                    {
                                        exa = xa;
                                        exb = xb;
                                        eya = ya;
                                        eyb = yb;
                                        powierzchnia_elipsy = p;
                                        jeszcze = true;
                                        duza_os_elipsy = lasso;
                                    }
                                }//for
                            }//while
                            mala_os_elipsy = sqrt(duza_os_elipsy*duza_os_elipsy - (exa-exb)*(exa-exb) - (eya-eyb)*(eya-eyb));
                            powierzchnia_elipsy *= (M_PI/4.0);

                            if(LmaxE != (unsigned int)-1) table[LmaxE] = duza_os_elipsy*norma;
                            if(LminE != (unsigned int)-1) table[LminE] = mala_os_elipsy*norma;
                            if(FE != (unsigned int)-1) table[FE] = powierzchnia_elipsy*norma*norma;
                            if(W1 != (unsigned int)-1) table[W1] = (mala_os_elipsy>0 ?(duza_os_elipsy/mala_os_elipsy) :0.0);
                        }
                    }
                }

                //Obliczanie promienia okregu wpisanego (w obiekt spojny)
                if(compute_okrag_wpisany)
                {
                    double x_wpisany = 0.0;
                    double y_wpisany = 0.0;
                    double r_wpisany = 0.0;
                    for(int yy = 1; yy < 2*obrazek_tymczasowy_y-1; yy++)
                        for(int xx = 1; xx < 2*obrazek_tymczasowy_x-1; xx++)
                        {
                            double x = (double)xx/2.0;
                            double y = (double)yy/2.0;
                            if(obrazek_tymczasowy[obrazek_tymczasowy_x*(int)y+(int)x]&2)
                            {
                                double dmin = duzy_double;
                                for(int f = 0; f < kontur_profil_max; f++)
                                {
                                    double d = 	(kontur_x[f]-x)*(kontur_x[f]-x)+
                                            (kontur_y[f]-y)*(kontur_y[f]-y);
                                    if(dmin > d) dmin = d;
                                }
                                if(r_wpisany < dmin)
                                {
                                    r_wpisany = dmin;
                                    y_wpisany = y;
                                    x_wpisany = x;
                                }
                            }
                        }
                    r_wpisany = sqrt(r_wpisany);
                    if(W7 != (unsigned int)-1) table[W7] = r_opisany/r_wpisany;
                    if(D1 != (unsigned int)-1) table[D1] = r_wpisany*norma;
                    if(Xo != (unsigned int)-1) table[Xo] = (x_wpisany-X_sred_)*norma;
                    if(Yo != (unsigned int)-1) table[Yo] = (y_wpisany-Y_sred_)*norma;
                    if(XYo != (unsigned int)-1) table[XYo] = sqrt((x_wpisany-X_sred_)*(x_wpisany-X_sred_)+(y_wpisany-Y_sred_)*(y_wpisany-Y_sred_))*norma;
                }

            }
            if(kontur_x!=NULL) delete[]kontur_x;
            kontur_x = NULL;
            if(kontur_y!=NULL) delete[]kontur_y;
            kontur_y = NULL;
        }
        if(obrazek_tymczasowy!=NULL) delete[]obrazek_tymczasowy;
        obrazek_tymczasowy = NULL;
    }
}
