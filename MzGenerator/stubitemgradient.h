/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STUBITEMGRADIENT_H
#define STUBITEMGRADIENT_H

#include "genentry.h"
#include <math.h>

class StubItemGradient : public StubItem
{
public:
    ~StubItemGradient();
    static StubItem* New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int index, NotifyType *notify);
    void Connect(StubItem* item);
    void Execute(StubItemData* data);
    bool IsNotifier(){return true;}
private:
    template <unsigned int VDimensions>
    void inRoi(StubItemDataEntryRoi* datas)
    {
        if(bits < 1) return;
        if(bits > sizeof(MazdaImagePixelType)*8) return;
        unsigned int shift = sizeof(MazdaImagePixelType)*8 - bits;
        double* table = datas->table;

//        typedef MazdaImage<double, VDimensions> MIDouble;
        typedef MazdaImage<MazdaImagePixelType, VDimensions> MITypeLocal;
        typedef MazdaRoi <MazdaRoiBlockType, VDimensions> MzRoiLocal;
        unsigned int size[VDimensions];
        double spacing[VDimensions];
        int begin[VDimensions];
        int end[VDimensions];
        MITypeLocal* image = static_cast<MITypeLocal*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MzRoiLocal* roi = static_cast<MzRoiLocal*> (datas->roi);
        if(roi->IsEmpty()) return;
        roi->GetBegin(begin);
        roi->GetEnd(end);
        bool quit = false;
        for(unsigned int d = 0; d < VDimensions; d++)
        {
            if(begin[d] >= (int)size[d]) {quit = true; break;}
            if(end[d] < 0) {quit = true; break;}
            if(begin[d] < 0) begin[d] = 0;
            if(end[d] >= (int)size[d]) end[d] = size[d]-1;
            if(end[d] - begin[d] < 2) {quit = true; break;}
        }
        if(quit) return;

        unsigned long int nz = 0;
        double i1 = 0.0;
        double i2 = 0.0;
        double i3 = 0.0;
        double i4 = 0.0;
        unsigned long int area = 0;

        if(VDimensions >= 3)
        {
            begin[2]++; end[2]--;
            begin[1]++; end[1]--;
            begin[0]++; end[0]--;
            MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);

            begin[0]++; end[0]++;
            MazdaImageRegionIterator<MITypeLocal> cli = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rli = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[0] -= 2; end[0] -= 2;
            MazdaImageRegionIterator<MITypeLocal> cri = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[0]++; end[0]++;

            begin[1]++; end[1]++;
            MazdaImageRegionIterator<MITypeLocal> cti = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rti = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[1] -= 2; end[1] -= 2;
            MazdaImageRegionIterator<MITypeLocal> cbi = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rbi = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[1]++; end[1]++;

            begin[2]++; end[2]++;
            MazdaImageRegionIterator<MITypeLocal> cfi = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rfi = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[2] -= 2; end[2] -= 2;
            MazdaImageRegionIterator<MITypeLocal> cci = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rci = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[2]++; end[2]++;

            while(!ri.IsBehind())
            {
                if(ri.GetPixel() && rli.GetPixel() && rri.GetPixel() && rti.GetPixel() && rbi.GetPixel() && rci.GetPixel() && rfi.GetPixel())
                {
                    int xx = ((int)cli.GetPixel()>>shift) - ((int)cri.GetPixel()>>shift);
                    int yy = ((int)cbi.GetPixel()>>shift) - ((int)cti.GetPixel()>>shift);
                    int zz = ((int)cci.GetPixel()>>shift) - ((int)cfi.GetPixel()>>shift);
                    double p = sqrt(xx*xx+yy*yy+zz*zz);
                    double pp = p*p;
                    i1 += p;
                    i2 += pp;
                    i3 += pp*p;
                    i4 += pp*pp;
                    if(p > 0.5) nz++;
                    area++;
                }
                ++ri; ++rli; ++rri; ++rti; ++rbi; ++rci; ++rfi;
                ++cli; ++cri; ++cti; ++cbi; ++cci; ++cfi;
            }
        }
        else
        {
            begin[1]++; end[1]--;
            begin[0]++; end[0]--;
            MazdaRoiRegionIterator<MzRoiLocal> ri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);

            begin[0]++; end[0]++;
            MazdaImageRegionIterator<MITypeLocal> cli = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rli = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[0] -= 2; end[0] -= 2;
            MazdaImageRegionIterator<MITypeLocal> cri = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rri = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[0]++; end[0]++;

            begin[1]++; end[1]++;
            MazdaImageRegionIterator<MITypeLocal> cti = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rti = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[1] -= 2; end[1] -= 2;
            MazdaImageRegionIterator<MITypeLocal> cbi = MazdaImageRegionIterator<MITypeLocal>(image, (unsigned int*) begin, (unsigned int*) end);
            MazdaRoiRegionIterator<MzRoiLocal> rbi = MazdaRoiRegionIterator<MzRoiLocal>(roi, begin, end, true);
            begin[1]++; end[1]++;

            while(!ri.IsBehind())
            {
                if(ri.GetPixel() && rli.GetPixel() && rri.GetPixel() && rti.GetPixel() && rbi.GetPixel())
                {
                    int xx = ((int)cli.GetPixel()>>shift) - ((int)cri.GetPixel()>>shift);
                    int yy = ((int)cbi.GetPixel()>>shift) - ((int)cti.GetPixel()>>shift);
                    double p = sqrt(xx*xx+yy*yy);
                    double pp = p*p;
                    i1 += p;
                    i2 += pp;
                    i3 += pp*p;
                    i4 += pp*pp;
                    if(p > 0.5) nz++;
                    area++;
                }
                ++ri; ++rli; ++rri; ++rti; ++rbi;
                ++cli; ++cri; ++cti; ++cbi;
            }
        }

        if(Area != (unsigned int)-1) table[Area] = area;
        if(area > 0)
        {
            if(NonZeros != (unsigned int)-1) table[NonZeros] = (double)nz/area;
            i1 /= area;
            i2 /= area;
            i3 /= area;
            i4 /= area;
            double var = i2 - (i1*i1);
            if(Mean != (unsigned int)-1) table[Mean] = i1;
            if(Variance != (unsigned int)-1) table[Variance] = var;
            if(var > 0)
            {
                if(StdDev != (unsigned int)-1) table[StdDev] = sqrt(var);
                if(Skewness != (unsigned int)-1)
                {
                    double mi3 = - 2.0*i1*i1*i1 + 3.0*i1*i2 - i3;
                    table[Skewness] = mi3/(sqrt(var)*var);
                }
                if(Kurtosis != (unsigned int)-1)
                {
                    double mi4 = -3.0*i1*i1*i1*i1 + 6.0*i1*i1*i2 - 4.0*i1*i3 + i4;
                    table[Kurtosis] = (mi4/(var*var)) - 3;
                }
            }
            else
            {
                if(StdDev != (unsigned int)-1) table[StdDev] = 0.0;
                if(Skewness != (unsigned int)-1) table[Skewness] = 0.0;
                if(Kurtosis != (unsigned int)-1) table[Kurtosis] = 0.0;
            }
        }
    }
    unsigned int bits;
    unsigned int Area;
    unsigned int Mean;
    unsigned int Variance;
    unsigned int StdDev;
    unsigned int Skewness;
    unsigned int Kurtosis;
    unsigned int NonZeros;
};

#endif // STUBITEMGRADIENT_H
