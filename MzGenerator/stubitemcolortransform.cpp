/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "genentry.h"
#include "stubitemcolortransform.h"
#include "../SharedImage/mazdaimageio.h"
//StubItemColorTransform::StubItemColorTransform()
//{
//    imageid = 0;
//    mono = NULL;
//}
StubItemColorTransform::~StubItemColorTransform()
{
//    if(mono != NULL) delete mono;
}

StubItem* StubItemColorTransform::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemColorTransform* item = new StubItemColorTransform;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();
    item->channel = nameStubs->back().at(0);
    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemColorTransform::Execute(StubItemData* data)
{
    StubItemDataEntry* datas = static_cast<StubItemDataEntryRoi*> (data);
    StubItemDataEntryRoi* datasr = dynamic_cast<StubItemDataEntryRoi*> (data);
    StubItemDataEntryLocal* datasl = dynamic_cast<StubItemDataEntryLocal*> (data);

#ifdef USE_DEBUG_LOG
    if(debugfile.is_open())
    {
        debugfile << "########  ColorTransform::Execute()  ########"<< std::endl;
        debugfile << "Channel: "<< channel << " RGB: " << Rch << Gch << Bch << " Pixel: " << sizeof(PixelRGBType) << std::endl;
    }
#endif
//    {
//        unsigned int size[MIType2DRGB::Dimensions];
//        int x[MIType2DRGB::Dimensions];
//        double min = 0;
//        double max = 0;
//        static_cast<MIType2DRGB*>(datas->image)->GetSize(size);
//        MazdaImageIterator<MIType2DRGB> iterator(static_cast<MIType2DRGB*>(datas->image));
//        for(x[1] = 0; x[1] < size[1]; x[1]++)
//        {
//            for(x[0] = 0; x[0] < size[0]; x[0]++)
//            {
//                PixelRGBType* pels = iterator.GetPixelPointer();
//                MazdaMapPixelType pel = pels->channel[1];
//                if(min > pel)min = pel;
//                if(max < pel)max = pel;
//                ++iterator;
//            }
//        }
//        iterator.GoToBegin();
//        for(x[1] = 0; x[1] < size[1]; x[1]++)
//        {
//            for(x[0] = 0; x[0] < size[0]; x[0]++)
//            {
//                PixelRGBType* pels = iterator.GetPixelPointer();
//                MazdaMapPixelType pel = pels->channel[1];
//                pel -= min;
//                pel /= (max-min);
//                if(pel < 0.2) std::cout << "M";
//                else if(pel < 0.4) std::cout << "X";
//                else if(pel < 0.6) std::cout << "/";
//                else if(pel < 0.8) std::cout << ".";
//                else std::cout << " ";
//                ++iterator;
//            }
//            printf("\n");
//        }
//    }


    if(!datas->isRGB)
    {
        if(channel == 'Y')
        {
            for(unsigned int i = 0; i < Connected(); i++) Connected(i)->Execute(data);
        }
        //if(notify!=NULL) (*notify)();
        return;
    }
    unsigned int size[3];
    double spacing[3];
    MazdaDummy* mono = NULL;

    switch(dimensions)
    {
    case 3:
    {
        MIType3DRGB* image = static_cast<MIType3DRGB*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MIType3D* monot = new MIType3D(size, spacing, false);
        mono = monot;
        MzImageColorConvert<char, MIType3DRGB, MIType3D>(channel, image, monot);
    } break;
    case 2:
    {
        MIType2DRGB* image = static_cast<MIType2DRGB*> (datas->image);
        image->GetSize(size);
        image->GetSpacing(spacing);
        MIType2D* monot = new MIType2D(size, spacing, false);
        mono = monot;
        MzImageColorConvert<char, MIType2DRGB, MIType2D> (channel, image, monot);

//Test
//        static int i = 0;
//        if(channel == 'Y')
//        {
//            char n[256];
//            sprintf(n, "Ychannel%i.tif", i);

//            MazdaImageIO< MIType2D, MazdaImagePixelType >::Write(n, monot);
//            i++;
//        }

    } break;
    default: break;
    }
    if(mono == NULL) return;

    if(datasl != NULL)
    {
        StubItemDataEntryLocal newdata = *datasl;
        newdata.image = mono;
        newdata.isRGB = false;
        for(unsigned int i = 0; i < Connected(); i++) Connected(i)->Execute(&newdata);
    }
    else if(datasr != NULL)
    {
        StubItemDataEntryRoi newdata = *datasr;
        newdata.image = mono;
        newdata.isRGB = false;
        for(unsigned int i = 0; i < Connected(); i++) Connected(i)->Execute(&newdata);
    }
    if(mono != NULL) delete mono;
    return;
}



