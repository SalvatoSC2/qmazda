/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stubitemlbp.h"

StubItemLbp::~StubItemLbp()
{
}
StubItem* StubItemLbp::New(unsigned int dimensions, StubItem* parent, std::vector<std::string>* nameStubs, unsigned int /*index*/, NotifyType* notify)
{
    StubItemLbp* item = new StubItemLbp;

    for(unsigned int n = 0; n < 256; n++)
        item->Count[n] = (unsigned int) -1;
    item->dimensions = dimensions;
    item->notify = notify;
    item->name = nameStubs->back();

    if(item->name == "Oc4n") item->algorithm = Oc4n;
    else if(item->name == "Oc8n") item->algorithm = Oc8n;
    else if(item->name == "Tr4n") item->algorithm = Tr4n;
    else if(item->name == "Tr8n") item->algorithm = Tr8n;
    else if(item->name == "Cs4n") item->algorithm = Cs4n;
    else if(item->name == "Cs8n") item->algorithm = Cs8n;
    else if(item->name == "Cs12n") item->algorithm = Cs12n;

    if(parent != NULL) parent->Connect(item);
    return item;
}
void StubItemLbp::Connect(StubItem* item)
{
    StubItemFinal* itemf = static_cast <StubItemFinal*> (item);

    int index = atoi( itemf->Name().c_str());
    if(index >= 0 && index < 256)
        Count[index] = itemf->Index();

    children.push_back(item);
}

void StubItemLbp::Execute(StubItemData* data)
{

    StubItemDataEntryRoi* datas = static_cast<StubItemDataEntryRoi*> (data);

    if(datas->roi != NULL)
    {
        try
        {
            switch(algorithm)
            {
            case Oc4n:
                switch(dimensions)
                {
                case 3: inRoi<3> (datas, 4, dx_4n, dy_4n, Oc); break;
                case 2: inRoi<2> (datas, 4, dx_4n, dy_4n, Oc); break;
                default: break;
                }
                break;
            case Oc8n:
                switch(dimensions)
                {
                case 3: inRoi<3> (datas, 8, dx_8n, dy_8n, Oc); break;
                case 2: inRoi<2> (datas, 8, dx_8n, dy_8n, Oc); break;
                default: break;
                }
            break;
            case Tr4n:
                switch(dimensions)
                {
                case 3: inRoi<3> (datas, 4, dx_4n, dy_4n, Tr); break;
                case 2: inRoi<2> (datas, 4, dx_4n, dy_4n, Tr); break;
                default: break;
                }
            break;
            case Tr8n:
                switch(dimensions)
                {
                case 3: inRoi<3> (datas, 8, dx_8n, dy_8n, Tr); break;
                case 2: inRoi<2> (datas, 8, dx_8n, dy_8n, Tr); break;
                default: break;
                }
            break;
            case Cs4n:
                switch(dimensions)
                {
                case 3: inRoi<3> (datas, 4, dx_4n, dy_4n, Cs); break;
                case 2: inRoi<2> (datas, 4, dx_4n, dy_4n, Cs); break;
                default: break;
                }
            break;
            case Cs8n:
                switch(dimensions)
                {
                case 3: inRoi<3> (datas, 8, dx_8n, dy_8n, Cs); break;
                case 2: inRoi<2> (datas, 8, dx_8n, dy_8n, Cs); break;
                default: break;
                }
            break;
            case Cs12n:
                switch(dimensions)
                {
                case 3: inRoi<3> (datas, 12, dx_12n, dy_12n, Cs); break;
                case 2: inRoi<2> (datas, 12, dx_12n, dy_12n, Cs); break;
                default: break;
                }
            break;
            }
        }
        catch(...){}
    }
    if(notify!=NULL) (*notify)();

    return;
}


