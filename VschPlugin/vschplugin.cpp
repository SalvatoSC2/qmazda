/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//#include <QtGlobal>
//#include <QtPlugin>
//#include <QFileDialog>
#include "../MzShared/mzdefines.h"

#include <string.h>
#include "vschplugin.h"
#include "../MzShared/multidimselection.h"

void* MzNewPluginObject(void)
{
    return new VschPlugin();
}

void MzDeletePluginObject(void* object)
{
    delete static_cast<VschPlugin *> (object);
}

void VschPlugin::NotifyProgressStep(void)
{
    (*step_notifier)(notifier_object);
}
void VschPlugin::NotifyProgressText(std::string text)
{
    (*text_notifier)(notifier_object, text);
}
void VschPlugin::NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected)
{
    time_t currenttime = time(NULL);
    char* ct = ctime(&currenttime);
    strtok(ct, "\n\r");
    char text[256];

    switch(notification)
    {
    case BEGINS: sprintf(text, "Begins %iD at %s\n", dimensionality, ct); (*text_notifier)(notifier_object, text); break;
    case STOPPED: sprintf(text, "Stopped %.2f%%\n", (float)(value*100.0)); (*text_notifier)(notifier_object, text); break;
    case CANCELED: sprintf(text, "Canceled\n"); (*text_notifier)(notifier_object, text); break;
    case COMPLETED: sprintf(text, "Completed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case FAILED: sprintf(text,"Failed %iD\n", dimensionality); (*text_notifier)(notifier_object, text); break;
    case SUCCESS:
        sprintf(text,"D = %i Q = %f\n", dimensionality, (float)value); (*text_notifier)(notifier_object, text);
        if(data->featurenames != NULL && selected != NULL)
        {
            for(int d = 0; d < dimensionality; d++)
            {
                sprintf(text," %s %i\n", data->featurenames[selected[d]].c_str(), selected[d]);
                (*text_notifier)(notifier_object, text);
            }
        }
        break;
    }
}

void VschPlugin::cancelAnalysis(void)
{
    breakanalysis = true;
}


VschPlugin::VschPlugin()
{
    data = NULL;
    mainclassifier = NULL;
    classifieruse = NULL;
    Qtable = NULL;
    Qsorted = NULL;
    classtodistinguish = -1;
    dimensions = 3;
    maxtime = 60;
    normalizeoption = STANDARDIZE;
    modeUpscaleOrExpand = 'U';
    minimumExpandOrUpscale = 0.001;
    pull_data = NULL;
    gui_tools = NULL;
}

VschPlugin::~VschPlugin()
{
    if(data != NULL) delete data;
    if(mainclassifier != NULL) delete mainclassifier;
}

const char *VschPlugin::getName(void)
{
    return "Convex hull discrimination";
}

bool VschPlugin::startThreadIn(void)
{
    if(data != NULL) delete data;
    data = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;

    data = new DataForSelection();
    pull_data->getData(data);

    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Features missing", 3);
        return false;
    }

    int cs, ce;
    if(classtodistinguish < data->classnumber && classtodistinguish >= 0)
    {
        cs = classtodistinguish;
        ce = cs + 1;
    }
    else
    {
        cs = 0;
        ce = data->classnumber;
    }
    bool isbad = false;
    for(int c = cs; c < ce; c++)
    {
        if(c > 0)
        {
            if(data->classendvectorindex[c] - data->classendvectorindex[c-1] < dimensions + 1)
                isbad = true;
        }
        else if(data->classendvectorindex[c] < dimensions + 1)
            isbad = true;
    }
    if(isbad)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Not enough vectors", 3);
        return false;
    }
    return true;
}
bool VschPlugin::startThreadIn(std::vector<std::string>* featureNames)
{
    if(data != NULL) delete data;
    data = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;

    data = new DataForSelection();
    pull_data->getData(featureNames, data);
    if(data->featurenumber < 1)
    {
        delete data;
        data = NULL;
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    return true;
}

void VschPlugin::on_menuAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>qmazda VschPlugin</h2> " << std::endl;
    ss << QMAZDA_VERSION << "<br>" << std::endl;
    ss << QMAZDA_COPYRIGHT << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." <<  std::endl;
    ss << "Home: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html </a> <br>" << std::endl;
    ss << "Sources: <a href=\"https://gitlab.com/qmazda/qmazda\"> https://gitlab.com/qmazda/qmazda </a> <br>" << std::endl;
    ss << "<br>Built with:<br>" << std::endl;
    ss << "- qhull" << " <a href=\"http://www.qhull.org/\">http://www.qhull.org</a> <br>" << std::endl;
    gui_tools->showAbout("About LdaPlugin", ss.str().c_str());
}

bool VschPlugin::SetMachineLearningOptions(void)
{
    std::stringstream ss;
    ss << "t;";
    switch(normalizeoption)
    {
    case NORMNONE: ss << "none"; break;
    case NORMALIZE: ss << "minmax"; break;
    default: ss << "standardize"; break;
    }
    ss << ";none;standardize;minmax";
    gui_tools->addPropertyOptionsDialog("Normalization", ss.str(), "Feature values normalization");
    ss.str(std::string());
    ss << "i;" << maxtime << ";1;2000000000";
    gui_tools->addPropertyOptionsDialog("Time", ss.str(), "Maximum time for selection in seconds");
    ss.str(std::string());
    ss << "i;" << dimensions << ";1;32";
    gui_tools->addPropertyOptionsDialog("Dimensionality", ss.str(), "Maximum number of features selected");
    ss.str(std::string());
    ss << "t;*;*";

    if(data != NULL)
    {
        for(int c = 0; c < data->classnumber; c++)
            ss<< ";" << data->classnames[c];
    }
    gui_tools->addPropertyOptionsDialog("Class", ss.str(), "Select class to discriminate");
    ss.str(std::string());
    ss << "t;";
    switch(modeUpscaleOrExpand)
    {
    case 'E':
    case 'e':ss << "expanding"; break;
    default: ss << "upscaling"; break;
    }
    ss << ";expanding;upscaling";
    gui_tools->addPropertyOptionsDialog("Margin mode", ss.str(), "Margin estimation mode");
    ss.str(std::string());
    ss << "f;" << minimumExpandOrUpscale;
    gui_tools->addPropertyOptionsDialog("Margin size", ss.str(), "Margin size arround convex hull");
    ss.str(std::string());

    if(gui_tools->openOptionsDialog("Linear classifier training"))
    {
        std::string ns;
        char* end;
        gui_tools->getValueOptionsDialog("Time", &ns);
        maxtime = strtol(ns.c_str(), &end, 10);
        gui_tools->getValueOptionsDialog("Dimensionality", &ns);
        dimensions = strtol(ns.c_str(), &end, 10);
        gui_tools->getValueOptionsDialog("Normalization", &ns);
        if(ns == "none") normalizeoption = NORMNONE;
        else if(ns == "minmax") normalizeoption = NORMALIZE;
        else normalizeoption = STANDARDIZE;
        classtodistinguish = -1;
        gui_tools->getValueOptionsDialog("Class", &ns);
        if(data != NULL && ns != "*")
        {
            for(int c = 0; c < data->classnumber; c++)
            {
                if(data->classnames[c] == ns)
                {
                    classtodistinguish = c;
                    break;
                }
            }
        }
        gui_tools->getValueOptionsDialog("Margin mode", &ns);
        if(ns == "expanding") modeUpscaleOrExpand = 'E';
        else modeUpscaleOrExpand = 'U';
        gui_tools->getValueOptionsDialog("Margin size", &ns);
        minimumExpandOrUpscale = atof(ns.c_str());
        gui_tools->closeOptionsDialog();
        return true;
    }
    gui_tools->closeOptionsDialog();
    return false;
}

bool VschPlugin::SelectClassifiersOptions(void)
{
    std::vector<std::string> classifierNames;
    std::vector<bool> selectionResult;
    if(mainclassifier != NULL)
    {
        unsigned int imax = mainclassifier->classifiers.size();
        if(classifieruse == NULL)
        {
            classifieruse = new bool[imax];
            for(unsigned int i = 0; i < imax; i++) classifieruse[i] = true;
        }
        for(unsigned int i = 0; i < imax; i++)
        {
            classifierNames.push_back(mainclassifier->classifiers[i].getName());
            selectionResult.push_back(classifieruse[i]);
        }
        if(gui_tools->selectClassifiers(&classifierNames, &selectionResult, "Select classifiers"))
        {
            if(imax > selectionResult.size()) imax = selectionResult.size();
            for(unsigned int i = 0; i < imax; i++)
            {
                classifieruse[i] = selectionResult[i];
            }
            return true;
        }
    }
    return false;
}


bool VschPlugin::newTempClassifier(void)
{
    releaseTempClassifier();
    classifier = new Classifiers(VschSelectionReductionClassifierName);
    return true;
}

bool VschPlugin::setTempClassifier(void)
{
    releaseTempClassifier();
    if(mainclassifier == NULL) return false;
    if(classifieruse == NULL) *classifier = *mainclassifier;
    else
    {
        classifier = new Classifiers(VschSelectionReductionClassifierName);
        bool none = true;
        int size = mainclassifier->classifiers.size();

        for(int i = 0; i < size; i++)
        {
            if(classifieruse[i])
            {
                classifier->classifiers.push_back(mainclassifier->classifiers[i]);
                none = false;
            }
        }
        if(none)
        {
            releaseTempClassifier();
            return false;
        }
    }
    return true;
}

bool VschPlugin::releaseTempClassifier(void)
{
    if(classifier != NULL) delete classifier;
    classifier = NULL;
    return true;
}

bool VschPlugin::setClassifierFromTemp(void)
{
    if(mainclassifier != NULL) delete mainclassifier;
    mainclassifier = classifier;
    classifier = NULL;

    if(classifier != NULL)
    {
        int size = mainclassifier->classifiers.size();
        if(classifieruse != NULL) delete[] classifieruse;
        classifieruse = new bool[size];
        for(int i = 0; i < size; i++) classifieruse[i] = true;
    }
    return true;
}

void VschPlugin::stopThreadIn(void)
{
    gui_tools->closeProgressDialog();
}

void VschPlugin::stopThreadOut(void)
{
    if(data != NULL) delete data;
    data = NULL;
    if(Qsorted != NULL) delete[] Qsorted;
    Qsorted = NULL;
    if(Qtable != NULL) delete[] Qtable;
    Qtable = NULL;
    if(mainclassifier == NULL)
    {
        gui_tools->menuEnable(plugin_save, false);
        gui_tools->menuEnable(plugin_test, false);
    }
    else
    {
        gui_tools->menuEnable(plugin_save, true);
        gui_tools->menuEnable(plugin_test, true);
    }
}









bool VschPlugin::before_segmentation_this(void)
{
    if(! SelectClassifiersOptions()) return false; //User canceled
    if(! setTempClassifier()) return false;
    dataMap.result = NULL;
    dataMap.values = NULL;
    dataMap.featurenames.clear();
    dataMap.resultnames.clear();
    unsigned int classifiers_size = classifier->classifiers.size();
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int features_number = (classifier->classifiers)[c].featurenames.size();
        for(unsigned int k = 0; k < features_number; k++)
        {
            dataMap.featurenames.push_back((classifier->classifiers)[c].featurenames[k]);
        }
    }
    dataMap.resultnames.push_back("ConvexHull");
    if(! pull_data->getData(&dataMap))
    {
        gui_tools->showMessage("Error", "Incomplete data or invalid parameters", 3);
        return false;
    }
    if(classifier == NULL)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }
    gui_tools->openProgressDialog(0, dataMap.vectornumber, this, "Convex hull segmentation");
    return true;
}
void VschPlugin::thread_segmentation_this()
{
    breakanalysis = false;
    success = computeSegmentation();
}
void VschPlugin::after_segmentation_this(void)
{
    stopThreadIn();
    stopThreadOut();
}
bool VschPlugin::before_segmentation(void* object)
{
    return ((VschPlugin*)object)->before_segmentation_this();
}
void VschPlugin::thread_segmentation(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((VschPlugin*)object)->notifier_object = notifier_object;
    ((VschPlugin*)object)->step_notifier = step_notifier;
    ((VschPlugin*)object)->text_notifier = text_notifier;
    ((VschPlugin*)object)->thread_segmentation_this();
}
void VschPlugin::after_segmentation(void* object)
{
    ((VschPlugin*)object)->after_segmentation_this();
}
void VschPlugin::on_menuSegment_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_segmentation, &thread_segmentation, &after_segmentation);
}


bool VschPlugin::before_selection_this(void)
{
    if(! startThreadIn()) return false;
    if(! SetMachineLearningOptions()) return false; //User canceled
    newTempClassifier();
    if(classifier == NULL)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }
    unsigned int sizeQsorted = data->featurenumber;
    if(sizeQsorted > 0)
    {
        if(Qtable != NULL) delete[] Qtable;
        if(Qsorted != NULL) delete[] Qsorted;
        Qtable = new double[sizeQsorted];
        Qsorted = new unsigned int[sizeQsorted];
    }
    mz_uint64 maxSteps = 1;
    mz_uint64 maxstepsold = maxSteps;
    if((int)sizeQsorted < dimensions) dimensions = sizeQsorted;
    for(int d = 0; d < dimensions; d++)
    {
        for(int k = 0; k <= d; k++)
        {
            maxSteps *= (data->featurenumber-k);
            if(maxSteps / (data->featurenumber-k) != maxstepsold)
            {
                maxSteps = 0;
                k = d = dimensions;
            }
            maxstepsold = maxSteps;
        }
    }
    gui_tools->openProgressDialog(maxtime+data->classnumber, maxSteps, this, "Convex hull classifier learning");
    return true;
}
void VschPlugin::thread_selection_this()
{
    breakanalysis = false;
    if(normalizeoption == STANDARDIZE) Standardize();
    else if(normalizeoption == NORMALIZE) NormalizeMinMax();
    success = Select();
}
void VschPlugin::after_selection_this(void)
{
    stopThreadIn();
    if(success)
    {
        setClassifierFromTemp();
        std::vector<std::string> classifierNames;
        std::vector<bool> selectionResult;
        unsigned int features = data->featurenumber;
        if(Qtable != NULL && Qsorted!= NULL)
        {
            for(unsigned int c = 0; c < features; c++)
            {
                if(Qsorted[c] < features)
                {
                    std::stringstream ss;
                    ss << data->featurenames[Qsorted[c]] << " (" << Qtable[Qsorted[c]] << ")";
                    classifierNames.push_back(ss.str());
                    selectionResult.push_back(true);
                }
                else break;
            }
        }
        else
        {
            for(unsigned int c = 0; c < features; c++)
            {
                std::stringstream ss;
                ss << data->featurenames[c] << " (" << Qtable[Qsorted[c]] << ")";
                classifierNames.push_back(ss.str());
                selectionResult.push_back(true);
            }
        }

        if(gui_tools->selectClassifiers(&classifierNames, &selectionResult, "Select features"))
        {
            int count = 0;
            for(unsigned int c = 0; c < selectionResult.size(); c++)
            {
                if(selectionResult[c])  count ++;
            }
            if(count > 0)
            {
                SelectedFeatures ret;
                ret.featurenumber = count;
                ret.featureoriginalindex = new int[count];
                count = 0;
                int cc = 0;
                for(int c = 0; c < data->featurenumber; c++)
                {
                    if(Qsorted[c] < features)
                    {
                        if(selectionResult[cc])
                        {
                            ret.featureoriginalindex[count] = data->featureoriginalindex[Qsorted[c]];
                            count++;
                        }
                        cc++;
                    }
                }
                pull_data->setSelection(&ret);
            }
        }
    }
    else
    {
        releaseTempClassifier();
        gui_tools->showMessage("Error", "Selection failed", 3);
    }
    stopThreadOut();
}
bool VschPlugin::before_selection(void* object)
{
    return ((VschPlugin*)object)->before_selection_this();
}
void VschPlugin::thread_selection(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((VschPlugin*)object)->notifier_object = notifier_object;
    ((VschPlugin*)object)->step_notifier = step_notifier;
    ((VschPlugin*)object)->text_notifier = text_notifier;
    ((VschPlugin*)object)->thread_selection_this();
}
void VschPlugin::after_selection(void* object)
{
    ((VschPlugin*)object)->after_selection_this();
}
void VschPlugin::on_menuSelect_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_selection, &thread_selection, &after_selection);
}


bool VschPlugin::before_test_this(void)
{
    if(!SelectClassifiersOptions()) return false; //User canceled
    if(!setTempClassifier()) return false;
    if(classifier == NULL)
    {
        gui_tools->showMessage("Error", "Invalid classifier", 3);
        return false;
    }
    std::vector<std::string> featureNames;
    unsigned int classifiers_size = classifier->classifiers.size();
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int features_number = (classifier->classifiers)[c].featurenames.size();
        for(unsigned int k = 0; k < features_number; k++)
        {
            featureNames.push_back((classifier->classifiers)[c].featurenames[k]);
        }
    }
    if(!startThreadIn(&featureNames)) return false;
    featureNames.clear();

    mz_uint64 maxSteps = data->vectornumber;
    unsigned int sizeQsorted = (classifier->classifiers.size() + 1) * data->classnumber;
    if(sizeQsorted > 0)
    {
        if(Qsorted != NULL) delete[] Qsorted;
        Qsorted = new unsigned int[sizeQsorted];
    }
//    confusionTableText = &confusionMatrixText;
    gui_tools->openProgressDialog(0, maxSteps, this, "Convex hull classifier test");
    return true;
}
void VschPlugin::thread_test_this()
{
    breakanalysis = false;
    success = Test();
}
void VschPlugin::after_test_this(void)
{
    stopThreadIn();
    if(success)
    {
        gui_tools->showTestResults(&confusionMatrixText, "Test results");
    }
    else
    {
        gui_tools->showMessage("Error", "Test failed. Features required by the classifier may be missing.", 3);
    }
    stopThreadOut();
}
bool VschPlugin::before_test(void* object)
{
    return ((VschPlugin*)object)->before_test_this();
}
void VschPlugin::thread_test(void* object, void* notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier)
{

    ((VschPlugin*)object)->notifier_object = notifier_object;
    ((VschPlugin*)object)->step_notifier = step_notifier;
    ((VschPlugin*)object)->text_notifier = text_notifier;
    ((VschPlugin*)object)->thread_test_this();
}
void VschPlugin::after_test(void* object)
{
    ((VschPlugin*)object)->after_test_this();
}
void VschPlugin::on_menuTest_triggered()
{
    gui_tools->InitiateAnalysisActions(this, &before_test, &thread_test, &after_test);
}















void* VschPlugin::connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function)
{
    void* ret = gui_tools->addMenuAction(name, tip, onActionTable.size());
    if(name != NULL && function != NULL)
        onActionTable.push_back(function);
    return ret;
}
void VschPlugin::callBack(const unsigned int index)
{
    if(index < onActionTable.size())
        if(onActionTable[index] != NULL)
            (this->*(onActionTable[index]))();
}


bool VschPlugin::initiateTablePlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    connectMenuAction("Load classifier...", "Load rules to classify data", &VschPlugin::on_menuLoad_triggered);
    plugin_save = connectMenuAction("Save classifier...", "Save rules to classify data", &VschPlugin::on_menuSave_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("Selection and training...", "Selection of features and training of the classifier", &VschPlugin::on_menuSelect_triggered);
    plugin_test = connectMenuAction("Test classifier...", "Test classifier performance", &VschPlugin::on_menuTest_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &VschPlugin::on_menuAbout_triggered);
    stopThreadOut();
    return true;
}

bool VschPlugin::initiateMapsPlugin(MzPullDataInterface* pull_data, MzGuiRelatedInterface* gui_tools)
{
    this->pull_data = pull_data;
    this->gui_tools = gui_tools;
    connectMenuAction("Load classifier...", "Load rules to classify data", &VschPlugin::on_menuLoad_triggered);
    plugin_save = connectMenuAction("Save classifier...", "Save rules to classify data", &VschPlugin::on_menuSave_triggered);
    connectMenuAction(NULL, NULL, NULL);
    plugin_test = connectMenuAction("Segment image...", "Image segmentation by the convex hull classifier", &VschPlugin::on_menuSegment_triggered);
    connectMenuAction(NULL, NULL, NULL);
    connectMenuAction("About...", "Info about this plugin", &VschPlugin::on_menuAbout_triggered);
    stopThreadOut();
    return true;
}

void VschPlugin::on_menuSave_triggered()
{
    if(mainclassifier != NULL)
    {
        std::string fileName;
        unsigned int filter = 0;
        if(gui_tools->getSaveFile(&fileName, &filter))
        {
            if(!SelectClassifiersOptions()) return;
            if(!setTempClassifier()) return;
            if(!classifier->saveClassifier(fileName.c_str(), (filter==1)))
            {
                gui_tools->showMessage("Error", "Failed to save classifier", 3);
            }
            releaseTempClassifier();
        }
    }
    else
    {
        gui_tools->showMessage("Warning", "No classifier to save", 2);
    }
}

bool VschPlugin::openFile(string *filename)
{
    newTempClassifier();
    if(!classifier->loadClassifier(filename->c_str()))
    {
        releaseTempClassifier();
        return false;
    }
    else
    {
        if(classifier != NULL)
        {
            setClassifierFromTemp();
        }
        stopThreadOut();
        return true;
    }
}

void VschPlugin::on_menuLoad_triggered()
{
    std::string fileName;
    if(gui_tools->getOpenFile(&fileName))
    {
        newTempClassifier();
        if(!classifier->loadClassifier(fileName.c_str()))
        {
            releaseTempClassifier();
            gui_tools->showMessage("Error", "Failed to load classifier", 3);
        }
        else
        {
            setClassifierFromTemp();
            if(classifieruse != NULL) delete[] classifieruse;
            classifieruse = NULL;
        }
    }
    stopThreadOut();
}



/*
bool VschPlugin::startSegmentThread(const char* analysisSlot, const char* finishSlot, quint64 maxSteps, unsigned int pixel)
{
    bool ok = true;
    QObject* par = QObject::sender()->parent();
    pull_maps = qobject_cast<MzPullMapsInterface*>(par);


    if(inputmaps.size() > 0) clearMaps(&inputmaps);
    if(outputmaps.size() > 0) clearMaps(&outputmaps);

    if(!pull_maps->getImages(&inputmaps)) ok = false;
    else if(inputmaps.size() > 0)
    {
        for(unsigned int i = 0; i < currentclassifier->classifiers.size(); i++)
        {
            MapForSegmentation* map = new MapForSegmentation();
            unsigned int allsize = 1;
            for(unsigned int d = 0; d < MZDIMENSIONS; d++)
            {
                map->size[d] = inputmaps[0]->size[d];
                allsize *= map->size[d];
            }
            map->pixel = pixel;
            map->name = currentclassifier->classifiers[i].getName();

            switch(pixel)
            {
            case MZPIXELF64:
            {
                double* ptr = new double[allsize];
                map->data = ptr;
            } break;
            case MZPIXELF32:
            {
                float* ptr = new float[allsize];
                map->data = ptr;
            } break;
            }
            outputmaps.push_back(map);
        }
    }
    else ok = false;



    if(!ok)
    {
        clearMaps(&inputmaps);
        clearMaps(&outputmaps);
        QMessageBox msgBox;
        msgBox.setText("Error starting analysis");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
        return false;
    }

    QThread* thread = new QThread;
    QWidget* window = qobject_cast<QWidget*>(par);
    progressdialog = new Progress(window);

    worker = new PluginWorker(&inputmaps, &outputmaps, currentclassifier, progressdialog);

    progressdialog->setMaxValues(0, maxSteps, worker);
    progressdialog->setWindowModality(Qt::WindowModal);
    progressdialog->show();
    worker->moveToThread(thread);

    connect(thread, SIGNAL(started()), worker, analysisSlot);
    connect(worker, SIGNAL(finished(bool)), this, finishSlot);
    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(worker, SIGNAL(notifyProgressStep(void)), progressdialog, SLOT(NotifyProgressStep(void)));
    connect(worker, SIGNAL(notifyProgressText(QString)), progressdialog, SLOT(NotifyProgressText(QString)));

    window->hide();
    thread->start();
    return true;
}
void VschPlugin::on_menuSegment_triggered()
{
//    if(setTempClassifier() == false) return;
    quint64 maxSteps = currentclassifier->classifiers.size();
    startSegmentThread(SLOT(process_segment()), SLOT(on_menuSegment_finished(bool)), maxSteps, MZPIXELF32);
}

void VschPlugin::on_menuSegment_finished(bool success)
{
    stopThreadIn();

    //releaseTempClassifier();
    if(success)
    {
        //QObject* par = QObject::sender()->parent();
        //MzPullMapsInterface* pull_maps = qobject_cast<MzPullMapsInterface*>(par);
        pull_maps->setImages(&outputmaps);
        clearMaps(&outputmaps);
        clearMaps(&inputmaps);
    }
    else
    {
        clearMaps(&outputmaps);
        clearMaps(&inputmaps);
        QMessageBox msgBox;
        msgBox.setText("Analysis failed");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
    stopThreadOut();
}

void VschPlugin::on_menuTest_triggered()
{
    if(!startThreadIn()) return;
    quint64 maxSteps = data->vectornumber;
    unsigned int sizeQsorted = (currentclassifier->classifiers.size() + 1) * data->classnumber;
    startThreadOut(SLOT(process_test()), SLOT(on_menuTest_finished(bool)), maxSteps, 0, 0, sizeQsorted, false);
}
void VschPlugin::on_menuTrain_triggered()
{
    if(!startThreadIn()) return;
    unsigned int sizeQsorted = data->featurenumber;
    quint64 maxsteps = data->classnumber;
    startThreadOut(SLOT(process_train()), SLOT(on_menuTrain_finished(bool)), maxsteps, 0, sizeQsorted, sizeQsorted, true);
}

void VschPlugin::on_menuSelect_triggered()
{
    if(!startThreadIn()) return;
    unsigned int sizeQsorted = data->featurenumber;

    quint64 maxSteps = 1;
    quint64 maxstepsold = maxSteps;
    int drn = data->featurenumber < dimensionsrestriction ? data->featurenumber: dimensionsrestriction;
    for(int d = 0; d < drn; d++)
    {
        for(int k = 0; k <= d; k++)
        {
            maxSteps *= (data->featurenumber-k);
            if(maxSteps / (data->featurenumber-k) != maxstepsold)
            {
                maxSteps = 0;
                k = d = drn;
            }
            maxstepsold = maxSteps;
        }
    }

    startThreadOut(SLOT(process_select()), SLOT(on_menuSelect_finished(bool)),
                   maxSteps, timerestriction+data->classnumber,
                   sizeQsorted, sizeQsorted, true);
}

void VschPlugin::on_menuTest_finished(bool success)
{
    stopThreadIn();

    if(success)
    {
        TestDialog testdialog;

        int cla = 1;

        for(vector<Classifier>::iterator c = currentclassifier->classifiers.begin(); c != currentclassifier->classifiers.end(); ++c)
        {
            if(c->classnames.size() <= 0) continue;
            if(c->featurenames.size() <= 0) continue;
            if(c->values.size() <= 0) continue;
            cla++;
        }

        std::string* cn = new std::string[cla];
        cn[cla-1] = "#";

        int cc = 0;
        for(vector<Classifier>::iterator c = currentclassifier->classifiers.begin(); c != currentclassifier->classifiers.end(); ++c, cc++)
        {
            if(c->classnames.size() <= 0) continue;
            if(c->featurenames.size() <= 0) continue;
            if(c->values.size() <= 0) continue;

            stringstream ss;
            for(vector<string>::iterator vs = c->classnames.begin(); vs != c->classnames.end(); ++vs)
            {
                ss << *vs;
            }
            ss << c->featurenames.size() << "D";
            cn[cc] = ss.str();
        }
        testdialog.setTableData(cla, data->classnumber, cn, data->classnames, Qsorted);
        testdialog.exec();
        delete[] cn;
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Test failed");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
    stopThreadOut();
}

void VschPlugin::on_menuTrain_finished(bool success)
{
    stopThreadIn();

    if(success)
    {
        QMessageBox msgBox;
        msgBox.setText("Training completed");
        msgBox.setWindowTitle("Information");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Training failed");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
    stopThreadOut();
}

void VschPlugin::on_menuSelect_finished(bool success)
{
    stopThreadIn();

    if(success)
    {
        SelectionList selectiondialog;
        selectiondialog.setList(data, Qtable, Qsorted, dimensionsrestriction);
        if(selectiondialog.exec() == QDialog::Accepted)
        {
            if(selectiondialog.checkedFeatures != NULL)
            {
                int count = 0;

                for(int c = 0; c < data->featurenumber; c++)
                {
                    if(selectiondialog.checkedFeatures[c]) count ++;
                }
                if(count > 0)
                {
                    SelectedFeatures ret;
                    ret.featurenumber = count;
                    ret.featureoriginalindex = new int[count];
                    int cc = 0;
                    for(int c = 0; c < data->featurenumber; c++)
                    {
                        if(selectiondialog.checkedFeatures[c])
                        {
                            ret.featureoriginalindex[cc] = data->featureoriginalindex[Qsorted[c]];
                            cc ++;
                        }
                    }
                    pull_data->setSelection(&ret);
                }
            }
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Selection failed");
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
    stopThreadOut();
}

void VschPlugin::on_menuOptions_triggered()
{
    QObject* par = QObject::sender()->parent();
    pull_data = qobject_cast<MzPullDataInterface*>(par);

    OptionsDialog dialog;
    DataForSelection data;
    pull_data->getData(&data);

    dialog.setOptions(&data, classname, dimensionsrestriction, 32000, timerestriction, mode_index, normalizeoption, margin, expand);
    if(dialog.exec() == QDialog::Accepted)
    {
        dialog.getOptions(&dimensionsrestriction, &timerestriction, &mode_index, &normalizeoption, &margin, &expand);
        if(mode_index < 0 || mode_index >= data.classnumber) classname.clear();
        else classname = QString::fromStdString(data.classnames[mode_index]);
    }
}


void VschPlugin::on_menuSave_triggered()
{
    if(currentclassifier != NULL)
    {
        QString filter;
        bool hexa = false;
        QWidget* window = qobject_cast<QWidget*>(QObject::sender()->parent());
        QString fileName = QFileDialog::getSaveFileName(window,
                                                     tr("Save linear classifier"),
                                                     NULL,//domyslna nazwa pliku
                                                     tr("Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)"),
                                                     &filter);
        if(filter == "Double precision hexadecimals (*.txtv) (*.txtv)") hexa = true;

        if (!fileName.isEmpty())
        {

            if(!currentclassifier->saveClassifier(fileName.toStdString().c_str(), hexa))
            {
                QMessageBox msgBox;
                msgBox.setText("Failed to save classifier");
                msgBox.setWindowTitle("Error");
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.exec();
            }
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("No classifier to save. Train first.");
        msgBox.setWindowTitle("Warning");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
}

void VschPlugin::on_menuLoad_triggered()
{
    QString filter;
    QWidget* window = qobject_cast<QWidget*>(QObject::sender()->parent());
    QString fileName = QFileDialog::getOpenFileName(window,
                                                 tr("Load linear classifier"),
                                                 NULL,//domyslna nazwa pliku
                                                 tr("Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)"),
                                                 &filter);
    if (!fileName.isEmpty())
    {

        Classifiers* newclassifier = new Classifiers("MzVschClassifiers2013");
        if(!newclassifier->loadClassifier(fileName.toStdString().c_str()))
        {
            if(newclassifier != NULL) delete newclassifier;
            QMessageBox msgBox;
            msgBox.setText("Failed to load classifier");
            msgBox.setWindowTitle("Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
        }
        else
        {
            if(currentclassifier != NULL) delete currentclassifier;
            currentclassifier = newclassifier;
        }
    }
    if(currentclassifier == NULL)
    {
        plugin_save->setEnabled(false);
        plugin_test->setEnabled(false);
    }
    else
    {
        plugin_save->setEnabled(true);
        plugin_test->setEnabled(true);
    }
}

bool VschPlugin::openFile(QString filename)
{
    Classifiers* newclassifier = new Classifiers(classifiername);
    if(!newclassifier->loadClassifier(filename.toStdString().c_str()))
    {
        if(newclassifier != NULL) delete newclassifier;
        return false;
    }
    else
    {
        if(currentclassifier != NULL) delete currentclassifier;
        currentclassifier = newclassifier;
        if(currentclassifier == NULL)
        {
            plugin_save->setEnabled(false);
            plugin_test->setEnabled(false);
        }
        else
        {
            plugin_save->setEnabled(true);
            plugin_test->setEnabled(true);
        }
        return true;
    }

}
*/
