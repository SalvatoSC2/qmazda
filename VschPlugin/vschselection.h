/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VSCHSELECTION_H
#define VSCHSELECTION_H

#include <math.h>

#include "../MzShared/multidimselection.h"
#include "../MzShared/dataforconsole.h"

extern "C"
{
    #include <libqhull_r/libqhull_r.h>
    //#include <libqhull_r.h>
    //#include <qhull_ra.h>
    //#include <qhull_a.h>
    //#include <qhull/qhull.h>
}

#include "../MzShared/classifierio.h"

#define NORMNONE -1
#define STANDARDIZE 0
#define NORMALIZE 1

const char VschSelectionReductionClassifierName[] = "MzVschClassifiers2013";

class VschSelectionReduction : public MultiDimensionalSelection, public ClassifierAccessInterface
{
public:
    virtual void NotifyProgressText(std::string text);
    virtual void NotifyProgressStep(void);
    virtual void NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected);

    double GoalFunction(unsigned int dimensions, unsigned int *picked_features);
    double ClassifierTraining(unsigned int dimensions, unsigned int* selected);
    void StoreClassifier(unsigned int dim, int faces, unsigned int* picked, double* pars);

    const char* getClassifierMagic(void){return VschSelectionReductionClassifierName;}
    bool configureForClassification(vector<string>* featurenames);
    unsigned int classifyFeatureVector(double* reindexedFeatureVector, double *distances);
    unsigned int classifyFeatureVector(double* featureVector);
    void segmentImage(unsigned int vectornumber, MazdaMapPixelType **values, MazdaMapPixelType *result);
    bool loadClassifierFromFile(const char *filename);

    std::vector<std::string> getFeatureNames(void);
    std::vector<std::string> getClassNames(void);

    VschSelectionReduction();
    ~VschSelectionReduction();

    //bool Train(void);
    bool Test(void);
    bool Select(void);
    bool computeSegmentation(void);

    void NormalizeMinMax(void);
    void Standardize(void);

    int dimensions;
    int maxtime;

    double* Qtable;
    unsigned int* Qsorted;

    DataForSelection* data;
    Classifiers* classifier;

    char modeUpscaleOrExpand;
    double minimumExpandOrUpscale;
    int classtodistinguish;

    std::string confusionMatrixText;

    DataForSegmentation dataMap;

private:
    unsigned int featureReindexLutSize;
    unsigned int* featureReindexLut;
    unsigned int classReindexLutSize;
    unsigned int* classReindexLut;
    double* reindexedFeatureVector;
    double* distances;

    vector<string> outClassNames;
    //unsigned int* outClassIndices;
    double* normal_plus;
    double* normal_multi;
    double max1d;
    double min1d;
    double cen1d;
    double spn1d;

    int vschDimensions;
    int vschInvectors;
    double* vschCenter;
    int vschFacets;
    coordT *vschPoints;
    double vschExpandOrUpscale;

    int vschInititialize(int dimensions, int invectors);
    int vschComputeHull(qhT *qh, int dim, unsigned int *picked);
    int vschComputeHull1(unsigned int *picked);
    void vschFreeHull(qhT *qh);
    void vschComputeCenter(unsigned int *picked);
    double vschPenaltyIndexU(qhT *qh, unsigned int *picked);
    double vschPenaltyIndexE(qhT *qh, unsigned int *picked);
    double vschPenaltyIndex1U(unsigned int *picked);
    double vschPenaltyIndex1E(unsigned int *picked);

    void GetFacets(qhT *qh, double *ie);
    void GetFacetsU(qhT *qh, double *ie);
    void GetFacetsE(qhT *qh, double *ie);

    void GetFacets1(double *ie);
    void GetFacets1U(double *ie);
    void GetFacets1E(double *ie);
};

#endif // VSCHSELECTION_H
