#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T08:44:48
#
#-------------------------------------------------

#QT       += core

CONFIG -= app_bundle
CONFIG -= qt

TEMPLATE        = lib
CONFIG         += shared
CONFIG         += plugin

TARGET          = $$qtLibraryTarget(VschPlugin)
DESTDIR         = ../../Executables

SOURCES += \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp \
    ../MzShared/multidimselection.cpp \
    vschselection.cpp \
    vschplugin.cpp \
    ../MzShared/classifierio.cpp


HEADERS +=\
    ../MzShared/dataforconsole.h \
    ../MzShared/mzselectioninterface.h \
    ../MzShared/multidimselection.h \
    vschplugin.h \
    vschselection.h \
    ../MzShared/classifierio.h

include(../Pri/config.pri)
include(../Pri/qhull.pri)
