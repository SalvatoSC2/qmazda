/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits>

#include "vschselection.h"
#include "../MzShared/multidimselection.h"
#include "../MzShared/dataforconsole.h"

const double machineepsilon = 1e-15;
const double vschminupscale = 1.0 + machineepsilon;

VschSelectionReduction::VschSelectionReduction()//(DataForSelection* data_in, Classifiers *classifier_in,
                                               //int normalize, int classtodistinguish_in, char margin, double expand)
{
    featureReindexLut = NULL;
    reindexedFeatureVector = NULL;
    classReindexLut = NULL;
    distances = NULL;
    classifier = NULL;
    normal_plus = NULL;
    normal_multi = NULL;
    vschInvectors = -1;
    vschCenter = NULL;
    vschFacets = 0;
    vschPoints = NULL;
}
VschSelectionReduction::~VschSelectionReduction()
{
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(classReindexLut != NULL) delete[] classReindexLut;
    classReindexLut = NULL;
    if(reindexedFeatureVector != NULL) delete[] reindexedFeatureVector;
    reindexedFeatureVector = NULL;
    if(distances != NULL) delete[] distances;
    distances = NULL;
    if(classifier != NULL) delete classifier;
    classifier = NULL;
    if(normal_plus != NULL) free(normal_plus);
    normal_plus = NULL;
    if(normal_multi != NULL) free(normal_multi);
    normal_multi = NULL;
}

void VschSelectionReduction::NotifyProgressText(std::string)
{
}
void VschSelectionReduction::NotifyProgressStep(void)
{
}
void VschSelectionReduction::NotifyProgressStage(const int, const int, const double, const unsigned int*)
{
}

//------------------------------------------------------------------------------
void VschSelectionReduction::NormalizeMinMax(void)
{
    double min, max, d;
    int f, v;

    if(normal_plus) free(normal_plus);
    if(normal_multi) free(normal_multi);
    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

    for(f = 0; f < data->featurenumber; f++)
    {
        v = 0;
        min = max = data->values[data->featurenumber * v + f];
        for(v = 1; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];

            if(d > max) max = d;
            if(d < min) min = d;
        }
        normal_plus[f] = -(max+min)/2.0;
        max -= min;
        if(max >= machineepsilon) max = 2.0/max;
        else max = 1.0;
        normal_multi[f] = max;
        min = normal_plus[f];

        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            d += min; d *= max;
            data->values[data->featurenumber * v + f] = d;
        }
    }
}

//------------------------------------------------------------------------------
void VschSelectionReduction::Standardize(void)
{
    double mean, stdd, d;
    int f, v;

    if(normal_plus) free(normal_plus);
    if(normal_multi) free(normal_multi);
    normal_plus = (double*)malloc(sizeof(double) * data->featurenumber);
    normal_multi = (double*)malloc(sizeof(double) * data->featurenumber);

    for(f = 0; f < data->featurenumber; f++)
    {
        mean = 0;
        stdd = 0;
        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            mean += d;
            stdd += (d * d);
        }
        mean /= data->vectornumber;
        stdd /= data->vectornumber;
        stdd -= (mean * mean);
        stdd = sqrt(stdd);

        normal_plus[f] = -mean;
        if(stdd >= machineepsilon) stdd = 1.0/stdd;
        else stdd = 1.0;
        normal_multi[f] = stdd;

        for(v = 0; v < data->vectornumber; v++)
        {
            d = data->values[data->featurenumber * v + f];
            d -= mean; d *= stdd;
            data->values[data->featurenumber * v + f] = d;
        }
    }
}

//------------------------------------------------------------------------------
int VschSelectionReduction::vschInititialize(int dimensions, int invectors)
{
//    if(dimensions <= 0  || invectors < dimensions+1) return -1;
    if(vschDimensions != dimensions || vschInvectors != invectors)
    {
        if(vschPoints != NULL) delete[] vschPoints;
        vschPoints = new coordT[dimensions*invectors];
        if(vschPoints == NULL)
            return -2;

        if(vschCenter != NULL) delete[] vschCenter;
        vschCenter = new double[dimensions];
        if(vschCenter == NULL)
            return -3;

        vschDimensions = dimensions;
        vschInvectors = invectors;
    }
    return 0;
}

void VschSelectionReduction::vschFreeHull(qhT *qh)
{
    int curlong, totlong;
    if(qh == NULL) return;
    qh_freeqhull(qh, !qh_ALL);
    qh_memfreeshort(qh, &curlong, &totlong);
    if (curlong || totlong)
      fprintf(stderr, "qhull warning: did not free %d bytes of long memory (%d pieces)\n", totlong, curlong);
}

//------------------------------------------------------------------------------
int VschSelectionReduction::vschComputeHull1(unsigned int* picked)
{
    int v;
    int vs = classtodistinguish > 0 ? data->classendvectorindex[classtodistinguish-1] : 0;
    int vm = data->classendvectorindex[classtodistinguish];
    vschInititialize(1, vm - vs);

    double vect;
    vect = data->values[data->featurenumber * vs + picked[0]];
    min1d = vect; max1d = vect;
    for(v = vs+1; v < vm; v++)
    {
        vect = data->values[data->featurenumber * v + picked[0]];
        if(min1d > vect) min1d = vect;
        if(max1d < vect) max1d = vect;
    }
    vschFacets = 2;
    return vschFacets;

}

int VschSelectionReduction::vschComputeHull(qhT *qh, int dim, unsigned int* picked)
{
    int v, d;
    int vs = classtodistinguish > 0 ? data->classendvectorindex[classtodistinguish-1] : 0;
    int vm = data->classendvectorindex[classtodistinguish];
    vschInititialize(dim, vm - vs);


    coordT *point;
    vertexT *vertex, **vertexp;


    //int dim;                  /* dimension of points */
    //int numpoints;            /* number of points */
    //coordT *points;           /* array of coordinates for each point */
    boolT ismalloc = False;     /* True if qhull should free points in qh_freeqhull() or reallocation */
    char flags[]= "qhull ";  /* option flags for qhull, see qh_opt.htm "qhull h" "qhull Tv"*/
    FILE *outfile= NULL;    /* output from qh_produce_output(qh)
                                 use NULL to skip qh_produce_output(qh) */
    FILE *errfile= stderr;    /* error messages from qhull code */
    int exitcode;             /* 0 if no error from qhull */
    facetT *facet;            /* set by FORALLfacets */
    //int curlong, totlong;     /* memory remaining after qh_memfreeshort */
    //qhT qh_qh;                /* Qhull's data structure.  First argument of most calls */
    //qhT *qh= &qh_qh;          /* Alternatively -- qhT *qh= (qhT*)malloc(sizeof(qhT)) */
    //QHULL_LIB_CHECK /* Check for compatible library */

    qh_zero(qh, errfile);
    point = vschPoints;
    for(v = vs; v < vm; v++)
    {
        for (d = 0; d < vschDimensions; d++, point++)
        {
            *point = data->values[data->featurenumber * v + picked[d]];
        }
    }
    /* initialize dim, numpoints, points[], ismalloc here */
    exitcode = qh_new_qhull(qh, dim, vm-vs, vschPoints, ismalloc, flags, outfile, errfile);

    if (!exitcode)
    {
        double divider = 0;
        double area;
        for(d = 0; d < dim; d++) vschCenter[d] = 0.0;

// Tu jest blad jesli w wersji release nie linkuje sie z qhull_r
        vschFacets = qh->num_facets;

        FORALLfacets
        {
            if(facet->isarea) area = facet->f.area;
            else area = 0; //qh_facetarea(qh, facet);

            FOREACHvertex_(facet->vertices)
            {
                divider += area;
                for (d = 0; d < qh->hull_dim; d++)
                    vschCenter[d] += area*vertex->point[d];
            }
        }
        for(d = 0; d < dim; d++)
            vschCenter[d] /= divider;
    }
    else
    {
        vschFacets = 0;
    }
    return vschFacets;
}

//------------------------------------------------------------------------------
void VschSelectionReduction::vschComputeCenter(unsigned int* picked)
{
    int v, d;
    int vs = classtodistinguish > 0 ? data->classendvectorindex[classtodistinguish-1] : 0;
    int vm = data->classendvectorindex[classtodistinguish];
    if(vschDimensions==1)
    {
        cen1d = (min1d+max1d) / 2.0;
        spn1d = (max1d-min1d) / 2.0;
        vschCenter[0] = cen1d;
        return;
    }
    for(d = 0; d < vschDimensions; d++) vschCenter[d] = 0.0;
    for(v = vs; v < vm; v++)
    {
        for (d = 0; d < vschDimensions; d++)
        {
             vschCenter[d] += data->values[data->featurenumber * v + picked[d]];
        }
    }
    v = vm-vs;

    for(d = 0; d < vschDimensions; d++)
    {
        vschCenter[d] /= v;
    }
}

//------------------------------------------------------------------------------
double VschSelectionReduction::vschPenaltyIndex1U(unsigned int* picked)
{
    double qq2 = 0.0;
    unsigned int qq1 = 0;
    double qq2max = 0.0;
    int c, v, vs, vm;

    double vect;
    qq2max = -1.0;

    vs = 0;
    for(c = 0; c < data->classnumber; c++)
    {
        vm = data->classendvectorindex[c];
        if(c != classtodistinguish)
        {
            for(v = vs; v < vm; v++)
            {
                vect = data->values[data->featurenumber * v + picked[0]];
                if(min1d - minimumExpandOrUpscale <= vect && max1d + minimumExpandOrUpscale >= vect) qq1++;
                else
                {
                    vect -= cen1d;
                    if(0.0 < vect)
                    {
                        qq2 = vect / spn1d;
                    }
                    else
                    {
                        qq2 = - vect / spn1d;
                    };
                    if(qq2max > qq2 || qq2max < 0.0)
                    {
                        qq2max = qq2;
                    }
                }
            }
        }
        vs = vm;
    }

    vschExpandOrUpscale = sqrt(qq2max);
    if(qq2max < vschminupscale) qq2max = vschminupscale;
    qq2max = 1.0/qq2max;
    return ((double)qq1+qq2max);


}


double VschSelectionReduction::vschPenaltyIndexU(qhT *qh, unsigned int* picked)
{
    unsigned int qq1 = 0;
    double qq2max = 0.0;
    facetT *facet;
    int c, d, v, vs, vm;

    FORALLfacets
    {
        double cen = 0.0;
        for(d = 0; d < vschDimensions; d++)
        {
            cen += facet->normal[d]*vschCenter[d];
        }
        cen += (facet->offset - minimumExpandOrUpscale);
        if(cen > -minimumExpandOrUpscale) cen = -minimumExpandOrUpscale;
        facet->furthestdist = cen;
    }


    vs = 0;
    for(c = 0; c < data->classnumber; c++)
    {
        vm = data->classendvectorindex[c];

        for(v = vs; v < vm; v++)
        {
            double dmax = -1.0;
            FORALLfacets
            {
                double dis = 0.0;
                for(d = 0; d < vschDimensions; d++)
                {
                    dis += (facet->normal[d] * data->values[data->featurenumber * v + picked[d]]);
                }
                dis += facet->offset - minimumExpandOrUpscale;
                dis = 1.0 - (dis/(facet->furthestdist));
                if(dmax < dis)
                {
                    dmax = dis;
                }
            }
            if(c != classtodistinguish)
            {
                if(dmax <= 1.0)
                {
                    qq1++;
                }
                else
                {
                    if(qq2max > dmax || qq2max < 1.0)
                    {
                        qq2max = dmax;
                    }
                }
            }
            else
            {
                if(dmax > 1.0) return -2.0;
            }
        }
        vs = vm;
    }
    vschExpandOrUpscale = sqrt(qq2max);
    if(qq2max < vschminupscale) qq2max = vschminupscale;
    qq2max = 1.0/qq2max;
    return ((double)qq1+qq2max);
}

//------------------------------------------------------------------------------
double VschSelectionReduction::vschPenaltyIndex1E(unsigned int* picked)
{
    double qq2 = 0.0;
    unsigned int qq1 = 0;
    double qq2max = -1.0;
    int c, v, vs, vm;

//        double tocen;
    double vect;
//        tocen = (max1d-min1d)/2.0 + min_expanding;
    qq2max = -1.0;
    vs = 0;
    for(c = 0; c < data->classnumber; c++)
    {
        vm = data->classendvectorindex[c];
        if(c != classtodistinguish)
        {
            for(v = vs; v < vm; v++)
            {
                vect = data->values[data->featurenumber * v + picked[0]];
                if(min1d - minimumExpandOrUpscale <= vect && max1d + minimumExpandOrUpscale >= vect) qq1++;
                else
                {
                    if(cen1d < vect)
                    {
                        qq2 = vect - max1d;
                    }
                    else
                    {
                        qq2 = min1d - vect;
                    };
                    if(qq2max > qq2 || qq2max < 0.0)
                    {
                        qq2max = qq2;
                    }
                }
            }
        }
        vs = vm;
    }
    vschExpandOrUpscale = qq2max/2.0;
//        *Q1 = qq1;
    qq2max += vschminupscale;
    if(qq2max < vschminupscale) qq2max = vschminupscale;
    qq2max = 1.0/(qq2max);
//        *Q2 = qq2max;
    return ((double)qq1+qq2max);

}

double VschSelectionReduction::vschPenaltyIndexE(qhT *qh, unsigned int* picked)
{
    unsigned int qq1 = 0;
    double qq2max = -1.0;
    facetT *facet;
    int c, d, v, vs, vm;
    vs = 0;
    for(c = 0; c < data->classnumber; c++)
    {
        vm = data->classendvectorindex[c];

        for(v = vs; v < vm; v++)
        {
            double dmax = -1.0;
            FORALLfacets
            {
                double dis = 0.0;
                for(d = 0; d < vschDimensions; d++)
                {
                    dis += (facet->normal[d] * data->values[data->featurenumber * v + picked[d]]);
                }
                dis += facet->offset - minimumExpandOrUpscale;
                if(dmax < dis)
                {
                    dmax = dis;
                }
            }
            if(c != classtodistinguish)
            {
                if(dmax <= 0.0)
                {
                    qq1++;
                }
                else
                {
                    if(qq2max > dmax || qq2max < 0.0)
                    {
                        qq2max = dmax;
                    }
                }
            }
            else
            {
                if(dmax > 0.0) return -2.0;
            }
        }
        vs = vm;
    }
    vschExpandOrUpscale = qq2max/2.0;
    qq2max += vschminupscale;
    if(qq2max < vschminupscale) qq2max = vschminupscale;
    qq2max = 1.0/(qq2max);
    return ((double)qq1+qq2max);
}

//------------------------------------------------------------------------------

void VschSelectionReduction::GetFacets(qhT *qh, double *ie)
{
    if(modeUpscaleOrExpand == 'E') GetFacetsE(qh, ie);
    else GetFacetsU(qh, ie);
}
void VschSelectionReduction::GetFacets1(double *ie)
{
    if(modeUpscaleOrExpand == 'E') GetFacets1E(ie);
    else GetFacets1U(ie);
}
//------------------------------------------------------------------------------
void VschSelectionReduction::GetFacets1U(double *ie)
{
    double *pf;
    pf = ie;
    *pf = -1.0; pf++;
    *pf = min1d - minimumExpandOrUpscale - spn1d*(vschExpandOrUpscale-1.0); pf++;
    *pf = 1.0; pf++;
    *pf = -max1d - minimumExpandOrUpscale - spn1d*(vschExpandOrUpscale-1.0);
}

void VschSelectionReduction::GetFacetsU(qhT *qh, double *ie)
{
    double *pf;
    double sup;
    facetT *facet;
    int d;

    pf = ie;
    FORALLfacets
    {
        sup = 0.0;
        for (d = 0; d < qh->hull_dim; d++)
        {

            *pf = facet->normal[d];
            sup += ((*pf)*vschCenter[d]);
            pf++;
        }
        *pf = (facet->offset - minimumExpandOrUpscale)*vschExpandOrUpscale+(sup)*(vschExpandOrUpscale-1.0);
        pf++;
    }
}

//------------------------------------------------------------------------------
void VschSelectionReduction::GetFacets1E(double *ie)
{
    double *pf;
    pf = ie;
    *pf = -1.0; pf++;
    *pf = min1d - minimumExpandOrUpscale - vschExpandOrUpscale; pf++;
    *pf = 1.0; pf++;
    *pf = -max1d - minimumExpandOrUpscale - vschExpandOrUpscale;
}
void VschSelectionReduction::GetFacetsE(qhT *qh, double *ie)
{
    double *pf;
    double sup;
    facetT *facet;
    int d;
    pf = ie;
    FORALLfacets
    {
        sup = 0.0;
        for (d = 0; d < qh->hull_dim; d++)
        {
            *pf = facet->normal[d];
            sup += ((*pf)*vschCenter[d]);
            pf++;
        }
        *pf = facet->offset - minimumExpandOrUpscale - vschExpandOrUpscale;
        pf++;
    }
}
//------------------------------------------------------------------------------
void VschSelectionReduction::StoreClassifier(unsigned int dim, int faces, unsigned int* picked, double* pars)
{
    int f;
    bool nor = (normal_plus != NULL && normal_multi != NULL);
    vector< vector<double> > vvalues;
    vvalues.clear();
    //vvalues.resize(faces);

    int degenerated = 0;
    for(int v = 0; v < faces; v++)
    {
        vector<double> vface;
        vface.clear();
        int ff = v*(dim+1);
        double threshold = pars[ff + dim];
        double d = 0.0;
        for(f = 0; f < (int)dim; f++, ff++)
        {
            double ww = pars[ff];
            if(nor)
            {
                ww *= normal_multi[picked[f]];
                threshold += (ww * normal_plus[picked[f]]);
                d += ((vschCenter[f]/normal_multi[picked[f]]-normal_plus[picked[f]]) * ww);
            }
            else
            {
                d += (vschCenter[f] * ww);
            }
            vface.push_back(ww);
        }
        vface.push_back(threshold);
        f++;
        d += threshold;

        if(d >= 0)
        {
            degenerated++;
            //continue;//zdegenerowane
        }
        vface.push_back(d);
        vvalues.push_back(vface);
    }

    vector<string> vclassnames;
    if(classtodistinguish >= 0)
    {
        vclassnames.resize(1);
        vclassnames[0] = data->classnames[classtodistinguish];
    }
    else
    {
        vclassnames.resize(1);
        vclassnames[0] = "?";
    }
    vector<string> vfeaturenames;
    vfeaturenames.resize(dim);
    for(f = 0; f < (int)dim; f++)
    {
        vfeaturenames[f] = data->featurenames[picked[f]];
    }

    Classifier vclassifier;
    vclassifier.classnames = vclassnames;
    vclassifier.featurenames = vfeaturenames;
    vclassifier.values = vvalues;

    if(degenerated > 0)
    {
        printf("%s degenerated %i\n", vclassifier.getName().c_str(), degenerated);
        fflush(stdout);
    }
    classifier->classifiers.push_back(vclassifier);
}

//------------------------------------------------------------------------------
double VschSelectionReduction::GoalFunction(unsigned int dim, unsigned int* picked)
{
    double Q = -1.0;
    if(dim <= 1)
    {
        int fc = vschComputeHull1(picked);
        if(fc > (int)dim)
        {
            vschComputeCenter(picked);
            if(modeUpscaleOrExpand == 'E') Q = vschPenaltyIndex1E(picked);
            else Q = vschPenaltyIndex1U(picked);
        }
    }
    else
    {
        qhT qh_qh;                /* Qhull's data structure.  First argument of most calls */
        qhT *qh = &qh_qh;          /* Alternatively -- qhT *qh= (qhT*)malloc(sizeof(qhT)) */
        int fc = vschComputeHull(qh, dim, picked);
        if(fc > (int)dim)
        {
            vschComputeCenter(picked);
            if(modeUpscaleOrExpand == 'E') Q = vschPenaltyIndexE(qh, picked);
            else Q = vschPenaltyIndexU(qh, picked);
        }
        vschFreeHull(qh);
    }
    return Q;
}
//------------------------------------------------------------------------------
double VschSelectionReduction::ClassifierTraining(unsigned int dim, unsigned int* picked)
{
    double Q = -1.0;
    if(dim <= 1)
    {
        int fc = vschComputeHull1(picked);
        if(fc > (int)dim)
        {
            vschComputeCenter(picked);
            if(modeUpscaleOrExpand == 'E') Q = vschPenaltyIndex1E(picked);
            else Q = vschPenaltyIndex1U(picked);
            if(Q >= 0.0)
            {
                double* pars;
                pars = new double[(dim+1)*fc];
                GetFacets1(pars);
                StoreClassifier(dim, fc, picked, pars);
                delete[] pars;
            }
        }
    }
    else
    {
        qhT qh_qh;                /* Qhull's data structure.  First argument of most calls */
        qhT *qh = &qh_qh;          /* Alternatively -- qhT *qh= (qhT*)malloc(sizeof(qhT)) */
        int fc = vschComputeHull(qh, dim, picked);
        if(fc > (int)dim)
        {
            vschComputeCenter(picked);
            if(modeUpscaleOrExpand == 'E') Q = vschPenaltyIndexE(qh, picked);
            else Q = vschPenaltyIndexU(qh, picked);
            if(Q >= 0.0)
            {
                double* pars;
                pars = new double[(dim+1)*fc];
                GetFacets(qh, pars);
                StoreClassifier(dim, fc, picked, pars);
                delete[] pars;
            }
        }
        vschFreeHull(qh);
    }
    return Q;
}

//------------------------------------------------------------------------------
bool VschSelectionReduction::Select(void)
{
    int d, c;
    breakanalysis = false;
    if(data->classnumber <= 0)
        return false;
    if(dimensions > data->featurenumber) dimensions = data->featurenumber;
    if(classtodistinguish < data->classnumber && classtodistinguish >= 0)
    {
        if(FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, maxtime, false))
            return true;
        return false;
    }
    else
    {
        bool ret = true;
        unsigned int* tempsort = new unsigned int[data->classnumber * dimensions];

        int ile = data->classnumber;
        ile = (maxtime+ile-1)/ile;

        for(c = 0; c < data->classnumber; c++)
        {
            if(breakanalysis) return false;
            classtodistinguish = c;


            bool okl = FullSearch(data->featurenumber, Qtable, Qsorted, dimensions, ile, false);

            if(okl)
            {
                for(d = 0; d < dimensions; d++)
                    tempsort[classtodistinguish * dimensions + d] = Qsorted[d];
            }
            else
            {
                for(d = 0; d < dimensions; d++)
                    tempsort[classtodistinguish * dimensions + d] = -1;
            }
            ret = ret && okl;
        }


        c = 0;
        for(d = 0; d < data->featurenumber; d++) Qtable[d] = 0.0;
        for(d = 0; d < dimensions*data->classnumber && c < data->featurenumber; d++)
        {
            bool found = false;
            for(int dd = 0; dd < c; dd++)
            {
                if(Qsorted[dd] == tempsort[d])
                {
                    Qtable[Qsorted[dd]] += 1.0;
                    found = true;
                    break;
                }
            }
            if(!found)
            {
                Qsorted[c] =  tempsort[d];
                Qtable[Qsorted[c]] = 1.0;
                c++;
            }
        }
        delete[] tempsort;
        for(; c < data->featurenumber; c++) Qsorted[c] = (unsigned int)(-1);

        classtodistinguish = -1;
        return ret;
    }
}

bool VschSelectionReduction::configureForClassification(vector<string>* featurenames)
{
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(classReindexLut != NULL) delete[] classReindexLut;
    classReindexLut = NULL;
    if(reindexedFeatureVector != NULL) delete[] reindexedFeatureVector;
    reindexedFeatureVector = NULL;
    if(distances != NULL) delete[] distances;
    distances = NULL;
    outClassNames.clear();

    unsigned int classifiers_size = classifier->classifiers.size();
    if(classifiers_size <= 0)
        return false;

    featureReindexLutSize = 0;
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        featureReindexLutSize += classifier->classifiers[c].featurenames.size();
    }
    outClassNames = getClassNames();
    classReindexLutSize = outClassNames.size();
//    for(unsigned int c = 0; c < classifiers_size; c++)
//    {
//        featureReindexLutSize += classifier->classifiers[c].featurenames.size();
//        unsigned int class_number = classifier->classifiers[c].classnames.size();
//        for(unsigned int k = 0; k < class_number; k++)
//        {
//            bool on_list = false;
//            for(unsigned int kk = 0; kk < outClassNames.size(); kk++)
//            {
//                if(outClassNames[kk] == classifier->classifiers[c].classnames[k])
//                {
//                    on_list = true;
//                    break;
//                }
//            }
//            if(!on_list) outClassNames.push_back(classifier->classifiers[c].classnames[k]);
//        }
//    }
    if(classReindexLutSize <= 0 || featureReindexLutSize <= 0)
        return false;

    classReindexLut = new unsigned int[classReindexLutSize];
    featureReindexLut = new unsigned int[featureReindexLutSize];
    reindexedFeatureVector = new double[featureReindexLutSize];
    distances = new double[classifiers_size];

    unsigned int reindexLutPosition = 0;
    for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c)
    {
        classReindexLut[reindexLutPosition] = 0;
//        for(vector<string>::iterator cn = c->classnames.begin(); cn != c->classnames.end(); ++cn)
//        {
            std::string* cn = & c->classnames[0];
            for(unsigned int kk = 0; kk < classReindexLutSize; kk++)
            {
                if(outClassNames[kk] == *cn)
                {
                    classReindexLut[reindexLutPosition] = kk;
                }
            }
//        }
           reindexLutPosition++;
    }




    reindexLutPosition = 0;
    for(unsigned int c = 0; c < classifiers_size; c++)
    {
        unsigned int features_number = classifier->classifiers[c].featurenames.size();

        for(unsigned int k = 0; k < features_number; k++)
        {
            bool found = false;
            unsigned int fns = featurenames->size();
            for(unsigned int kk = 0; kk < fns; kk++)
            {
                if((*featurenames)[kk] == classifier->classifiers[c].featurenames[k])
                {
                    found = true;
                    featureReindexLut[reindexLutPosition+k] = kk;
                    break;
                }
            }
            if(!found)
                return false;
        }

        reindexLutPosition += features_number;
    }
    return true;
}


unsigned int VschSelectionReduction::classifyFeatureVector(double* featureVector)
{
    for(unsigned int i = 0; i < featureReindexLutSize; i++)
        reindexedFeatureVector[i] = featureVector[featureReindexLut[i]];
    unsigned int ret = classifyFeatureVector(reindexedFeatureVector, distances);
    return ret;
}

unsigned int VschSelectionReduction::classifyFeatureVector(double* reindexedFeatureVector, double* distances)
{
    unsigned int c;
    unsigned int featureReindexLutPosition = 0;
    unsigned int classifiers_size = classifier->classifiers.size();
//    unsigned int classname_size = outClassNames.size();
//    double* distances = new double[classifiers_size];

    for(c = 0; c < classifiers_size; c++)
    {
        distances[c] = -std::numeric_limits<double>::max();
        unsigned int features_number = classifier->classifiers[c].featurenames.size();
        unsigned int faces_number = classifier->classifiers[c].values.size();

        for(unsigned int ff = 1; ff < faces_number; ff++)
        {
            unsigned int f;
            double d = 0.0;
            for(f = 0; f < features_number; f++)
            {
                d += (reindexedFeatureVector[featureReindexLutPosition+f] * classifier->classifiers[c].values[ff][f]);
            }
            d += classifier->classifiers[c].values[ff][f];
            if(d > 0.0)
            {
                distances[c] = 1.0;
                break;
            }
            if(classifier->classifiers[c].values[ff].size() > f+1)
            {
                if(d >= 0)
                {
                    distances[c] = d = 0.0;
                }
                else
                {
                    d /= (-classifier->classifiers[c].values[ff][f+1]); //distance from wall to center is available
                }
            }
            if(distances[c] < d) distances[c] = d;
        }
        featureReindexLutPosition += features_number;
    }

    double d = 1.0;
    unsigned int cn = classReindexLutSize;
    for(c = 0; c < classifiers_size; c++)
    {
        if(distances[c] < d)
        {
            d = distances[c];
            cn = c;
        }
    }
    if(cn >= classReindexLutSize)
        cn = 0;
    else
        cn = classReindexLut[cn];

//    delete[] distances;
//    if(cn > 0 && cn <= classifiers_size)
//        cn = outClassIndices[cn-1]+1;
    return cn;
}



void VschSelectionReduction::segmentImage(unsigned int vectornumber, MazdaMapPixelType **values, MazdaMapPixelType *result)
{
    breakanalysis = false;
    for(unsigned int v = 0; v < vectornumber; v++)
    {
        result[v] = 0;
        for(unsigned int i = 0; i < featureReindexLutSize; i++)
            reindexedFeatureVector[i] = values[featureReindexLut[i]][v];
        result[v] = classifyFeatureVector(reindexedFeatureVector, distances);
        if(breakanalysis) break;
        NotifyProgressStep();
    }
}

//------------------------------------------------------------------------------
bool VschSelectionReduction::Test(void)
{
    breakanalysis = false;
    if(classifier->classifiers.size() <= 0)
        return false;

    vector<string> featurenames;
    for(int f = 0; f < data->featurenumber; f++)
    {
        featurenames.push_back(data->featurenames[f]);
    }
    if(!configureForClassification(&featurenames))
        return false;
    unsigned int* fire_table = new unsigned int[data->classnumber*(classReindexLutSize)];
    memset(fire_table, 0, sizeof(unsigned int)*(data->classnumber*(classReindexLutSize)));

//    unsigned int classifiers_size = classifier->classifiers.size();
//    double* distances = new double[classifiers_size];

    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        int vs = ccc > 0 ? data->classendvectorindex[ccc-1] : 0;
        int vm = data->classendvectorindex[ccc];
        for(int v = vs; v < vm; v++)
        {
            double* ptri = data->values + (data->featurenumber * v);
            unsigned int cc = classifyFeatureVector(ptri);
            if((int) cc > classReindexLutSize) cc = 0;
            fire_table[cc + classReindexLutSize*ccc]++;

            NotifyProgressStep();
            if(breakanalysis)
            {
                delete[] fire_table;
                if(featureReindexLut != NULL) delete[] featureReindexLut;
                featureReindexLut = NULL;
                if(classReindexLut != NULL) delete[] classReindexLut;
                classReindexLut = NULL;
                if(reindexedFeatureVector != NULL) delete[] reindexedFeatureVector;
                reindexedFeatureVector = NULL;
                if(distances != NULL) delete[] distances;
                distances = NULL;
                outClassNames.clear();
                return false;
            }
        }
    }
    if(distances != NULL) delete[] distances;
    distances = NULL;

    std::stringstream ss;
    for(unsigned int cni = 1; cni < classReindexLutSize; cni++)
        ss << "\t" << outClassNames[cni];
    ss << "\t" << outClassNames[0] << std::endl;

    for(int ccc = 0; ccc < data->classnumber; ccc++)
    {
        ss << data->classnames[ccc];
        for(unsigned int cc = 1; cc < classReindexLutSize; cc++)
        {
            ss << "\t" << fire_table[cc + classReindexLutSize*ccc];
        }
        ss << "\t" << fire_table[classReindexLutSize*ccc];
        ss << std::endl;
    }
    confusionMatrixText = ss.str();
    delete[] fire_table;
    if(featureReindexLut != NULL) delete[] featureReindexLut;
    featureReindexLut = NULL;
    if(classReindexLut != NULL) delete[] classReindexLut;
    classReindexLut = NULL;
    if(reindexedFeatureVector != NULL) delete[] reindexedFeatureVector;
    reindexedFeatureVector = NULL;
//    if(distances != NULL) delete[] distances;
//    distances = NULL;
    outClassNames.clear();
    return true;
}





bool VschSelectionReduction::computeSegmentation(void)
{
    breakanalysis = false;
    if(! configureForClassification(&dataMap.featurenames))
        return false;

    segmentImage(dataMap.vectornumber, dataMap.values, dataMap.result[0]);
    return true;
}

//void VschSelectionReduction::segmentImage(unsigned int vectornumber, MazdaMapPixelType **values, MazdaMapPixelType *result)
//{
//    breakanalysis = false;
//    unsigned int classifiers_size = classifier->classifiers.size();
//    unsigned int featureCount = 0;
//    for(unsigned int c = 0; c < classifiers_size; c++)
//    {
//        featureCount += classifier->classifiers[c].featurenames.size();
//    }
//    double* featureBuffer = new double[featureCount];
//    double* distances = new double[classifiers_size];

//    for(unsigned int v = 0; v < vectornumber; v++)
//    {
//        result[v] = 0;
//        for(unsigned int f = 0; f < featureCount; f++)
//        {
//            featureBuffer[f] = values[featureReindexLut[f]][v];
//        }
//        result[v] = classifyFeatureVector(featureBuffer, distances);
//        if(breakanalysis) break;
//        NotifyProgressStep();
//    }
//    delete[] distances;
//    delete[] featureBuffer;
//}


bool VschSelectionReduction::loadClassifierFromFile(const char* filename)
{
    Classifiers* tmpClassifier = new Classifiers(VschSelectionReductionClassifierName);
    if(tmpClassifier->loadClassifier(filename))
    {
        if(classifier != NULL) delete classifier;
        classifier = tmpClassifier;
        return true;
    }
    else
        delete tmpClassifier;
    return false;
}

//std::vector<std::string> VschSelectionReduction::getClassNames(void)
//{
//    std::vector<std::string> names;
//    names.push_back(std::string("!"));
//    for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c)
//    {
//        if(c->classnames.size() <= 0) continue;
//        if(c->featurenames.size() <= 0) continue;
//        if(c->values.size() <= 0) continue;
//        names.push_back(c->getName());
//    }
//    return names;
//}





std::vector<std::string> VschSelectionReduction::getClassNames(void)
{
    std::vector<std::string> names;
    if(classifier == NULL)
        return names;
    unsigned int classifiers_size = classifier->classifiers.size();
    if(classifiers_size <= 0)
        return names;

    names.push_back(std::string("!"));
    for(vector<Classifier>::iterator c = classifier->classifiers.begin(); c != classifier->classifiers.end(); ++c)
    {
//        for(vector<string>::iterator cn = c->classnames.begin(); cn != c->classnames.end(); ++cn)
//        {
            std::string* cn = & c->classnames[0];
            unsigned int kk;
            for(kk = 0; kk < names.size(); kk++)
            {
                if(names[kk] == *cn)
                    break;
            }
            if(kk >= names.size())
                names.push_back(*cn);
//        }
    }
    return names;
}




std::vector<std::string> VschSelectionReduction::getFeatureNames(void)
{
    std::vector<std::string> names;
    if(classifier == NULL) return names;

    unsigned int nc = classifier->classifiers.size();
    for(unsigned int c = 0; c < nc; c++)
    {
        std::vector <std::string>* cl;
        cl = & classifier->classifiers[c].featurenames;
        //else cl = & classifier->classifiers[c].classnames;
        unsigned int nn = cl->size();
        for(unsigned int n = 0; n < nn; n++)
        {
            unsigned int nm = names.size();
            unsigned int m;
            for(m = 0; m < nm; m++)
            {
                if((*cl)[n] == names[m]) break;
            }
            if(m >= nm) names.push_back((*cl)[n]);
        }
    }
    return names;
}


