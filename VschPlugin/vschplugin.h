/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TUTORIALPLUGIN_H
#define TUTORIALPLUGIN_H

#include "../MzShared/mzselectioninterface.h"
//#include "../MzShared/progress.h"
#include "vschselection.h"

class VschPlugin : public MzSelectionPluginInterface, VschSelectionReduction
{
public:
    void NotifyProgressStep(void);
    void NotifyProgressText(std::string text);
    void NotifyProgressStage(const int notification, const int dimensionality,
                             const double value, const unsigned int* selected);


    VschPlugin();
    ~VschPlugin();
    const char* getName(void);
    bool initiateTablePlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools);
    bool initiateMapsPlugin(MzPullDataInterface *pull_data, MzGuiRelatedInterface *gui_tools);
    void callBack(const unsigned int index);
    bool openFile(std::string* filename);
    void cancelAnalysis(void);

    bool before_selection_this(void);
    static bool before_selection(void* object);
    void thread_selection_this(void);
    static void thread_selection(void* object,
                                 void *notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier);
    void after_selection_this(void);
    static void after_selection(void* object);


    bool before_test_this(void);
    static bool before_test(void* object);
    void thread_test_this(void);
    static void thread_test(void* object,
                                 void *notifier_object,
                                 MzStepNotificationFunction step_notifier,
                                 MzTextNotificationFunction text_notifier);
    void after_test_this(void);
    static void after_test(void* object);

    bool before_segmentation_this(void);
    void thread_segmentation_this();
    void after_segmentation_this(void);
    static bool before_segmentation(void* object);
    static void thread_segmentation(void* object, void* notifier_object,
                                     MzStepNotificationFunction step_notifier,
                                     MzTextNotificationFunction text_notifier);
    static void after_segmentation(void* object);

    void on_menuAbout_triggered();
    void on_menuTest_triggered();
//    void on_menuTest_finished(bool success);
//    void on_menuTrain_triggered();
//    void on_menuTrain_finished(bool success);
    //void on_menuMdfs_triggered();
    //void on_menuMdfs_finished(bool success);
    void on_menuSelect_triggered();
//    void on_menuSelect_finished(bool success);
//    void on_menuOptions_triggered();
    void on_menuSave_triggered();
    void on_menuLoad_triggered();
    void on_menuSegment_triggered();
//    void on_menuSegment_finished(bool success);

private:
    bool success;
    void* plugin_save;
    void* plugin_test;
    void* notifier_object;
    MzStepNotificationFunction step_notifier;
    MzTextNotificationFunction text_notifier;
    bool SetMachineLearningOptions(void);
    bool SelectClassifiersOptions(void);

    bool newTempClassifier(void);
    bool setTempClassifier(void);
    bool releaseTempClassifier(void);
    bool setClassifierFromTemp(void);

    bool startThreadIn(void);
    bool startThreadIn(std::vector<std::string>* featureNames);
    void stopThreadIn(void);
    void stopThreadOut(void);

    //bool startSegmentThread(const char* analysisSlot, const char* finishSlot, quint64 maxSteps, unsigned int pixel);

//    void clearMaps(MapsForSegmentation* maps)
//    {
//        for(unsigned int m = 0; m < maps->size(); m++)
//        {
//            if((*maps)[m] != NULL)
//            {
//                delete (*maps)[m];
//            }
//            (*maps)[m] = NULL;
//        }
//        maps->clear();
//    }
//    MapsForSegmentation inputmaps;
//    MapsForSegmentation outputmaps;

    bool* classifieruse;
    Classifiers* mainclassifier;
    int normalizeoption;

    typedef void(VschPlugin::*OnActionFunctionPointer)(void);
    MzPullDataInterface* pull_data;
    MzGuiRelatedInterface* gui_tools;
    std::vector <OnActionFunctionPointer> onActionTable;
    void* connectMenuAction(const char* name, const char* tip, OnActionFunctionPointer function);
};

extern "C"
{
    MZ_DECL_EXPORT void* MzNewPluginObject(void);
    MZ_DECL_EXPORT void MzDeletePluginObject(void* object);
}
#endif // TUTORIALPLUGIN_H
