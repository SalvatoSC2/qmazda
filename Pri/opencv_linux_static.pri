    INCLUDEPATH += /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/include/

    LIBS += \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_dnn.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_highgui.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_ml.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_objdetect.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_shape.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_stitching.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_superres.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_videostab.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_calib3d.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_videoio.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_imgcodecs.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_features2d.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_video.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_photo.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_imgproc.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_flann.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/lib/libopencv_core.a \

    LIBS += \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/share/OpenCV/3rdparty/lib/libittnotify.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/share/OpenCV/3rdparty/lib/liblibprotobuf.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/share/OpenCV/3rdparty/lib/liblibwebp.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/share/OpenCV/3rdparty/lib/liblibjasper.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/share/OpenCV/3rdparty/lib/libquirc.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/share/OpenCV/3rdparty/lib/libippiw.a \
        /home/piotr/Program/_3rdParty/usr/opencv3_4_10Static/share/OpenCV/3rdparty/lib/libippicv.a


LIBS += -L/usr/lib/x86_64-linux-gnu -ljpeg -lpng -lz -ltiff -lImath -lIlmImf -lIex -lHalf -lIlmThread -ldc1394 -lavcodec -lavformat -lavutil -lswscale -lavresample -ldl -lm -lpthread -lrt
