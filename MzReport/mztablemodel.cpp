/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mztablemodel.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//#define _USE_MATH_DEFINES
#include <math.h>
#include "../MzShared/filenameremap.h"
#include "../MzShared/csvio.h"

MzTableModel::MzTableModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    datatable.column_count = 0;
    datatable.row_count = 0;
    datatable.row_names = NULL;
    datatable.column_names = NULL;
    datatable.column_tips = NULL;
    datatable.feature_values = NULL;

    column_icons = NULL;
    row_icons = NULL;

    folds = NULL;
    numberoffolds = 0;
    foldmask = 0;
    icon_v.addFile(":/icons/icons/dialog-ok-apply.png");
    icon_o.addFile(":/icons/icons/dialog-cancel.png");
    icon_z.addFile(":/icons/icons/status_unknown.png");

    QLocale::setDefault(QLocale::C);
    setlocale( LC_ALL, "C" );
}

MzTableModel::~MzTableModel(void)
{
    if(folds != NULL) delete[] folds;
    if(row_icons != NULL) delete[] row_icons;
    if(column_icons != NULL) delete[] column_icons;
}


Qt::ItemFlags MzTableModel::flags(const QModelIndex & /*index*/) const
 {
     return Qt::ItemIsSelectable |  Qt::ItemIsEditable | Qt::ItemIsEnabled ;
 }

/*===========================================================================*/


/*
#include <qmath.h>
#include <random>
*/


double MzTableModel::getData(int column, int row)
{
    return datatable.feature_values[column*datatable.row_count + row];
}

void MzTableModel::addNoise(int distribution, double snr)
{
    srand(time(0));
    snr = sqrt(snr);

    beginResetModel();
    for(int f = 0; f < datatable.row_count; f++)
    if(row_icons[f] > 0)
    {
        int c = 0;
        int v;
        double a = 0;
        double b = 0;
        double u1, u2, xx, ff;
        switch(distribution)
        {
        case 0: // normal
            for(v = 0; v < datatable.column_count; v++)
            {
                ff = datatable.feature_values[v*datatable.row_count + f];
                if(ff == ff)
                {
                    a += ff;
                    b += (ff*ff);
                    c++;
                }
            }
            if(c > 0)
            {
                a /= c;
                b /= c;
                b -= (a*a);
            }

            if(b > 0)
            {
                b = sqrt(b);
                for(v = 0; v < datatable.column_count; v++)
                {
                    //Box–Muller transform
                    u1 = (double)rand()/RAND_MAX;
                    u2 = (double)rand()/RAND_MAX;
                    xx = sqrt(-2.0 * log(u1)) * cos(2.0 * M_PI * u2);
                    xx *= b;
                    xx /= snr;
                    datatable.feature_values[v*datatable.row_count + f] += xx;
                }
            }
        break;

        case 1: // uniform
            bool set = false;
            for(v = 0; v < datatable.column_count; v++)
            {
                ff = datatable.feature_values[v*datatable.row_count + f];
                if(ff == ff)
                {
                    if(set)
                    {
                        if(a > ff) a = ff;
                        if(b < ff) b = ff;
                    }
                    else
                    {
                        a = ff;
                        b = ff;
                    }
                }
            }
            if(set)
            {
                b -= a;
                a = b /2.0;

                for(int v = 0; v < datatable.column_count; v++)
                {
                    xx = ((double)rand()*b/RAND_MAX) - a;
                    datatable.feature_values[v*datatable.row_count + f] += xx;
                }
            }
        break;
        }
    }
    endResetModel();
}



bool MzTableModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole)
    {
    bool ok;
    QString text = value.toString();
        if(index.column()<datatable.column_count && index.row()<datatable.row_count && index.row() >=0 && index.column() >= 0)
        {
            datatable.feature_values[index.column()*datatable.row_count + index.row()] = text.toDouble(&ok);
            if(!ok) datatable.feature_values[index.column()*datatable.row_count + index.row()] = CsvIo::hexToDouble(text.toStdString().c_str());
            text = QString("%1").arg(datatable.feature_values[index.column()*datatable.row_count + index.row()]);
        }
        emit editCompleted(text);
    }
    return true;
}

bool MzTableModel::setData(DataForSelection* inputdata)
{
    int v, f, c;
    if(inputdata == NULL) return false;

    if(datatable.feature_values != NULL) delete[] datatable.feature_values;
    if(row_icons != NULL) delete[] row_icons;
    if(datatable.row_names != NULL) delete[] datatable.row_names;
    if(column_icons != NULL) delete[] column_icons;
    if(datatable.column_names != NULL) delete[] datatable.column_names;
    if(datatable.column_tips != NULL) delete[] datatable.column_tips;
    datatable.column_count = inputdata->vectornumber;
    datatable.row_count = inputdata->featurenumber;
    datatable.column_names = new std::string[datatable.column_count];
    datatable.column_tips = new std::string[datatable.column_count];
    column_icons = new char[datatable.column_count];
    for(int c = 0; c < datatable.column_count; c++) column_icons[c] = 1;
    datatable.row_names = new std::string[datatable.row_count];
    row_icons = new char[datatable.row_count];
    datatable.feature_values = new double[datatable.column_count*datatable.row_count];

    for(v = 0; v < datatable.row_count; v++)
    {
        datatable.row_names[v] = inputdata->featurenames[v];
        if(datatable.row_names[v].at(0) == '_') row_icons[v] = 0;
        else row_icons[v] = 1;
    }
    int vs = 0;
    for(c = 0; c < inputdata->classnumber; c++)
    {
        int vm = inputdata->classendvectorindex[c];
        for(v = vs; v < vm; v++)
        {
            datatable.column_names[v] = inputdata->classnames[c];
            for(f = 0; f < inputdata->featurenumber; f++)
            {
                datatable.feature_values[v * datatable.row_count + f] = inputdata->values[v * inputdata->featurenumber + f];
            }
        }
        vs = vm;
    }
    return true;
}

bool MzTableModel::getRequiredData(std::vector<std::string>* featureNames, DataForSelection* outputdata)
{
    if(datatable.column_count <= 0 || datatable.row_count <= 0 || datatable.row_names == NULL ||
       datatable.column_names == NULL || datatable.feature_values == NULL || column_icons == NULL) return false;

    int c, r;
    QStringList classlist;
    QStringList featurelist;

    int* vectorsinclass = new int[datatable.column_count];
    for(c = 0; c < datatable.column_count; c++) vectorsinclass[c] = 0;

    int* vectorclassindex = new int[datatable.column_count];
    for(c = 0; c < datatable.column_count; c++) vectorclassindex[c] = -1;


    bool* wanted = new bool[datatable.row_count];

    for(r = 0; r < datatable.row_count; r++)
    {
        wanted[r] = false;
        for(unsigned int f = 0; f < featureNames->size(); f++)
        {
            if((*featureNames)[f] == datatable.row_names[r])
            {
                wanted[r] = true;
                break;
            }
        }
    }


    int vectornumber = 0;
    for(c = 0; c < datatable.column_count; c++)
    {
        if(column_icons[c] > 0)
        {
            int index = classlist.indexOf(QString::fromStdString(datatable.column_names[c]));
            if(index < 0)
            {
                vectorsinclass[classlist.size()]++;
                vectorclassindex[c] = classlist.size();
                classlist.append(QString::fromStdString(datatable.column_names[c]));
            }
            else
            {
                vectorsinclass[index]++;
                vectorclassindex[c] = index;
            }
            vectornumber++;
        }
    }
    int classnumber = classlist.size();
    int featurenumber = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
        if(wanted[r])
        {
            featurelist.append(QString::fromStdString(datatable.row_names[r]));
            featurenumber++;
        }
    }

    outputdata->Reset();
    outputdata->classnumber = classnumber;
    outputdata->featurenumber = featurenumber;
    outputdata->vectornumber = vectornumber;

    outputdata->classendvectorindex = new int[classnumber];
    r = 0;
    for(c = 0; c < classnumber; c++)
    {
        r += vectorsinclass[c];
        vectorsinclass[c] = 0;
        outputdata->classendvectorindex[c] = r;
    }

    int v = 0;
    outputdata->values = new double[featurenumber*vectornumber];
    for(c = 0; c < datatable.column_count; c++)
    {
        if(vectorclassindex[c] >= 0)
        {
            int classs = vectorclassindex[c];
            int vv = vectorsinclass[classs] + (classs == 0 ? 0 : outputdata->classendvectorindex[classs-1]);
            vectorsinclass[classs]++;

            if(vv >= vectornumber)
                vv = vectornumber-1;

            int f = 0;
            for(r = 0; r < datatable.row_count; r++)
            {
                if(wanted[r])
                {
                    outputdata->values[featurenumber*vv+f] = datatable.feature_values[datatable.row_count*c+r];
                    f++;
                }
            }
            v++;
        }
    }

    delete[] vectorclassindex;
    delete[] vectorsinclass;

    outputdata->classnames = new std::string[classnumber];
    for(c = 0; c < classnumber; c++)
    {
        outputdata->classnames[c] = classlist.at(c).toStdString();
    }
    outputdata->featurenames = new std::string[featurenumber];
    for(r = 0; r < featurenumber; r++)
    {
        outputdata->featurenames[r] = featurelist.at(r).toStdString();
    }
    outputdata->featureoriginalindex = new int[featurenumber];
    c = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
        if(wanted[r])
        {
            outputdata->featureoriginalindex[c] = r;
            c++;
        }
    }
    delete[] wanted;
    return true;
}


bool MzTableModel::getRequiredData(DataForSelection* outputdata, int checked_mask)
{
    if(datatable.column_count <= 0 || datatable.row_count <= 0 || datatable.row_names == NULL ||
       datatable.column_names == NULL || datatable.feature_values == NULL ||
       column_icons == NULL || row_icons == NULL) return false;

    int c, r;
    QStringList classlist;
    QStringList featurelist;

    int* vectorsinclass = new int[datatable.column_count];
    for(c = 0; c < datatable.column_count; c++) vectorsinclass[c] = 0;

    int* vectorclassindex = new int[datatable.column_count];
    for(c = 0; c < datatable.column_count; c++) vectorclassindex[c] = -1;

    int vectornumber = 0;
    for(c = 0; c < datatable.column_count; c++)
    {
        //if(checked_vectors ? column_icons[c] > 0 : column_icons[c] < 0)
        if(((checked_mask & USE_CHECKED_VECTORS) && column_icons[c] > 0) || ((checked_mask & USE_UNCHECKED_VECTORS) && column_icons[c] < 0))

        {
            int index = classlist.indexOf(QString::fromStdString(datatable.column_names[c]));
            if(index < 0)
            {
                vectorsinclass[classlist.size()]++;
                vectorclassindex[c] = classlist.size();
                //classlist << datatable.column_names[c];
                classlist.append(QString::fromStdString(datatable.column_names[c]));
            }
            else
            {
                vectorsinclass[index]++;
                vectorclassindex[c] = index;
            }
            vectornumber++;
        }
    }
//    classlist.removeDuplicates();
    int classnumber = classlist.size();

    int featurenumber = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
//        if((checked_features ? row_icons[r] > 0 : row_icons[r] < 0))
        if(((checked_mask & USE_CHECKED_FEATURES) && row_icons[r] > 0) || ((checked_mask & USE_UNCHECKED_FEATURES) && row_icons[r] < 0))
        {
            featurelist.append(QString::fromStdString(datatable.row_names[r]));
            featurenumber++;
        }
    }

    outputdata->Reset();
    outputdata->classnumber = classnumber;
    outputdata->featurenumber = featurenumber;
    outputdata->vectornumber = vectornumber;

    outputdata->classendvectorindex = new int[classnumber];
    r = 0;
    for(c = 0; c < classnumber; c++)
    {
        r += vectorsinclass[c];
        vectorsinclass[c] = 0;
        outputdata->classendvectorindex[c] = r;
    }

    int v = 0;
    outputdata->values = new double[featurenumber*vectornumber];

    //vectorsinclass[c] = 0;
    //vectorclassindex[c] = index;
    //outputdata->classendvectorindex[c]
    for(c = 0; c < datatable.column_count; c++)
    {
        //if(checked_vectors ? column_icons[c] > 0 : column_icons[c] < 0)
        if(vectorclassindex[c] >= 0)
        {
            int classs = vectorclassindex[c];
            int vv = vectorsinclass[classs] + (classs == 0 ? 0 : outputdata->classendvectorindex[classs-1]);
            vectorsinclass[classs]++;

            if(vv >= vectornumber)
                vv = vectornumber-1;

            int f = 0;
            for(r = 0; r < datatable.row_count; r++)
            {
                //if(checked_features ? row_icons[r] > 0 : row_icons[r] < 0)
                if(((checked_mask & USE_CHECKED_FEATURES) && row_icons[r] > 0) || ((checked_mask & USE_UNCHECKED_FEATURES) && row_icons[r] < 0))

                {
                    outputdata->values[featurenumber*vv+f] = datatable.feature_values[datatable.row_count*c+r];
                    f++;
                }
            }
            v++;
        }
    }

    delete[] vectorclassindex;
    delete[] vectorsinclass;

    outputdata->classnames = new std::string[classnumber];
    for(c = 0; c < classnumber; c++)
    {
        outputdata->classnames[c] = classlist.at(c).toStdString();
    }

    outputdata->featurenames = new std::string[featurenumber];
    for(r = 0; r < featurenumber; r++)
    {
        outputdata->featurenames[r] = featurelist.at(r).toStdString();
    }

    outputdata->featureoriginalindex = new int[featurenumber];
    c = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
        //if((checked_features ? row_icons[r] > 0 : row_icons[r] < 0))
        if(((checked_mask & USE_CHECKED_FEATURES) && row_icons[r] > 0) || ((checked_mask & USE_UNCHECKED_FEATURES) && row_icons[r] < 0))
        {
            outputdata->featureoriginalindex[c] = r;
            c++;
        }
    }
/*
    printf("=========getCheckedData \n");
    for(c = 0; c < classnumber; c++)
    {
        printf("%s %i\n", outputdata->classnames[c].toStdString().c_str(), outputdata->classendvectorindex[c]);
    }
    printf("\n");
    for(r = 0; r < featurenumber; r++)
    {
        printf("%s %i\n", outputdata->featurenames[r].toStdString().c_str(), outputdata->featureoriginalindex[r]);
    }
    printf("\n");


    for(r = 0; r < featurenumber; r++)
    {
        for(c = 0; c < vectornumber; c++)
        {
            printf("%f ", (float)(outputdata->values[featurenumber*c+r]));
        }
        printf("\n");
    }
    printf("\n");
*/
    return true;
}






/*
bool MzTableModel::getCheckedData(int* vectornumber,
                                int* classnumber,
                                int* featurenumber,
                                int** vectorclassindex,
                                QString** classnames,
                                QString** featurenames,
                                double** values)
{
    if(datatable.column_count <= 0 || datatable.row_count <= 0 || datatable.row_names == NULL ||
       datatable.column_names == NULL || datatable.feature_values == NULL ||
       column_icons == NULL || row_icons == NULL) return false;

    int c, r, cc, rr;
    QStringList classlist;
    QStringList featurelist;

    *vectornumber = 0;
    for(c = 0; c < datatable.column_count; c++)
    {
        if(column_icons[c] > 0)
        {
            classlist << datatable.column_names[c];
            (*vectornumber)++;
        }
    }
    classlist.removeDuplicates();
    *classnumber = classlist.size();

    *featurenumber = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
        if(row_icons[r] > 0)
        {
            featurelist << datatable.row_names[r];
            (*featurenumber)++;
        }
    }

    *vectorclassindex = new int[*vectornumber];
    *classnames = new QString[*classnumber];
    *featurenames = new QString[*featurenumber];
    *values = new double[*featurenumber**vectornumber];

    cc = 0;
    for(c = 0; c < datatable.column_count; c++)
    {
        if(column_icons[c] > 0)
        {
            int i = classlist.indexOf(datatable.column_names[c]);
            (*vectorclassindex)[cc] = i;
            cc++;
        }
    }
    for(c = 0; c < *classnumber; c++)
    {
        (*classnames)[c] = classlist.at(c);
    }
    for(r = 0; r < *featurenumber; r++)
    {
        (*featurenames)[r] = featurelist.at(r);
    }

    rr = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
        if(row_icons[r] > 0)
        {
            cc = 0;
            for(c = 0; c < datatable.column_count; c++)
            {
                if(column_icons[c] > 0)
                {
                    (*values)[*featurenumber*cc+rr] = datatable.feature_values[datatable.row_count*c+r];
                    cc++;
                }
            }
            rr++;
        }
    }
    return true;
}
*/


int MzTableModel::saveToFile(QString fileName, bool saveall, bool hexa)
{
    if(saveall)
        return datatable.saveToFile(fileNameRemapWrite(&fileName), hexa, NULL, NULL);
    else
        return datatable.saveToFile(fileNameRemapWrite(&fileName), hexa, column_icons, row_icons);
}


int MzTableModel::loadFromFile(QString fileName)
{
    int t = datatable.loadFromFile(fileNameRemapRead(&fileName).c_str());
    if(t >= 0)
    {
        if(row_icons != NULL) delete[] row_icons;
        if(column_icons != NULL) delete[] column_icons;
        column_icons = new char[datatable.column_count];
        for(int c = 0; c < datatable.column_count; c++) column_icons[c] = 1;
        row_icons = new char[datatable.row_count];

        for(int r = 0; r < datatable.row_count; r++)
        {
            if(datatable.row_names[r].at(0) == '_') row_icons[r] = 0;
            else row_icons[r] = 1;
        }
    }
    return t;
}


int MzTableModel::rowCount(const QModelIndex & /*parent*/) const
{
    return datatable.row_count;
}

int MzTableModel::columnCount(const QModelIndex & /*parent*/) const
{
    return datatable.column_count;
}

QVariant MzTableModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();
    switch(role)
    {
    case Qt::EditRole:
    case Qt::DisplayRole:
        if(col < datatable.column_count && row < datatable.row_count && datatable.feature_values != NULL)
            return QString("%1").arg(datatable.feature_values[datatable.row_count*col+row]);
        break;
    case Qt::BackgroundRole:
        if(col < datatable.column_count && row < datatable.row_count && datatable.feature_values != NULL)
        {
            double d = datatable.feature_values[datatable.row_count*col+row];
            if(!(d == d)) //nan
            {
                QBrush redBackground(Qt::red);
                return redBackground;
            }
        }
        break;
    }
    return QVariant();
}

QVariant MzTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal)
    {
        switch(role)
        {
        case Qt::DisplayRole:
            if(section < datatable.column_count && datatable.column_names != NULL)
                return QString::fromStdString(datatable.column_names[section]);
            break;

        case Qt::DecorationRole:
            if(section < datatable.column_count && column_icons != NULL)
            {
                if(column_icons[section] > 0) return icon_v;
                else if(column_icons[section] < 0) return icon_o;
                else return icon_z;
            }
            break;

        case Qt::ToolTipRole:
            if(section < datatable.column_count && datatable.column_names != NULL)
                return QString::fromStdString(datatable.column_tips[section]);
            break;
        }
    }

    if (orientation == Qt::Vertical)
    {
        switch(role)
        {
        case Qt::DisplayRole:
            if(section < datatable.row_count && datatable.row_names != NULL)
                return QString::fromStdString(datatable.row_names[section]);
            break;

        case Qt::DecorationRole:
            if(section < datatable.row_count && row_icons != NULL)
            {
                if(row_icons[section] > 0) return icon_v;
                else if(row_icons[section] < 0) return icon_o;
                else return icon_z;
            }
            break;
        }
    }
    return QVariant();
}

void MzTableModel::setColumnName(int section, const QString& value)
{
    if(section < datatable.column_count)
    {
        datatable.column_names[section] = value.toStdString();
    }
}

void MzTableModel::horizontalHeaderIconClick(int section)
{
    if(section < datatable.column_count)
    {
        column_icons[section] = -column_icons[section];
        emit headerDataChanged(Qt::Horizontal, section, section);
    }
}
void MzTableModel::verticalHeaderIconClick(int section)
{
    if(section < datatable.row_count)
    {
        row_icons[section] = -row_icons[section];
        emit headerDataChanged(Qt::Vertical, section, section);
    }
}

void MzTableModel::checkAllVectors(bool check)
{
    if(check)
    {
        for(int i = 0; i < datatable.column_count; i++)
        {
            if(column_icons[i] < 0) column_icons[i] = -column_icons[i];
        }
    }
    else
    {
        for(int i = 0; i < datatable.column_count; i++)
        {
            if(column_icons[i] > 0) column_icons[i] = -column_icons[i];
        }
    }
    headerDataChanged(Qt::Horizontal, 0, datatable.column_count-1);
}
void MzTableModel::reverseAllVectorChecks(void)
{
    for(int i = 0; i < datatable.column_count; i++)
    {
        column_icons[i] = -column_icons[i];
    }
    headerDataChanged(Qt::Horizontal, 0, datatable.column_count-1);
}
int MzTableModel::isVectorChecked(int i)
{
    if(i < datatable.column_count) return column_icons[i];
    else return 0;
}
bool MzTableModel::checkVector(int i, bool check)
{
    if(i < datatable.column_count)
    {
        if(check)
        {
            if(column_icons[i] < 0) column_icons[i] = -column_icons[i];;
        }
        else
        {
            if(column_icons[i] > 0) column_icons[i] = -column_icons[i];;
        }
        headerDataChanged(Qt::Horizontal, i, i);
        return true;
    }
    else return false;
}
void MzTableModel::checkAllFeatures(bool check)
{
    if(check)
    {
        for(int i = 0; i < datatable.row_count; i++)
        {
            if(row_icons[i] < 0) row_icons[i] = -row_icons[i];
        }
    }
    else
    {
        for(int i = 0; i < datatable.row_count; i++)
        {
            if(row_icons[i] > 0) row_icons[i] = -row_icons[i];
        }
    }
    headerDataChanged(Qt::Vertical, 0, datatable.row_count-1);
}
void MzTableModel::reverseAllFeatureChecks(void)
{
    for(int i = 0; i < datatable.row_count; i++)
    {
        row_icons[i] = -row_icons[i];
    }
    headerDataChanged(Qt::Vertical, 0, datatable.row_count-1);
}
int MzTableModel::isFeatureChecked(int i)
{
    if(i < datatable.row_count) return row_icons[i];
    else return 0;
}

bool MzTableModel::checkFeature(int i, bool check)
{
    if(i < datatable.row_count)
    {
        if(check)
        {
            if(row_icons[i] < 0) row_icons[i] = -row_icons[i];;
        }
        else
        {
            if(row_icons[i] > 0) row_icons[i] = -row_icons[i];;
        }
        headerDataChanged(Qt::Vertical, i, i);
        return true;
    }
    else return false;
}

void MzTableModel::selectFromFile(QString fileName)
{
    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        QString stru;

        while(!in.atEnd())
        {
            in >> stru;
            QStringList list = stru.split(QRegExp("[,:;\"]"));
            foreach (const QString &str, list)
            {
                for(int r = 0; r < datatable.row_count; r++)
                {
                    if(datatable.row_names[r] == str.toStdString())
                        checkFeature(r, true);
                }
            }
        }
    }
}

void MzTableModel::selectFromString(QString names)
{
    QStringList list = names.split(QRegExp("[ \t\r\n,:;\"]"));
    foreach (const QString &str, list)
    {
        for(int r = 0; r < datatable.row_count; r++)
        {
            if(datatable.row_names[r] == str.toStdString())
                checkFeature(r, true);
        }
    }
}

bool MzTableModel::fromDataTableToErase(DataTable* newtable)
{
    if(newtable == NULL) return false;
    if( newtable->column_count == 0 ||
        newtable->row_count == 0 ||
        newtable->row_names == NULL ||
        newtable->column_names == NULL ||
        newtable->column_tips == NULL ||
        newtable->feature_values == NULL) return false;

    beginResetModel();
    datatable.Reset();
    datatable = *newtable;
    newtable->column_count = 0;
    newtable->row_count = 0;
    newtable->row_names = NULL;
    newtable->column_names = NULL;
    newtable->column_tips = NULL;
    newtable->feature_values = NULL;

    if(row_icons != NULL) delete[] row_icons;
    if(column_icons != NULL) delete[] column_icons;
    column_icons = new char[datatable.column_count];
    for(int c = 0; c < datatable.column_count; c++) column_icons[c] = 1;
    row_icons = new char[datatable.row_count];

    for(int r = 0; r < datatable.row_count; r++)
    {
        if(datatable.row_names[r].at(0) == '_') row_icons[r] = 0;
        else row_icons[r] = 1;
    }
    endResetModel();
    return true;
}




bool MzTableModel::appendColumns(DataTable* newtable)
{
    int r, rn, c, cn;
    if(datatable.row_count == 0 || datatable.column_count == 0)
    {
        return fromDataTableToErase(newtable);
    }
    int common_count = 0;
    int* common = new int[datatable.row_count];
    for(r = 0; r < datatable.row_count; r++)
    {
        common[r] = -1;
        for(rn = 0; rn < newtable->row_count; rn++)
        {
            if(datatable.row_names[r] == newtable->row_names[rn])
            {
                common[r] = rn;
                common_count++;
            }
        }
    }
    if(common_count <= 0)
    {
        delete[] common;
        return false;
    }

    DataTable finaltable;
    finaltable.row_count = common_count;
    finaltable.column_count = datatable.column_count + newtable->column_count;
    finaltable.column_names = new std::string[finaltable.column_count];
    finaltable.column_tips = new std::string[finaltable.column_count];
    finaltable.row_names = new std::string[finaltable.row_count];
    finaltable.feature_values = new double[finaltable.column_count * finaltable.row_count];

    char* final_row_icons = new char[common_count];
    char* final_column_icons = new char[datatable.column_count + newtable->column_count];

    rn = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
        if(common[r] >= 0)
        {
            finaltable.row_names[rn] = datatable.row_names[r];
            if(row_icons != NULL) final_row_icons[rn] = row_icons[r];
            else
            {
                if(finaltable.row_names[rn].at(0) == '_') final_row_icons[rn] = 0;
                else final_row_icons[rn] = 1;
            }
            rn++;
        }
    }

    cn = datatable.column_count;
    for(c = 0; c < cn; c++)
    {
        finaltable.column_names[c] = datatable.column_names[c];
        finaltable.column_tips[c] = datatable.column_tips[c];
        if(column_icons != NULL) final_column_icons[c] = column_icons[c];
        else final_column_icons[c] = 1;
    }
    for(c = 0; c < newtable->column_count; c++)
    {
        finaltable.column_names[cn + c] = newtable->column_names[c];
        finaltable.column_tips[cn + c] = newtable->column_tips[c];
        final_column_icons[cn + c] = 1;
    }

    rn = 0;
    for(r = 0; r < datatable.row_count; r++)
    {
        if(common[r] >= 0)
        {
            for(c = 0; c < cn; c++)
            {
                finaltable.feature_values[finaltable.row_count*c+rn] = datatable.feature_values[datatable.row_count*c+r];
            }
            for(c = 0; c < newtable->column_count; c++)
            {
                finaltable.feature_values[finaltable.row_count*(cn+c)+rn] = newtable->feature_values[newtable->row_count*c+common[r]];
            }
            rn++;
        }
    }

    beginResetModel();

    datatable.Reset();
    if(row_icons != NULL) delete[] row_icons;
    row_icons = final_row_icons;
    if(column_icons != NULL) delete[] column_icons;
    column_icons = final_column_icons;

    datatable = finaltable;
    finaltable.column_count = 0;
    finaltable.row_count = 0;
    finaltable.row_names = NULL;
    finaltable.column_names = NULL;
    finaltable.column_tips = NULL;
    finaltable.feature_values = NULL;

    delete[] common;
    if(folds != NULL) delete[] folds;
    folds = NULL;
    numberoffolds = 0;
    foldmask = 0;

    endResetModel();

    //?? skasować foldy
    return true;
}


bool MzTableModel::appendRows(DataTable* newtable)
{
    int r, c;

    if(datatable.row_count == 0 || datatable.column_count == 0)
    {
        return fromDataTableToErase(newtable);
    }

    if(newtable->column_count != datatable.column_count) return false;

    for(r = 0; r < datatable.row_count; r++)
    {
        for(c = 0; c < newtable->row_count; c++)
        {
            if(datatable.row_names[r] == newtable->row_names[c])
            {
                return false;
            }
        }
    }
    for(c = 0; c < datatable.column_count; c++)
    {
        if(!(newtable->column_names[c] == datatable.column_names[c])) return false;
    }

    DataTable finaltable;
    finaltable.row_count = datatable.row_count + newtable->row_count;
    finaltable.column_count = datatable.column_count;
    finaltable.column_names = datatable.column_names;
    finaltable.column_tips = datatable.column_tips;
    finaltable.row_names = new std::string[finaltable.row_count];
    finaltable.feature_values = new double[finaltable.column_count*finaltable.row_count];
    char* final_row_icons = new char[finaltable.row_count];

    c = datatable.row_count;

    for(r = 0; r < c; r++)
    {
        finaltable.row_names[r] = datatable.row_names[r];
        final_row_icons[r] = row_icons[r];
    }

    for(r = 0; r < newtable->row_count; r++)
    {
        finaltable.row_names[c+r] = newtable->row_names[r];
        if(newtable->row_names[r].at(0) == '_') final_row_icons[c+r] = 0;
        else final_row_icons[c+r] = 1;
    }

    for(c = 0; c < datatable.column_count; c++)
    {
        for(r = 0; r < datatable.row_count; r++)
        {
            finaltable.feature_values[finaltable.row_count*c+r] = datatable.feature_values[datatable.row_count*c+r];
        }
        for(r = 0; r < newtable->row_count; r++)
        {
            finaltable.feature_values[finaltable.row_count*c+(datatable.row_count+r)] = newtable->feature_values[newtable->row_count*c+r];
        }
    }
    beginResetModel();
    delete[] row_icons;
    row_icons = final_row_icons;

    if(datatable.row_names != NULL) delete[] datatable.row_names;
    datatable.row_count = finaltable.row_count;
    datatable.row_names = finaltable.row_names;
    if(datatable.feature_values != NULL) delete[] datatable.feature_values;
    datatable.feature_values = finaltable.feature_values;

    finaltable.column_count = 0;
    finaltable.row_count = 0;
    finaltable.row_names = NULL;
    finaltable.column_names = NULL;
    finaltable.column_tips = NULL;
    finaltable.feature_values = NULL;
    endResetModel();
    return true;
}



















