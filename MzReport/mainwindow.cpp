/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../MzShared/mzdefines.h"
#include <time.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "plot3d.h"
#include "../MzShared/dataforconsole.h"
#include "propertyvalue.h"
#include "foldsdialog.h"
#include "addnoise.h"
#include "../MzShared/testdialog.h"
#include "selectclassifiers.h"
#include "../MzShared/propertyvaluetree.h"
#include "../MzShared/filenameremap.h"

//bool MainWindow::plugins_loaded = false;
#include <QDesktopServices>
#include <sstream>

#include <QFileDialog>
#include <QLibrary>
#include <QObject>

//------------------------------------------------------------------
//                OBSLUGA WATKU
//------------------------------------------------------------------

void MainWindow::InitiateAnalysisActions(void* plugin_object,
                                         MzBeforeStaticPluginFunction before_plugin,
                                         MzThreadStaticPluginFunction threaded_plugin,
                                         MzAfterStaticPluginFunction after_plugin)
{
    this->plugin_object = plugin_object;
    this->after_plugin = after_plugin;
    if((*before_plugin)(plugin_object))
    {
        PlugWorker* worker = new PlugWorker(plugin_object, threaded_plugin);
        QThread* thread = new QThread;
        worker->moveToThread(thread);
        connect(thread, SIGNAL(started()), worker, SLOT(process()));
        connect(worker, SIGNAL(finished(bool)), this, SLOT(on_finished(bool)));
        connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
        connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
        connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
        connect(worker, SIGNAL(notify_progress(void)), this, SLOT(notifyProgressSlot(void)));
        connect(worker, SIGNAL(notify_progress(QString)), this, SLOT(notifyProgressSlot(QString)));
        thread->start();
    }
}
void MainWindow::on_finished(bool /*success*/)
{
    //printf("IN on_finished %i\n", (int) bb);
    (*after_plugin)(plugin_object);
}


void MainWindow::notifyProgressSlot(void)
{
    progressdialog->NotifyProgressStep();
}
void MainWindow::notifyProgressSlot(QString message)
{
    progressdialog->NotifyProgressText(message);
}


//------------------------------------------------------------------
//                PLUGIN INTERFACE
//------------------------------------------------------------------

//bool MainWindow::mzpconnect(const QObject *sender, const char *signal, const QObject *receiver, const char *member)
//{
//    return (connect(sender, signal, receiver, member) != NULL);
//}

//void MainWindow::mzpconnectall(QObject *worker, QObject *receiver, const char *analysisSlot, const char *finishSlot,
//                                quint64 maxSteps, int timerestriction, const char *title )
//{
//    QThread* thread = new QThread;
//    worker->moveToThread(thread);
//    openProgressDialog(timerestriction, maxSteps, receiver, title);
//    connect(thread, SIGNAL(started()), worker, analysisSlot);
//    connect(worker, SIGNAL(finished(bool)), receiver, finishSlot);
//    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
//    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
//    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
//    connect(worker, SIGNAL(notifyProgressStep(void)), receiver, SLOT(NotifyProgressStep(void)));
//    connect(worker, SIGNAL(notifyProgressText(QString)), receiver, SLOT(NotifyProgressText(QString)));
//    thread->start();
//}

bool MainWindow::selectClassifiers(std::vector<std::string>* classifierNames, std::vector<bool>* selectionResult, std::string title)
{
    SelectClassifiers dialog(this);
    if(title.length() > 0) dialog.setWindowTitle(QString::fromStdString(title));
    dialog.setOptions(classifierNames, selectionResult);
    if(dialog.exec() == QDialog::Accepted) return true;
    return false;
}
bool MainWindow::openProgressDialog(int maxTime, mz_uint64 maxSteps, void *object, string title)
{
    progressdialog = new Progress(this);
    if(progressdialog == NULL) return false;
    if(title.length() > 0) progressdialog->setWindowTitle(QString::fromStdString(title));
    progressdialog->setMaxValues(maxTime, maxSteps, (QObject*) object);
    //progressdialog->setWindowModality(Qt::WindowModal);
    progressdialog->show();
#ifdef MAZDA_HIDE_ON_PROGRESS
    this->hide();
    progressdialog->setWindowModality(Qt::NonModal);
#else
    progressdialog->setWindowModality(Qt::ApplicationModal);//(Qt::WindowModal);
#endif
    return true;
}
void MainWindow::closeProgressDialog(void)
{
    progressdialog->close();
    delete progressdialog;
    progressdialog = NULL;
#ifdef MAZDA_HIDE_ON_PROGRESS
    this->show();
#endif
}
bool MainWindow::openOptionsDialog(std::string title)
{
    if(optionsdialog == NULL) optionsdialog = new PropertyValueTree(this);
    if(title.length() > 0) optionsdialog->setWindowTitle(QString::fromStdString(title));
    optionsdialog->CreateEnd();
    optionsdialog->setWindowModality(Qt::WindowModal);
    if(optionsdialog->exec() == QDialog::Accepted) return true;
    return false;

//    optionsdialog->show();
//    return true;
}
void MainWindow::addGroupOptionsDialog(std::string property, std::string tip)
{
    if(optionsdialog == NULL) optionsdialog = new PropertyValueTree(this);
    optionsdialog->CreateGroup(property, tip);
}
void MainWindow::addPropertyOptionsDialog(std::string property, std::string value, std::string tip)
{
    if(optionsdialog == NULL) optionsdialog = new PropertyValueTree(this);
    optionsdialog->CreateOption(property, value, tip);
}
void MainWindow::getValueOptionsDialog(std::string property, std::string* value)
{
    QString val = optionsdialog->getValue(QString::fromStdString(property));
    *value = val.toStdString();
}

void MainWindow::closeOptionsDialog(void)
{
    if(optionsdialog != NULL)
    {
        optionsdialog->close();
        delete optionsdialog;
        optionsdialog = NULL;
    }
}


//bool MainWindow::getOptions(std::vector<std::string>* property, std::vector<std::string>* value)
//{
//    LearningOptions dialog(this);
//    unsigned int maxp = 0;
//    unsigned int maxv = 0;
//    if(property != NULL) maxp = property->size();
//    if(value != NULL) maxv = value->size();
//    if(maxp > maxv) maxp = maxv;

//    for(unsigned int i = 0; i < maxp; i++)
//    {
//        dialog.CreateOption((*property)[i], (*value)[i]);
//    }
//    if(dialog.exec() == QDialog::Accepted)
//    {
//        value->clear();
//        for(unsigned int i = 0; i < maxp; i++)
//        {
//            value->push_back(dialog.getValue(i).toStdString());
//        }
//        return true;
//    }
//    return false;
//}

void MainWindow::showTestResults(std::string* confusionTable, std::string title)
{
    TestDialog testdialog;
    if(title.length() > 0) testdialog.setWindowTitle(QString::fromStdString(title));
    testdialog.setTableData(confusionTable);
    testdialog.exec();
}
void MainWindow::showAbout(std::string title, std::string text)
{
    QMessageBox::about(this, QString::fromStdString(title), QString::fromStdString(text));
}
void MainWindow::showMessage(std::string title, std::string text, unsigned int iconType)
{
    QMessageBox msgBox;
    msgBox.setText(QString::fromStdString(text));
    msgBox.setWindowTitle(QString::fromStdString(title));
    msgBox.setIcon((QMessageBox::Icon) iconType);
    msgBox.exec();
}
bool MainWindow::getOpenFile(std::string* fileName)
{
    QString filter;
    QString fileNameTemp = QFileDialog::getOpenFileName(this,
                                                 tr("Load classifier"),
                                                 NULL,//domyslna nazwa pliku
                                                 tr("Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)"),
                                                 &filter);
    if (!fileNameTemp.isEmpty())
    {
        *fileName = fileNameRemapRead(&fileNameTemp).c_str();//fileNameTemp.toStdString();
        return true;
    }
    return false;
}

bool MainWindow::getSaveFile(std::string* fileName, unsigned int *filter)
{
    QString sfilter;
    QString fileNameTemp = QFileDialog::getSaveFileName(this,
                                                 tr("Save classifier"),
                                                 NULL,//domyslna nazwa pliku
                                                 tr("Text file (*.txt) (*.txt);;Double precision hexadecimals (*.txtv) (*.txtv);;All (*) (*)"),
                                                 &sfilter);
    if(sfilter == "Double precision hexadecimals (*.txtv) (*.txtv)") *filter = 1;
    else *filter = 0;

    if (!fileNameTemp.isEmpty())
    {
        *fileName = fileNameRemapWrite(&fileNameTemp).c_str();//fileNameTemp.toStdString();
        return true;
    }
    return false;
}

void MainWindow::pluginMenuTriggered()
{
    MzAction* plugin_action = qobject_cast<MzAction*> (sender());
    plugin_action->interfaceMzSelectionPluginInterface->callBack(plugin_action->indexToOnActionCall);
}


void *MainWindow::addMenuAction(const char* name, const char* tip, const unsigned int index)
{
    if(plugin_menu == NULL) return NULL;

    if(name == NULL)
    {
        plugin_menu->addSeparator();
        return NULL;
    }

    MzAction* plugin_action = new MzAction(tr(name), this);
    plugin_action->setStatusTip(tr(tip));
    plugin_action->interfaceMzSelectionPluginInterface = selectionplugin;
    plugin_action->indexToOnActionCall = index;

    plugin_menu->addAction(plugin_action);
    connect(plugin_action, SIGNAL(triggered()), this, SLOT(pluginMenuTriggered()));
    return plugin_action;
}

void MainWindow::menuEnable(void* menu, bool enable)
{
    ((QAction*)menu)->setEnabled(enable);
}

//------------------------------------------------------------------
//                DATA TRANSFER INTERFACE
//------------------------------------------------------------------


bool MainWindow::setData(DataForSelection* data, SelectedFeatures *selected_features)
{
    MainWindow* w = new MainWindow;
    w->show();
    if(w->setDataHere(data))
    {
        return w->setSelection(selected_features);
    }
    return false;
}

bool MainWindow::getData(DataForSelection* data)
{
    int mask = USE_CHECKED_VECTORS;
    mask |= USE_CHECKED_FEATURES;
    // mask |= USE_UNCHECKED_FEATURES;

    if((MzTableModel*)ui->tableView->model() == NULL)
        return false;
    return ((MzTableModel*)ui->tableView->model())->getRequiredData(data, mask);
}

bool MainWindow::getData(DataForSegmentation* /*data*/)
{
    return false;
}

bool MainWindow::getData(std::vector<std::string>* featureNames, DataForSelection* data)
{
    if((MzTableModel*)ui->tableView->model() == NULL)
        return false;
    return ((MzTableModel*)ui->tableView->model())->getRequiredData(featureNames, data);
}

bool MainWindow::setSelection(SelectedFeatures* selected_features)
{
//    if(! ui->actionUse_unchecked->isChecked())
    ((MzTableModel*)ui->tableView->model())->checkAllFeatures(false);

    int count = selected_features->featurenumber;
    for(int i = 0; i < count; i++)
    {
        int in = selected_features->featureoriginalindex[i];
        ((MzTableModel*)ui->tableView->model())->checkFeature(in);
    }
    return true;
}


//------------------------------------------------------------------
//                MAIN WINDOW
//------------------------------------------------------------------

void MainWindow::on_actionAbout_triggered()
{
    std::stringstream ss;
    ss << "<h2>qmazda MzReport</h2> " << std::endl;
    ss << QMAZDA_VERSION << "<br>" << std::endl;
    ss << QMAZDA_COPYRIGHT << "<br>" << std::endl;
    ss << "Built on " << __DATE__ << " at " << __TIME__ << "<br> <br>" << std::endl;
    ss << "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE." << "<br> <br>" << std::endl;
    ss << "Home: <a href=\"http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html\"> http://www.eletel.p.lodz.pl/pms/SoftwareQmazda.html </a> <br>" << std::endl;
    ss << "Sources: <a href=\"https://gitlab.com/qmazda/qmazda\"> https://gitlab.com/qmazda/qmazda </a> <br>" << std::endl;
    ss << "<br>Built with:<br>" << std::endl;
    ss << "- Qt " << QT_VERSION_STR << " <a href=\"https://www.qt.io/developers/\">https://www.qt.io/developers/</a> <br>" << std::endl;

    QMessageBox::about(this, "About MzReport", ss.str().c_str());
}

void MainWindow::on_actionContents_triggered()
{
    QDir pluginsDir(qApp->applicationDirPath());
#if defined(__APPLE__) && defined(MAZDA_IN_MACOS_BUNDLE)
    pluginsDir.cdUp();
    pluginsDir.cdUp();
    pluginsDir.cdUp();
#endif
    QDesktopServices::openUrl(QUrl::fromLocalFile(pluginsDir.absolutePath() + "/qmazda.pdf"));
}

//void MainWindow::on_actionContents_triggered()
//{
//    QDesktopServices::openUrl(QUrl(getExecutableWithPath("/qmazda.pdf"), QUrl::TolerantMode));
//}

//QString MainWindow::getExecutableWithPath(const char* name)
//{
//    QDir execDir(qApp->applicationDirPath());
//    return execDir.absolutePath() + QString(name);
//}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    temporary_edit = NULL;
    edited_section = -1;
    progressdialog = NULL;
    optionsdialog = NULL;
    previous_model = NULL;
    connect(ui->tableView->horizontalHeader(), SIGNAL(sectionDoubleClicked (int)), SLOT(headerDoubleClicked(int)));
    connect(ui->tableView->verticalHeader(), SIGNAL(sectionDoubleClicked (int)), SLOT(verticalDoubleClicked(int)));

    loadPlugins();
    if(selectionplugins.count() > 0) initiatePlugins();

    if(QApplication::arguments().size() >= 2)
    {
        QString fn = QApplication::arguments()[1];
        if(openReport(fn))
        {
            if(QDir::tempPath() == fn.mid(0, QDir::tempPath().length()))
            {
                QFile::remove(fn);
            }
        }
    }
    plugin_menu = NULL;
}

void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    // if some actions should not be usable, like move, this code must be adopted
    /*
    const QMimeData* mimeData = event->mimeData();
    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();
        if(urlList.size() == 1)
        {
            event->acceptProposedAction();
        }
    }
    */
    event->acceptProposedAction();
}

void MainWindow::dragMoveEvent(QDragMoveEvent* event)
{
    // if some actions should not be usable, like move, this code must be adopted
    /*
    const QMimeData* mimeData = event->mimeData();
    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();
        if(urlList.size() == 1)
        {
            event->acceptProposedAction();
        }
    }*/
    event->acceptProposedAction();
}


void MainWindow::dragLeaveEvent(QDragLeaveEvent* event)
{
    event->accept();
}

void MainWindow::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();

    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();

        //if(urlList.size() >= 1)
        for (int i = 0; i < urlList.size(); ++i)
        {
            QString filename = urlList.at(i).toLocalFile();
            MzSelectionPluginInterface* selectionplugin;
            foreach(selectionplugin, selectionplugins)
            {
                std::string sfn = fileNameRemapRead(&filename);
                if(selectionplugin->openFile(&sfn))
                {
                    event->acceptProposedAction();
                    return;
                };
            }
            if(appendReport(filename)) event->acceptProposedAction();
        }
/*
        QStringList pathList;
        for (int i = 0; i < urlList.size() && i < 1; ++i)
        {
            pathList.append(urlList.at(i).toLocalFile());
        }

        if(openFiles(pathList))
            event->acceptProposedAction();
*/
    }
}

bool MainWindow::setDataHere(DataForSelection* data)
{
    MzTableModel* model = new MzTableModel(0);
    if(model->setData(data))
    {
        ui->tableView->setModel(model);
        ui->tableView->show();
        if(previous_model != NULL) delete previous_model;
        previous_model = model;
    }
    else delete model;
    return true;
}

bool MainWindow::initiatePlugins(void)
{
    foreach(selectionplugin, selectionplugins)
    {
        if(selectionplugin == NULL) continue;
        plugin_menu = new QMenu(ui->menuAnalysis);
        plugin_menu->setObjectName("plugin_menu");
        ui->menuAnalysis->addAction(plugin_menu->menuAction());
        plugin_menu->setTitle(selectionplugin->getName());
        if(! selectionplugin->initiateTablePlugin(
                dynamic_cast<MzPullDataInterface*>(this),
                dynamic_cast<MzGuiRelatedInterface*>(this)))
            delete plugin_menu;
    }
    plugin_menu = NULL;
    return true;
}

void MainWindow::loadPlugins(void)
{
    QDir pluginsDir(qApp->applicationDirPath());

/*
#if defined(Q_OS_WIN)
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
        pluginsDir.cdUp();
#elif defined(Q_OS_MAC)
    if (pluginsDir.dirName() == "MacOS") {
        pluginsDir.cdUp();
        pluginsDir.cdUp();
        pluginsDir.cdUp();
    }
#endif
    pluginsDir.cd("plugins");
*/

#if defined(__APPLE__) && defined(MAZDA_IN_MACOS_BUNDLE)
    pluginsDir.cdUp();
    pluginsDir.cdUp();
    pluginsDir.cdUp();
#endif
    foreach (QString fileName, pluginsDir.entryList(QDir::Files))
    {
        QString fiext = QFileInfo(fileName).suffix();
        if(fiext != "so" && fiext != "dll" &&fiext != "dylib")
            continue;

        MzSelectionPluginInterface* plugin_interface = NULL;
        QLibrary* lib = new QLibrary(pluginsDir.absoluteFilePath(fileName));
//        printf("File: %s\n", fileName.toStdString().c_str());
        lib->load();
        if(lib->isLoaded())
        {
//            printf("File: %s loaded\n", fileName.toStdString().c_str());
//            fflush(stdout);
            MzNewPluginFunction functionNew = (MzNewPluginFunction) lib->resolve("MzNewPluginObject");
//            printf("Resolve: %i\n", functionNew);
//            fflush(stdout);
            if (functionNew != NULL)
            {
//                printf("Function: %i\n", functionNew);
//                fflush(stdout);
                //interface = static_cast<MzSelectionPluginInterface*>(pointer);
                plugin_interface = (MzSelectionPluginInterface*) functionNew();
                if (plugin_interface != NULL)
                {
                    selectionplugins.append(plugin_interface);
//                    printf("Interface: %s %i %i\n", fileName.toStdString().c_str(), plugin_interface, pointer);
//                    fflush(stdout);
                }
            }


        }
        if(plugin_interface == NULL)
        {
            lib->unload();
            delete lib;
        }


//        QPluginLoader pluginLoader(pluginsDir.absoluteFilePath(fileName));
//        QObject *plugin = pluginLoader.instance();
//        if (plugin)
//        {
//            MzSelectionPluginInterfaceRoot* plugin_interface = qobject_cast<MzSelectionPluginInterfaceRoot*>(plugin);
//            if (plugin_interface)
//            {
//                selectionplugins.append(plugin_interface->createInstance());
//            }
//        }
    }
    //fflush(stdout);
}

MainWindow::~MainWindow()
{
    delete ui;
    //MzSelectionPluginInterface* selectionplugin;
    foreach(selectionplugin, selectionplugins)
    {
        delete selectionplugin;
    }
}

void MainWindow::headerDoubleClicked(int index)
{
    int icon_right = ui->tableView->horizontalHeader()->style()->pixelMetric(QStyle::PM_SmallIconSize);
    icon_right += ui->tableView->horizontalHeader()->style()->pixelMetric(QStyle::PM_HeaderMargin);

    QPoint mouse = ui->tableView->horizontalHeader()->mapFromGlobal(QCursor::pos());
    int pos = ui->tableView->horizontalHeader()->sectionViewportPosition(index);
    if(mouse.x() - pos < icon_right)
    {
        ((MzTableModel*)ui->tableView->model())->horizontalHeaderIconClick(index);
        return;
    }
    else
    {
        edited_section = index;
        int width = ui->tableView->horizontalHeader()->sectionSize(index);
        QSize size = ui->tableView->horizontalHeader()->sizeHint();
        if(temporary_edit == NULL) temporary_edit = new QLineEdit(ui->tableView->horizontalHeader());
        temporary_edit->setGeometry(pos+icon_right, 0, width-icon_right, size.height());
        temporary_edit->setText(ui->tableView->model()->headerData(index, Qt::Horizontal, Qt::DisplayRole).toString());
        connect(temporary_edit, SIGNAL(editingFinished()), SLOT(temporaryEditingFinished()));
        temporary_edit->show();
        temporary_edit->setFocus();
    }
}

void MainWindow::verticalDoubleClicked(int index)
{
    ((MzTableModel*)ui->tableView->model())->verticalHeaderIconClick(index);
}

void MainWindow::temporaryEditingFinished(void)
{
    if(temporary_edit != NULL && edited_section >= 0)
    {
        ((MzTableModel*)ui->tableView->model())->setColumnName(edited_section, temporary_edit->text());

        //ui->tableView->model()->setHeaderData(edited_section, Qt::Horizontal, temporary_edit->text(), Qt::EditRole);
        temporary_edit->hide();//close();
        //delete temporary_edit;
    }
    //temporary_edit = NULL;
}

void MainWindow::on_actionScatter_plot_triggered()
{
/*
    int vectornumber;
    int classnumber;
    int featurenumber;
    int* vectorclassindex;
    QString* classnames;
    QString* featurenames;
    double* values;
*/
    if(ui->tableView->model() == NULL) return;

    DataForSelection dataforselection;
    int mask = USE_CHECKED_VECTORS;
    //mask |= USE_UNCHECKED_FEATURES;
    mask |= USE_CHECKED_FEATURES;
    ((MzTableModel*)ui->tableView->model())->getRequiredData(&dataforselection, mask);
    //((MzTableModel*)ui->tableView->model())->getCheckedData(&vectornumber, &classnumber, &featurenumber, &vectorclassindex, &classnames, &featurenames, &values);
    Plot3D dialog(this);
    dialog.setDataForSelection(&dataforselection);
    //dialog.setDataForSelection(vectornumber, classnumber, featurenumber, vectorclassindex, classnames, featurenames, values);
    //dialog.setModal(Qt::WindowModal);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setWindowTitle(QString("%1 - %2").arg(openFileName).arg(tr("MZScatterPlot")));
    dialog.exec();

    SelectedFeatures selected;
    if(dialog.getSelectedFeatures(&selected))
    {
        ((MzTableModel*)ui->tableView->model())->checkAllFeatures(false);
        for(int i = 0; i < selected.featurenumber; i++)
        {
            ((MzTableModel*)ui->tableView->model())->checkFeature(selected.featureoriginalindex[i]);
        }
    }
}


bool MainWindow::openReport(QString fileName)
{
    bool ret;
    QCursor old = cursor();
    setCursor(Qt::WaitCursor);
    MzTableModel* model = new MzTableModel(0);
    int r = model->loadFromFile(fileName);
    if(r > 0)
    {
        ui->tableView->setModel(model);
        ui->tableView->show();
        if(previous_model != NULL) delete previous_model;
        previous_model = model;
        openFileName = QFileInfo(fileName).fileName();
        this->setWindowTitle(QString("%1 - %2").arg(openFileName).arg(tr("MzReport")));
        ret = true;
    }
    else
    {
        delete model;
        QMessageBox msgBox;
        msgBox.setText(QString("Error loading file %1.").arg(fileName));
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
        ret = false;
    }
    setCursor(old);
    return ret;
}


bool MainWindow::appendReport(QString fileName)
{
    bool ret = false;
    if(previous_model == NULL) return openReport(fileName);
    QCursor old = cursor();
    setCursor(Qt::WaitCursor);
    DataTable dt;
    int r = dt.loadFromFile(fileNameRemapRead(&fileName).c_str());
    if(r > 0)
    {
        bool b;
        if(ui->tableView->model() == NULL)
        {
            MzTableModel* model = new MzTableModel(0);
            b = model->appendColumns(&dt);
            if(b)
            {
                ui->tableView->setModel(model);
                ui->tableView->show();
                if(previous_model != NULL) delete previous_model;
                previous_model = model;
            }
            else delete model;
        }
        else
        {
            MzTableModel* model = (MzTableModel*)ui->tableView->model();
            b = model->appendColumns(&dt);
            ui->tableView->show();
        }

        if(!b)
        {
            QMessageBox msgBox;
            msgBox.setText(QString("Cannot append columns for %1.\nFeature names differ.").arg(fileName));
            msgBox.setWindowTitle("Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
        }
        else ret = true;
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText(QString("Error loading file %1.").arg(fileName));
        msgBox.setWindowTitle("Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
    setCursor(old);
    return ret;
}

void MainWindow::on_actionOpen_report_triggered()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this,
                                                    tr("Load or append"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Comma separated vectors(*.csv) (*.csv);;All Files (*)"));

    for (QStringList::const_iterator fileName = fileNames.constBegin(); fileName != fileNames.constEnd(); ++fileName)
    {
        int column_count = 0;
        if(ui->tableView->model() != NULL)
            column_count = ((MzTableModel*)ui->tableView->model())->columnCount();

        if (!fileName->isEmpty())
        {
            if(column_count > 0)
            {
                appendReport(*fileName);
            }
            else
            {
                openReport(*fileName);
            }
        }
    }
}

void MainWindow::on_actionSelect_all_triggered()
{

    QAbstractItemModel* model = ui->tableView->model();
    if(model == NULL) return;
    QItemSelectionModel* selection = ui->tableView->selectionModel();
    if(selection == NULL)
    {
        ((MzTableModel*)model)->checkAllFeatures(true);
        return;
    }
    QModelIndexList indexes = selection->selectedIndexes();
    if(indexes.count() < 1)
    {
        ((MzTableModel*)model)->checkAllFeatures(true);
        return;
    }
    QModelIndex current;
    int count = ((MzTableModel*)model)->rowCount();
    if(count <= 0)
        return;
    bool* sel = new bool[count];
    for(int i = 0; i < count; i++)
        sel[i] = false;

    foreach(current, indexes)
    {
        sel[current.row()] = true;
    }
    for(int i = 0; i < count; i++)
    {
        if(sel[i])
            ((MzTableModel*)model)->checkFeature(i, true);
    }
    delete[] sel;
}

void MainWindow::on_actionDeselect_all_triggered()
{
    QAbstractItemModel* model = ui->tableView->model();
    if(model == NULL) return;
    QItemSelectionModel* selection = ui->tableView->selectionModel();
    if(selection == NULL)
    {
        ((MzTableModel*)model)->checkAllFeatures(false);
        return;
    }
    QModelIndexList indexes = selection->selectedIndexes();
    if(indexes.count() < 1)
    {
        ((MzTableModel*)model)->checkAllFeatures(false);
        return;
    }
    QModelIndex current;
    int count = ((MzTableModel*)model)->rowCount();
    if(count <= 0)
        return;
    bool* sel = new bool[count];
    for(int i = 0; i < count; i++)
        sel[i] = false;

    foreach(current, indexes)
    {
        sel[current.row()] = true;
    }
    for(int i = 0; i < count; i++)
    {
        if(sel[i])
            ((MzTableModel*)model)->checkFeature(i, false);
    }
    delete[] sel;
}

void MainWindow::on_actionInvert_selection_triggered()
{
    QAbstractItemModel* model = ui->tableView->model();
    if(model == NULL) return;
    QItemSelectionModel* selection = ui->tableView->selectionModel();
    if(selection == NULL)
    {
        ((MzTableModel*)model)->reverseAllFeatureChecks();
        return;
    }
    QModelIndexList indexes = selection->selectedIndexes();
    if(indexes.count() < 1)
    {
        ((MzTableModel*)model)->reverseAllFeatureChecks();
        return;
    }
    QModelIndex current;
    int count = ((MzTableModel*)model)->rowCount();
    if(count <= 0)
        return;
    bool* sel = new bool[count];
    for(int i = 0; i < count; i++)
        sel[i] = false;

    foreach(current, indexes)
    {
        sel[current.row()] = true;
    }
    for(int i = 0; i < count; i++)
    {
        if(sel[i])
        {
            int c = ((MzTableModel*)model)->isFeatureChecked(i);
            ((MzTableModel*)model)->checkFeature(i, c > 0 ? false : true);
        }
    }
    delete[] sel;
}

void MainWindow::on_actionSelect_all_vectors_triggered()
{
    QAbstractItemModel* model = ui->tableView->model();
    if(model == NULL) return;
    QItemSelectionModel* selection = ui->tableView->selectionModel();
    if(selection == NULL)
    {
        ((MzTableModel*)model)->checkAllVectors(true);
        return;
    }
    QModelIndexList indexes = selection->selectedIndexes();
    if(indexes.count() < 1)
    {
        ((MzTableModel*)model)->checkAllVectors(true);
        return;
    }
    QModelIndex current;
    int count = ((MzTableModel*)model)->columnCount();
    if(count <= 0)
        return;
    bool* sel = new bool[count];
    for(int i = 0; i < count; i++)
        sel[i] = false;

    foreach(current, indexes)
    {
        sel[current.column()] = true;
    }
    for(int i = 0; i < count; i++)
    {
        if(sel[i])
            ((MzTableModel*)model)->checkVector(i, true);
    }
    delete[] sel;
}

void MainWindow::on_actionDeselect_all_vectors_triggered()
{
    QAbstractItemModel* model = ui->tableView->model();
    if(model == NULL) return;
    QItemSelectionModel* selection = ui->tableView->selectionModel();
    if(selection == NULL)
    {
        ((MzTableModel*)model)->checkAllVectors(false);
        return;
    }
    QModelIndexList indexes = selection->selectedIndexes();
    if(indexes.count() < 1)
    {
        ((MzTableModel*)model)->checkAllVectors(false);
        return;
    }
    QModelIndex current;
    int count = ((MzTableModel*)model)->columnCount();
    if(count <= 0)
        return;
    bool* sel = new bool[count];
    for(int i = 0; i < count; i++)
        sel[i] = false;

    foreach(current, indexes)
    {
        sel[current.column()] = true;
    }
    for(int i = 0; i < count; i++)
    {
        if(sel[i])
            ((MzTableModel*)model)->checkVector(i, false);
    }
    delete[] sel;

}

void MainWindow::on_actionInvert_selection_vectors_triggered()
{
    QAbstractItemModel* model = ui->tableView->model();
    if(model == NULL) return;
    QItemSelectionModel* selection = ui->tableView->selectionModel();
    if(selection == NULL)
    {
        ((MzTableModel*)model)->reverseAllVectorChecks();
        return;
    }
    QModelIndexList indexes = selection->selectedIndexes();
    if(indexes.count() < 1)
    {
        ((MzTableModel*)model)->reverseAllVectorChecks();
        return;
    }
    QModelIndex current;

    int count = ((MzTableModel*)model)->columnCount();
    if(count <= 0)
        return;
    bool* sel = new bool[count];
    for(int i = 0; i < count; i++)
        sel[i] = false;

    foreach(current, indexes)
    {
        sel[current.column()] = true;
    }
    for(int i = 0; i < count; i++)
    {
        if(sel[i])
        {
            int c = ((MzTableModel*)model)->isVectorChecked(i);
            ((MzTableModel*)model)->checkVector(i, c > 0 ? false : true);
        }
    }
    delete[] sel;
}

void MainWindow::on_actionReneame_classes_triggered()
{
    if(ui->tableView->model() == NULL) return;
/*
    QPair<QString, QVariant> table[4];

    table[0].first = QString("string");
    table[0].second = QString("Class_name");

    QStringList list;
    list << "ala" << "mela" << "ola";
    table[1].first = QString("combo");
    table[1].second = list;

    QRect rect(2, 4, 15, 0);
    table[2].first = QString("minmax");
    table[2].second = rect;


    double valued = 6.78;
    table[3].first = QString("double");
    table[3].second = valued;
*/


    QStringList classlist;
    int column_count = ((MzTableModel*)ui->tableView->model())->columnCount();
    for(int c = 0; c < column_count; c++)
    {
        QString name = ((MzTableModel*)ui->tableView->model())->headerData(c, Qt::Horizontal, Qt::DisplayRole).value<QString>();
        int index = classlist.indexOf(name);
        if(index < 0)
        {
            classlist.append(name);
        }
    }
    int classnumber = classlist.size();
    QPair<QString, QVariant>* table = new QPair<QString, QVariant>[classnumber];
    for(int c = 0; c < classnumber; c++)
    {
        table[c].first = classlist.at(c);
        table[c].second = classlist.at(c);
    }

    PropertyValue dialog;
    dialog.setWindowTitle("Rename classes");

    dialog.setTable(table, classnumber);
    if(dialog.exec() == QDialog::Accepted)
    {
        QStringList out = dialog.getTable();

        if(classlist.size() == out.size())
        {
            for(int c = 0; c < column_count; c++)
            {
                QString name = ((MzTableModel*)ui->tableView->model())->headerData(c, Qt::Horizontal, Qt::DisplayRole).value<QString>();
                int index = classlist.indexOf(name);
                if(index >= 0)
                {
                    ((MzTableModel*)ui->tableView->model())->setColumnName(c, out.at(index));
                }
            }
        }
    }
    delete[] table;
}

void MainWindow::on_actionRandom_selection_triggered()
{
    if(ui->tableView->model() == NULL) return;

    int c;
    QStringList classlist;
    int column_count = ((MzTableModel*)ui->tableView->model())->columnCount();

    int* vectorclassindex = new int[column_count];
    for(c = 0; c < column_count; c++) vectorclassindex[c] = -1;

    int* vectorsinclass = new int[column_count];
    for(c = 0; c < column_count; c++) vectorsinclass[c] = 0;

    for(c = 0; c < column_count; c++)
    {
        QString name = ((MzTableModel*)ui->tableView->model())->headerData(c, Qt::Horizontal, Qt::DisplayRole).value<QString>();
        int index = classlist.indexOf(name);
        if(index < 0)
        {
            vectorsinclass[classlist.size()]++;
            vectorclassindex[c] = classlist.size();
            classlist.append(name);
        }
        else
        {
            vectorsinclass[index]++;
            vectorclassindex[c] = index;
        }
    }
    int classnumber = classlist.size();
    QPair<QString, QVariant>* table = new QPair<QString, QVariant>[classnumber];
    for(c = 0; c < classnumber; c++)
    {
        QRect rect(0, vectorsinclass[c], vectorsinclass[c]+1, 0);
        table[c].first = classlist.at(c);
        table[c].second = rect;
    }

    srand(time(NULL));
    PropertyValue dialog;
    dialog.setTable(table, classnumber);
    if(dialog.exec() == QDialog::Accepted)
    {
        QStringList out = dialog.getTable();
        if(classlist.size() == out.size())
        {
            for(c = 0; c < classnumber; c++)
            {
                int v = vectorsinclass[c];
                int vvmax = out.at(c).toInt();
                if(vvmax > v) vvmax = v;
                for(int vv = 0; vv < vvmax; vv++, v--)
                {
                    int k = rand() % v;
                    int ll = 0;
                    for(int l = 0; l < column_count; l++)
                    {
                        if(vectorclassindex[l] == c)
                        {
                            if(ll == k)
                            {
                                vectorclassindex[l] = -1;
                                break;
                            }
                            ll++;
                        }
                    }
                }
            }

            for(int l = 0; l < column_count; l++)
            {
                ((MzTableModel*)ui->tableView->model())->checkVector(l, (vectorclassindex[l] < 0));
            }
        }
    }
    delete[] vectorsinclass;
    delete[] vectorclassindex;
    delete[] table;
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionSave_triggered()
{
    QString filter;
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save all"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Comma separated values (*.csv) (*.csv);;Double precision hexadecimals (*.cshv) (*.cshv)"),
                                                    &filter);
    if (!fileName.isEmpty())
    {
        if(filter == "Double precision hexadecimals (*.cshv) (*.cshv)")
        {
            ((MzTableModel*)ui->tableView->model())->saveToFile(fileName, true, true);
        }
        else
        {
            ((MzTableModel*)ui->tableView->model())->saveToFile(fileName, true, false);
        }
    }
}

void MainWindow::on_actionSave_selected_triggered()
{
    QString filter;
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save selected"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Comma separated values (*.csv) (*.csv);;Double precision hexadecimals (*.cshv) (*.cshv)"),
                                                    &filter);
    if (!fileName.isEmpty())
    {
        if(filter == "Double precision hexadecimals (*.cshv) (*.cshv)")
        {
            ((MzTableModel*)ui->tableView->model())->saveToFile(fileName, false, true);
        }
        else
        {
            ((MzTableModel*)ui->tableView->model())->saveToFile(fileName, false, false);
        }
    }
}

void MainWindow::on_actionNew_window_triggered()
{
//    DataForSelection dataforselection;
//    ((MzTableModel*)ui->tableView->model())->getCheckedData(&dataforselection);

    MainWindow* w = new MainWindow;
//    w->setDataHere(&dataforselection);
    w->show();


//    QProcess P(this);// = new QProcess(this);//(NULL);
//    P.startDetached("kate");
    //P->start("kate");

}

void MainWindow::on_actionVectorsFromFolds_triggered()
{
    if((MzTableModel*)ui->tableView->model() == NULL) return;
    FoldsDialog fd((MzTableModel*)ui->tableView->model(), this);
    fd.exec();
}


void MainWindow::on_actionNames_from_file_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load feature names"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Text file (*.txt) (*.txt);;All Files (*)"));
    if (!fileName.isEmpty())
    {
        ((MzTableModel*)ui->tableView->model())->selectFromFile(fileName);
    }
}

//void MainWindow::on_actionUse_checked_toggled(bool arg1)
//{
//    if(!arg1) ui->actionUse_unchecked->setChecked(true);
//}

//void MainWindow::on_actionUse_unchecked_toggled(bool arg1)
//{
//    if(!arg1) ui->actionUse_checked->setChecked(true);
//}


/*
void MainWindow::on_actionAppend_columns_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Load columns to append"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Comma separated vectors(*.csv) (*.csv);;All Files (*)"));
    if (!fileName.isEmpty())
    {
        QCursor old = cursor();
        setCursor(Qt::WaitCursor);
        DataTable dt;
        int r = dt.loadFromFile(fileName.toStdString());
        if(r > 0)
        {
            bool b;
            if(ui->tableView->model() == NULL)
            {
                MzTableModel* model = new MzTableModel(0);
                b = model->appendColumns(&dt);
                if(b)
                {
                    ui->tableView->setModel(model);
                    ui->tableView->show();
                    if(previous_model != NULL) delete previous_model;
                    previous_model = model;
                }
                else delete model;
            }
            else
            {
                MzTableModel* model = (MzTableModel*)ui->tableView->model();
                b = model->appendColumns(&dt);
                ui->tableView->show();
            }

            if(!b)
            {
                QMessageBox msgBox;
                msgBox.setText("Cannot append columns.\nFeature names differ.");
                msgBox.setWindowTitle("Error");
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.exec();
            }
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText("Error loading CSV file");
            msgBox.setWindowTitle("Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
        }
        setCursor(old);
    }
}
*/
void MainWindow::on_actionAppend_rows_triggered()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this,
                                                    tr("Load rows to append"),
                                                    NULL,//domyslna nazwa pliku
                                                    tr("Comma separated vectors(*.csv) (*.csv);;All Files (*)"));

    for (QStringList::const_iterator fileName = fileNames.constBegin(); fileName != fileNames.constEnd(); ++fileName)
    {
        if (!fileName->isEmpty())
        {
            QCursor old = cursor();
            setCursor(Qt::WaitCursor);
            DataTable dt;
            int r = dt.loadFromFile(fileNameRemapRead(&(*fileName)).c_str());
            if(r > 0)
            {
                bool b;
                if(ui->tableView->model() == NULL)
                {
                    MzTableModel* model = new MzTableModel(0);
                    b = model->appendRows(&dt);
                    if(b)
                    {
                        ui->tableView->setModel(model);
                        ui->tableView->show();
                        if(previous_model != NULL) delete previous_model;
                        previous_model = model;
                    }
                    else delete model;
                }
                else
                {
                    MzTableModel* model = (MzTableModel*)ui->tableView->model();
                    b = model->appendRows(&dt);
                    ui->tableView->show();
                }

                if(!b)
                {
                    QMessageBox msgBox;
                    msgBox.setText("Cannot append rows.\nColumn names do not match or duplicate feature names.");
                    msgBox.setWindowTitle("Error");
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.exec();
                }
            }
            else
            {
                QMessageBox msgBox;
                msgBox.setText("Error loading CSV file");
                msgBox.setWindowTitle("Error");
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.exec();
            }
            setCursor(old);
        }
    }
}

void MainWindow::on_actionAdd_noise_triggered()
{
    AddNoise dialog(0, 103.0, this);
    if(dialog.exec() == QDialog::Accepted)
    {
        QCursor old = cursor();
        setCursor(Qt::WaitCursor);
        ((MzTableModel*)ui->tableView->model())->addNoise(dialog.distribution, dialog.snr);
        setCursor(old);
    }
}

void MainWindow::on_actionClose_report_triggered()
{
    if(previous_model != NULL) delete previous_model;
    previous_model = NULL;
    this->setWindowTitle(QString("%1").arg(tr("MzReport")));
}

void MainWindow::on_actionNames_from_clipboard_triggered()
{
    QString names = QApplication::clipboard()->text();
    ((MzTableModel*)ui->tableView->model())->selectFromString(names);
}

void MainWindow::on_actionCopy_checked_names_triggered()
{
    QAbstractItemModel* model = ui->tableView->model();
    if(model == NULL) return;
    int rows = ((MzTableModel*)model)->rowCount();
    if(rows > 0)
    {
        QString text;
        for(int row = 0; row < rows; row++)
        {
            if(((MzTableModel*)model)->isFeatureChecked(row) > 0)
            {
                text.append(((MzTableModel*)model)->headerData(row, Qt::Vertical, Qt::DisplayRole).toString());
                text.append("\n");
            }
        }
        if(text.length() > 0) QApplication::clipboard()->setText(text);
    }
}

void MainWindow::on_actionUncheck_Nan_Row_triggered()
{
    MzTableModel* model = (MzTableModel*) ui->tableView->model();
    if(model == NULL) return;

    int columns = model->columnCount();
    int rows = model->rowCount();

    for(int r = 0; r < rows; r++)
    {
        if(model->isFeatureChecked(r) > 0)
        {
            for(int c = 0; c < columns; c++)
            {
                double d = model->getData(c, r);
                if(!(d == d))
                {
                    if(model->isVectorChecked(c) > 0)
                    {
                        model->checkFeature(c, false);
                        break;
                    }
                }
            }
        }
    }
}

void MainWindow::on_actionUncheck_Nan_Col_triggered()
{
    MzTableModel* model = (MzTableModel*) ui->tableView->model();
    if(model == NULL) return;

    int columns = model->columnCount();
    int rows = model->rowCount();

    for(int c = 0; c < columns; c++)
    {
        if(model->isVectorChecked(c) > 0)
        {
            for(int r = 0; r < rows; r++)
            {
                double d = model->getData(c, r);
                if(!(d == d))
                {
                    if(model->isFeatureChecked(r) > 0)
                    {
                        model->checkVector(c, false);
                        break;
                    }
                }
            }
        }
    }
}
















void MainWindow::on_actionCopy_selected_triggered()
{
    QAbstractItemModel* model = ui->tableView->model();
    if(model == NULL) return;
    QItemSelectionModel* selection = ui->tableView->selectionModel();
    if(selection == NULL) return;
    QString selected_text;
    int c, r;
    int cols = ((MzTableModel*)model)->columnCount();
    if(cols <= 0) return;
    bool* columns = new bool[cols];
    for(c = 0; c < cols; c++)
    {
        if(selection->columnIntersectsSelection(c, QModelIndex()))
        {
            selected_text.append('\t');
            selected_text.append(((MzTableModel*)model)->headerData(c, Qt::Horizontal, Qt::DisplayRole).toString());
            columns[c] = true;
        }
        else columns[c] = false;
    }
    int rows = ((MzTableModel*)model)->rowCount();
    for(r = 0; r < rows; r++)
    {
        if(selection->rowIntersectsSelection(r, QModelIndex()))
        {
            selected_text.append('\n');
            selected_text.append(((MzTableModel*)model)->headerData(r, Qt::Vertical, Qt::DisplayRole).toString());

            for(c = 0; c < cols; c++)
            {
                if(! columns[c]) continue;
                selected_text.append('\t');

                QModelIndex mi = ((MzTableModel*)model)->index(r, c);
                if(selection->isSelected(mi))
                {
                    selected_text.append(((MzTableModel*)model)->data(mi).toString());
                }
            }
        }
    }
    QApplication::clipboard()->setText(selected_text);
}

void MainWindow::on_actionHighlight_all_triggered()
{
    QAbstractItemModel* model = ui->tableView->model();
    if(model == NULL) return;

    ui->tableView->selectAll();
}


