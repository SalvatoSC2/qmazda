/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "propertyvalue.h"
#include "ui_propertyvalue.h"

PropertyValue::PropertyValue(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PropertyValue)
{
    ui->setupUi(this);

    namevalidator = new QRegExpValidator(QRegExp("^[a-zA-Z_][a-zA-Z0-9_]*$"), this);
    textvalidator = new QRegExpValidator(QRegExp("^[a-zA-Z0-9_]*$"), this);
    intvalidator = new QRegExpValidator(QRegExp("^-?[0-9]*$"), this);
    doublevalidator = new QRegExpValidator(QRegExp("^[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?$"), this);
}

PropertyValue::~PropertyValue()
{
    delete textvalidator;
    delete intvalidator;
    delete doublevalidator;
    delete ui;
}

QStringList PropertyValue::getTable(void)
{
    QStringList result;

    int count = ui->treeWidget->topLevelItemCount();
    for(int i = 0; i< count; i++)
    {
        QTreeWidgetItem * item = ui->treeWidget->topLevelItem(i);
        if(item == NULL) continue;
        QWidget* widget = ui->treeWidget->itemWidget(item, 1);
        if(widget == NULL) continue;
        if(qobject_cast<QLineEdit*>(widget) != NULL) result.append(qobject_cast<QLineEdit*>(widget)->text());
        else if(qobject_cast<QComboBox*>(widget) != NULL) result.append(qobject_cast<QComboBox*>(widget)->currentText());
        else if(qobject_cast<QSpinBox*>(widget) != NULL) result.append(qobject_cast<QSpinBox*>(widget)->cleanText());
    }
    return result;
}



bool PropertyValue::setTable(QPair<QString, QVariant> *table, int count)
{

    bool isok = true;
    if(count < 1) isok = false;
    for(int i = 0; i< count; i++)
    {
         if(! table[i].second.isValid())
         {
             isok = false;
             continue;
         }

         switch((int)(table[i].second.type()))
         {

             case QVariant::StringList:
             {
                 QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
                 item->setText(0, table[i].first);
                 QComboBox* combo = new QComboBox();
                 QStringList ls = table[i].second.value<QStringList>();

                 for(int ii = 0; ii < ls.count(); ii++)
                     combo->addItem(ls.at(ii));
                 ui->treeWidget->setItemWidget(item, 1, combo);
             }
             break;
             case QVariant::String:
             {
                 QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
                 item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
                 item->setText(0, table[i].first);
                 QLineEdit* edit = new QLineEdit();
                 edit->setValidator(textvalidator);

                 edit->setText(table[i].second.value<QString>());
                 ui->treeWidget->setItemWidget(item, 1, edit);
             }
             break;
             case QVariant::Int:
             {
                 QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
                 item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
                 item->setText(0, table[i].first);
                 QLineEdit* edit = new QLineEdit();
                 edit->setValidator(intvalidator);
                 edit->setText(table[i].second.value<QString>());
                 ui->treeWidget->setItemWidget(item, 1, edit);
             }
             break;
             case QVariant::Double:
             {
                 QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
                 item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
                 item->setText(0, table[i].first);
                 QLineEdit* edit = new QLineEdit();
                 edit->setValidator(doublevalidator);
                 edit->setText(table[i].second.value<QString>());
                 ui->treeWidget->setItemWidget(item, 1, edit);
             }
             break;
             case QVariant::Rect:
             {
                 QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
                 item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
                 item->setText(0, table[i].first);
                 QSpinBox* spin = new QSpinBox();
                 QRect rect = table[i].second.value<QRect>();
                 spin->setMinimum(rect.left());
                 spin->setMaximum(rect.right());
                 spin->setValue(rect.top());
                 ui->treeWidget->setItemWidget(item, 1, spin);
             }
             break;
         default: isok = false;
         }
     }

    return isok;
}

void PropertyValue::on_okButton_clicked()
{
    done(QDialog::Accepted);
}

void PropertyValue::on_cancelButton_clicked()
{
    done(QDialog::Rejected);
}
