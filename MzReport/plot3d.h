/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLOT3D_H
#define PLOT3D_H

#include <QSvgGenerator>
#include <QPrinter>
#include <QDialog>
#include <QWidget>

#include "../MzShared/dataforconsole.h"

const int default_colors_number = 16;
const QRgb default_colors[default_colors_number] =
{
   (QRgb)0x00FF0000, (QRgb)0x0000FF00,(QRgb)0x000000FF,
   (QRgb)0x0000FFFF, (QRgb)0x00FF00FF, (QRgb)0x00FFFF00,

   (QRgb)0x00FF8000, (QRgb)0x00FF0080, (QRgb)0x0080FF00,
   (QRgb)0x0000FF80, (QRgb)0x008000FF, (QRgb)0x000080FF,
   (QRgb)0x00FFC400, (QRgb)0x00C4FF00, (QRgb)0x00C400FF,
   (QRgb)0x0000FFC4
 };


namespace Ui {
class Plot3D;
}

class Plot3D : public QDialog
{
    Q_OBJECT
    
public:
    explicit Plot3D(QWidget *parent = 0);
    ~Plot3D();


    bool setDataForSelection(DataForSelection* dataforselection_in);
    bool getSelectedFeatures(SelectedFeatures* selected);
    
private slots:
    void on_xComboBox_currentIndexChanged(int);

    void on_yComboBox_currentIndexChanged(int);

    void on_zComboBox_currentIndexChanged(int);

    void on_toolButtonSave_clicked();

    void on_toolButtonFont_clicked();

    void on_toolButtonPlay_toggled(bool checked);

    void on_lineEdit_editingFinished();

    void on_toolButton_clicked();

    void on_closePushButton_clicked();

    void on_selectPushButton_clicked();

private:
    Ui::Plot3D *ui;
    bool setWidgetData(void);
    void drawLegend(QString _symbols);
    int selfeat[3];
    DataForSelection* dataforselection;
    QColor* colors;
};

#endif // PLOT3D_H
