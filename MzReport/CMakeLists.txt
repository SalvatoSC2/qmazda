cmake_minimum_required(VERSION 3.1)

project(MzReport LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5 COMPONENTS Widgets REQUIRED)
find_package(Qt5 COMPONENTS Svg REQUIRED)
find_package(Qt5 COMPONENTS PrintSupport REQUIRED)

add_executable(MzReport 
    main.cpp
    mainwindow.cpp
    mztablemodel.cpp
    plot3d.cpp
    plot3dwidget.cpp
    ../MzShared/dataforconsole.cpp
    foldsdialog.cpp
    addnoise.cpp
    ../MzShared/progress.cpp
    ../MzShared/selectionlist.cpp
    ../MzShared/testdialog.cpp
    selectclassifiers.cpp
    ../MzShared/propertyvaluetree.cpp
    ../MzShared/parametertreewidget.cpp
    propertyvalue.cpp
    ../MzShared/parametertreetemplate.cpp
    ../MzShared/filenameremap.cpp
    ../MzShared/csvio.cpp
    ../MzShared/PlugWorker.cpp
    mainwindow.ui
    plot3d.ui
    foldsdialog.ui
    addnoise.ui
    ../MzShared/progress.ui
    ../MzShared/selectionlist.ui
    ../MzShared/testdialog.ui
    selectclassifiers.ui
    ../MzShared/propertyvaluetree.ui
    propertyvalue.ui
    mzreport.qrc
    mzreport.icns
    MZReport.rc
)

target_include_directories(MzReport PRIVATE ../MzShared)

# target_compile_definitions(MzReport PRIVATE USE_DEBUG_LOG)
# target_compile_definitions(MzReport PRIVATE USE_NOTIFICATIONS)
# target_compile_definitions(MzReport PRIVATE MAZDA_IMAGE_8BIT_PIXEL)
# target_compile_definitions(MzReport PRIVATE MAZDA_CONVERT_BGR_IMAGE)
# target_compile_definitions(MzReport PRIVATE MAZDA_HIDE_ON_PROGRESS)
target_compile_definitions(MzReport PRIVATE MAZDA_IN_MACOS_BUNDLE)

target_link_libraries(MzReport PRIVATE Qt5::Widgets)
target_link_libraries(MzReport PRIVATE Qt5::Svg)
target_link_libraries(MzReport PRIVATE Qt5::PrintSupport)

install(TARGETS MzReport DESTINATION bin)
