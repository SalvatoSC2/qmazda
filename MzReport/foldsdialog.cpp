/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <time.h>
#include "foldsdialog.h"
#include "ui_foldsdialog.h"


FoldsDialog::FoldsDialog(MzTableModel *model, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FoldsDialog)
{
    ui->setupUi(this);
    this->model = model;
    ui->foldsSpinBox->setMaximum(sizeof(model->foldmask)*8);
    setListOfFolds();
}

FoldsDialog::~FoldsDialog()
{
    delete ui;
}

void FoldsDialog::setListOfFolds()
{
    ui->listWidget->clear();
    if(model == NULL) return;
    if(model->folds == NULL || model->numberoffolds <= 0) return;

    ui->applyPushButton->setEnabled(true);
    ui->okPushButton->setEnabled(true);
    long unsigned int mask = 1;
    for(int c = 0; c < model->numberoffolds; c++)
    {
        QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
        item->setText(QString("Fold %1").arg(c+1));
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);

        if((model->foldmask & mask) != 0)
            item->setCheckState(Qt::Checked);
        else
            item->setCheckState(Qt::Unchecked);

        mask <<= 1;
    }
    ui->foldsSpinBox->setValue(model->numberoffolds);
}

void FoldsDialog::on_okPushButton_clicked()
{
    on_applyPushButton_clicked();
    done(QDialog::Accepted);
}

void FoldsDialog::on_applyPushButton_clicked()
{
    if(model == NULL) return;
    if(model->folds == NULL || model->numberoffolds <= 0) return;
    long unsigned int mask = 1;
    int column_count = model->columnCount();
    model->checkAllVectors(false);
    model->foldmask = 0;
    for(int f = 0; f < model->numberoffolds; f++)
    {
        if(ui->listWidget->item(f)->checkState() == Qt::Checked)
        {
            for(int c = 0; c < column_count; c++)
            {
                if(model->folds[c] == f) model->checkVector(c, true);
                model->foldmask |= mask;
            }
        }
        mask <<= 1;
    }
}

void FoldsDialog::on_cancelPushButton_clicked()
{
    done(QDialog::Rejected);
}

void FoldsDialog::on_createPushButton_clicked()
{
    int c, f;
    if(model == NULL) return;

    int numberoffolds = ui->foldsSpinBox->value();
    QStringList classlist;

    int column_count = model->columnCount();
    int* vectorclassindex = new int[column_count];
    int* vectorsinclass = new int[column_count];
    if(model->folds != NULL) delete[] model->folds;
    model->folds = new int[column_count];

    for(c = 0; c < column_count; c++)
    {
        vectorclassindex[c] = -1;
        vectorsinclass[c] = 0;
        model->folds[c] = -1;
        QString name = model->headerData(c, Qt::Horizontal, Qt::DisplayRole).value<QString>();
        int index = classlist.indexOf(name);
        if(index < 0)
        {
            vectorsinclass[classlist.size()]++;
            vectorclassindex[c] = classlist.size();
            classlist.append(name);
        }
        else
        {
            vectorsinclass[index]++;
            vectorclassindex[c] = index;
        }
    }
    int classnumber = classlist.size();
    int* table = new int[classnumber * (numberoffolds+1)];

/*
    printf("VectorsInClass\n");
    for(c = 0; c < classnumber; c++) printf("%i - %i, ", c, vectorsinclass[c]);
    printf("\n\nVectorsClassIndex\n");
    for(c = 0; c < column_count; c++) printf("%i - %i, ", c, vectorclassindex[c]);
    printf("\n\n");
*/

    for(c = 0; c < classnumber; c++)
    {
        int vif = vectorsinclass[c]/numberoffolds;
        if(vectorsinclass[c]%numberoffolds > 0) vif++;
        for(f = 0; f < numberoffolds; f++) table[classnumber * f + c] = vif;
        table[classnumber * f + c] = vif*numberoffolds;
    }


/*
    for(c = 0; c < classnumber; c++)
    {
        printf("\nClass %i\n", c);
        int vif = vectorsinclass[c]/numberoffolds;
        if(vectorsinclass[c]%numberoffolds > 0) vif++;
        for(f = 0; f < numberoffolds; f++)
            printf("%i, ", table[classnumber * f + c]);
        printf("%i, ", table[classnumber * f + c]);
    }
*/

    srand(time(NULL));

    for(c = 0; c < column_count; c++)
    {
        int cla = vectorclassindex[c];
        if(cla >= 0)
        {
            int k = rand() % table[classnumber * numberoffolds + cla];
            for(f = 0; f < numberoffolds; f++)
            {
                k -= table[classnumber * f + cla];
                if(k < 0) break;

            }
            if(f < numberoffolds)
            {
                table[classnumber * numberoffolds + cla]--;
                table[classnumber * f + cla]--;
                model->folds[c] = f;
            }
            else model->folds[c] = -1;
        }
    }
    model->numberoffolds = numberoffolds;

    delete[] table;
    delete[] vectorsinclass;
    delete[] vectorclassindex;

    setListOfFolds();
}
