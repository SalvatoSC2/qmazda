/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLOT3DWIDGET_H
#define PLOT3DWIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QPainter>
#include <QFontDialog>
#include <QColorDialog>
#include <QTimer>
#include <QMouseEvent>
#include "../MzShared/dataforconsole.h"
static const double PRAWIE_ZERO = 0.000000000001;

class Macierz
{
public:

    static double podwyznacznik(double M[2][2], int w, int k);
    static double podwyznacznik(double M[3][3], int w, int k);
    static double podwyznacznik(double M[4][4], int w, int k);
    static void transpozycja(double MM[4][4], double M[4][4]);
    static void transpozycja(double MM[4][4]);
    static void transpozycja(double MM[3][3], double M[3][3]);
    static void transpozycja(double MM[3][3]);
    static void transpozycja(double MM[2][2], double M[2][2]);
    static void transpozycja(double MM[2][2]);
    static double wyznacznik(double M[4][4]);
    static double wyznacznik(double M[3][3]);
    static double wyznacznik(double M[2][2]);
    static double inwersja(double MM[4][4], double M[4][4]);
    static double inwersja(double MM[3][3], double M[3][3]);
    static bool inwersja(double MM[2][2], double M[2][2]);
};

class Plot3dWidget : public QWidget, Macierz
{
    Q_OBJECT
public:
    explicit Plot3dWidget(QWidget *parent = 0);
    ~Plot3dWidget();

    bool setWidgetData(DataForSelection* dataforselection_in, int tmpw[3], QString _symbols, QColor*_colors);
    void Draw3DPlot(QPainter *hdc, QRect viewport);
    void selectFont(QFont font);
    void toggleAnimate(bool checked);
    void setSymbols(QString _symbols);
    void setColor(QColor color, int index);
    QFont getFont(void) {return hfont;}

signals:
    
public slots:
    void animatePlot(void);

private:
    DataForSelection* dataforselection;

    QString symbols;
    QColor* colors;
    int* sorted;
    int* classmembership;
    int featurestoshow[3];

    int mousex;
    int mousey;

    double W[3][3];
    double WI[3][3];
    double mean[3];
    double span[3];

    QFont hfont;
    int font_height;
    QTimer *plottimer;

    void Rotate(int rot, int l1, int l2);
    void Project(double i[3], double o[2]);
    void sortFeatures(void);

    void paintEvent(QPaintEvent*);
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *ev);
};

#endif // PLOT3DWIDGET_H
