/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MZTABLEMODEL_H
#define MZTABLEMODEL_H

#include <QIcon>
#include <QString>
#include <QList>
#include <QStringList>
#include <QLocale>
#include <QFile>
#include <QTextStream>
#include <QBrush>

#include <QAbstractTableModel>
#include "../MzShared/dataforconsole.h"

class MzTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit MzTableModel(QObject *parent = 0);
    ~MzTableModel(void);
    int rowCount(const QModelIndex &parent = QModelIndex()) const ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

//    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role = Qt::EditRole);
    void setColumnName(int section, const QString& value);
    void horizontalHeaderIconClick(int section);
    void verticalHeaderIconClick(int section);


    int loadFromFile(QString fileName);
    int saveToFile(QString fileName, bool saveall, bool hexa);
    bool fromDataTableToErase(DataTable* newtable);
    bool appendColumns(DataTable* newtable);
    bool appendRows(DataTable* newtable);

    bool setData(DataForSelection* inputdata);
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    void addNoise(int distribution, double snr);

    Qt::ItemFlags flags(const QModelIndex & index) const ;

    bool getRequiredData(DataForSelection* outputdata, int checked_mask);// = (USE_CHECKED_FEATURES | USE_CHECKED_VECTORS));
    bool getRequiredData(std::vector<std::string>* featureNames, DataForSelection* outputdata);

    void checkAllVectors(bool check = true);
    void reverseAllVectorChecks(void);
    int isVectorChecked(int i);
    bool checkVector(int i, bool check = true);
    void checkAllFeatures(bool check = true);
    void reverseAllFeatureChecks(void);
    int isFeatureChecked(int i);
    bool checkFeature(int i, bool check = true);
    void selectFromFile(QString fileName);
    void selectFromString(QString names);

    int* folds;
    int numberoffolds;
    long unsigned int foldmask;


    double getData(int column, int row);

private:
    QIcon icon_v;
    QIcon icon_o;
    QIcon icon_z;

    char* column_icons;
    char* row_icons;

    DataTable datatable;

    QList<QStringList> csv;

    void checkString(QString &temp, QChar character = 0);


//    QStandardItemModel *model;
//    QList<QStandardItem*> standardItemList;



signals:
     void editCompleted(const QString &);

};











#endif // MZTABLEMODEL_H
