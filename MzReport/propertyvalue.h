/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROPERTYVALUE_H
#define PROPERTYVALUE_H

#include <QString>
#include <QStringList>
#include <QValidator>
#include <QDialog>
#include <QDir>
#include <QMessageBox>
#include <QFileInfo>
#include <QClipboard>
#include <QFileDialog>
#include <QPluginLoader>
#include <QComboBox>
#include <QSpinBox>
#include <QLineEdit>

namespace Ui {
class PropertyValue;
}

class PropertyValue : public QDialog
{
    Q_OBJECT
    
public:
    explicit PropertyValue(QWidget *parent = 0);
    ~PropertyValue();

    bool setTable(QPair<QString, QVariant> *table, int count);
    QStringList getTable(void);

private slots:
    void on_okButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::PropertyValue *ui;

    QValidator *namevalidator;
    QValidator *textvalidator;
    QValidator *intvalidator;
    QValidator *doublevalidator;
};

#endif // PROPERTYVALUE_H
