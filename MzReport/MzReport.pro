#-------------------------------------------------
#
# Project created by QtCreator 2013-06-27T11:54:31
#
#-------------------------------------------------

QT       += core gui svg printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DESTDIR         = ../../Executables
TARGET = MzReport
TEMPLATE = app
# CONFIG -= import_plugins

DEFINES += MAZDA_IN_MACOS_BUNDLE
#DEFINES += MAZDA_HIDE_ON_PROGRESS

SOURCES += main.cpp\
        mainwindow.cpp \
    mztablemodel.cpp \
    plot3d.cpp \
    plot3dwidget.cpp \
    ../MzShared/dataforconsole.cpp \
    foldsdialog.cpp \
    addnoise.cpp \
    ../MzShared/progress.cpp \
    ../MzShared/selectionlist.cpp \
    ../MzShared/testdialog.cpp \
    selectclassifiers.cpp \
    ../MzShared/propertyvaluetree.cpp \
    ../MzShared/parametertreewidget.cpp \
    propertyvalue.cpp \
    ../MzShared/parametertreetemplate.cpp \
    ../MzShared/filenameremap.cpp \
    ../MzShared/csvio.cpp \
    ../MzShared/PlugWorker.cpp

HEADERS  += mainwindow.h \
    mztablemodel.h \
    plot3d.h \
    plot3dwidget.h \
    ../MzShared/progress.h \
    ../MzShared/selectionlist.h \
    ../MzShared/testdialog.h \
    ../MzShared/dataforconsole.h \
    ../MzShared/mzselectioninterface.h \
    foldsdialog.h \
    addnoise.h \
    selectclassifiers.h \
    ../MzShared/propertyvaluetree.h \
    ../MzShared/parametertreewidget.h \
    propertyvalue.h \
    ../MzShared/parametertreetemplate.h \
    ../MzShared/filenameremap.h \
    ../MzShared/PlugWorker.h \
    ../MzShared/csvio.h

FORMS    += mainwindow.ui \
    plot3d.ui \
    foldsdialog.ui \
    addnoise.ui \
    ../MzShared/progress.ui \
    ../MzShared/selectionlist.ui \
    ../MzShared/testdialog.ui \
    selectclassifiers.ui \
    ../MzShared/propertyvaluetree.ui \
    propertyvalue.ui

RESOURCES += \
    mzreport.qrc

INCLUDEPATH += ../MzShared

macx{
    ICON = mzreport.icns
}
else:unix{
}
else:win32{
    RC_FILE = MZReport.rc
}

include(../Pri/config.pri)
#include(../Pri/itk.pri)
#include(../Pri/tiff.pri)

