/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MZMAPIO_H
#define MZMAPIO_H

#include "../MzShared/mapstruct.h"
#include <stdio.h>
#include <string.h>
#include <vector>

//PMS_REMOVE

class MzMap : public MapForSegmentation
{
public:
    MzMap()
    {
        pixel = 0;
        data = NULL;
        for(int i = 0; i < MZDIMENSIONS; i++)
        {
            size[i] = 0;
        }
    }
    ~MzMap()
    {
        erase();
    }

    double value(unsigned int x[MZDIMENSIONS])
    {
        unsigned int p = x[MZDIMENSIONS - 1];
        for(int d = MZDIMENSIONS-2; d >= 0; d--)
        {
            p *= size[d];
            p += x[d];
        }

        double v;
        switch(pixel)
        {
        case MZPIXELF64: v = *(((double*) data)+p); break;
        case MZPIXELF32: v = *(((float*) data)+p); break;
        }
        return v;
    }

    void erase(void)
    {
        if(data != NULL)
        {
            switch(pixel)
            {
            case MZPIXELF32: delete[] (float*)data; break;
            case MZPIXELF64: delete[] (double*)data; break;
            }
        }
        data = NULL;
        pixel = 0;
        for(int i = 0; i < MZDIMENSIONS; i++)
        {
            size[i] = 0;
        }
    }
    void allocate(unsigned int x[MZDIMENSIONS], unsigned int pixeltype)
    {
        erase();
        unsigned int allsize = 1;
        for(int i = 0; i < MZDIMENSIONS; i++)
        {
            size[i] = x[i];
            allsize *= x[i];
        }
        if(allsize > 0)
        {
            switch(pixeltype)
            {
            case MZPIXELF32:
                data = (void*) new float[allsize];
                pixel = MZPIXELF32;
                memset(data, 0, allsize*sizeof(float));
                break;
            case MZPIXELF64:
                data = (void*) new double[allsize];
                pixel = MZPIXELF64;
                memset(data, 0, allsize*sizeof(double));
                break;
            }
        }
    }
};


class MzMapFunctions
{
public:
/**
 * @brief SaveSubsequentMapToPagedTiff saves a susequent map to paged, floating point tiff file.
 * @param filename name of the file. If NULL, the next map to the same file is saved.
 * @param map pointer to map structure.
 * @param page consecutive number of a map
 * @param pages number of all maps to be saved
 * @return true on success or false otherwise.
 */
bool static SaveSubsequentMapToPagedTiff(const char* filename, MzMap* map, int page, int pages);


/**
 * @brief SaveMapsToPagedTiff saves the set of maps to paged, floating point tiff file.
 * @param filename name of the file.
 * @param maps vector of maps.
 * @return true on success or false otherwise.
 */
bool static SaveMapsToPagedTiff(const char* filename, std::vector<MzMap> maps);


/**
 * @brief LoadSubsequentMapFromPagedTiff loads single map from paged, floating point tiff file.
 * @param filename name of the file. If NULL, the next map from previously open file is fetched.
 * @param map pointer to map structure. If NULL, the new file is opened or closed.
 * @return positive if there are more maps to fetch or negative for error.
 */
int static LoadSubsequentMapFromPagedTiff(const char* filename, MzMap* map);


/**
 * @brief LoadMapsFromPagedTiff loads set of maps from paged, floating point tiff file.
 * @param filename name of the file.
 * @return vector of maps.
 */
void static LoadMapsFromPagedTiff(const char* filename, std::vector<MzMap*>* toReturn);
};

#endif // MZMAPIO_H
