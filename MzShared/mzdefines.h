#ifndef MZDEFINES_H
#define MZDEFINES_H

#if defined(__APPLE__) && defined(MAZDA_IN_MACOS_BUNDLE)
const char generator_name[] = "/MzGengui.app/Contents/MacOS/MzGengui";
const char report_viewer_name[] = "/MzReport.app/Contents/MacOS/MzReport";
const char map_viewer_name[] = "/MzMaps.app/Contents/MacOS/MzMaps";
#else
const char generator_name[] = "/MzGengui";
const char report_viewer_name[] = "/MzReport";
const char map_viewer_name[] = "/MzMaps";
#endif

const char QMAZDA_VERSION[] = "Version 20.12";
const char QMAZDA_COPYRIGHT[] = "Copyright 2013-2020 by Piotr M. Szczypiński";

#endif // MZDEFINES_H
