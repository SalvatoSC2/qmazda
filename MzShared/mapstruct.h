/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



//PMS_REMOVE


#ifndef MZMAPSTRUCT_H
#define MZMAPSTRUCT_H

#include <string>

#define MZPIXELF32 4
#define MZPIXELF64 8

#ifndef MZDIMENSIONS
#define MZDIMENSIONS 3
#endif



struct MapForSegmentation
{
    unsigned int pixel;
    void* data;
    unsigned int size[MZDIMENSIONS];
    std::string name;
};

#endif //MZMAPSTRUCT_H
