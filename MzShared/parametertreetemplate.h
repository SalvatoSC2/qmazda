/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAMETERTREETEMPLATE_H
#define PARAMETERTREETEMPLATE_H

#include <string.h>
#include <stdlib.h>

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>


template <class T>
/**
 * @brief The ParameterTreeItem class feature name stub item in tree
 */
class ParameterTreeItem
{
public:
    ~ParameterTreeItem()
    {
        for(unsigned int i = 0; i < children.size(); i++) delete children[i];
    }
    ParameterTreeItem* parent;
    std::vector < ParameterTreeItem* > children;
    T data;
};

template <class T>
/**
 * @brief The ParameterTree class
 */
class ParameterTree
{
public:
    ParameterTree()
    {
        root = NULL;
    }
    ParameterTreeItem<T>* getRoot(void)
    {
        return root;
    }
    ParameterTreeItem<T>* getChildItem(ParameterTreeItem<T>* item, unsigned int index)
    {
        if(index < item->children.size())
            return item->children[index];
        return NULL;
    }
    ParameterTreeItem<T>* getParentItem(ParameterTreeItem<T>* item)
    {
        return item->parent;
    }
    bool isLeaf(ParameterTreeItem<T>* item)
    {
        if(item == NULL)
            return false;
        return (item->children.size() == 0);
    }
    T* getData(ParameterTreeItem<T>* item)
    {
        return &item->data;
    }
    /**
     * @brief parse builds tree from text stream
     * @param text
     * @param indentChar
     * @return
     */
    bool parse(std::istream* stream, const char indentChar)
    {
        beginBuildFromLine();
        while(!stream->eof())
        {
            std::string line;
            getline(*stream, line);

            if (!line.empty() && line[line.size() - 1] == '\r')
                line.erase(line.size() - 1);

            if (line.empty())
                return true;
            if(buildFromLine(line, indentChar) == NULL)
            {
                endBuildFromLine();
                return false;
            }
        }
        endBuildFromLine();
        return true;
    }
    bool parse(const char* text, const char indentChar)
    {
        std::stringstream ss(text);
        return parse(& ss, indentChar);
    }
    bool parse(std::string* text, const char indentChar)
    {
        std::stringstream ss(text->c_str()); //OSX
        return parse(ss, indentChar);
    }


    /**
     * @brief load loads tree from text file
     * @param fileName
     * @param indentChar
     * @return
     */
    bool load(const char *fileName, const char indentChar)
    {
        std::ifstream file;
        file.open(fileName);
        if (!file.is_open()) return false;
        if (!file.good()) return false;
        return parse(file, indentChar);
    }

    /**
     * @brief save saves tree to a text file
     * @param fileName
     * @param indentChar
     */
    bool save(const char *fileName, const char indentChar)
    {
        if(root == NULL) return false;
        std::ofstream file;
        file.open(fileName);
        if (!file.is_open()) return false;
        if (!file.good()) return false;

        for(unsigned int i = 0; i < root->children.size(); i++)
            saveRecursive(&file, root->children[i], indentChar, 0);
        return true;
    }
    std::string serialize(const char indentChar)
    {
        std::stringstream ss;
        for(unsigned int i = 0; i < root->children.size(); i++)
            saveRecursive(&ss, root->children[i], indentChar, 0);
        return ss.str();
    }
    std::vector<ParameterTreeItem<T>*> vectorize()
    {
        std::vector<ParameterTreeItem<T>*> toret;
        for(unsigned int i = 0; i < root->children.size(); i++)
            vectorizeRecursive(&toret, root->children[i]);
        return toret;
    }



    /**
     * @brief endBuildFromLine
     */
    void endBuildFromLine(void)
    {
        stack.clear();
    }

    /**
     * @brief beginBuildFromLine
     * @return
     */
    ParameterTreeItem<T>* beginBuildFromLine(void)
    {
        stack.clear();
        if(root != NULL) delete root;
        root = new ParameterTreeItem<T>;
        stack.push_back(root);
        return root;
    }
    /**
     * @brief buildFromLine
     * @param line
     * @return
     */
    ParameterTreeItem<T>* buildFromLine(std::string line, const char indentChar)
    {
        size_t start = line.find_first_not_of(indentChar);
        if(start + 1 == stack.size())
        {
            ParameterTreeItem<T>* item = new ParameterTreeItem<T>;
            item->data.fromString(line.substr(start));
            stack.back()->children.push_back(item);
            stack.push_back(item);
            return item;
        }
        else if(start < stack.size())
        {
            stack.resize(start + 1);
            ParameterTreeItem<T>* item = new ParameterTreeItem<T>;
            item->data.fromString(line.substr(start));
            stack.back()->children.push_back(item);
            stack.push_back(item);
            return item;
        }
        else return NULL;
    }
private:
    ParameterTreeItem<T>* root;
    std::vector<ParameterTreeItem<T>*> stack;
    void saveRecursive(std::ostream* stream, ParameterTreeItem<T>* item, const char indentChar, unsigned int depth)
    {
        for(unsigned int i = 0; i < depth; i++)
            *stream << indentChar;
        *stream << item->data.toString() << std::endl;
        for(unsigned int i = 0; i < item->children.size(); i++)
            saveRecursive(stream, item->children[i], indentChar, depth+1);
    }
    void vectorizeRecursive(std::vector<ParameterTreeItem<T>*>* toret, ParameterTreeItem<T>* item)
    {
        toret->push_back(item);
        for(unsigned int i = 0; i < item->children.size(); i++)
            vectorizeRecursive(toret, item->children[i]);
    }
};

enum NameStubType {NSTUnknown, NSTInt, NSTText, NSTName, NSTRange};

class NameStub
{
public:
    std::string toString(void);
    void fromString(std::string);
    NameStubType type;
    int iprefered;
    int min;
    int max;
    std::vector <std::string> list;
    std::string tprefered;
    std::string tip;
    void* ptr;
};

class NameStubTree : public ParameterTree <NameStub>
{
public:
    //bool isLeaf(ParameterTreeItem<NameStub>* pitem);
    std::string getOption(ParameterTreeItem<NameStub>* pitem);
    std::vector<std::string> getChildrenOption(const ParameterTreeItem<NameStub>* pitem);
    int doesItMatch(const char* name, ParameterTreeItem<NameStub>** pitem, char **left = NULL, int depth = 1024);
    int doesItMatch(const char* name);
};

#endif // PARAMETERTREETEMPLATE_H
