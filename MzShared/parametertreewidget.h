/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAMETERTREEWIDGET_H
#define PARAMETERTREEWIDGET_H

#include <QTreeWidget>
#include <QSpinBox>
#include <QComboBox>
#include <QStyledItemDelegate>
#include "parametertreetemplate.h"


class ParameterTreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    explicit ParameterTreeWidget(QWidget *parent = 0);
    void setSeparator(char mergeSeparator);
    void setTemplate(NameStubTree* featuresTemplate);

    QModelIndex parameterIndexFromItem(QTreeWidgetItem *item, int column = 0);
    QTreeWidgetItem *parameterItemFromIndex(const QModelIndex &index);
    QString getValue(QString name);
    QString getValue(unsigned int index);
    //QStringList getNames(QString separator);
    void resetValues(int column);
    QTreeWidgetItem* createMethod(const QString name, const QString tooltip = "");
    QTreeWidgetItem* createProperty(QTreeWidgetItem *parent, const QString name, const QString value, const QString tooltip = "");

    void addMatchedChildren(QTreeWidgetItem* item);
    void getUniqueLeafs(QStringList* list);
    void setUniqueLeafs(QStringList* list);
    void removeItem(QTreeWidgetItem* item);
    void promoteChange(QTreeWidgetItem* item);
signals:
public slots:
private:
    void getUniqueLeafsRecursive(QStringList* list, QTreeWidgetItem* item);
    void removeUnmatchedChildren(QTreeWidgetItem* item);
    void removeUnmatchedChildrenRecursive(QTreeWidgetItem* item);
    void promoteChangeRecursive(QTreeWidgetItem* item, QString prefix);
    void removeItemRecursive(QTreeWidgetItem* item);
    void resetValuesRecursive(QTreeWidgetItem* item, int column);
    void findBestMatchingBranch(const QString *name, QTreeWidgetItem* item, QTreeWidgetItem** itematched, int* matched);
    void appendToBranch(const QString* name, QTreeWidgetItem* item);
    QBrush brush;
    char mergeSeparator;
    NameStubTree* featuresTemplate;

};

/**
 * @brief The NoEditDelegate class gets redirected requests to edit first column of parameter tree, and refuses to create editor. Thus, the first column cannot be edited.
 */
class NoEditDelegate: public QStyledItemDelegate
{
public:
    NoEditDelegate(QObject* parent=0);
    QWidget* createEditor(QWidget*, const QStyleOptionViewItem &, const QModelIndex &) const;
};

/**
 * @brief The EditDelegate class redirects editing to spinbox (http://doc.qt.io/qt-5/qtwidgets-itemviews-spinboxdelegate-example.html)
 */
class EditDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    EditDelegate(QObject *parent, ParameterTreeWidget* tree, char mergeSeparator);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem & option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
private:
    ParameterTreeWidget* tree;
    char mergeSeparator;
};

#endif // PARAMETERTREEWIDGET_H
