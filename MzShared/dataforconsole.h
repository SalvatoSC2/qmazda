/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef DATATODATA_H
#define DATATODATA_H

#include <iostream>
#include <string>
#include <vector>

//#include "../MzShared/mapstruct.h"

using namespace std;

#define USE_CHECKED_FEATURES 1
#define USE_UNCHECKED_FEATURES 2
#define USE_CHECKED_VECTORS 4
#define USE_UNCHECKED_VECTORS 8


typedef float MazdaMapPixelType;


//typedef enum {IT_UINT8, IT_INT8, IT_UINT16, IT_INT16, IT_FLOAT32, IT_FLOAT64} ImageType;


//typedef std::vector<MapForSegmentation*> MapsForSegmentation;

//struct MapsForSegmentation
//{
//    std::vector <MapForSegmentation*> maps;
//};

class DataTable
{
    public:
    DataTable();
    ~DataTable();
//    static char* doubleToHex(const double value);
//    static double hexToDouble(const char* text);
    void Reset(void);
    int loadFromFile(string fileName);
    int saveToFile(string fileName, bool hexa, char* columns2save, char* rows2save);

    int column_count;
    int row_count;
    string* row_names;
    string* column_names;
    string* column_tips;
    double* feature_values;
};

class DataForSegmentation
{
public:
    DataForSegmentation();
    std::vector<std::string> featurenames;
    std::vector<std::string> resultnames;
    unsigned int size[4];
    unsigned int vectornumber;
    MazdaMapPixelType** values;  //input buffers values[feature][vector]
    MazdaMapPixelType** result; //output buffer result[feature][vector]
};

class DataForSelection
{
public:
    DataForSelection();
    ~DataForSelection();
    void Reset(void);
//    bool getData(DataTable* datatable, int checked_mask = 0, char* row_icons = 0, char* column_icons = 0);
//    bool setData(DataTable* datatable);
    int vectornumber;
    int classnumber;
    int featurenumber;
    int* classendvectorindex; // na ktorym wektorze konczy sie klasa c (posortowane wektory wg klas)
    int* featureoriginalindex; // indeks cechy w tabeli (w strukturze moze byc mniej cech niz oryginalnie w tabelce)
    string* classnames;
    string* featurenames;
    double* values;  //values[featurenumber * v + f]
};

class SelectedFeatures
{
public:
    SelectedFeatures();
    ~SelectedFeatures();
    void Reset(void);

    int featurenumber;
    int* featureoriginalindex; // indeks cechy w tabeli (w strukturze moze byc mniej cech niz oryginalnie w tabelce)
};

#endif // DATATODATA_H
