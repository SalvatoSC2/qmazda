/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2021  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PlugWorker.h"

PlugWorker::PlugWorker(void* plugin_object, MzThreadStaticPluginFunction threaded_plugin) :
                      QObject(NULL)
{
    this->plugin_object = plugin_object;
    this->threaded_plugin = threaded_plugin;
}
PlugWorker::~PlugWorker()
{}
void PlugWorker::static_notify_step(void* object_this)
{
    emit ((PlugWorker*)object_this)->notify_progress();
}
void PlugWorker::static_notify_text(void* object_this, std::string text)
{
    emit ((PlugWorker*)object_this)->notify_progress(QString::fromStdString(text));
}
void PlugWorker::process()
{
    threaded_plugin(plugin_object, this, static_notify_step, static_notify_text);
    emit finished(true);
    emit finished();
}
