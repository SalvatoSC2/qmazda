/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MULTIDIMSELECTION_H
#define MULTIDIMSELECTION_H

#define FAILED 0
#define SUCCESS 1
#define BEGINS 2
#define STOPPED 3
#define COMPLETED 4
#define STEP 5
#define CANCELED 6

#include <vector>
#include <string>

//extern bool breakanalysis;

class MultiDimensionalSelection
{
public:
    virtual void NotifyProgressStep(void) = 0;
    virtual void NotifyProgressStage(const int notification, const int dimensionality, const double value, const unsigned int* selected) = 0;

    virtual double GoalFunction(unsigned int dimensions, unsigned int* picked_features) = 0;
    virtual double ClassifierTraining(unsigned int dimensions, unsigned int* selected) = 0;

    virtual ~MultiDimensionalSelection(){}
    void cancelFullSearch(void);
    int GenerateNextCombination(int dim, unsigned int *feature);
    bool FullSearch(int nooffeatures, double* Qtable, unsigned int* Qsorted,
                    int parameter_dimensions, int parameter_seconds, bool parameter_maximize);
    bool breakanalysis;

private:
    void QualitySorter(int nooffeatures, double* Qtable, unsigned int* Qsorted);
    void PenaltySorter(int nooffeatures, double* Qtable, unsigned int* Qsorted);
};

#endif //MULTIDIMSELECTION_H
