/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "colorconvert.h"
#include "math.h"
#include <string>

#define colorConvertBegin \
    int x, y;\
    int pixel = image->pixelSize();\
    int xsize = image->size[0];\
    int ysize = 1;\
    for(y = 1; y < MZDIMENSIONS; y++) ysize *= image->size[y];\
    for(y = 0; y < ysize; y++){\
        unsigned char* iptr = image->data + y*image->linesize;\
        uint16_t* out = (uint16_t*)(newimage->data + y*newimage->linesize);\
        for(x = 0; x < xsize; x++){\
            uint16_t r, g, b;\
            switch(pixel){\
            case MZPIXEL_UI16: r = g = b = *((uint16_t*)iptr); iptr+=2; break;\
            case MZPIXEL_RGB24: b = (uint16_t(*iptr))*0x101; iptr++; g = (uint16_t(*iptr))*0x101; iptr++; r = (uint16_t(*iptr))*0x101; iptr++; break;\
            case MZPIXEL_RGB48: b = *(uint16_t*)iptr; iptr+=2; g = *(uint16_t*)iptr; iptr+=2; r = *(uint16_t*)iptr; iptr+=2; break;\
            default: r = g = b = (uint16_t(*iptr))*0x101; iptr++;\
            }

#define colorConvertEnd out++;}}

void colorConvertY(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    *out = (uint16_t)(((int)114*b+(int)587*g+(int)299*r)/1000);
    colorConvertEnd
}
void colorConvertR(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    *out = r;
    colorConvertEnd
}
void colorConvertG(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    *out = g;
    colorConvertEnd
}
void colorConvertB(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    *out = b;
    colorConvertEnd
}

void colorConvertU(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    *out = (((int)886*b-(int)587*g-(int)299*r+886*65535)/1772);
    colorConvertEnd
}
void colorConvertV(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    *out = ((-(int)114*b-(int)587*g+(int)701*r+701*65535)/1402);
    colorConvertEnd
}
void colorConvertI(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    *out = ((-(int)3213*b-(int)2744*g+(int)5957*r+5958*65535)/(11916));
    colorConvertEnd
}
void colorConvertQ(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    *out = (((int)3111*b-(int)5226*g+(int)2115*r+5226*65535)/(10452));
    colorConvertEnd
}
void colorConvertH(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double u = ((886.0*(double)b-587.0*(double)g-299.0*(double)r)/886.0);
    double v = ((-114.0*(double)b-587.0*(double)g+701.0*(double)r)/701.0);
    if(u == 0 && v == 0) *out = 0;
    else
    {
        double h = atan2(v, u);
        if (h<0) h+=(2*M_PI);
        *out = (uint16_t)(h*10430.378);
    }
    colorConvertEnd
}
void colorConvertS(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double u = ((886.0*(double)b-587.0*(double)g-299.0*(double)r)/886.0);
    double v = ((-114.0*(double)b-587.0*(double)g+701.0*(double)r)/701.0);
    *out =  (uint16_t)(0.93652797*sqrt(u*u+v*v));
    colorConvertEnd
}

void colorConvertu(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double u = (886.0*(double)b-587.0*(double)g-299.0*(double)r);
    double bb = (114.0*(double)b+587.0*(double)g+299.0*(double)r+1.0);
    *out = (uint16_t)((u/bb+1.0)*256*29.07);
    colorConvertEnd
}
void colorConvertv(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double v = (-114.0*(double)b-587.0*(double)g+701.0*(double)r);
    double bb = (114.0*(double)b+587.0*(double)g+299.0*(double)r+1.0);
    *out = (uint16_t)((v/bb+1.0)*256*76.245);
    colorConvertEnd
}
void colorConverti(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double i = (-321.3*(double)b-274.4*(double)g+595.7*(double)r);
    double bb = (114.0*(double)b+587.0*(double)g+299.0*(double)r+1.0);
    *out = (uint16_t)((i/bb+2.8185)*256*53.006);
    colorConvertEnd
}
void colorConvertq(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double q = (311.1*(double)b-522.6*(double)g+211.5*(double)r);
    double bb = (114.0*(double)b+587.0*(double)g+299.0*(double)r+1.0);
    *out = (uint16_t)((q/bb+0.8903)*256*70.457);
    colorConvertEnd
}
void colorConverth(MzImage* image, MzImage* newimage)
{
    colorConvertBegin

    double ii = ((-(int)3213*b-(int)2744*g+(int)5957*r)/(11916));
    double qq = (((int)3111*b-(int)5226*g+(int)2115*r)/(10452));
//    double u = ((886.0*(double)b-587.0*(double)g-299.0*(double)r)/886.0);
//    double v = ((-114.0*(double)b-587.0*(double)g+701.0*(double)r)/701.0);
//    if(u == 0 && v == 0) *out = 0;
    if(ii == 0 && qq == 0) *out = 0;
    else
    {
        double h = atan2(qq, ii);
        if (h<0) h+=(2*M_PI);
        *out = (uint16_t)(h*10430.378);
    }
    colorConvertEnd
}
void colorConvertx(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double rr = ((double)r)/65535.0;
    if (rr <= 0.04045) rr = rr / 12.92;
    else rr = pow(((rr + 0.055) / 1.055), 2.4);
    double gg = ((double)g)/65535.0;
    if (gg <= 0.04045) gg = gg / 12.92;
    else gg = pow(((gg + 0.055) / 1.055), 2.4);
    double bb = ((double)b)/65535.0;
    if (bb <= 0.04045) bb = bb / 12.92;
    else bb = pow(((bb + 0.055) / 1.055), 2.4);
    *out = (uint16_t)((105.162*rr+91.188*gg+46.0275*bb)*256);
    colorConvertEnd
}
void colorConverty(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double rr = ((double)r)/65535.0;
    if (rr <= 0.04045) rr = rr / 12.92;
    else rr = pow(((rr + 0.055) / 1.055), 2.4);
    double gg = ((double)g)/65535.0;
    if (gg <= 0.04045) gg = gg / 12.92;
    else gg = pow(((gg + 0.055) / 1.055), 2.4);
    double bb = ((double)b)/65535.0;
    if (bb <= 0.04045) bb = bb / 12.92;
    else bb = pow(((bb + 0.055) / 1.055), 2.4);
    *out = (uint16_t)((54.213*rr+182.376*gg+18.411*bb)*256);
    colorConvertEnd
}
void colorConvertz(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double rr = ((double)r)/65535.0;
    if (rr <= 0.04045) rr = rr / 12.92;
    else rr = pow(((rr + 0.055) / 1.055), 2.4);
    double gg = ((double)g)/65535.0;
    if (gg <= 0.04045) gg = gg / 12.92;
    else gg = pow(((gg + 0.055) / 1.055), 2.4);
    double bb = ((double)b)/65535.0;
    if (bb <= 0.04045) bb = bb / 12.92;
    else bb = pow(((bb + 0.055) / 1.055), 2.4);
    *out = (uint16_t)((4.5162*rr+27.8928*gg+222.417*bb)*256);
    colorConvertEnd
}
void colorConvertL(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double rr = ((double)r)/65535.0;
    if (rr <= 0.04045) rr = rr / 12.92;
    else rr = pow(((rr + 0.055) / 1.055), 2.4);
    double gg = ((double)g)/65535.0;
    if (gg <= 0.04045) gg = gg / 12.92;
    else gg = pow(((gg + 0.055) / 1.055), 2.4);
    double bb = ((double)b)/65535.0;
    if (bb <= 0.04045) bb = bb / 12.92;
    else bb = pow(((bb + 0.055) / 1.055), 2.4);
    double yy = 0.2126*rr+0.7152*gg+0.0722*bb;
    yy = pow(yy, 1.0 / 3.0);
    *out = (uint16_t)(65535*yy);
    colorConvertEnd
}
void colorConverta(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double rr = ((double)r)/65535.0;
    if (rr <= 0.04045) rr = rr / 12.92;
    else rr = pow(((rr + 0.055) / 1.055), 2.4);
    double gg = ((double)g)/65535.0;
    if (gg <= 0.04045) gg = gg / 12.92;
    else gg = pow(((gg + 0.055) / 1.055), 2.4);
    double bb = ((double)b)/65535.0;
    if (bb <= 0.04045) bb = bb / 12.92;
    else bb = pow(((bb + 0.055) / 1.055), 2.4);
    double xx = 0.43387*rr+0.37622*gg+0.18990*bb;
    double yy = 0.2126*rr+0.7152*gg+0.0722*bb;
    xx = pow(xx, 1.0 / 3.0);
    yy = pow(yy, 1.0 / 3.0);
    *out = (uint16_t)(376.128385*(xx-yy+0.33898))*257;
    colorConvertEnd
}
void colorConvertb(MzImage* image, MzImage* newimage)
{
    colorConvertBegin
    double rr = ((double)r)/65535.0;
    if (rr <= 0.04045) rr = rr / 12.92;
    else rr = pow(((rr + 0.055) / 1.055), 2.4);
    double gg = ((double)g)/65535.0;
    if (gg <= 0.04045) gg = gg / 12.92;
    else gg = pow(((gg + 0.055) / 1.055), 2.4);
    double bb = ((double)b)/65535.0;
    if (bb <= 0.04045) bb = bb / 12.92;
    else bb = pow(((bb + 0.055) / 1.055), 2.4);
    double yy = 0.2126*rr+0.7152*gg+0.0722*bb;
    double zz = 0.01772*rr+0.109458*gg+0.872819*bb;
    yy = pow(yy, 1.0 / 3.0);
    zz = pow(zz, 1.0 / 3.0);
    *out = (uint16_t)(159.235668*(yy-zz+0.8007))*257;
    colorConvertEnd
}

typedef void ColorConvertType(MzImage* image, MzImage* newimage);
struct ChannelFunctions
{
    std::string name;
    ColorConvertType* function;
};

ChannelFunctions colorConvertionTable[] =
{
    {std::string("Y"), colorConvertY},
    {std::string("R"), colorConvertR},
    {std::string("G"), colorConvertG},
    {std::string("B"), colorConvertB},
    {std::string("U"), colorConvertU},
    {std::string("V"), colorConvertV},
    {std::string("H"), colorConvertH},
    {std::string("S"), colorConvertS},
    {std::string("I"), colorConvertI},
    {std::string("Q"), colorConvertQ},
    {std::string("u"), colorConvertu},
    {std::string("v"), colorConvertv},
    {std::string("i"), colorConverti},
    {std::string("q"), colorConvertq},
    {std::string("h"), colorConverth},
    {std::string("x"), colorConvertx},
    {std::string("y"), colorConverty},
    {std::string("z"), colorConvertz},
    {std::string("L"), colorConvertL},
    {std::string("a"), colorConverta},
    {std::string("b"), colorConvertb},
    {std::string(""), NULL}
};

void colorConvert(const char* chan, MzImage* image, MzImage* newimage)
{
    int i = 0;
    do
    {
        if(colorConvertionTable[i].name.compare(chan) == 0)
        {
            colorConvertionTable[i].function(image, newimage);
            return;
        }
        i++;
    }while(colorConvertionTable[i].function != NULL);
    colorConvertionTable[0].function(image, newimage);
}

void colorConvertNames(std::vector<std::string>* names)
{
    int i = 0;
    do
    {
        names->push_back(colorConvertionTable[i].name);
        i++;
    }while(colorConvertionTable[i].function != NULL);
}

