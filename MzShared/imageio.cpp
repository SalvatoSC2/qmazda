/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "imageio.h"
//#include "bitscopy.h"
//#include <tiffio.h>

#include <opencv2/opencv.hpp>


/**
 * @brief LoadMzImage
 * @param filename
 * @return image
 */
bool MzImageFunctions::LoadMzImage(const char* filename, MzImage* toReturn)
{
    IplImage* src = NULL;
    int x, y;

    if( (src = cvLoadImage(filename, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR )) == 0 ) return false;
    if((src->dataOrder != 0) || (src->nChannels != 1  && src->nChannels != 3 && src->nChannels != 4) || (src->depth != IPL_DEPTH_8U && src->depth != IPL_DEPTH_16U))
    {
        cvReleaseImage(&src);
        return false;
    }

    unsigned int vecs[MZDIMENSIONS];
    for(int d = 2; d < MZDIMENSIONS; d++) vecs[d] = 1;

    int pixel;
    if(src->depth == IPL_DEPTH_16U)
    {
        if(src->nChannels == 3 || src->nChannels == 4) pixel =  MZPIXEL_RGB48;
        else pixel =  MZPIXEL_UI16;
    }
    else
    {
        if(src->nChannels == 3 || src->nChannels == 4) pixel =  MZPIXEL_RGB24;
        else pixel =  MZPIXEL_UI8;
    }

    vecs[0] = src->width;
    vecs[1] = src->height;
    toReturn->allocate(vecs, pixel);

    if(src->nChannels == 4)
    {
        if(src->origin == 0)
        {
            for(y = 0; y < src->height; y++)
            {
                if(src->depth == IPL_DEPTH_16U)
                {
                    uint16_t* s = (uint16_t*)(src->imageData) + y*src->widthStep;
                    uint16_t* d = (uint16_t*)(toReturn->data) + y*toReturn->linesize;
                    for(x = 0; x < src->width; x++)
                    {
                        *d = *s; d++; s++;
                        *d = *s; d++; s++;
                        *d = *s; d++; s++;
                        s++;
                    }
                }
                else
                {
                    unsigned char* s = (unsigned char*)(src->imageData) + y*src->widthStep;
                    unsigned char* d  = toReturn->data + y*toReturn->linesize;
                    for(x = 0; x < src->width; x++)
                    {
                        *d = *s; d++; s++;
                        *d = *s; d++; s++;
                        *d = *s; d++; s++;
                        s++;
                    }
                }

            }
        }
        else
        {
            for(y = 0; y < src->height; y++)
            {
                if(src->depth == IPL_DEPTH_16U)
                {
                    uint16_t* s = (uint16_t*)(src->imageData) + (src->height-1-y)*src->widthStep;
                    uint16_t* d  = (uint16_t*)(toReturn->data) + y*toReturn->linesize;
                    for(x = 0; x < src->width; x++)
                    {
                        *d = *s; d++; s++;
                        *d = *s; d++; s++;
                        *d = *s; d++; s++;
                        s++;
                    }
                }
                else
                {
                    unsigned char* s = (unsigned char*)(src->imageData) + (src->height-1-y)*src->widthStep;
                    unsigned char* d  = toReturn->data + y*toReturn->linesize;
                    for(x = 0; x < src->width; x++)
                    {
                        *d = *s; d++; s++;
                        *d = *s; d++; s++;
                        *d = *s; d++; s++;
                        s++;
                    }
                }

            }
        }
    }
    else //channel 3, 1
    {
        if(src->origin == 0)
        {
            for(y = 0; y < src->height; y++)
            {
                unsigned char* s = (unsigned char*)(src->imageData) + y*src->widthStep;
                unsigned char* d  = toReturn->data + y*toReturn->linesize;
                memcpy(d, s, toReturn->linesize);
            }
        }
        else
        {
            for(y = 0; y < src->height; y++)
            {
                unsigned char* s = (unsigned char*)(src->imageData) + (src->height-1-y)*src->widthStep;
                unsigned char* d  = toReturn->data + y*toReturn->linesize;
                memcpy(d, s, toReturn->linesize);
            }
        }
    }
    cvReleaseImage(&src);
    return true;
}

/**
 * @brief SaveMzImage
 * @param filename
 * @param image
 * @return true on success or false otherwise.
 */
bool MzImageFunctions::SaveMzImage(const char *filename, MzImage* image)
{
    IplImage ipl_image_header;
    int bpp, chan;

    if(image->data == NULL) return false;

    switch(image->pixelSize())
    {
    //case MZPIXEL_UI8: bpp = 8; chan = 1; break;
    case MZPIXEL_UI16: bpp = 16; chan = 1; break;
    case MZPIXEL_RGB24: bpp = 8; chan = 3; break;
    case MZPIXEL_RGB48: bpp = 16; chan = 3; break;
    default: bpp = 8; chan = 1;
    }

    cvInitImageHeader( &ipl_image_header, cvSize(image->size[0], image->size[1]), bpp, chan, IPL_ORIGIN_BL, 4);
    ipl_image_header.widthStep = image->linesize;
    ipl_image_header.origin = 0;
    cvSetData(&ipl_image_header, image->data, image->linesize);

    cvSaveImage(filename, &ipl_image_header);
    ipl_image_header.imageData = NULL;
    ipl_image_header.imageDataOrigin = NULL;
    return true;
}



