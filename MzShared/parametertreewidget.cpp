/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2016 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QHBoxLayout>
#include "parametertreewidget.h"

/**
 * @brief ParameterTreeWidget::ParameterTreeWidget constructor of tree widget
 * @param parent
 */
ParameterTreeWidget::ParameterTreeWidget(QWidget *parent) : QTreeWidget(parent)
{
    this->mergeSeparator = 0;
}

/**
 * @brief ParameterTreeWidget::findBestMatchingBranch compares the feature name with existing tree to find a location to extend the tree
 * @param name feature name to be added to the tree
 * @param item tree item to start search, may be NULL to start form the tree root
 * @param itematched pointer to return the best matching branch/item
 * @param matched number of characters of the feature name which were matched
 */
void ParameterTreeWidget::findBestMatchingBranch(const QString* name, QTreeWidgetItem* item, QTreeWidgetItem** itematched, int* matched)
{
    if(item == NULL) item = this->topLevelItem(0);
    if(item == NULL) return;
    if(item->parent() != NULL)
    {
        QString str = item->text(0);
        str = str.remove(mergeSeparator);
        int length = str.length();

        if(name->startsWith(str) && length > *matched)
        {
            if(name->length() <= length)
            {
                *matched = length;
                *itematched = item;
            }
            else if(!(name->at(length-1).isDigit() && name->at(length).isDigit()))
            {
                *matched = length;
                *itematched = item;
            }

        }
    }
    int kmax = item->childCount();
    for(int k = 0; k < kmax; k++)
        findBestMatchingBranch(name, item->child(k), itematched, matched);
}


/**
 * @brief ParameterTreeWidget::appendToBranch Appends new feature name to the tree
 * @param name feature name to be appended
 * @param item point from which the feature name is to be appended
 */
void ParameterTreeWidget::appendToBranch(const QString* name, QTreeWidgetItem* item)
{
    QString tstr;

    if(item == NULL) item = this->topLevelItem(0);
    if(item == NULL) return;
    if(item->parent() != NULL) tstr = item->text(0);
    tstr = tstr.remove(mergeSeparator);
    int length = tstr.length();

    char* namebuffer = new char[name->length()+1];
    strcpy(namebuffer, name->toStdString().c_str());

    if(name->startsWith(tstr))
    {
        char* left = namebuffer + length;
        ParameterTreeItem<NameStub>* matched = NULL;
        int ma;
        if(length > 0) ma = featuresTemplate->doesItMatch(tstr.toStdString().c_str(), &matched);
        else ma = 1;
        while(ma > 0)
        {
            //ParameterTreeItem<NameStub>* parmatched = matched;
            char* parleft = left;
            std::vector<std::string> options = featuresTemplate->getChildrenOption(matched);
            ma = featuresTemplate->doesItMatch(left, &matched, &left, 1);
            if(ma >= 0 && matched != NULL)
            {
                std::string nstr = matched->data.toString();
                std::string::size_type slash = nstr.find_first_of('/');
                QTreeWidgetItem* paritem = item;

//Adding options to stub from template
                QString nameoptions;
                QString nametip;
                if(slash >= nstr.length())
                {
                    nameoptions = nstr.c_str();
                }
                else
                {
                    nameoptions = nstr.substr(0, slash).c_str();
                    nametip = nstr.substr(slash + 1).c_str();
                }
                if(options.size() == 1)
                {
                    QStringList nlist = nameoptions.split(";");

                    std::string::size_type slasho = options[0].find_first_of('/');
                    if(slasho >= options[0].length())
                    {
                        nameoptions = options[0].c_str();
                    }
                    else
                    {
                        nameoptions = options[0].substr(0, slasho).c_str();
                        if(nametip.isEmpty())
                            nametip = options[0].substr(slasho + 1).c_str();
                    }
                    QStringList olist = nameoptions.split(";");
                    if(nlist.size() > 1 && olist.size() > 1) //OSX
                        olist[1] = nlist[1];
                    nameoptions = olist.join(";");
                }
                item = this->createProperty(item, "", nameoptions, nametip);
//                if(slash >= nstr.length())
//                    item = this->createProperty(item, "", nstr.c_str(), "");
//                else
//                    item = this->createProperty(item, "", nstr.substr(0, slash).c_str(), nstr.substr(slash + 1).c_str());
                QString nname(parleft);
                nname.truncate(left-parleft);
                nname = paritem->text(0) + mergeSeparator + nname;
                item->setText(0, nname);
                item->setExpanded(true);
            }
        }
    }
    delete[] namebuffer; //OSX
}

/**
 * @brief ParameterTreeWidget::setUniqueLeafs creates/supplements the tree from feature names, e.g. loaded from file
 * @param list feature name list
 */
void ParameterTreeWidget::setUniqueLeafs(QStringList* list)
{
    int kmax = list->count();
    for(int k = 0; k < kmax; k++)
    {
        ParameterTreeItem<NameStub>* matched = NULL;
        if(featuresTemplate->doesItMatch(list->at(k).toStdString().c_str(), &matched) == 0)
        {
            QTreeWidgetItem* item = NULL;
            int charsma = -1;
            findBestMatchingBranch(&(list->at(k)), NULL, &item, &charsma);
            if(charsma < list->at(k).length())
            {
                appendToBranch(&(list->at(k)), item);
            }
        }
    }
}

/**
 * @brief ParameterTreeWidget::getUniqueLeafsRecursive creates a list of filenames for save
 * @param list returns a list through this pointer
 * @param item starts with this item, may be NULL for all the tree
 */
void ParameterTreeWidget::getUniqueLeafsRecursive(QStringList* list, QTreeWidgetItem* item)
{
    if(item == NULL)
    {
        item = this->topLevelItem(0);
        if(item == NULL) return;
    }
    int kmax = item->childCount();
    if(kmax == 0)
    {
        ParameterTreeItem<NameStub>* matched = NULL;
        QString str = item->text(0);
        if(featuresTemplate->doesItMatch(str.toStdString().c_str(), &matched) == 0)
        {
            bool apnd = true;
            str = str.remove(mergeSeparator);
            for (int i = 0; i < list->size(); ++i)
            {
                 if (list->at(i) == str)
                 {
                     item->setForeground(0, QBrush(Qt::red));
                     apnd = false;
                     break;
                 }
            }
            if(apnd)
            {
                list->append(str);
                item->setForeground(0, brush);
            }
        }
    }
    else
    {
        for(int k = 0; k < kmax; k++)
            getUniqueLeafsRecursive(list, item->child(k));
    }
}

/**
 * @brief ParameterTreeWidget::getUniqueLeafs creates a list of filenames for save
 * @param list returns a list through this pointer
 */
void ParameterTreeWidget::getUniqueLeafs(QStringList* list)
{
    getUniqueLeafsRecursive(list, NULL);
}

/**
 * @brief ParameterTreeWidget::parameterIndexFromItem makes indexFromItem public
 * @param item
 * @param column
 * @return
 */
QModelIndex ParameterTreeWidget::parameterIndexFromItem(QTreeWidgetItem *item, int column)
{
    return indexFromItem(item, column);
}

/**
 * @brief ParameterTreeWidget::parameterItemFromIndex makes itemFromIndex public
 * @param index
 * @return
 */
QTreeWidgetItem* ParameterTreeWidget::parameterItemFromIndex(const QModelIndex &index)
{
    return itemFromIndex(index);
}

/**
 * @brief ParameterTreeWidget::setSeparator sets the separator character used to separate feature name parts, should be space
 * @param mergeSeparator
 */
void ParameterTreeWidget::setSeparator(char mergeSeparator)
{
    this->mergeSeparator = mergeSeparator;
}

/**
 * @brief ParameterTreeWidget::setTemplate sets tree template object used for feature name parsing
 * @param featuresTemplate
 */
void ParameterTreeWidget::setTemplate(NameStubTree* featuresTemplate)
{
    this->featuresTemplate = featuresTemplate;
}

/**
 * @brief ParameterTreeWidget::removeItemRecursive removes item and its children from the tree
 * @param item
 */
void ParameterTreeWidget::removeItemRecursive(QTreeWidgetItem* item)
{
    if(item == NULL) return;
    if(item->parent() == NULL)
    {
        int kmax = item->childCount();
        for(int k = kmax-1; k>=0; k--)
        {
            QTreeWidgetItem * kid = item->takeChild(k);
            if(kid != NULL) delete kid;
        }
    }
    else
    {
        delete item;
    }
}

/**
 * @brief ParameterTreeWidget::removeItem removes item and its children from the tree
 * @param item
 */
void ParameterTreeWidget::removeItem(QTreeWidgetItem* item)
{
    removeItemRecursive(item);
    QStringList list;
    this->getUniqueLeafsRecursive(&list, NULL);
}

/**
 * @brief ParameterTreeWidget::removeUnmatchedChildrenRecursive removes items which do not match featuresTemplate object
 * @param item
 */
void ParameterTreeWidget::removeUnmatchedChildren(QTreeWidgetItem* item)
{
    if(item == NULL) return;
    int kmax = item->childCount();
    for(int k = kmax-1; k>=0; k--)
    {
        removeUnmatchedChildrenRecursive(item->child(k));
    }
}

void ParameterTreeWidget::removeUnmatchedChildrenRecursive(QTreeWidgetItem* item)
{
    if(item == NULL) return;
    ParameterTreeItem<NameStub>* matched = NULL;
    if(featuresTemplate->getRoot() == NULL)
    {
        if(item->parent() != NULL)
        {
            delete item;
        }
    }
    else if(featuresTemplate->doesItMatch(item->text(0).toStdString().c_str(), &matched) < 0)
    {
        delete item;
    }
    else
    {
        int kmax = item->childCount();
        for(int k = kmax-1; k>=0; k--)
        {
            removeUnmatchedChildrenRecursive(item->child(k));
        }
    }
}

/**
 * @brief ParameterTreeWidget::addMatchedChildren adds children matching the featuresTemplate object after the user changed the item's value
 * @param item
 */
void ParameterTreeWidget::addMatchedChildren(QTreeWidgetItem* item)
{
    int code;
    ParameterTreeItem<NameStub>* matched = NULL;
    if(item == NULL) return;

    if(featuresTemplate->getRoot() == NULL) return;
    if(item->parent() == NULL)
    {
        code = 1;
        matched = NULL;
    }
    else
    {
        code = featuresTemplate->doesItMatch(item->text(0).toStdString().c_str(), &matched);
    }

    while(item != NULL && code > 0)
    {
        if(featuresTemplate->isLeaf(matched))
            break;
        std::vector<std::string> options = featuresTemplate->getChildrenOption(matched);
        unsigned int size = options.size();
        if(size == 1)
        {
            if(options[0].at(0) == 'r')
            {
                QStringList valist;
                QString tip;
                std::string::size_type slash = options[0].find_first_of('/');
                if(slash >= options[0].length())
                {
                    valist = QString(options[0].c_str()).split(";");
                    tip = "";
                }
                else
                {
                    valist = QString(options[0].substr(0, slash).c_str()).split(";");
                    tip = options[0].substr(slash + 1).c_str();
                }

                if(valist.size() > 2)
                {
                    unsigned int ib = valist[2].toInt();
                    unsigned int ie = valist[3].toInt();

                    for(unsigned int i = ib; i <= ie; i++)
                    {
                        this->createProperty(item, "", QString("r;%1;%2;%3").arg(i).arg(ib).arg(ie), tip);
                    }
                    item = NULL;
                    matched = NULL;
                    this->setCurrentItem(item);
                }
            }
            else
            {
                std::string::size_type slash = options[0].find_first_of('/');
                if(slash >= options[0].length())
                    item = this->createProperty(item, "", options[0].c_str(), "");
                else
                    item = this->createProperty(item, "", options[0].substr(0, slash).c_str(), options[0].substr(slash + 1).c_str());
                item->setExpanded(true);
                this->setCurrentItem(item);
                matched = NULL;
                code = featuresTemplate->doesItMatch(item->text(0).toStdString().c_str(), &matched);
            }
        }
        else if(size > 1)
        {
            for(unsigned int i = 0; i < size; i++)
            {
                std::string::size_type slash = options[i].find_first_of('/');
                if(slash >= options[i].length())
                    this->createProperty(item, "", options[i].c_str(), "");
                else
                    this->createProperty(item, "", options[i].substr(0, slash).c_str(), options[i].substr(slash + 1).c_str());
            }
            item = NULL;
            matched = NULL;
            this->setCurrentItem(item);
        }
        else
        {
            item = NULL;
        }
    }
    QStringList list;
    this->getUniqueLeafsRecursive(&list, NULL);
}

/**
 * @brief ParameterTreeWidget::promoteChangeRecursive promotes the item's value to all the children after user modifies the item
 * @param item
 * @param prefix
 */
void ParameterTreeWidget::promoteChangeRecursive(QTreeWidgetItem* item, QString prefix)
{
    QStringList valist = item->text(0).split(mergeSeparator);
    item->setText(0, prefix + mergeSeparator + valist.last());
    int kmax = item->childCount();
    for(int k = kmax-1; k >= 0; k--)
    {
        promoteChangeRecursive(item->child(k), item->text(0));
    }
}

/**
 * @brief ParameterTreeWidget::promoteChange promotes the item's value to all the children after user modifies the item
 * @param item
 */
void ParameterTreeWidget::promoteChange(QTreeWidgetItem* item)
{
    if(item == NULL) return;
    int kmax = item->childCount();
    for(int k = kmax-1; k >= 0; k--)
    {
        promoteChangeRecursive(item->child(k), item->text(0));
    }
    removeUnmatchedChildren(item);
    if(item->childCount() <= 0) addMatchedChildren(item);

    QStringList list;
    this->getUniqueLeafsRecursive(&list, NULL);
}


/**
 * @brief ParameterTreeWidget::getValue gets parameter value in non feature name mode
 * @param name property name in form of "Algorithm:Parameter"
 * @return
 */
QString ParameterTreeWidget::getValue(QString name)
{
    QString ret;
    QStringList namelist = name.split(":");
    int level = 0;
    QTreeWidgetItem* item;
    int maxitems = topLevelItemCount();
    for(int i = 0; i < maxitems; i++)
    {
        item = topLevelItem(i);
        if(item == NULL) return ret;
        if(item->text(0) == namelist[level]) break;
    }
    level++;
    while(item != NULL && level < namelist.count())
    {
        int i;
        QTreeWidgetItem* itemt;
        maxitems = item->childCount();
        for(i = 0; i < maxitems; i++)
        {
            itemt = item->child(i);
            if(itemt == NULL) return ret;
            if(itemt->text(0) == namelist[level])
            {
            item = itemt;
            break;
            }
        }
        if(i >= maxitems) return ret;
        level++;
    }
    ret = item->text(1);
    return ret;
}

QString ParameterTreeWidget::getValue(unsigned int index)
{
    QString ret;
    QTreeWidgetItem* item;
    item = topLevelItem(index);
    if(item == NULL) return ret;
    ret = item->text(1);
    return ret;
}

/**
 * @brief ParameterTreeWidget::resetValues resets property values to default values in non feature name mode
 * @param column should be equal to 1 in non feature name mode
 */
void ParameterTreeWidget::resetValues(int column)
{
    QTreeWidgetItem* item;
    int maxitems = topLevelItemCount();
    for(int i = 0; i < maxitems; i++)
    {
        item = topLevelItem(i);
        if(item == NULL) return;
        resetValuesRecursive(item, column);
    }
}

/**
 * @brief ParameterTreeWidget::resetValuesRecursive resets property values to default values in non feature name mode
 * @param item to start
 * @param column should be equal to 1 in non feature name mode
 */
void ParameterTreeWidget::resetValuesRecursive(QTreeWidgetItem* item, int column)
{
    QStringList valist = item->data(1, Qt::UserRole).toString().split(";");
    if(valist.size() >= 2)
    {
        item->setText(column, valist[1]);
    }
    int maxitems = item->childCount();
    if(maxitems <= 0) return;
    for(int i = 0; i < maxitems; i++)
    {
        QTreeWidgetItem* itemt = item->child(i);
        if(item != NULL)
        {
            resetValuesRecursive(itemt, column);
        }
    }
}

/**
 * @brief ParameterTreeWidget::createMethod creates method (top level item)
 * @param name
 * @param tooltip
 * @return pointer to the created item
 */
QTreeWidgetItem* ParameterTreeWidget::createMethod(const QString name, const QString tooltip)
{
    QTreeWidgetItem* methoditem = new QTreeWidgetItem();
    methoditem->setText(0, name);
    methoditem->setToolTip(0, tooltip);
    addTopLevelItem(methoditem);
    brush = methoditem->foreground(0);
    return methoditem;
}

/**
 * @brief ParameterTreeWidget::createProperty creates child item (non top level item)
 * @param parent
 * @param name
 * @param value
 * @param tooltip
 * @return
 */
QTreeWidgetItem* ParameterTreeWidget::createProperty(QTreeWidgetItem* parent, const QString name, const QString value, const QString tooltip)
{
    QTreeWidgetItem* parameteritem = new QTreeWidgetItem();

    char mergeSeparator = this->mergeSeparator;
    if(parent == NULL) mergeSeparator = 0;
    else if(parent->parent() == NULL) mergeSeparator = 0;

    int column = 0;
    if(! name.isEmpty())
    {
        if(mergeSeparator != 0)
            parameteritem->setText(column, parent->text(column) + mergeSeparator + name);
        else
            parameteritem->setText(column, name);
        parameteritem->setToolTip(column, tooltip);
        column++;
    }
    if(! value.isEmpty())
    {
        parameteritem->setToolTip(column, tooltip);
        parameteritem->setData(column, Qt::UserRole, QString(value));
        QStringList valist = value.split(";");
        if(column == 0 && mergeSeparator != 0)
        {
            if(valist.size() > 1)
            {
                parameteritem->setText(column, parent->text(column) + mergeSeparator + valist[1]);
            }
            else parameteritem->setText(column, parent->text(column) + mergeSeparator + value);
        }
        else
        {
            if(valist.size() > 1)
            {
                parameteritem->setText(column, valist[1]);
            }
            else parameteritem->setText(column, value);
        }
    }
    parameteritem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable);
    if(parent != NULL) parent->addChild(parameteritem);
    else addTopLevelItem(parameteritem);
    return parameteritem;
}

/**
 * @brief NoEditDelegate::NoEditDelegate delegate constructor for non editable items
 * @param parent
 */
NoEditDelegate::NoEditDelegate(QObject* parent) : QStyledItemDelegate(parent)
{
}

/**
 * @brief NoEditDelegate::createEditor creates null editor for non editable items
 * @return
 */
QWidget* NoEditDelegate::createEditor(QWidget*, const QStyleOptionViewItem &, const QModelIndex &) const
{
    return 0;
}


/**
 * @brief EditDelegate::EditDelegate delegate constructor for editable items
 * @param parent
 * @param tree
 * @param mergeSeparator
 */
EditDelegate::EditDelegate(QObject *parent, ParameterTreeWidget* tree, char mergeSeparator)
    : QStyledItemDelegate(parent)
{
    this->tree = tree;
    this->mergeSeparator = mergeSeparator;
}

/**
 * @brief EditDelegate::createEditor creates editors: combo or spin boxes
 * @param parent
 * @param option
 * @param index
 * @return
 */
QWidget* EditDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QStringList valist = index.model()->data(index, Qt::UserRole).toString().split(";");
    if(valist[0] == "n") return NULL;
    if(valist[0] == "r") return NULL;

    if(valist.size() >= 4)
    {
        if(valist[0] == "i")
        {
            QSpinBox *editor = new QSpinBox(parent);
            editor->setFrame(false);
            editor->setMinimum(valist[2].toInt());
            editor->setMaximum(valist[3].toInt());
            return editor;
        }
        if(valist[0] == "f")
        {
            QDoubleSpinBox *editor = new QDoubleSpinBox(parent);
            editor->setDecimals(3);
            editor->setFrame(false);
            editor->setMinimum(valist[2].toDouble());
            editor->setMaximum(valist[3].toDouble());
            return editor;
        }
        else if(valist[0] == "t")
        {
            QComboBox *editor = new QComboBox(parent);
            editor->setFrame(false);
            editor->setAutoFillBackground(true);
            for(int i = 2; i < valist.size(); i++) editor->addItem(valist[i]);
            return editor;
        }
    }
    return QStyledItemDelegate::createEditor(parent, option, index);
}

/**
 * @brief EditDelegate::setEditorData sets current data for editor
 * @param editor
 * @param index
 */
void EditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QSpinBox *spinBox = qobject_cast <QSpinBox *>(editor);
    QComboBox *comboBox = qobject_cast <QComboBox *>(editor);
    QDoubleSpinBox *doubleSpinBox = qobject_cast <QDoubleSpinBox *>(editor);
    if(spinBox != NULL)
    {
        int value;
        if(mergeSeparator != 0)
        {
            QStringList list = index.model()->data(index, Qt::EditRole).toString().split(mergeSeparator);
            value = list.takeLast().toInt();
        }
        else value = index.model()->data(index, Qt::EditRole).toInt();
        spinBox->setValue(value);
    }
    else if(doubleSpinBox != NULL)
    {
        double value;
        if(mergeSeparator != 0)
        {
            QStringList list = index.model()->data(index, Qt::EditRole).toString().split(mergeSeparator);
            value = list.takeLast().toDouble();
        }
        else value = index.model()->data(index, Qt::EditRole).toDouble();
        doubleSpinBox->setValue(value);
    }
    else if(comboBox != NULL)
    {
        QString value;
        if(mergeSeparator != 0)
        {
            QStringList list = index.model()->data(index, Qt::EditRole).toString().split(mergeSeparator);
            value = list.takeLast();
        }
        else value = index.model()->data(index, Qt::EditRole).toString();
        comboBox->setCurrentText(value);
    }
    else QStyledItemDelegate::setEditorData(editor, index);
}

/**
 * @brief EditDelegate::setModelData sets item's data after editing completes
 * @param editor
 * @param model
 * @param index
 */
void EditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QSpinBox *spinBox = qobject_cast <QSpinBox *>(editor);
    if(spinBox)
    {
        if(mergeSeparator)
        {
            QTreeWidgetItem* item = tree->parameterItemFromIndex(index);
            if(item != NULL)
            {
                if(item->parent() != NULL)
                {
                    model->setData(index,
                                   item->parent()->text(0) + mergeSeparator + spinBox->text(),
                                   Qt::EditRole);
                    tree->promoteChange(item);
                    return;
                }
            }
        }
        QStyledItemDelegate::setModelData(editor, model, index);
        return;
    }
    QComboBox *comboBox = qobject_cast <QComboBox *>(editor);
    if(comboBox)
    {
        if(mergeSeparator)
        {
            QTreeWidgetItem* item = tree->parameterItemFromIndex(index);
            if(item != NULL)
            {
                if(item->parent() != NULL)
                {
                    model->setData(index,
                                   item->parent()->text(0) + mergeSeparator + comboBox->currentText(),
                                   Qt::EditRole);
                    tree->promoteChange(item);
                    return;
                }
            }
        }
        QStyledItemDelegate::setModelData(editor, model, index);
        return;
    }
    QDoubleSpinBox *doubleSpinBox = qobject_cast <QDoubleSpinBox *>(editor);
    if(doubleSpinBox)
    {
        if(mergeSeparator)
        {
            QTreeWidgetItem* item = tree->parameterItemFromIndex(index);
            if(item != NULL)
            {
                if(item->parent() != NULL)
                {
                    model->setData(index,
                                   item->parent()->text(0) + mergeSeparator + doubleSpinBox->text(),
                                   Qt::EditRole);
                    tree->promoteChange(item);
                    return;
                }
            }
        }
        QStyledItemDelegate::setModelData(editor, model, index);
        return;
    }

    QStyledItemDelegate::setModelData(editor, model, index);
    QStringList valist = model->data(index, Qt::UserRole).toString().split(";");
    if(valist.size() >= 2)
    {
        if(valist[0] == "f")
        {
            double v = model->data(index, Qt::EditRole).toDouble();
            if(valist.size() >= 3)
            {
                double m = valist[2].toDouble();
                if(v < m) v = m;
            }
            if(valist.size() >= 4)
            {
                double m = valist[3].toDouble();
                if(v > m) v = m;
            }
            model->setData(index, v, Qt::EditRole);
        }
        else if(valist[0] == "i")
        {
            int v = model->data(index, Qt::EditRole).toInt();
            if(valist.size() >= 3)
            {
                int m = valist[2].toInt();
                if(v < m) v = m;
            }
            if(valist.size() >= 4)
            {
                int m = valist[3].toInt();
                if(v > m) v = m;
            }
            model->setData(index, v, Qt::EditRole);
        }
    }
    if(mergeSeparator)
    {
        QTreeWidgetItem* item = tree->parameterItemFromIndex(index);
        if(item != NULL)
        {
            if(item->parent() != NULL)
            {
                model->setData(index,
                               item->text(0) + mergeSeparator + model->data(index, Qt::EditRole).toString(),
                               Qt::EditRole);

                tree->promoteChange(item);
            }
        }
    }
}

/**
 * @brief EditDelegate::updateEditorGeometry updates editor's size when required
 * @param editor
 * @param option
 */
void EditDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}

