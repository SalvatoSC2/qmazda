#ifndef PLUGWORKER_H
#define PLUGWORKER_H

#include "mzselectioninterface.h"
#include "QObject"
class PlugWorker : public QObject
{
    Q_OBJECT
public:
    explicit PlugWorker(void* plugin_object,
                        MzThreadStaticPluginFunction threaded_plugin);
    ~PlugWorker();
    static void static_notify_step(void* object_this);
    static void static_notify_text(void* object_this, std::string text);

public slots:
    void process();
signals:
    void finished();
    void finished(bool);
    void notify_progress();
    void notify_progress(QString text);

private:
    void* plugin_object;
    MzThreadStaticPluginFunction threaded_plugin;
};

#endif // PLUGWORKER_H
