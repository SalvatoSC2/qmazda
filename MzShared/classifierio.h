#ifndef CLASSIFIERIO_H
#define CLASSIFIERIO_H
#include "../MzShared/dataforconsole.h"
#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>

// using namespace std;

/*
struct DataLine
{
    vector<double> values;
};

struct ParameterLine
{
    string name;
    int value;
};
*/
class Classifier
{
public:
    std::string getName(void);
//    vector<ParameterLine> parameters;
    std::vector<std::string> classnames;
    std::vector<std::string> featurenames;
    std::vector< std::vector<double> > values;
};

class Classifiers
{
public:
    Classifiers(std::string s) {identifier = s;}
    std::string identifier;
    std::vector<Classifier> classifiers;
    bool loadClassifier(std::ifstream *file, bool appended = false);
    bool loadClassifier(const char* fileName, bool appended = false);
    bool saveClassifier(std::ofstream *file, bool hexa);
    bool saveClassifier(const char* fileName, bool hexa, bool append = false);
};

class ClassifierAccessInterface
{
public:
    virtual ~ClassifierAccessInterface(){}
    virtual const char* getClassifierMagic(void) = 0;
    virtual bool configureForClassification(std::vector<std::string>* featurenames) = 0;
    virtual unsigned int classifyFeatureVector(double* featureVector) = 0;
    virtual void segmentImage(unsigned int vectornumber,
                              MazdaMapPixelType **values,
                              MazdaMapPixelType *result) = 0;
    virtual bool loadClassifierFromFile(const char* filename) = 0;
//    virtual bool saveClassifierToFile(const char* filename) = 0;

    virtual std::vector<std::string> getFeatureNames(void) = 0;
    virtual std::vector<std::string> getClassNames(void) = 0;

//    virtual bool setOption(const char* property, const char* value) = 0;
//    virtual bool training(int classCount,
//                       int featureCount,
//                       int* classVectorCount, // na ktorym wektorze konczy sie klasa c (posortowane wektory wg klas)
//                       double* values,  //values[featurenumber * v + f]
//                       std::string* classNames,
//                       std::string* featureNames
//                       )= 0;  //values[featurenumber * v + f]
};

#endif // CLASSIFIERIO_H
