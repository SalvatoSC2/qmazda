/*
 * QMAZDA project: Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2017 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "filenameremap.h"

//This is workarround for Windows crappy file name API

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#include <windows.h>
std::string fileNameRemapWrite(const QString *input)
{
    SetFileApisToANSI();
    std::wstring ws = input->toStdWString();
    long length = GetShortPathNameW(ws.c_str(), NULL, 0);
    if (length == 0)
    {
        HANDLE hFile = CreateFileW(ws.c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
        DWORD written;
        WriteFile(hFile, ws.c_str(), length*sizeof(wchar_t), &written, NULL);
        CloseHandle(hFile);
        length = GetShortPathNameW(ws.c_str(), NULL, 0);
        if (length == 0)
            return input->toLocal8Bit().data();
    }
    wchar_t* buffer = new wchar_t[length];
    GetShortPathNameW(ws.c_str(), buffer, length);
    ws = buffer;
    std::string rs(ws.begin(), ws.end());
    delete[] buffer;
    return rs;
}
std::string fileNameRemapRead(const QString *input)
{
    SetFileApisToANSI();
    std::wstring ws = input->toStdWString();
    long length = GetShortPathNameW(ws.c_str(), NULL, 0);
    if (length == 0)
        return input->toLocal8Bit().data();
    wchar_t* buffer = new wchar_t[length];
    GetShortPathNameW(ws.c_str(), buffer, length);
    ws = buffer;
    std::string rs(ws.begin(), ws.end());
    delete[] buffer;
    return rs;
}
#else
std::string fileNameRemapWrite(const QString* input)
{
    return input->toStdString().c_str();
    //return input->toLocal8Bit().data();
}
std::string fileNameRemapRead(const QString* input)
{
    return input->toStdString().c_str();
    //return input->toLocal8Bit().data();
}
#endif
