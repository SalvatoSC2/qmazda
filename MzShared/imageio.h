/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGEIO_H
#define IMAGEIO_H

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#ifndef MZDIMENSIONS
#define MZDIMENSIONS 3
#endif

//#define MZPIXEL8 0
//#define MZPIXEL16 2
//#define MZPIXELRGB 1

#define MZPIXEL_UI8 1
#define MZPIXEL_UI16 2
#define MZPIXEL_RGB24 3
#define MZPIXEL_RGB48 6


extern const unsigned int RoiDefaultColors[16];


#define for_begin(x, xmin, nmin, nmax) \
    for(j = nmin; j < nmax; j++) x[j] = xmin[j];\
    do {

#define for_end(x, xmin, xmax, nmin, nmax) \
    for(j = nmin; j < nmax; j++) {\
        x[j]++;\
        if((int)x[j] < (int)xmax[j]) {break;}\
        else{x[j] = xmin[j];}\
    }}while(j < nmax);


struct PMS_ROI_2001
{
  char magic[12];
  uint16_t xmax;
  uint16_t ymax;
  uint16_t roi_text_length[16];
  uint16_t reserved[32];
};

struct MzBounds
{
    unsigned int min[MZDIMENSIONS];
    unsigned int max[MZDIMENSIONS];
};

class MzImage
{
public:
    MzImage()
    {
        pixel = MZPIXEL_UI8;
        data = NULL;
        //deleteondestruct = true;
        for(int i = 0; i < MZDIMENSIONS; i++)
        {
            size[i] = 0;
            voxelsize[i] = 1.0;
        }
    }
    ~MzImage()
    {
        //if(data != NULL && deleteondestruct) delete[] data;
        if(data != NULL) delete[] data;
    }
    void allocate(unsigned int x[MZDIMENSIONS], int pixeltype)
    {
        if(data != NULL) delete[] data;
        for(int i = 0; i < MZDIMENSIONS; i++)
        {
            size[i] = x[i];
//            voxelsize[i] = 1.0;
        }
        //pixelsize = (pixeltype&MZPIXEL16 ?2 :1)*(pixeltype&MZPIXELRGB ?3 :1);
        pixel = pixeltype;
        linesize = (((pixel * size[0])+3)>>2)<<2;
        unsigned int allsize = 1;
        for(int i = 1; i < MZDIMENSIONS; i++) allsize *= size[i];
        data = new unsigned char[allsize*linesize];
        memset(data, 0,  allsize*linesize);
    }
    unsigned char* line(unsigned int x[MZDIMENSIONS])
    {
        unsigned int p = x[MZDIMENSIONS - 1];
        for(unsigned int d = MZDIMENSIONS-2; d > 0; d--)
        {
            p *= size[d];
            p += x[d];
        }
        return data+(p*linesize);
    }

    unsigned char* memlocation(unsigned char* line, unsigned int x)
    {
        return line + (x*pixel);
    }

    unsigned char* memlocation(unsigned int x[MZDIMENSIONS])
    {
        return line(x) + (x[0]*pixel);
    }


    void clear(void)
    {
       if(data != NULL) delete[] data;
       data = NULL;
       //deleteondestruct = true;
       for(int i = 0; i < MZDIMENSIONS; i++)
       {
           size[i] = 0;
//           voxelsize[i] = 1.0;
       }
    }
    void zeromem(void)
    {
        unsigned int allsize = 1;
        for(int i = 1; i < MZDIMENSIONS; i++) allsize *= size[i];
        memset(data, 0,  allsize*linesize);
    }
    unsigned int pixelSize() {return pixel;}
    unsigned char* data;
    unsigned int size[MZDIMENSIONS];
    double voxelsize[MZDIMENSIONS];
    unsigned int linesize;
    //bool deleteondestruct;
private:
    unsigned int pixel;
    //unsigned int pixelsize;
};

struct MzNormalization
{
    int min;
    int max;
    unsigned int bpp;
};

class MzImageFunctions
{
public:
/**
 * @brief LoadMzImage
 * @param filename
 * @return image
 */
bool static LoadMzImage(const char *filename, MzImage* toReturn);


/**
 * @brief SaveMzImage
 * @param filename
 * @param image
 * @return true on success or false otherwise.
 */
bool static SaveMzImage(const char* filename, MzImage* image);
};

#endif // IMAGEIO_H

















//class MzRoi
//{
//public:
//    MzRoi()
//    {
//        visible = true;
//        _data = NULL;
//        for(int i = 0; i < MZDIMENSIONS; i++)
//        {
//            size[i] = 0;
//            shift[i] = 0;
//        }
//    }
//    MzRoi(const MzRoi& other)
//    {
//        //erase();
//        for(int i = 0; i < MZDIMENSIONS; i++)
//        {
//            size[i] = other.size[i];
//            shift[i] = other.shift[i];
//        }
//        rgbacolor = other.rgbacolor;
//        linesize = other.linesize;
//        name = other.name;
//        visible = other.visible;
//        _data = other._data;
//        _data++;

//        printf
//    }
//    ~MzRoi()
//    {
//        erase();
//    }
//    void erase(void)
//    {
//        if(_data != NULL)
//        {
//            if(*_data <= 0) delete[] _data;
//            else *_data--;
//        }
//        _data = NULL;
//        for(int i = 0; i < MZDIMENSIONS; i++)
//        {
//            size[i] = 0;
//            shift[i] = 0;
//        }
//    }
//    void allocate(unsigned int x[MZDIMENSIONS])
//    {
//        erase();
//        for(int i = 0; i < MZDIMENSIONS; i++) size[i] = x[i];
//        linesize = size[0] + ((sizeof(MZROIALIGNTYPE)<<3) - 1);
//        linesize /= (sizeof(MZROIALIGNTYPE)<<3);
//        unsigned int allsize = 1;
//        for(int i = 1; i < MZDIMENSIONS; i++) allsize *= size[i];
//        _data = new MZROIALIGNTYPE[allsize*linesize+1];
//        memset(_data, 0,  (allsize*linesize+1)*sizeof(MZROIALIGNTYPE));
//    }
//    MZROIALIGNTYPE* line(unsigned int x[MZDIMENSIONS])
//    {
//        unsigned int p = x[MZDIMENSIONS - 1];
//        for(unsigned int d = MZDIMENSIONS-2; d > 0; d--)
//        {
//            p *= size[d];
//            p += x[d];
//        }
//        return data()+(p*linesize);
//    }
//    MZROIALIGNTYPE* memlocation(unsigned int x[MZDIMENSIONS])
//    {
//        return line(x) + (x[0] / (sizeof(MZROIALIGNTYPE)<<3));
//    }
//    static void memlocation(unsigned int x, MZROIALIGNTYPE* l, MZROIALIGNTYPE* mask, MZROIALIGNTYPE** pack)
//    {
//        *mask = ((MZROIALIGNTYPE)1 << (x & ((sizeof(MZROIALIGNTYPE)<<3) - 1)));
//        *pack = (l + (x / (sizeof(MZROIALIGNTYPE)<<3)));
//    }
//    void memlocation(unsigned int x[MZDIMENSIONS], MZROIALIGNTYPE* mask, MZROIALIGNTYPE** pack)
//    {
//        MZROIALIGNTYPE* l = line(x);
//        memlocation(x[0], l, mask, pack);
//    }
//    static bool is(unsigned int x, MZROIALIGNTYPE* l)
//    {
//        MZROIALIGNTYPE mask;
//        MZROIALIGNTYPE* pack;
//        memlocation(x, l, &mask, &pack);
//        if(*pack & mask) return true;
//        else return false;
//    }
//    bool is(unsigned int x[MZDIMENSIONS])
//    {
//        MZROIALIGNTYPE mask;
//        MZROIALIGNTYPE* pack;
//        memlocation(x, &mask, &pack);
//        if(*pack & mask) return true;
//        else return false;
//    }
//    static void set(unsigned int x, MZROIALIGNTYPE* l)
//    {
//        MZROIALIGNTYPE mask;
//        MZROIALIGNTYPE* pack;
//        memlocation(x, l, &mask, &pack);
//        *pack |= mask;
//    }
//    void set(unsigned int x[MZDIMENSIONS])
//    {
//        MZROIALIGNTYPE mask;
//        MZROIALIGNTYPE* pack;
//        memlocation(x, &mask, &pack);
//        *pack |= mask;
//    }
//    static void clr(unsigned int x, MZROIALIGNTYPE* l)
//    {
//        MZROIALIGNTYPE mask;
//        MZROIALIGNTYPE* pack;
//        memlocation(x, l, &mask, &pack);
//        mask = ~mask;
//        *pack &= mask;
//    }
//    void clr(unsigned int x[MZDIMENSIONS])
//    {
//        MZROIALIGNTYPE mask;
//        MZROIALIGNTYPE* pack;
//        memlocation(x, &mask, &pack);
//        mask = ~mask;
//        *pack &= mask;
//    }

//    MZROIALIGNTYPE* data(void)
//    {
//        if(_data==NULL) return NULL;
//        return _data+1;
//    }
//    unsigned int rgbacolor;
//    unsigned int size[MZDIMENSIONS];
//    unsigned int linesize;
//    int shift[MZDIMENSIONS];
//    std::string name;
//    bool visible;
//private:
//    MZROIALIGNTYPE* _data;
//};
