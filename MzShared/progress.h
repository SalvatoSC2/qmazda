/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef PROGRESS_H
#define PROGRESS_H

#include <QDialog>
#include <QThread>
#include "mzselectioninterface.h"

//class LdaWorker;

namespace Ui {
class Progress;
}

class PluginWorker;

class Progress : public QDialog
{
    Q_OBJECT
    
public:
    explicit Progress(QWidget *parent = 0);
    ~Progress();
    void closeEvent(QCloseEvent *e);

    //void setMaxValues(int timerestriction, int maxdimensions, int featurenumber, PluginWorker* worker);
    void setMaxValues(int timerestriction, quint64 maxstepsi, QObject *cancel);
    void NotifyProgressStep(void);
    void NotifyProgressText(QString text);

private slots:
    void on_cancelButton_clicked();

private:
    Ui::Progress *ui;
    time_t starttime, maxtime;
    quint64 maxsteps, step;
    MzSelectionPluginInterface* canceler;
};
#endif // PROGRESS_H
