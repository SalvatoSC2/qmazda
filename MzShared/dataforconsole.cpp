/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "dataforconsole.h"
#include "csvio.h"
#include <limits>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <string>
#include <vector>

using namespace std;

DataTable::DataTable()
{
    column_count = 0;
    row_count = 0;
    row_names = NULL;
    column_names = NULL;
    column_tips = NULL;
    feature_values = NULL;

//    setlocale( LC_ALL, "C" );
//  moved to save/load functions
}

DataTable::~DataTable()
{
    if(feature_values != NULL) delete[] feature_values;
    if(row_names != NULL) delete[] row_names;
    if(column_names != NULL) delete[] column_names;
}

void DataTable::Reset(void)
{
    if(feature_values != NULL) delete[] feature_values;
    if(row_names != NULL) delete[] row_names;
    if(column_names != NULL) delete[] column_names;
    if(column_tips != NULL) delete[] column_tips;
    column_count = 0;
    row_count = 0;
    row_names = NULL;
    column_names = NULL;
    column_tips = NULL;
    feature_values = NULL;
}



/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
///                                    TO DO
/// Wywalic ladowanie i zapis danych csv i oprzec je na klasie CsvIo
/// wywalic hexToDouble doubleToHex
/// Zrobic dynamiczne values:  std:vector <double*> values;
/// Pozbyc sie nadmiarowych licznikow vectornumber; classnumber; featurenumber;
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//char* DataTable::doubleToHex(const double value)
//{
//    static char text[sizeof(double)*2+4];
//    int test_endian;
//    unsigned char* byte;
//    int v;
//    char* ptr;

//    test_endian = 1;
//    if(((unsigned char*)&test_endian)[0] == 0) test_endian = 0;

//    ptr = text;
//    byte = (unsigned char*)&value;
//    sprintf(ptr, "$"); ptr++;//ptr+=2;
//    if(test_endian) for(v = sizeof(double)-1; v >= 0; v--)
//    {
//        sprintf(ptr, "%.2X", (int)byte[v]); ptr+=2;
//    }
//    else for(v = 0; v < (int) sizeof(double); v++)
//    {
//        sprintf(ptr, "%.2X", (int)byte[v]); ptr+=2;
//    }
//    return text;
//}

//#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
//    #define strtouq _strtoui64
//#endif

//double DataTable::hexToDouble(const char* text)
//{
//    if(text[0] == '$')
//    {
//        unsigned long long longint;
//        char* ptr;
//        longint = strtouq(text+1, &ptr, 16);
//        if(ptr == text) return std::numeric_limits<double>::quiet_NaN();
//        return *((double*)&longint);
//    }
//    else
//    {
//        char* endptr = NULL;
//        double value = strtod(text, &endptr);
//        if (*endptr) return std::numeric_limits<double>::quiet_NaN();
//        return value;
//    }
//}


//int DataTable::loadFromFile(string fileName)
//{
//    int number_of_features = 0;
//    int category_index = -2;
//    int comment_index = -2;
//    int lines_of_data = 0;
//    string line;
//    ifstream file;
//    file.open(fileName.c_str());
//    if (!file.is_open()) return -1;
//    if (!file.good()) return -1;
//    char* separator = (char*)",;\t";
//    char* toremove = (char*)"\r\n\"";

//    char *prevloc = setlocale(LC_ALL, NULL);
//    if(prevloc) prevloc = strdup(prevloc);
//    setlocale(LC_ALL, "C");

//    getline(file, line);

//    size_t found = line.find_first_of(toremove);
//    while (found != std::string::npos)
//    {
//        line.erase(line.begin()+found);
//        found=line.find_first_of(toremove, found);
//    }

//    number_of_features = 1;
//    found = line.find_first_of(separator);
//    while (found != std::string::npos)
//    {
//        number_of_features++;
//        found=line.find_first_of(separator, found+1);
//    }

//    if(number_of_features < 2)
//    {
//        setlocale(LC_ALL, prevloc);
//        free(prevloc);
//        return -2;
//    }

//    if(row_names != NULL) delete[] row_names;
//    //row_count = number_of_features-1;
//    row_names = new string[number_of_features];

//    category_index = -1;
//    comment_index = -1;
//    size_t lastfound = 0;
//    found = 0;
//    for(int r = 0; r < number_of_features; r++)
//    {
//        found = line.find_first_of(separator, found+1);
//        if(found == std::string::npos) found = line.length();

//        if(line.substr(lastfound, found-lastfound) == "Category")
//        {
//            category_index = r;
//        }
//        else if(line.substr(lastfound, found-lastfound) == "Comment")
//        {
//            comment_index = r;
//        }
//        else
//        {
//            row_names[category_index>=0 ?r-1 :r] = line.substr(lastfound, found-lastfound);
//        }
//        lastfound = found+1;
//    }
//    //fflush(stdout);

//    row_count = number_of_features;
//    if(comment_index >= 0) row_count--;
//    if(category_index >= 0) row_count--;

//    while(file.good())
//    {
//        getline(file, line);
//        size_t found = line.find_first_of(toremove);
//        while (found != std::string::npos)
//        {
//            line.erase(found, found);
//            found=line.find_first_of(toremove, found+1);
//        }

//        int number_of = 1;
//        found = line.find_first_of(separator);
//        while (found != std::string::npos)
//        {
//            number_of++;
//            found=line.find_first_of(separator, found+1);
//        }
//        if(number_of == number_of_features) lines_of_data++;
//    }

//    if(lines_of_data <= 0)
//    {
//        setlocale(LC_ALL, prevloc);
//        free(prevloc);
//        return -4;
//    }
//    file.close();
//    file.open(fileName.c_str());
//    if (!file.good())
//    {
//        setlocale(LC_ALL, prevloc);
//        free(prevloc);
//        return -5;
//    }

//    if(feature_values != NULL) delete[] feature_values;
//    if(column_names != NULL) delete[] column_names;
//    if(column_tips != NULL) delete[] column_tips;
//    column_count = lines_of_data;

//    column_names = new string[column_count];
//    column_tips = new string[column_count];

//    feature_values = new double[column_count*row_count];

//    getline(file, line);

//    int c = 0;
//    while(file.good())
//    {
//        getline(file, line);
//        found = line.find_first_of(toremove);
//        while (found != std::string::npos)
//        {
//            line.erase(found, found);
//            found=line.find_first_of(toremove, found+1);
//        }
//        int number_of = 1;
//        found = line.find_first_of(separator);
//        while (found != std::string::npos)
//        {
//            number_of++;
//            found=line.find_first_of(separator, found+1);
//        }

//        if(number_of == number_of_features)
//        {
//            lastfound = 0;
//            found = 0;
//            int rr = 0;
//            for(int r = 0; r < number_of_features; r++)
//            {
//                found = line.find_first_of(separator, found+1);
//                if(found == std::string::npos) found = line.length();

//                if(category_index == r)
//                {
//                    column_names[c] = line.substr(lastfound, found-lastfound);
//                }
//                else if(comment_index == r)
//                {
//                    column_tips[c] = line.substr(lastfound, found-lastfound);
//                }
//                else
//                {
//                    double vv = hexToDouble(line.substr(lastfound, found-lastfound).c_str());
//                    feature_values[c*row_count + rr] = vv;
//                    rr++;
//                }
//                lastfound = found+1;
//            }
//            c++;
//        }
//    }
//    setlocale(LC_ALL, prevloc);
//    free(prevloc);
//    return c;
//}

//int DataTable::saveToFile(string fileName, bool hexa, char* columns2save, char* rows2save)
//{
//    bool do_r, do_c;
//    ofstream file;
//    file.open(fileName.c_str());
//    //QFile file(fileName);

//    if (!file.is_open()) return -1;
//    if (!file.good()) return -1;

//    //if(!file.open(QIODevice::WriteOnly| QIODevice::Text)) return -1;
//    //QTextStream out(&file);
//    int r, c;

//    char *prevloc = setlocale(LC_ALL, NULL);
//    if(prevloc) prevloc = strdup(prevloc);
//    setlocale(LC_ALL, "C");

//    for(r = 0; r < row_count; r++)
//    {
//        do_r = true;
//        if(rows2save != NULL) do_r = (rows2save[r] > 0);

//        if(do_r)
//        {
//            file<<row_names[r]<<",";
//        }
//    }

//    bool tipfound = false;
//    if(column_tips != NULL)
//    {
//        for(c = 0; c < column_count; c++)
//        {
//            if(! column_tips[c].empty())
//            {
//                tipfound = true;
//                break;
//            }
//        }
//    }
//    if(tipfound)
//        file<<"Category,Comment\n";
//    else
//        file<<"Category\n";

//    for(c = 0; c < column_count; c++)
//    {
//        do_c = true;
//        if(columns2save != NULL) do_c = (columns2save[c] > 0);

//        if(do_c)
//        {
//            for(r = 0; r < row_count; r++)
//            {
//                do_r = true;
//                if(rows2save != NULL) do_r = (rows2save[r] > 0);

//                if(do_r)
//                {
//                    if(hexa) file << doubleToHex(feature_values[row_count*c+r]) <<",";
//                    else file << feature_values[row_count*c+r] << ",";
//                }
//            }

//            if(tipfound)
//                file << column_names[c] << "," << column_tips[c] << "\n";
//            else
//                file << column_names[c] << "\n";
//        }
//    }
//    setlocale(LC_ALL, prevloc);
//    free(prevloc);
//    return 0;
//}


int DataTable::loadFromFile(string fileName)
{
    int r, c;
    ifstream file;
    file.open(fileName.c_str());
    if (!file.is_open()) return -1;
    if (!file.good()) return -1;

    CsvIo csvio;
    std::vector <std::string> feature_names;
    if(! csvio.loadCSVStreamHeader(&file, &feature_names) || feature_names.size() <= 0)
    {
        csvio.loadCSVStreamClose();
        return -1;
    }
    unsigned int feature_names_size = feature_names.size();
    std::vector <std::string> category_table;
    std::vector <std::string> comment_table;
    std::vector <double*> feature_vector_table;
    double* feature_vector = new double[feature_names_size];
    for(r = 0; r < feature_names_size; r++)
        feature_vector[r] = std::numeric_limits<double>::quiet_NaN();
    std::string category;
    std::string comment;
    while(csvio.loadCSVStreamDataLine(&file, feature_vector, &category, &comment))
    {
        category_table.push_back(category);
        comment_table.push_back(comment);
        feature_vector_table.push_back(feature_vector);
        feature_vector = NULL;
        feature_vector = new double[feature_names_size];
        for(r = 0; r < feature_names_size; r++)
            feature_vector[r] = std::numeric_limits<double>::quiet_NaN();
    }
    delete[] feature_vector;

    if(feature_vector_table.size() <= 0)
    {
        csvio.loadCSVStreamClose();
        return -2;
    }
    if(row_names != NULL) delete[] row_names;
    if(feature_values != NULL) delete[] feature_values;
    if(column_names != NULL) delete[] column_names;
    if(column_tips != NULL) delete[] column_tips;

    row_count = feature_names.size();
    column_count = feature_vector_table.size();
    row_names = new std::string[row_count];
    feature_values = new double[column_count*row_count];

    for(r = 0; r < row_count; r++)
        row_names[r] = feature_names[r];

    for(c = 0; c < column_count; c++)
        for(r = 0; r < row_count; r++)
            feature_values[c*row_count + r] = feature_vector_table[c][r];
    for(c = 0; c < column_count; c++)
        delete[] feature_vector_table[c];
    feature_vector_table.clear();

    column_names = new std::string[column_count];
    column_tips = new std::string[column_count];
    for(c = 0; c < column_count; c++)
    {
        column_names[c] = category_table[c];
        column_tips[c] = comment_table[c];
    }
    category_table.clear();
    comment_table.clear();
    csvio.loadCSVStreamClose();
    return column_count;
}

int DataTable::saveToFile(string fileName, bool hexa, char* columns2save, char* rows2save)
{
    int r, c;
    bool do_r, do_c;
    ofstream file;
    file.open(fileName.c_str());
    if (!file.is_open()) return -1;
    if (!file.good()) return -1;

    bool tipfound = false;
    if(column_tips != NULL)
    {
        for(c = 0; c < column_count; c++)
        {
            do_c = true;
            if(columns2save != NULL) do_c = (columns2save[c] > 0);
            if(do_c)
            {
                if(! column_tips[c].empty())
                {
                    tipfound = true;
                    break;
                }
            }
        }
    }

    CsvIo csvio;
    std::vector <std::string> feature_names;
    for(r = 0; r < row_count; r++)
    {   do_r = true;
        if(rows2save != NULL) do_r = (rows2save[r] > 0);
        if(do_r)
        {
            feature_names.push_back(row_names[r]);
        }
    }
    csvio.saveCSVStreamHeader(&file, &feature_names, true, tipfound);
    feature_names.clear();
//    double* feature_vector = new double[column_count];
    double* feature_vector = new double[row_count];
    for(c = 0; c < column_count; c++)
    {
        do_c = true;
        if(columns2save != NULL)
            do_c = (columns2save[c] > 0);

        if(do_c)
        {
            unsigned int k = 0;
            for(r = 0; r < row_count; r++)
            {
                do_r = true;
                if(rows2save != NULL) do_r = (rows2save[r] > 0);
                if(do_r)
                {
                    feature_vector[k] = feature_values[row_count*c+r];
                    k++;
                }
            }
            csvio.saveCSVStreamDataLine(&file, feature_vector, hexa, &(column_names[c]), tipfound ? &(column_tips[c]) :NULL);
        }
    }
    delete[] feature_vector;

    csvio.saveCSVStreamClose();
    return 0;
}


DataForSegmentation::DataForSegmentation()
{
    featurenames.clear();
    resultnames.clear();
    vectornumber = 0;
    size[3] = 0;
    size[2] = 0;
    size[1] = 0;
    size[0] = 0;
    values = NULL;
    result = NULL;
}


DataForSelection::DataForSelection()
{
    vectornumber = 0;
    classnumber = 0;
    featurenumber = 0;
    classendvectorindex = NULL;
    featureoriginalindex = NULL;
    classnames = NULL;
    featurenames = NULL;
    values = NULL;
}

DataForSelection::~DataForSelection()
{
    if(classendvectorindex != NULL) delete[] classendvectorindex;
    if(featureoriginalindex != NULL) delete[] featureoriginalindex;
    if(classnames != NULL) delete[] classnames;
    if(featurenames != NULL) delete[] featurenames;
    if(values != NULL) delete[] values;
}

void DataForSelection::Reset(void)
{
    //if(vectorclassindex != NULL) delete[] vectorclassindex;
    if(classendvectorindex != NULL) delete[] classendvectorindex;
    if(featureoriginalindex != NULL) delete[] featureoriginalindex;
    if(classnames != NULL) delete[] classnames;
    if(featurenames != NULL) delete[] featurenames;
    if(values != NULL) delete[] values;
    vectornumber = 0;
    classnumber = 0;
    featurenumber = 0;
    //vectorclassindex = NULL;
    classendvectorindex = NULL;
    featureoriginalindex = NULL;
    classnames = NULL;
    featurenames = NULL;
    values = NULL;
}

//DataForSelection::DataForSelection()
//{
//    vectornumber = 0;
//    classendvectorindex = NULL;
//    values = NULL;
//    featureoriginalindex = NULL;
//}

//DataForSelection::~DataForSelection()
//{
//    if(featureoriginalindex != NULL)
//        delete[] featureoriginalindex;
//    if(classendvectorindex != NULL)
//        delete[] classendvectorindex;
//    if(values != NULL)
//        delete[] values;
//}

//void DataForSelection::Reset(void)
//{
//    if(featureoriginalindex != NULL)
//        delete[] featureoriginalindex;
//    featureoriginalindex = NULL;
//    if(classendvectorindex != NULL)
//        delete[] classendvectorindex;
//    classendvectorindex = NULL;
//    if(values != NULL)
//        delete[] values;
//    values = NULL;
//    vectornumber = 0;
//    classnames.clear();
//    featurenames.clear();
//}


SelectedFeatures::SelectedFeatures()
{
    featurenumber = 0;
    featureoriginalindex = NULL;
}

SelectedFeatures::~SelectedFeatures()
{
    featurenumber = 0;
    if(featureoriginalindex != NULL) delete[] featureoriginalindex;
}

void SelectedFeatures::Reset(void)
{
    featurenumber = 0;
    if(featureoriginalindex != NULL) delete[] featureoriginalindex;
    featureoriginalindex = NULL;
}

//bool DataForSelection::setData(DataTable* datatable)
//{
//    int v, f, c;
//    if(datatable->feature_values != NULL) delete[] datatable->feature_values;
//    if(datatable->row_names != NULL) delete[] datatable->row_names;
//    if(datatable->column_names != NULL) delete[] datatable->column_names;
//    datatable->column_count = vectornumber;
//    datatable->row_count = featurenumber;
//    datatable->column_names = new string[datatable->column_count];
//    datatable->row_names = new string[datatable->row_count];
//    datatable->feature_values = new double[datatable->column_count*datatable->row_count];

//    for(v = 0; v < datatable->row_count; v++)
//    {
//        datatable->row_names[v] = featurenames[v];
//    }
//    int vs = 0;
//    for(c = 0; c < classnumber; c++)
//    {
//        int vm = classendvectorindex[c];
//        for(v = vs; v < vm; v++)
//        {
//            datatable->column_names[v] = classnames[c];
//            for(f = 0; f < featurenumber; f++)
//            {
//                datatable->feature_values[v * datatable->row_count + f] = values[v * featurenumber + f];
//            }
//        }
//        vs = vm;
//    }
//    return true;
//}


//bool DataForSelection::getData(DataTable* datatable, int checked_mask, char* row_icons, char* column_icons)
//{
//    bool doit;

//    if(datatable->column_count <= 0 || datatable->row_count <= 0 || datatable->row_names == NULL ||
//       datatable->column_names == NULL || datatable->feature_values == NULL) return false;

//    int c, r;
//    vector<string> classlist;
//    vector<string> featurelist;

//    int* vectorsinclass = new int[datatable->column_count];
//    for(c = 0; c < datatable->column_count; c++) vectorsinclass[c] = 0;

//    int* vectorclassindex = new int[datatable->column_count];
//    for(c = 0; c < datatable->column_count; c++) vectorclassindex[c] = -1;

//    int vectornumber = 0;
//    for(c = 0; c < datatable->column_count; c++)
//    {
//        if(checked_mask == 0) doit = true;
//        else if(((checked_mask & USE_CHECKED_VECTORS) && column_icons[c] > 0) || ((checked_mask & USE_UNCHECKED_VECTORS) && column_icons[c] < 0)) doit = true;
//        else doit = false;

//        if(doit)
//        {


//            int index;// = -1;//classlist.indexOf(datatable->column_names[c]);
//            for(index = 0; index < (int)classlist.size(); index++)
//                if(classlist.at(index) == datatable->column_names[c]) break;
//            if(index >= (int)classlist.size()) index = -1;

//            if(index < 0)
//            {
//                vectorsinclass[classlist.size()]++;
//                vectorclassindex[c] = classlist.size();
//                //classlist << datatable->column_names[c];
//                //classlist.append(datatable->column_names[c]);
//                classlist.insert(classlist.end(), datatable->column_names[c]);
//            }
//            else
//            {
//                vectorsinclass[index]++;
//                vectorclassindex[c] = index;
//            }
//            vectornumber++;
//        }
//    }
////    classlist.removeDuplicates();
//    int classnumber = classlist.size();

//    int featurenumber = 0;
//    for(r = 0; r < datatable->row_count; r++)
//    {
//        if(checked_mask == 0) doit = true;
//        else if(((checked_mask & USE_CHECKED_FEATURES) && row_icons[r] > 0) || ((checked_mask & USE_UNCHECKED_FEATURES) && row_icons[r] < 0)) doit = true;
//        else doit = false;

//        if(doit)
//        {
//            //featurelist.append(datatable->row_names[r]);
//            featurelist.insert(featurelist.end(), datatable->row_names[r]);
//            featurenumber++;
//        }
//    }

//    Reset();
//    this->classnumber = classnumber;
//    this->featurenumber = featurenumber;
//    this->vectornumber = vectornumber;

//    classendvectorindex = new int[classnumber];
//    r = 0;
//    for(c = 0; c < classnumber; c++)
//    {
//        r += vectorsinclass[c];
//        vectorsinclass[c] = 0;
//        classendvectorindex[c] = r;
//    }

//    int v = 0;
//    values = new double[featurenumber*vectornumber];

//    //vectorsinclass[c] = 0;
//    //vectorclassindex[c] = index;
//    //outputdata->classendvectorindex[c]
//    for(c = 0; c < datatable->column_count; c++)
//    {
//        //if(checked_vectors ? column_icons[c] > 0 : column_icons[c] < 0) doit = true;
//        if(vectorclassindex[c] >= 0)
//        {
//            int classs = vectorclassindex[c];
//            int vv = vectorsinclass[classs] + (classs == 0 ? 0 : classendvectorindex[classs-1]);
//            vectorsinclass[classs]++;

//            if(vv >= vectornumber)
//                vv = vectornumber-1;

//            int f = 0;
//            for(r = 0; r < datatable->row_count; r++)
//            {
//                //if(checked_features ? row_icons[r] > 0 : row_icons[r] < 0)
//                if(checked_mask == 0) doit = true;
//                else if(((checked_mask & USE_CHECKED_FEATURES) && row_icons[r] > 0) || ((checked_mask & USE_UNCHECKED_FEATURES) && row_icons[r] < 0)) doit = true;
//                else doit = false;

//                if(doit)
//                {
//                    values[featurenumber*vv+f] = datatable->feature_values[datatable->row_count*c+r];
//                    f++;
//                }
//            }
//            v++;
//        }
//    }

//    delete[] vectorclassindex;
//    delete[] vectorsinclass;

//    classnames = new string[classnumber];
//    for(c = 0; c < classnumber; c++)
//    {
//        classnames[c] = classlist.at(c);
//    }

//    featurenames = new string[featurenumber];
//    for(r = 0; r < featurenumber; r++)
//    {
//        featurenames[r] = featurelist.at(r);
//    }

//    featureoriginalindex = new int[featurenumber];
//    c = 0;
//    for(r = 0; r < datatable->row_count; r++)
//    {
//        //if((checked_features ? row_icons[r] > 0 : row_icons[r] < 0))
//        if(checked_mask == 0) doit = true;
//        else if(((checked_mask & USE_CHECKED_FEATURES) && row_icons[r] > 0) || ((checked_mask & USE_UNCHECKED_FEATURES) && row_icons[r] < 0)) doit = true;
//        else doit = false;

//        if(doit)
//        {
//            featureoriginalindex[c] = r;
//            c++;
//        }
//    }
//    return true;
//}


