#ifndef PROPERTYVALUETREE_H
#define PROPERTYVALUETREE_H

#include <QDialog>
#include "../MzShared/parametertreewidget.h"

namespace Ui {
class PropertyValueTree;
}

class PropertyValueTree : public QDialog
{
    Q_OBJECT

public:
    explicit PropertyValueTree(QWidget *parent = 0);
    ~PropertyValueTree();
    QString getValue(QString name);
    QString getValue(unsigned int index);
    void CreateGroup(const std::string name, const std::string tip);
    void CreateOption(const std::string name, const std::string value, const std::string tip = "");
    void CreateEnd(void);

private:
    Ui::PropertyValueTree *ui;
    ParameterTreeWidget* tree;
    QTreeWidgetItem* item;
};

#endif // PROPERTYVALUETREE_H
