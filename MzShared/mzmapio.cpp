/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../MzShared/mzmapio.h"
#include <tiffio.h>

/**
 * @brief SaveSubsequentMapToPagedTiff saves a subsequent map to paged, floating point tiff file.
 * @param filename name of the file. If NULL, the next map is saved to the file previously specified.
 * @param map pointer to map structure.
 * @param page consecutive number of a map
 * @param pages number of all maps to be saved
 * @return true on success or false otherwise.
 */
bool MzMapFunctions::SaveSubsequentMapToPagedTiff(const char *filename, MzMap* map, int page, int pages)
{
    static TIFF* tiff = NULL;

    if(filename != NULL)
    {
        if(tiff != NULL)
        {
            TIFFClose(tiff);
            tiff = NULL;
        }
        tiff = TIFFOpen(filename, "w");
        if(!tiff) return false;
    }

    if(map != NULL)
    {
        unsigned int y;
        TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, map->size[0]);
        TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, map->size[1]);
        switch(map->pixel)
        {
        case MZPIXELF64:
        {
            TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, sizeof(double)*8);
        } break;
        case MZPIXELF32:
        {
            TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, sizeof(float)*8);
        } break;
        }
        TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
        TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
        TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
        TIFFSetField(tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
        TIFFSetField(tiff, TIFFTAG_XRESOLUTION, (float)100);
        TIFFSetField(tiff, TIFFTAG_YRESOLUTION, (float)100);
        TIFFSetField(tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);

         if(map->name.c_str() != NULL)
        {
            TIFFSetField(tiff, TIFFTAG_IMAGEDESCRIPTION, map->name.c_str());
            TIFFSetField(tiff, TIFFTAG_DOCUMENTNAME, map->name.c_str());
        }
        TIFFSetField(tiff, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
        TIFFSetField(tiff, TIFFTAG_PAGENUMBER, page, pages);

        switch(map->pixel)
        {
        case MZPIXELF64:
        {
            for(y = 0; y < map->size[1]; y++)
            {
                TIFFWriteScanline(tiff, (double*)map->data+(map->size[0]*y), y, 0);
            }
        } break;
        case MZPIXELF32:
        {
            for(y = 0; y < map->size[1]; y++)
            {
                TIFFWriteScanline(tiff, (float*)map->data+(map->size[0]*y), y, 0);
            }
        } break;
        }
        TIFFWriteDirectory(tiff);
        return true;
    }
    if(filename == NULL && map == NULL)
    {
        if(tiff != NULL)
        {
            TIFFClose(tiff);
            tiff = NULL;
        }
    }
    return true;
}

/**
 * @brief SaveMapsToPagedTiff saves the set of maps to paged, floating point tiff file.
 * @param filename name of the file.
 * @param maps vector of maps.
 * @return true on success or false otherwise.
 */
bool MzMapFunctions::SaveMapsToPagedTiff(const char *filename, std::vector<MzMap> maps)
{
    unsigned int page, pages;
    pages = maps.size();

    if(!SaveSubsequentMapToPagedTiff(filename, NULL, 0, pages)) return false;
    for(page = 0; page < pages;  page++)
    {
        SaveSubsequentMapToPagedTiff(NULL, &(maps[page]), page, pages);
    }
    SaveSubsequentMapToPagedTiff(NULL, NULL, page, pages);
    return true;
}

/**
 * @brief LoadSubsequentMapFromPagedTiff loads single map from paged, floating point tiff file.
 * @param filename name of the file. If NULL, the next map from previously open file is fetched.
 * @param map pointer to map structure. If NULL, the new file is opened or closed.
 * @return positive if there are more maps to fetch or negative for error.
 */
int MzMapFunctions::LoadSubsequentMapFromPagedTiff(const char *filename, MzMap* map)
{
    static TIFF* tiff = NULL;
    if(filename != NULL)
    {
        if(tiff != NULL)
        {
            TIFFClose(tiff);
            tiff = NULL;
        }
        tiff = TIFFOpen(filename, "r");
        if(!tiff) return -1;
    }

    if(map != NULL)
    {
        map->erase();
        char* name;
        uint32 temp32;
        uint16 temp16, bps, format;
        if(!tiff) return -2;
        unsigned int size[MZDIMENSIONS];
        TIFFGetFieldDefaulted(tiff, TIFFTAG_IMAGEWIDTH, &temp32); size[0] = temp32;
        TIFFGetFieldDefaulted(tiff, TIFFTAG_IMAGELENGTH, &temp32); size[1] = temp32;
        for(int i = 2; i < MZDIMENSIONS; i++) size[i] = 1;
        TIFFGetFieldDefaulted(tiff, TIFFTAG_BITSPERSAMPLE, &bps);
        TIFFGetFieldDefaulted(tiff, TIFFTAG_SAMPLESPERPIXEL, &temp16); if(temp16 != 1)  return -3;
        TIFFGetFieldDefaulted(tiff, TIFFTAG_SAMPLEFORMAT, &format);

        if (!TIFFGetField(tiff, TIFFTAG_IMAGEDESCRIPTION, &name))
        {
            if (!TIFFGetField(tiff, TIFFTAG_DOCUMENTNAME, &name)) name = NULL;
        }
        if(name == NULL) map->name = "";
        else map->name = name;

        if(bps == 32 && format == SAMPLEFORMAT_IEEEFP)
        {
            map->allocate(size, MZPIXELF32);
            for (unsigned int y = 0; y < size[1]; y++) TIFFReadScanline(tiff, ((float*)map->data)+(size[0]*y), y);
        }
        else if(bps == 64 && format == SAMPLEFORMAT_IEEEFP)
        {
            map->allocate(size, MZPIXELF64);
            for (unsigned int y = 0; y < size[1]; y++) TIFFReadScanline(tiff, ((double*)map->data)+(size[0]*y), y);
        }
        else return -4;

        if(!TIFFReadDirectory(tiff))
        {
            TIFFClose(tiff);
            tiff = NULL;
            return 0;
        }
    }

    if(filename == NULL && map == NULL)
    {
        if(tiff != NULL)
        {
            TIFFClose(tiff);
            tiff = NULL;
        }
        return 0;
    }
    return 1;
}

/**
 * @brief LoadMapsFromPagedTiff loads set of maps from paged, floating point tiff file.
 * @param filename name of the file.
 * @return vector of maps.
 */
void MzMapFunctions::LoadMapsFromPagedTiff(const char *filename, std::vector<MzMap*> *toReturn)
{
    int result;
    if(LoadSubsequentMapFromPagedTiff(filename, NULL) <= 0) return;
    do{
        MzMap* newmap = new MzMap;
        result = LoadSubsequentMapFromPagedTiff(NULL, newmap);
        if(result >= 0)
        {
            toReturn->push_back(newmap);
        }
        else delete newmap;
    }while(result > 0);
    LoadSubsequentMapFromPagedTiff(NULL, NULL);
}
