/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "progress.h"
#include "ui_progress.h"
#include "multidimselection.h"
#include <QTime>
#include <stdlib.h>
#include <time.h>

Progress::Progress(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Progress)
{
    ui->setupUi(this);
}

Progress::~Progress()
{
    delete ui;
}

void Progress::NotifyProgressStep(void)
{
    quint64 per1 = 0;
    if(maxsteps > 0)
    {
        step++;
        per1= step*100/maxsteps;
    }
    unsigned int per2 = 0;
    if(maxtime > 0)
    {
        time_t curtime = time(NULL);
        per2 = (curtime - starttime)*100/maxtime;
    }
    per2 = (per1 < per2 ? per2 : per1);
    unsigned int v = ui->progressBar->value();
    if(per2 > v) ui->progressBar->setValue(per2);
}

void Progress::NotifyProgressText(QString text)
{
    text.replace("\r", "");
    text.replace("\n", "");
    ui->plainTextEdit->appendPlainText(text);
}

/*
void Progress::NotifyProgress(int notification, int dimensionality)
{
    QString qtime = QTime::currentTime().toString();
    switch(notification)
    {
    case BEGINS: ui->plainTextEdit->appendPlainText(QString("%1 Begins %2D").arg(qtime).arg(dimensionality)); break;
    case STOPPED: ui->plainTextEdit->appendPlainText(QString("%1 Stopped at feature %2").arg(qtime).arg(dimensionality)); break;
    case CANCELED: ui->plainTextEdit->appendPlainText(QString("%1 Canceled by user").arg(qtime)); break;
    case COMPLETED: ui->plainTextEdit->appendPlainText(QString("%1 Completed").arg(qtime)); break;
    case SUCCESS: ui->plainTextEdit->appendPlainText(QString("%1 Success %2D").arg(qtime).arg(dimensionality)); break;
    case FAILED: ui->plainTextEdit->appendPlainText(QString("%1 Failed %2D").arg(qtime).arg(dimensionality)); break;
    case STEP:
        {
            quint64 per1 = 0;
            if(maxsteps > 0)
            {
                step++;
                per1= step*100/maxsteps;
            }
            unsigned int per2 = 0;
            if(maxtime > 0)
            {
                time_t curtime = time(NULL);
                per2 = (curtime - starttime)*100/maxtime;
            }
            per2 = (per1 < per2 ? per2 : per1);
            unsigned int v = ui->progressBar->value();
            if(per2 > v) ui->progressBar->setValue(per2);
        }
        break;
    }
}

void Progress::setMaxValues(int timerestriction, int maxdimensions, int featurenumber, PluginWorker* worker)
{
    this->worker = worker;
    starttime = time(NULL);
    maxtime = timerestriction;
    maxsteps = 1;
    quint64 maxstepsold = maxsteps;
    step = 0;
    ui->progressBar->setMaximum(100);

    for(int d = 1; d <= maxdimensions; d++)
    {
        for(int k = 0; k < d; k++)
        {
            maxsteps *= (featurenumber-k);
            if(maxsteps / (featurenumber-k) != maxstepsold)
            {
                maxsteps = 0;
                if(maxtime == 0) ui->progressBar->setMaximum(0);
                return;
            }
            maxstepsold = maxsteps;
        }
    }
}
*/
void Progress::setMaxValues(int timerestriction, quint64 maxstepsi, QObject *cancel)
{
//    this->canceler = qobject_cast<MzSelectionPluginInterface*> (cancel);
    this->canceler = dynamic_cast<MzSelectionPluginInterface*> (cancel);

    starttime = time(NULL);
    maxtime = timerestriction;
    maxsteps = maxstepsi;
    step = 0;
    if(maxtime == 0 && maxsteps == 0) ui->progressBar->setMaximum(0);
    else ui->progressBar->setMaximum(100);
}

void Progress::closeEvent(QCloseEvent *e)
{
    on_cancelButton_clicked();
    QDialog::closeEvent(e);
}

void Progress::on_cancelButton_clicked()
{
    if(canceler != NULL) canceler->cancelAnalysis();
}
