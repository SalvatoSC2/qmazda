/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "multidimselection.h"

//------------------------------------------------------------------------------
int MultiDimensionalSelection::GenerateNextCombination(int dim, unsigned int* feature)
{
	int d;
	for(d = 0; d < dim; d++)
	{
		feature[d]++;
        if(feature[d] < feature[d+1]) return 0;
		feature[d] = d;
	}
    return 1;
}

//---------------------------------------------------------------------------
void MultiDimensionalSelection::QualitySorter(int nooffeatures, double* Qtable, unsigned int* Qsorted)
{
	int i, ii, iii;
    for(i = 0; i < nooffeatures; i++)
	{
		for(ii = 0; ii < i; ii++)
		{
            if(Qtable[Qsorted[ii]] < Qtable[i]) break;
		}
		for(iii = i; iii > ii; iii--)
		{
            Qsorted[iii] = Qsorted[iii-1];
		}
        Qsorted[ii] = i;
	}
}
//---------------------------------------------------------------------------
void MultiDimensionalSelection::PenaltySorter(int nooffeatures, double* Qtable, unsigned int* Qsorted)
{
	int i, ii, iii;
    for(i = 0; i < nooffeatures; i++)
	{
		for(ii = 0; ii < i; ii++)
		{
            if(Qtable[Qsorted[ii]] < 0.0 && Qtable[i] >= 0) break;
            if(Qtable[Qsorted[ii]] > Qtable[i] && Qtable[i] >= 0) break;
		}
		for(iii = i; iii > ii; iii--)
		{
            Qsorted[iii] = Qsorted[iii-1];
		}
        Qsorted[ii] = i;
	}
}

//------------------------------------------------------------------------------
bool MultiDimensionalSelection::FullSearch(int nooffeatures, double *Qtable, unsigned int *Qsorted,
                                           int parameter_dimensions, int parameter_seconds, bool parameter_maximize)
{
    unsigned int* feature;
    unsigned int* sfeature;
    unsigned int* best;
    int dims, d;
	int maxdims;
	double Q;
	double Qextr;
	time_t absstarttime;
	time_t absendtime;
	time_t starttime;
	time_t endtime;
	int getret = -1;

    for(d = 0; d < nooffeatures; d++)
	{
		Qsorted[d] = d;
		Qtable[d] = -1.0;
	}

	absstarttime = time(NULL);
	absendtime = absstarttime + parameter_seconds;

    maxdims = nooffeatures < parameter_dimensions ? nooffeatures : parameter_dimensions;
	
    feature = (unsigned int*) malloc((maxdims+1)*sizeof(int));
	if(feature == NULL) return false;
    sfeature = (unsigned int*) malloc((maxdims+1)*sizeof(int));
	if(sfeature == NULL) return false;
    best = (unsigned int*)malloc(maxdims*sizeof(unsigned int));
    if(best == NULL) return false;

	for(dims = 1; dims <= maxdims; dims++)
	{
		starttime = time(NULL);
		endtime = starttime + (absendtime - starttime)/(maxdims-dims+1);
		if(endtime < starttime) endtime = starttime; 

        NotifyProgressStage(BEGINS, dims, 0.0, NULL);

		for(d = 0; d < dims; d++)
		{
			feature[d] = d;
            best[d] = Qsorted[d];
		}
        feature[d] = nooffeatures;
		Qextr = -1.0;
		do
		{
			for(d = 0; d < dims; d++)
			{
				sfeature[d] = Qsorted[feature[d]];
			}
			Q = GoalFunction(dims, sfeature);
			if(Q >= 0.0)
			{
				for(d = 0; d < dims; d++)
				{
					if(Qtable[sfeature[d]]<0.0 || 
						(Qtable[sfeature[d]]>Q && (!parameter_maximize)) || 
						(Qtable[sfeature[d]]<Q && parameter_maximize)) 
						Qtable[sfeature[d]] = Q;
				}
				if(Qextr < 0.0 || 
					(Qextr > Q && (!parameter_maximize)) || 
					(Qextr < Q && parameter_maximize)) 
				{
					Qextr = Q;
                    for(d = 0; d < dims; d++)
                    {
                        best[d] = sfeature[d];
                    }
				}
			}
            NotifyProgressStep();
            if((parameter_seconds > 0) && (endtime < time(NULL))) break;
            if(breakanalysis) {getret = -2; break;}

			getret = GenerateNextCombination(dims, feature);
        }
        while(getret == 0);

/*
        printf("\n======================= Dimensions %i\n", dims);
        for(d = 0; d < dims; d++) printf("(%i:%f) ", best[d], Qtable[best[d]]);
        printf("\n  ===================\n");
*/

        if(getret == -2)
        {
            NotifyProgressStage(CANCELED, dims, (double)feature[dims-1]/nooffeatures, NULL);
        }
        else if(getret == 0)
		{
            NotifyProgressStage(STOPPED, dims, (double)feature[dims-1]/nooffeatures, NULL);
        }
		else
		{
            NotifyProgressStage(COMPLETED, dims, 1.0, NULL);
        }
		if(Qextr >= 0)
		{
//            for(d = 0; d < dims; d++)
//            {
//                sfeature[d] = Qsorted[best[d]];
//            }
            if(parameter_maximize)
            {
                for(d = 0; d < dims; d++) Qtable[best[d]] *= 2.0;
                QualitySorter(nooffeatures, Qtable, Qsorted);
                for(d = 0; d < dims; d++) Qtable[best[d]] /= 2.0;
            }
            else
            {
                for(d = 0; d < dims; d++) Qtable[best[d]] /= 2.0;
                PenaltySorter(nooffeatures, Qtable, Qsorted);
                for(d = 0; d < dims; d++) Qtable[best[d]] *= 2.0;
            }
            NotifyProgressStage(SUCCESS, dims, Qextr, best);
            ClassifierTraining(dims, best);
		}
		else
		{
            NotifyProgressStage(FAILED, dims, 0.0, NULL);
		}

/*
        for(d = 0; d < 3*dims && d < nooffeatures; d++) printf("(%i:%f) ", Qsorted[d], Qtable[Qsorted[d]]);
        fflush(stdout);
*/
        if(breakanalysis) break;
	}
	free(sfeature);
	free(feature);
	return true;
}
