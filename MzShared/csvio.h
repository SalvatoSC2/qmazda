/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CSVIO_H
#define CSVIO_H

#include <iostream>
#include <string>
#include <vector>

class CsvIo
{
public:
    CsvIo()
    {
        category_index = -2;
        comment_index = -2;
    }
    static char* doubleToHex(const double value);
    static double hexToDouble(const char* text);
    bool loadCSVStreamHeader(std::istream* file, std::vector<std::string>* feature_names);
    bool loadCSVStreamDataLine(std::istream* file, double* feature_values, std::string* category, std::string* comment);
    void loadCSVStreamClose();
    void saveCSVStreamHeader(std::ostream* file, std::vector<std::string> *feature_names, bool savecategory, bool savecomment);
    void saveCSVStreamDataLine(std::ostream* file, double* feature_values, bool hexa, std::string* category, std::string* comment);
    void saveCSVStreamClose();

private:
    const char* separator = (char*)",;\t";
    const char* toremove = (char*)"\r\n\"";
    char *prevloc;
    int number_of_features;
    int category_index;
    int comment_index;
};

#endif // CSVIO_H
