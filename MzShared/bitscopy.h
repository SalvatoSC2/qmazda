/*
 * qMaZda - Image Analysis and Pattern Recognition
 *
 * Copyright 2013-2014 Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BITSCOPY_H
#define BITSCOPY_H


//template<typename BITSHIFTBLOCK>
///**
// * @brief BitsBufferToIntegerCounterBufferLittleEndian counts number of regions at pixels and saves values into counter buffer
// * @param src
// * @param srcs
// * @param count number of pixels in line
// * @param dst counter buffer pointer
// */
//void BitsBufferToIntegerCounterBufferLittleEndian(BITSHIFTBLOCK* src, unsigned int srcs, unsigned int* counter, unsigned int count)
//{
//    int inputblocks = (count + srcs + (sizeof(BITSHIFTBLOCK)<<3) - 1)/(sizeof(BITSHIFTBLOCK)<<3);
//    int tail =  (- count - srcs) % (sizeof(BITSHIFTBLOCK)<<3);
//    BITSHIFTBLOCK* srctemp = src;
//    BITSHIFTBLOCK firstlast;
//    unsigned int* dsttemp = counter;

//    if(inputblocks == 1)
//    {
//        firstlast = (*srctemp << tail);
//        firstlast = (firstlast >> tail);
//        firstlast = (firstlast >> srcs);
//        for(int i = 0; i < count; i++)
//        {
//            if(firstlast & 1) (*dsttemp)++;
//            dsttemp++;
//            firstlast >>= 1;
//        }
//    }
//    else
//    {
//        inputblocks--;
//        firstlast = (*srctemp >> srcs);
//        int imax = (sizeof(BITSHIFTBLOCK)<<3)-srcs;
//        for(int i = 0; i < imax; i++)
//        {
//            if(firstlast & 1) (*dsttemp)++;
//            dsttemp++;
//            firstlast >>= 1;
//        }
//        srctemp++;

//        for(int xblocki = 1; xblocki < inputblocks; xblocki++)
//        {
//            firstlast = *srctemp;
//            for(int i = 0; i < (sizeof(BITSHIFTBLOCK)<<3); i++)
//            {
//                if(firstlast & 1) (*dsttemp)++;
//                dsttemp++;
//                firstlast >>= 1;
//            }
//            srctemp++;
//        }

//        firstlast = (*srctemp << tail);
//        firstlast = (firstlast >> tail);
//        imax = (sizeof(BITSHIFTBLOCK)<<3)-tail;
//        for(int i = 0; i < imax; i++)
//        {
//            if(firstlast & 1) (*dsttemp)++;
//            dsttemp++;
//            firstlast >>= 1;
//        }
//    }
//}

//template<typename BITSHIFTBLOCK>
///**
// * @brief BitsBufferToRGBABufferLittleEndian creates color overlay line to display regions (32bits)
// * @param src
// * @param srcs
// * @param counter
// * @param rgbabuf
// * @param rgbacolor
// * @param count
// */
//void BitsBufferToRGBABufferLittleEndian(BITSHIFTBLOCK* src, unsigned int srcs, unsigned int* counter, unsigned char* rgbabuf, unsigned int rgbacolor, unsigned int count)
//{
//    int inputblocks = (count + srcs + (sizeof(BITSHIFTBLOCK)<<3) - 1)/(sizeof(BITSHIFTBLOCK)<<3);
//    int tail =  (- count - srcs) % (sizeof(BITSHIFTBLOCK)<<3);
//    BITSHIFTBLOCK* srctemp = src;
//    BITSHIFTBLOCK firstlast;
//    unsigned int* countertemp = counter;
//    unsigned char* rgbabuftemp = rgbabuf;

//    if(inputblocks == 1)
//    {
//        firstlast = (*srctemp << tail);
//        firstlast = (firstlast >> tail);
//        firstlast = (firstlast >> srcs);
//        for(int i = 0; i < count; i++)
//        {
//            if(firstlast & 1)
//            {
//                *rgbabuftemp += (rgbacolor>>16)&0xff/(*countertemp);
//                rgbabuftemp++;
//                *rgbabuftemp += (rgbacolor>>8)&0xff/(*countertemp);
//                rgbabuftemp++;
//                *rgbabuftemp += rgbacolor&0xff/(*countertemp);;
//                rgbabuftemp++;
//                rgbabuftemp++;
//            }
//            else
//            {
//                rgbabuftemp += 4;
//            }
//            countertemp++;
//            firstlast >>= 1;
//        }
//    }
//    else
//    {
//        inputblocks--;
//        firstlast = (*srctemp >> srcs);
//        int imax = (sizeof(BITSHIFTBLOCK)<<3)-srcs;
//        for(int i = 0; i < imax; i++)
//        {
//            if(firstlast & 1)
//            {
//                *rgbabuftemp += (rgbacolor>>16)&0xff/(*countertemp);
//                rgbabuftemp++;
//                *rgbabuftemp += (rgbacolor>>8)&0xff/(*countertemp);
//                rgbabuftemp++;
//                *rgbabuftemp += rgbacolor&0xff/(*countertemp);;
//                rgbabuftemp++;
//                rgbabuftemp++;
//            }
//            else
//            {
//                rgbabuftemp += 4;
//            }
//            countertemp++;
//            firstlast >>= 1;
//        }
//        srctemp++;

//        for(int xblocki = 1; xblocki < inputblocks; xblocki++)
//        {
//            firstlast = *srctemp;
//            for(int i = 0; i < (sizeof(BITSHIFTBLOCK)<<3); i++)
//            {
//                if(firstlast & 1)
//                {
//                    *rgbabuftemp += (rgbacolor>>16)&0xff/(*countertemp);
//                    rgbabuftemp++;
//                    *rgbabuftemp += (rgbacolor>>8)&0xff/(*countertemp);
//                    rgbabuftemp++;
//                    *rgbabuftemp += rgbacolor&0xff/(*countertemp);;
//                    rgbabuftemp++;
//                    rgbabuftemp++;
//                }
//                else
//                {
//                    rgbabuftemp += 4;
//                }
//                countertemp++;
//                firstlast >>= 1;
//            }
//            srctemp++;
//        }

//        firstlast = (*srctemp << tail);
//        firstlast = (firstlast >> tail);
//        imax = (sizeof(BITSHIFTBLOCK)<<3)-tail;
//        for(int i = 0; i < imax; i++)
//        {
//            if(firstlast & 1)
//            {
//                *rgbabuftemp += (rgbacolor>>16)&0xff/(*countertemp);
//                rgbabuftemp++;
//                *rgbabuftemp += (rgbacolor>>8)&0xff/(*countertemp);
//                rgbabuftemp++;
//                *rgbabuftemp += rgbacolor&0xff/(*countertemp);;
//                rgbabuftemp++;
//                rgbabuftemp++;
//            }
//            else
//            {
//                rgbabuftemp += 4;
//            }
//            countertemp++;
//            firstlast >>= 1;
//        }
//    }
//}

//template<typename BITSHIFTBLOCK>
///**
// * @brief BitsBufferToRGBBufferLittleEndian creates color overlay line to display regions (24bits)
// * @param src
// * @param srcs
// * @param counter
// * @param rgbabuf
// * @param rgbacolor
// * @param count
// */
//void BitsBufferToRGBBufferLittleEndian(BITSHIFTBLOCK* src, unsigned int srcs, unsigned int* counter, unsigned char* rgbbuf, unsigned int rgbacolor, unsigned int count)
//{
//    int inputblocks = (count + srcs + (sizeof(BITSHIFTBLOCK)<<3) - 1)/(sizeof(BITSHIFTBLOCK)<<3);
//    int tail =  (- count - srcs) % (sizeof(BITSHIFTBLOCK)<<3);
//    BITSHIFTBLOCK* srctemp = src;
//    BITSHIFTBLOCK firstlast;
//    unsigned int* countertemp = counter;
//    unsigned char* rgbbuftemp = rgbbuf;

//    if(inputblocks == 1)
//    {
//        firstlast = (*srctemp << tail);
//        firstlast = (firstlast >> tail);
//        firstlast = (firstlast >> srcs);
//        for(int i = 0; i < count; i++)
//        {
//            if(*countertemp && (firstlast & 1))
//            {
//                *rgbbuftemp += (rgbacolor>>16)&0xff/(*countertemp);
//                rgbbuftemp++;
//                *rgbbuftemp += (rgbacolor>>8)&0xff/(*countertemp);
//                rgbbuftemp++;
//                *rgbbuftemp += rgbacolor&0xff/(*countertemp);
//                rgbbuftemp++;
//            }
//            else
//            {
//                rgbbuftemp += 3;
//            }
//            countertemp++;
//            firstlast >>= 1;
//        }
//    }
//    else
//    {
//        inputblocks--;
//        firstlast = (*srctemp >> srcs);
//        int imax = (sizeof(BITSHIFTBLOCK)<<3)-srcs;
//        for(int i = 0; i < imax; i++)
//        {
//            if(*countertemp && (firstlast & 1))
//            {
//                *rgbbuftemp += (rgbacolor>>16)&0xff/(*countertemp);
//                rgbbuftemp++;
//                *rgbbuftemp += (rgbacolor>>8)&0xff/(*countertemp);
//                rgbbuftemp++;
//                *rgbbuftemp += rgbacolor&0xff/(*countertemp);
//                rgbbuftemp++;

//            }
//            else
//            {
//                rgbbuftemp += 3;
//            }
//            countertemp++;
//            firstlast >>= 1;
//        }
//        srctemp++;

//        for(int xblocki = 1; xblocki < inputblocks; xblocki++)
//        {
//            firstlast = *srctemp;
//            for(int i = 0; i < (sizeof(BITSHIFTBLOCK)<<3); i++)
//            {
//                if(*countertemp && (firstlast & 1))
//                {
//                    *rgbbuftemp += (rgbacolor>>16)&0xff/(*countertemp);
//                    rgbbuftemp++;
//                    *rgbbuftemp += (rgbacolor>>8)&0xff/(*countertemp);
//                    rgbbuftemp++;
//                    *rgbbuftemp += rgbacolor&0xff/(*countertemp);
//                    rgbbuftemp++;
//                }
//                else
//                {
//                    rgbbuftemp += 3;
//                }
//                countertemp++;
//                firstlast >>= 1;
//            }
//            srctemp++;
//        }

//        firstlast = (*srctemp << tail);
//        firstlast = (firstlast >> tail);
//        imax = (sizeof(BITSHIFTBLOCK)<<3)-tail;
//        for(int i = 0; i < imax; i++)
//        {
//            if(*countertemp && (firstlast & 1))
//            {
//                *rgbbuftemp += (rgbacolor>>16)&0xff/(*countertemp);
//                rgbbuftemp++;
//                *rgbbuftemp += (rgbacolor>>8)&0xff/(*countertemp);
//                rgbbuftemp++;
//                *rgbbuftemp += rgbacolor&0xff/(*countertemp);
//                rgbbuftemp++;
//            }
//            else
//            {
//                rgbbuftemp += 3;
//            }
//            countertemp++;
//            firstlast >>= 1;
//        }
//    }
//}

//template<typename BITSHIFTBLOCK>
///**
// * @brief FindFirsAndLastBitInBlockLittleEndian
// * @param src pointer of first BITSHIFTBLOCK variable of source bits
// * @param srcs number of first bits not to search
// * @param count number of bits to search through
// * @param first first occurence of bit = 1
// * @param last last occurence of bit = 1
// * @return true if found or false otherwise
// */
//bool FindFirsAndLastBitInBlockLittleEndian(BITSHIFTBLOCK* src, unsigned int srcs, unsigned int count, unsigned int* first, unsigned int* last)
//{
//    BITSHIFTBLOCK pack = 0;
//    BITSHIFTBLOCK mask = 0;
//    BITSHIFTBLOCK* srctemp = src;
//    int inputblocks = (count + srcs + (sizeof(BITSHIFTBLOCK)<<3) - 1)/(sizeof(BITSHIFTBLOCK)<<3);
//    int tail =  (- count - srcs) % (sizeof(BITSHIFTBLOCK)<<3);
//    int i;
//    bool firstnotfound = true;

//    for(i = 0; i < (sizeof(BITSHIFTBLOCK)<<3)*inputblocks; i++)
//    {
//        if(i % (sizeof(BITSHIFTBLOCK)<<3) == 0)
//        {
//            mask = 1;
//            pack = (*srctemp);
//            srctemp++;
//            if(i == 0) pack = ((pack >> srcs) << srcs);
//            if(i/(sizeof(BITSHIFTBLOCK)<<3) == inputblocks-1)
//            {
//                pack = (pack << tail);
//                pack = (pack >> tail);
//            }
//        }
//        if(pack & mask)
//        {
//            if(firstnotfound)
//            {
//                firstnotfound = false;
//                *first = i;
//            }
//            *last = i;
//        }
//        mask <<= 1;
//    }
//    return (!firstnotfound);
//}

template<typename BITSHIFTBLOCK>
/**
 * @brief CopyShiftOrBitsBlockLittleEndian copies block of bits. Little-endian conversion: lower bits and lower-address bytes go first.
 * @param src pointer of first BITSHIFTBLOCK variable of source bits
 * @param srcs number of first bits not to copy from
 * @param dst pointer of first BITSHIFTBLOCK variable of destination bits
 * @param dsts number of first bits not to copy to
 * @param count number of bits to copy
 */
void CopyShiftOrBitsBlockLittleEndian(BITSHIFTBLOCK* src, unsigned int srcs, BITSHIFTBLOCK* dst, unsigned int dsts, unsigned int count)
{
    int inputblocks = (count + srcs + (sizeof(BITSHIFTBLOCK)<<3) - 1)/(sizeof(BITSHIFTBLOCK)<<3);
    int shiftleft = (int)srcs - (int)dsts;
    int tail =  (- count - srcs) % (sizeof(BITSHIFTBLOCK)<<3);
    int shiftright;
    BITSHIFTBLOCK* srctemp = src;
    BITSHIFTBLOCK* dsttemp = dst;
    BITSHIFTBLOCK firstlast;

    if(inputblocks == 1)
    {
        firstlast = (*srctemp << tail);
        firstlast = (firstlast >> tail);
        firstlast = ((firstlast >> srcs) << srcs);

        if(shiftleft > 0)
        {
            if(firstlast) *dsttemp |= (firstlast >> shiftleft);
        }
        else if(shiftleft < 0)
        {
            shiftright = -shiftleft;
            shiftleft = sizeof(BITSHIFTBLOCK)*8 - shiftright;
            *dsttemp |= (firstlast << shiftright);
            dsttemp++;
            firstlast >>= shiftleft;
            if(firstlast) *dsttemp |= firstlast;
        }
        else
        {
            if(firstlast) *dsttemp |= firstlast;
        }
    }
    else
    {
        if(shiftleft > 0)
        {
            shiftright = (sizeof(BITSHIFTBLOCK)<<3) - shiftleft;
            inputblocks--;
            *dsttemp |= (((*srctemp >> srcs) << srcs) >> shiftleft);
            srctemp++;
            for(int xblocki = 1; xblocki < inputblocks; xblocki++)
            {
                *dsttemp |= (*srctemp << shiftright);
                dsttemp++;
                *dsttemp |= (*srctemp >> shiftleft);
                srctemp++;
            }
            firstlast = (*srctemp << tail);
            firstlast = (firstlast >> tail);
            *dsttemp |= (firstlast << shiftright);
            dsttemp++;
            firstlast = (firstlast >> shiftleft);
            if(firstlast) *dsttemp |= firstlast;
        }
        else if(shiftleft < 0)
        {
            shiftright = -shiftleft;
            shiftleft = sizeof(BITSHIFTBLOCK)*8 - shiftright;
            inputblocks--;
            firstlast = ((*srctemp >> srcs) << srcs);
            *dsttemp |= (firstlast << shiftright);
            dsttemp++;
            *dsttemp |= (firstlast >> shiftleft);
            srctemp++;
            for(int xblocki = 1; xblocki < inputblocks; xblocki++)
            {
                *dsttemp |= (*srctemp << shiftright);
                dsttemp++;
                *dsttemp |= (*srctemp >> shiftleft);
                srctemp++;
            }
            firstlast = (*srctemp << tail);
            firstlast = (firstlast >> tail);
            *dsttemp |= (firstlast << shiftright);
            dsttemp++;
            firstlast >>= shiftleft;
            if(firstlast) *dsttemp |= firstlast;
        }
        else
        {
            inputblocks--;
            *dsttemp |= ((*srctemp >> srcs) << srcs);
            srctemp++;
            for(int xblocki = 1; xblocki < inputblocks; xblocki++)
            {
                dsttemp++;
                *dsttemp |= (*srctemp);
                srctemp++;
            }
            dsttemp++;
            firstlast = (*srctemp << tail);
            firstlast = (firstlast >> tail);
            if(firstlast) *dsttemp |= firstlast;
        }
    }
}
#endif // BITSCOPY_H
