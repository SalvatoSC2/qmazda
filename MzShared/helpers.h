/*
 * qMaZda - Image Analysis and Pattern Recognition
 * 
 * Copyright 2013  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HELPERS_H
#define HELPERS_H

#include <stdint.h>
#include <QImage>
#include "opencv2/imgproc/imgproc_c.h"

#ifdef _USE_DCMTK
#define HAVE_CONFIG_H
#include "dcmtk/config/osconfig.h"
//#include <dcmtk/config/cfunix.h>
#include <dcmtk/dcmdata/dctk.h>

void GenerateDicomInfo(QString *info, DcmFileFormat fileformat);
#endif

IplImage* QImage2IplImage(const QImage& qImage);
QImage IplImage2QImage(const IplImage *iplImage);
int mzImageConvert(char colortransform, IplImage* in, IplImage* out);

//char* doubleToHex(const double value);
//double hexToDouble(const char *text);


#endif // HELPERS_H
