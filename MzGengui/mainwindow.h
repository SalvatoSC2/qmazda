#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QObject>
#include <QString>
#include <QDir>
#include <iostream>
#include "../MzGenerator/genentry.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void notify();
    void notifyText(QString message);
    void finish();

private slots:
    void on_cancelButton_clicked();

private:
    Ui::MainWindow *ui;
    QThread* thread;

    QString getExecutableWithPath(const char* name, bool use_quotation);
};


class MazdaGenWorker : public QObject
{
    Q_OBJECT
public:
    explicit MazdaGenWorker(QObject *parent = 0);
    ~MazdaGenWorker();

    void emitnotify();
    void emitnotifyText(const char *message);

public slots:
    void start();

signals:
    void finished();
    void notify();
    void notifyText(QString message);

};


#endif // MAINWINDOW_H
