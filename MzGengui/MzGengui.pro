QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DESTDIR         = ../../Executables
TARGET = MzGengui
TEMPLATE = app

# DEFINES += MAZDA_IMAGE_8BIT_PIXEL
DEFINES += MAZDA_IN_MACOS_BUNDLE

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    ../MzShared/parametertreetemplate.cpp \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp \
    ../MzShared/filenameremap.cpp \
    ../MzGenerator/stubitem.cpp \
    ../MzGenerator/genentry.cpp \
    ../MzGenerator/templates.cpp \
    ../MzGenerator/stubitemnormalization.cpp \
    ../MzGenerator/stubitemcolortransform.cpp \
    ../MzGenerator/stubitemhistogram.cpp \
    ../MzGenerator/stubitemgradient.cpp \
    ../MzGenerator/stubitemarm.cpp \
    ../MzGenerator/stubitemlbp.cpp \
    ../MzGenerator/stubitemhog.cpp \
    ../MzGenerator/stubitemgrlm.cpp \
    ../MzGenerator/stubitemgabor.cpp \
    ../MzGenerator/stubitemglcm.cpp \
    ../MzGenerator/stubitemshape.cpp \
    ../MzGenerator/stubitemwindow.cpp \
    ../MzGenerator/featureio.cpp \
    ../MzGenerator/stubitemshapemz.cpp \
    ../MzGenerator/stubitemshapecv.cpp \
    ../MzGenerator/stubitemhaar.cpp

HEADERS += \
    ../MzShared/getoption.h \
    ../MzGenerator/stubitem.h \
    ../MzShared/parametertreetemplate.h \
    ../MzGenerator/genentry.h \
    ../SharedImage/mazdaimage.h \
    ../SharedImage/mazdaroi.h \
    ../MzGenerator/templates.h \
    ../MzGenerator/stubitemnormalization.h \
    ../MzGenerator/stubitemcolortransform.h \
    ../MzGenerator/stubitemhistogram.h \
    ../MzGenerator/stubitemgradient.h \
    ../MzGenerator/stubitemarm.h \
    ../MzGenerator/stubitemlbp.h \
    ../MzGenerator/stubitemhog.h \
    ../MzGenerator/stubitemgrlm.h \
    ../MzGenerator/stubitemgabor.h \
    ../MzGenerator/stubitemglcm.h \
    ../MzGenerator/stubitemshape.h \
    ../MzGenerator/stubitemwindow.h \
    mainwindow.h \
    ../MzGenerator/featureio.h \
    ../MzGenerator/stubitemshapemz.h \
    ../MzGenerator/stubitemshapecv.h \
    ../MzGenerator/stubitemhaar.h

FORMS += \
    mainwindow.ui  

DEFINES += USE_DEBUG_LOG

RESOURCES += \
    mzgengui.qrc

macx{
    ICON = mzgenerator.icns
}
else:unix{

}
else:win32{
    RC_FILE = MZGengui.rc
}

include(../Pri/config.pri)
include(../Pri/itk.pri)
include(../Pri/opencv.pri)
include(../Pri/tiff.pri)

