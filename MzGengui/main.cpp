#include "mainwindow.h"
//#include "parameters.h"
#include <QApplication>
#include <QMessageBox>
#include "../MzGenerator/genentry.h"
#include "../MzShared/getoption.h"


int main(int argc, char *argv[])
{
    gentryScan(argc, argv);
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    a.exec();
    return 0;
}


