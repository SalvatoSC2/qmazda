#include "../MzShared/mzdefines.h"
#include "../MzShared/filenameremap.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../MzGenerator/genentry.h"
#include <QProcess>

MazdaGenWorker* worker = NULL;
//extern char* execute_command;// = NULL;

void notify(void)
{
    if(worker != NULL)
        worker->emitnotify();
}

void notifyText(const char* message)
{
    if(worker != NULL)
        worker->emitnotifyText(message);
}

void MazdaGenWorker::emitnotify(void)
{
    emit notify();
}
void MazdaGenWorker::emitnotifyText(const char* message)
{
    QString text(message);
    emit notifyText(text);
}

MazdaGenWorker::MazdaGenWorker(QObject *parent) : QObject(parent)
{
}
MazdaGenWorker::~MazdaGenWorker()
{
}

void MazdaGenWorker::start()
{
    gentryRun();
    emit finished();
}

void MainWindow::notify()
{
    static unsigned int perclast = (unsigned int)-1;
    notifications++;
    unsigned int perc = 100*notifications/maxnotifications;
    if(perc != perclast)
    {
        perclast = perc;
        ui->progressBar->setValue(perc);
    }
}

void MainWindow::notifyText(QString text)
{
    text.replace("\r", "");
    text.replace("\n", "");
    ui->plainTextEdit->appendPlainText(text);
}

QString MainWindow::getExecutableWithPath(const char* name, bool use_quotation)
{
    QDir execDir(qApp->applicationDirPath());
#if defined(__APPLE__) && defined(MAZDA_IN_MACOS_BUNDLE)
    execDir.cdUp();
    execDir.cdUp();
    execDir.cdUp();
#endif
    QString pathq = execDir.absolutePath();

    if((pathq.contains(' ') || pathq.contains('\t')) && use_quotation)
        return QString("\"") + QString::fromStdString(fileNameRemapRead(& pathq)) + QString(name) + QString("\"");
    else
        return QString::fromStdString(fileNameRemapRead(& pathq)) + QString(name);
}

void MainWindow::finish()
{
    if(execute_command != NULL)
    {
        if(strcmp(execute_command, "*") == 0 && modestring != NULL)
        {
            if(strcmp(modestring, "local") == 0 && input_regions == NULL) // Map computation
            {
                QString command = getExecutableWithPath(map_viewer_name, true);
                command += " ";
                command += output_data;
                QProcess process(this);
                process.startDetached(command);
            }
            else
            {
                QString command = getExecutableWithPath(report_viewer_name, true);
                command += " ";
                command += output_data;
                QProcess process(this);
                process.startDetached(command);
            }
        }
        else
        {
            QProcess process(this);
            process.startDetached(execute_command);
        }
    }
    close();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    thread = new QThread;
    worker = new MazdaGenWorker();
    worker->moveToThread(thread);

    connect(thread, SIGNAL(started()), worker, SLOT(start()));
    connect(worker, SIGNAL(notify()), this, SLOT(notify()));
    connect(worker, SIGNAL(notifyText(QString)), this, SLOT(notifyText(QString)));
    connect(worker, SIGNAL(finished()), this, SLOT(finish()));
    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    //thread->setTerminationEnabled(true);
    thread->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_cancelButton_clicked()
{
    thread->quit();
    close();
    exit(-99);
}
