#-------------------------------------------------
#
# Project created by QtCreator 2014-02-28T13:16:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MzMaps
DESTDIR         = ../../Executables
TEMPLATE = app

DEFINES += MAZDA_IN_MACOS_BUNDLE
#DEFINES += MAZDA_HIDE_ON_PROGRESS

SOURCES += main.cpp\
        mainwindow.cpp \
    fpirenderer.cpp \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp \
    ../MzShared/mzmapio.cpp \
    ../MzShared/filenameremap.cpp \
    ../MzReport/selectclassifiers.cpp \
    ../MzShared/progress.cpp

HEADERS  += mainwindow.h \
    fpirenderer.h \
    ../MzShared/mzselectioninterface.h \
    ../MzShared/dataforconsole.h \
    ../MzShared/mzmapio.h \
    ../MzShared/mapstruct.h \
    ../MzShared/filenameremap.h \
    ../MzReport/selectclassifiers.h \
    ../MzShared/progress.h \
    ../SharedImage/mazdaimageio.h

FORMS    += mainwindow.ui \
    ../MzReport/selectclassifiers.ui \
    ../MzShared/progress.ui

RESOURCES += \
    mazdares.qrc

macx{
    ICON = mzmaps.icns
}
else:unix{

}
else:win32{
    RC_FILE = MZMaps.rc
}

include(../Pri/config.pri)
include(../Pri/itk.pri)
include(../Pri/tiff.pri)
