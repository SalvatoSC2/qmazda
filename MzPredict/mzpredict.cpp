/*
 * main.cpp - program do testowania modułu analizy ziaren jęczmienia
 *
 * Autor:  (2016) Piotr M. Szczypiński, Politechnika Łódzka
 *
 * Opracowano w ramach grantu: NCBR PBS3/A8/38/2015
 * Opracowanie przemysłowej metody automatycznej oceny parametrów
 * technologicznych i klasyfikacji ziarna z zastosowaniem analizy
 * obrazów.
 */

#include "../MzShared/mzdefines.h"

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>

#include "../MzShared/getoption.h"
#include "../MzShared/csvio.h"
#include "../MzGenerator/featureio.h"

//#include "../MzShared/filenameremap.h"
#include "../SharedImage/mazdaimageio.h"

#include <time.h>
//#include "dectree.h"
#include "../LdaPlugin/ldaselection.h"
#include "../VschPlugin/vschselection.h"
#include "../SvmPlugin/svmselection.h"
#include "../DecisionTreePlugin/decisiontree.h"

char* config = NULL;
char* filename_in = NULL;
char* filename_out = NULL;
bool isprinthelp = false;
//bool echo = false;
bool verbose = false;

//-----------------------------------------------------------------------------
int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-c", "--classifier", config)
        else GET_STRING_OPTION("-i", "--input-file", filename_in)
        else GET_STRING_OPTION("-o", "--output-file", filename_out)
//        else GET_NOARG_OPTION("-e", "--echo", echo, true)
        else GET_NOARG_OPTION("-v", "--verbose", verbose, true)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

//-----------------------------------------------------------------------------
void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Predicts classes of vectors from CSV\n");
    printf("%s\n", QMAZDA_VERSION);
    printf("%s\n", QMAZDA_COPYRIGHT);
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -c, --classifier <file>   Classification rules file\n");
    printf("  -i, --input-file <file>   Input file (csv or maps)\n");
    printf("  -o, --output-file <file>  Output file (text or tif)\n");
    printf("  -v, --verbose             Verbose mode\n");
    printf("  /?, --help                Display this help and exit\n\n");
    printf("Examples:\n");
    printf("  %s -c classifier.txt < input.csv > predictions.txt\n", name);
    printf("  %s -c classifier.txt -i input.csv -o predictions.txt -v\n", name);
    printf("  %s -c classifier.txt -i input.map -o segmented.tiff\n\n", name);
}

//-----------------------------------------------------------------------------
ClassifierAccessInterface* loadSingleClassifier(ClassifierAccessInterface* object, const char* fileName)
{
    if(object->loadClassifierFromFile(fileName))
    {
        return object;
    }
    delete object;
    return NULL;
}




//-----------------------------------------------------------------------------

int csvClassification(std::vector<std::string>* feature_names,
                      std::vector<std::string>* class_names,
                      ClassifierAccessInterface* classifier)
{
    std::ifstream file_in;
    std::istream* stream_in = &std::cin;
    if(filename_in != NULL)
    {
        file_in.open(filename_in);
        if (!file_in.is_open())
        {
            //if(verbose)
                std::cerr << "Bad input: " << filename_in << std::endl;
            return 4;
        }
        if (!file_in.good())
        {
            //if(verbose)
                std::cerr << "Bad input: " << filename_in << std::endl;
            return 5;
        }
        stream_in = & file_in;
    }

    std::ofstream file_out;
    std::ostream* stream_out = &std::cout;
    if(filename_out != NULL)
    {
        file_out.open(filename_out);
        if (!file_out.is_open())
        {
            //if(verbose)
                std::cerr << "Bad output: " << filename_in << std::endl;
            return 6;
        }
        if (!file_out.good())
        {
            //if(verbose)
                std::cerr << "Bad output: " << filename_in << std::endl;
            return 7;
        }
        stream_out = & file_out;
    }
    fflush(stdout);

    CsvIo csvio;
    if(! csvio.loadCSVStreamHeader(stream_in, feature_names))
    {
        std::cerr << "Failed to load csv header" << std::endl;
        csvio.loadCSVStreamClose();
        return 8;
    }



//    if(verbose)
//    {
//        unsigned int i;
//        if(classifier != NULL)
//        {
//            std::cout << "Classifier: " << classifier->getClassifierMagic() << std::endl;
//        }
//        std::cout << "Classes:" << std::endl;
//        for(i = 0; i < class_names->size(); i++)
//            std::cout << "  " << i << " " << (*class_names)[i]  << std::endl;

//        std::vector<std::string> feature_names_required;
//        if(classifier != NULL)
//            feature_names_required = classifier->getFeatureNames();
////        else
////            decisiontree->getFeatureNames(&feature_names_required);

//        std::cout << "Features required:" << std::endl;
//        for(i = 0; i < feature_names_required.size(); i++)
//            std::cout << "  " << feature_names_required[i]  << std::endl;
//        std::cout << "Features in csv:" << std::endl;
//        for(i = 0; i < feature_names->size(); i++)
//            std::cout << "  " << (*feature_names)[i]  << std::endl;
//    }

    if(classifier != NULL)
    {
        if(! classifier->configureForClassification(feature_names))
        {
            std::cerr << "Failed to configure for classification" << std::endl;
            csvio.loadCSVStreamClose();
            return 10;
        }
    }
    else
    {
//        if(! decisiontree->renumberFeatures(feature_names))
//        {
//            std::cerr << "Failed reindexing features" << std::endl;
//            csvio.loadCSVStreamClose();
            return 11;
//        }
    }

    *class_names = classifier->getClassNames();

    if(verbose)
    {
        unsigned int i;
        if(classifier != NULL)
        {
            std::cout << "Classifier: " << classifier->getClassifierMagic() << std::endl;
        }
        std::cout << "Classes:" << std::endl;
        for(i = 0; i < class_names->size(); i++)
            std::cout << "  " << i << " " << (*class_names)[i]  << std::endl;
        DecisionTreeClassifier* cc = dynamic_cast<DecisionTreeClassifier*>(classifier);
        if(cc != NULL)
        {
            std::cout << "Decision tree structure:" << std::endl;
            std::cout << cc->Serialize(' ') << std::endl;
        }
        std::vector<std::string> feature_names_required;
        if(classifier != NULL)
            feature_names_required = classifier->getFeatureNames();
        std::cout << "Features required:" << std::endl;
        for(i = 0; i < feature_names_required.size(); i++)
            std::cout << "  " << feature_names_required[i]  << std::endl;
//        std::cout << "Features available:" << std::endl;
//        for(i = 0; i < feature_names->size(); i++)
//            std::cout << "  " << (*feature_names)[i]  << std::endl;
    }

    std::string category;
    std::string comment;
    unsigned int feature_names_size = feature_names->size();
    double* feature_values = new double[feature_names_size];
    for(unsigned int r = 0; r < feature_names_size; r++)
        feature_values[r] = std::numeric_limits<double>::quiet_NaN();

    stream_out->flush();
    while(csvio.loadCSVStreamDataLine(stream_in, feature_values, &category, &comment))
    {
        int c;
        if(classifier != NULL)
        {
            //std::cout << std::endl << category << ": " ;

            c = classifier->classifyFeatureVector(feature_values);
        }
//        else
//            c = decisiontree->decisionTreeClassification(feature_values);
        *stream_out << c;
        if((unsigned int)c < class_names->size())
        {
            *stream_out << " " << (*class_names)[c];
        }
        else
        {
            *stream_out << " %Error%";
        }
        if(verbose)
            *stream_out << " : " << category << " " << comment;
        *stream_out << std::endl;

        stream_out->flush();
        for(unsigned int r = 0; r < feature_names_size; r++)
            feature_values[r] = std::numeric_limits<double>::quiet_NaN();
    }
    delete[] feature_values;

    if(classifier != NULL)
        delete classifier;
//    else
//        delete decisiontree;

    csvio.loadCSVStreamClose();

    return 0;
}






//-----------------------------------------------------------------------------

int mapSegmentation(std::vector<std::string>* feature_names,
                    ClassifierAccessInterface* classifier)
{
    typedef MazdaImage<MazdaMapPixelType, 3> MzMap;
    typedef MazdaImage<unsigned char, 2> MzMap8b2d;
    typedef MazdaImage<unsigned char, 3> MzMap8b3d;

    std::vector<MzMap*> maps = MazdaMapIO <MzMap>::Read(filename_in);
    int fmax = maps.size();
    if(filename_out == NULL)
    {
            std::cerr << "No output file name" << std::endl;
        return 6;
    }
    if(fmax <= 0)
    {
            std::cerr << "Cannot load " << filename_in << std::endl;
        return 4;
    }
    unsigned int map_size = (unsigned int)-1;
    for (MzMap* m : maps)
    {
        feature_names->push_back(m->GetName());
        size_t count = 1;

        unsigned int size[MzMap::Dimensions];
        m->GetSize(size);

        for(unsigned int d = 0; d < MzMap::Dimensions; d++)
            count *= size[d];
        if(map_size == (unsigned int)-1) map_size = count;
        if(map_size != count)
        {
            std::cerr << "Map sizes differ" << std::endl;
            return 5;
        }
    }
    if(! classifier->configureForClassification(feature_names))
    {
        std::cerr << "Failed to configure for classification" << std::endl;
        return 10;
    }

    std::vector<std::string> cnames = classifier->getClassNames();

    if(verbose)
    {
        unsigned int i;
        if(classifier != NULL)
        {
            std::cout << "Classifier: " << classifier->getClassifierMagic() << std::endl;
        }
        std::cout << "Classes:" << std::endl;
        for(i = 0; i < cnames.size(); i++)
            std::cout << "  " << i << " " << cnames[i]  << std::endl;
        DecisionTreeClassifier* cc = dynamic_cast<DecisionTreeClassifier*>(classifier);
        if(cc != NULL)
        {
            std::cout << "Decision tree structure:" << std::endl;
            std::cout << cc->Serialize(' ') << std::endl;
        }
        std::vector<std::string> feature_names_required;
        if(classifier != NULL)
            feature_names_required = classifier->getFeatureNames();
        std::cout << "Features required:" << std::endl;
        for(i = 0; i < feature_names_required.size(); i++)
            std::cout << "  " << feature_names_required[i]  << std::endl;
//        std::cout << "Maps available:" << std::endl;
//        for(i = 0; i < feature_names->size(); i++)
//            std::cout << "  " << (*feature_names)[i]  << std::endl;
    }

    MazdaMapPixelType* rmap = new MazdaMapPixelType[map_size];
    MazdaMapPixelType** pmaps = new MazdaMapPixelType*[fmax];
    MazdaMapPixelType** pmap = pmaps;
    for (MzMap* m : maps)
    {
        *pmap = m->GetDataPointer();
        pmap++;
    }
    classifier->segmentImage(map_size, pmaps, rmap);

    double spacing[MzMap::Dimensions];
    unsigned int size[MzMap::Dimensions];
    maps[0]->GetSize(size);
    maps[0]->GetSpacing(spacing);

    unsigned char* rmap8b = new unsigned char[map_size];
    for(unsigned int i = 0; i < map_size; i++)
        rmap8b[i] = rmap[i];


    if(cnames.size() < 255)
    {
        if(size[2] <= 1)
        {
            MzMap8b2d* newfmap = new MzMap8b2d(size, spacing, rmap8b);
            newfmap->SetName(classifier->getClassifierMagic());
            MazdaMapIO <MzMap8b2d>::Export(filename_out, newfmap);
            delete newfmap;
        }
        else
        {
            MzMap8b3d* newfmap = new MzMap8b3d(size, spacing, rmap8b);
            newfmap->SetName(classifier->getClassifierMagic());
            MazdaMapIO <MzMap8b3d>::Export(filename_out, newfmap);
            delete newfmap;
        }
        delete[] rmap;
    }
    else
    {
        MzMap* newfmap = new MzMap(size, spacing, rmap);
        newfmap->SetName(classifier->getClassifierMagic());
        std::vector<MzMap*> rmaps;
        rmaps.push_back(newfmap);
        MazdaMapIO <MzMap>::Write(filename_out, &rmaps);
        delete newfmap;
    }
    delete[] pmaps;
    return 0;
}




//-----------------------------------------------------------------------------

ClassifierAccessInterface* loadClassifier(const char* filename)
{
    ClassifierAccessInterface* classifier = NULL;
    if(classifier == NULL)
        classifier = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new LdaSelectionReduction()), filename);
    if(classifier == NULL)
        classifier = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new VschSelectionReduction()), filename);
    if(classifier == NULL)
        classifier = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new SvmSelectionReduction()), filename);
    if(classifier == NULL)
        classifier = loadSingleClassifier(dynamic_cast<ClassifierAccessInterface*>(new DecisionTreeClassifier()), filename);
    return classifier;
}


//-----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return 2;
    }

    std::vector<std::string> feature_names;
    std::vector<std::string> class_names;
    //DecisionTree* decisiontree = NULL;
    ClassifierAccessInterface* classifier = loadClassifier(config);

    if(classifier == NULL)
    {
        std::cerr << "Cannot load classifier from: " << config << std::endl;
        return 3;
    }


    //fileNameRemapRead(fileName).c_str();;;;;;;;;;

    std::ifstream file_test;
    if(filename_in != NULL)
    {
        file_test.open(filename_in);
        if (!file_test.is_open())
        {
            //if(verbose)
                std::cerr << "Bad input: " << filename_in << std::endl;
            return 4;
        }
        if (!file_test.good())
        {
            //if(verbose)
                std::cerr << "Bad input: " << filename_in << std::endl;
            return 5;
        }

        char * buffer = new char [4];
        file_test.read (buffer, 2);
        file_test.close();
        buffer[2] = 0;
        if(strncmp(buffer, "II", 2) == 0 || strncmp(buffer, "MM", 2) == 0)
        {
            return mapSegmentation(&feature_names, classifier);
        }
    }
    return csvClassification(&feature_names, &class_names, classifier);

}

