TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR         = ../../Executables
TARGET = MzPredict

HEADERS += \
    mzpredict.h \
    dectree.h \
    ../MzShared/csvio.h

SOURCES +=  \
    mzpredict.cpp \
    ../../qmazda/LdaPlugin/ldaselection.cpp \
    ../../qmazda/VschPlugin/vschselection.cpp \
    ../../qmazda/MzShared/classifierio.cpp \
    ../../qmazda/MzShared/parametertreetemplate.cpp \
    ../../qmazda/SvmPlugin/svmselection.cpp \
    ../../qmazda/MzShared/multidimselection.cpp \
    ../../qmazda/SvmPlugin/libsvm/svm.cpp \
    ../../qmazda/DecisionTreePlugin/dectree.cpp \
    ../../qmazda/DecisionTreePlugin/decisiontree.cpp \
    ../MzShared/dataforconsole.cpp \
    ../MzShared/csvio.cpp

#dectree.cpp \
#../../qmazda/MzGenerator/featureio.cpp \

include(../Pri/config.pri)
include(../Pri/itk.pri)
include(../Pri/alglib.pri)
include(../Pri/qhull.pri)
include(../Pri/tiff.pri)
