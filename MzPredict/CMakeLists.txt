cmake_minimum_required(VERSION 3.1)

project(MzPredict LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

add_executable(MzPredict 
    mzpredict.cpp
    ../LdaPlugin/ldaselection.cpp
    ../VschPlugin/vschselection.cpp
    ../MzShared/classifierio.cpp
    ../MzShared/parametertreetemplate.cpp
    ../SvmPlugin/svmselection.cpp
    ../MzShared/multidimselection.cpp
    ../SvmPlugin/libsvm/svm.cpp
    ../DecisionTreePlugin/dectree.cpp
    ../DecisionTreePlugin/decisiontree.cpp
    ../MzShared/dataforconsole.cpp
    ../MzShared/csvio.cpp
)

# target_compile_definitions(MzPredict PRIVATE USE_DEBUG_LOG)
# target_compile_definitions(MzPredict PRIVATE USE_NOTIFICATIONS)

find_package(ITK REQUIRED)
include(${ITK_USE_FILE})
if( "${ITK_VERSION_MAJOR}" LESS 4 )
  target_link_libraries(MzPredict ITKReview ${ITK_LIBRARIES})
else( "${ITK_VERSION_MAJOR}" LESS 4 )
  target_link_libraries(MzPredict PRIVATE ${ITK_LIBRARIES})
endif( "${ITK_VERSION_MAJOR}" LESS 4 )

find_package(ALGLIB REQUIRED)
target_link_libraries(MzPredict PRIVATE ${ALGLIB_LIB})
target_include_directories(MzPredict PRIVATE ${ALGLIB_INCLUDE_DIRS})

find_package(Qhull REQUIRED)
target_link_libraries(MzPredict PRIVATE Qhull::qhullstatic_r)

find_package(TIFF REQUIRED)
target_link_libraries(MzPredict PRIVATE TIFF::TIFF)

install(TARGETS MzPredict DESTINATION bin)
