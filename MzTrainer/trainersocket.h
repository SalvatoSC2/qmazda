#ifndef TRAINERSOCKET_H
#define TRAINERSOCKET_H

#include "../MzShared/mzselectioninterface.h"
#include "../MzShared/dataforconsole.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>



struct TrainerSocketAction
{
    std::string name;
    unsigned int index;
};
struct TrainerSocketPropertyValue
{
    std::string property;
    std::string value;
};

class TrainerSocket : public MzPullDataInterface, MzGuiRelatedInterface
{
public:
    TrainerSocket();
    MzSelectionPluginInterface* loadPlugin(const char* filename);


    bool getData(DataForSegmentation* data);
    bool getData(std::vector<std::string>* featureNames, DataForSelection* data);
    bool getData(DataForSelection* data);
    bool setSelection(SelectedFeatures* selected_features);
    bool setData(DataForSelection* data, SelectedFeatures* selected_features);

    void InitiateAnalysisActions(void *plugin_object,
                                 MzBeforeStaticPluginFunction before_plugin,
                                 MzThreadStaticPluginFunction threaded_plugin,
                                 MzAfterStaticPluginFunction after_plugin);

    bool selectClassifiers(std::vector<std::string>* classifierNames, std::vector<bool>* selectionResult, string title);
    bool openProgressDialog(int maxTime, mz_uint64 maxSteps, void *object, string title);
    void closeProgressDialog(void);

    bool openOptionsDialog(std::string title);
    void addGroupOptionsDialog(std::string property, std::string tip);
    void addPropertyOptionsDialog(std::string property, std::string value, std::string tip);
    void getValueOptionsDialog(std::string property, std::string* value);
    void closeOptionsDialog(void);

    void showTestResults(std::string* confusionTable, string title);
    void showAbout(std::string title, std::string text);
    void showMessage(std::string title, std::string text, unsigned int iconType);
    bool getOpenFile(std::string* fileName);
    bool getSaveFile(std::string* fileName, unsigned int* filter);
    void* addMenuAction(const char *name, const char *tip, const unsigned int index);
    void menuEnable(void* menu, bool enable);

    static void static_notify_step(void* object_this)
    {
    }
    static void static_notify_text(void* object_this, std::string text)
    {
        std::cout << text << std::flush;
    }

    int loadFromFile(const char* fileName);
    bool getRequiredData(DataForSelection* outputdata);// = (USE_CHECKED_FEATURES | USE_CHECKED_VECTORS));
    bool saveClassifier(std::string fileName);
    bool loadOptions(const char* fileName);
    bool trainClassifier(void);

private:

    std::vector <TrainerSocketAction> actions;
    std::vector <TrainerSocketPropertyValue> options;
    DataTable datatable;
    MzSelectionPluginInterface* selectionplugin;
    std::string classifier_filename;
//    void* plugin_object;
//    MzAfterStaticPluginFunction after_plugin;
};

#endif // TRAINERSOCKET_H
