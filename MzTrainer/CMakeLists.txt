cmake_minimum_required(VERSION 3.1)

project(MzTrainer LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5 COMPONENTS Widgets REQUIRED)

add_executable(MzTrainer 
    ../MzShared/dataforconsole.cpp
    ../MzShared/csvio.cpp
    mztrainer.cpp
    trainersocket.cpp
)

target_link_libraries(MzTrainer PRIVATE Qt5::Widgets)

install(TARGETS MzTrainer DESTINATION bin)
